-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 30, 2023 at 09:37 AM
-- Server version: 8.0.32-cll-lve
-- PHP Version: 8.1.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `komprazo_empenosauto`
--

--
-- Dumping data for table `provincias`
--

INSERT INTO `provincias` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Araba/Álava', NULL, NULL),
(2, 'Albacete', NULL, NULL),
(3, 'Alicante/Alacant', NULL, NULL),
(4, 'Almería', NULL, NULL),
(5, 'Ávila', NULL, NULL),
(6, 'Badajoz', NULL, NULL),
(7, 'Balears, Illes', NULL, NULL),
(8, 'Barcelona', NULL, NULL),
(9, 'Burgos', NULL, NULL),
(10, 'Cáceres', NULL, NULL),
(11, 'Cádiz', NULL, NULL),
(12, 'Castellón/Castelló', NULL, NULL),
(13, 'Ciudad Real', NULL, NULL),
(14, 'Córdoba', NULL, NULL),
(15, 'Coruña, A', NULL, NULL),
(16, 'Cuenca', NULL, NULL),
(17, 'Girona', NULL, NULL),
(18, 'Granada', NULL, NULL),
(19, 'Guadalajara', NULL, NULL),
(20, 'Gipuzkoa', NULL, NULL),
(21, 'Huelva', NULL, NULL),
(22, 'Huesca', NULL, NULL),
(23, 'Jaén', NULL, NULL),
(24, 'León', NULL, NULL),
(25, 'Lleida', NULL, NULL),
(26, 'Rioja, La', NULL, NULL),
(27, 'Lugo', NULL, NULL),
(28, 'Madrid', NULL, NULL),
(29, 'Málaga', NULL, NULL),
(30, 'Murcia', NULL, NULL),
(31, 'Navarra', NULL, NULL),
(32, 'Ourense', NULL, NULL),
(33, 'Asturias', NULL, NULL),
(34, 'Palencia', NULL, NULL),
(35, 'Palmas, Las', NULL, NULL),
(36, 'Pontevedra', NULL, NULL),
(37, 'Salamanca', NULL, NULL),
(38, 'Santa Cruz de Tenerife', NULL, NULL),
(39, 'Cantabria', NULL, NULL),
(40, 'Segovia', NULL, NULL),
(41, 'Sevilla', NULL, NULL),
(42, 'Soria', NULL, NULL),
(43, 'Tarragona', NULL, NULL),
(44, 'Teruel', NULL, NULL),
(45, 'Toledo', NULL, NULL),
(46, 'Valencia/València', NULL, NULL),
(47, 'Valladolid', NULL, NULL),
(48, 'Bizkaia', NULL, NULL),
(49, 'Zamora', NULL, NULL),
(50, 'Zaragoza', NULL, NULL),
(51, 'Ceuta', NULL, NULL),
(52, 'Melilla', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
