-- phpMyAdmin SQL Dump
-- version 5.2.1deb1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 27-07-2023 a las 05:14:26
-- Versión del servidor: 10.11.3-MariaDB-1
-- Versión de PHP: 8.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `empenosauto`
--

--
-- Volcado de datos para la tabla `marcas`
--

INSERT INTO `marcas` (`id`, `nombre`, `created_at`, `updated_at`) VALUES
(1, 'Abarth', NULL, NULL),
(2, 'Alfa Romeo', NULL, NULL),
(3, 'Aro', NULL, NULL),
(4, 'Asia', NULL, NULL),
(6, 'Aston Martin', NULL, NULL),
(7, 'Audi', NULL, NULL),
(8, 'Austin', NULL, NULL),
(9, 'Auverland', NULL, NULL),
(10, 'Bentley', NULL, NULL),
(11, 'Bertone', NULL, NULL),
(12, 'BMW', NULL, NULL),
(13, 'Cadillac', NULL, NULL),
(14, 'Chevrolet', NULL, NULL),
(15, 'Chrysler', NULL, NULL),
(16, 'Citroen', NULL, NULL),
(17, 'Corvette', NULL, NULL),
(18, 'Dacia', NULL, NULL),
(19, 'Daewoo', NULL, NULL),
(20, 'Daf', NULL, NULL),
(21, 'Daihatsu', NULL, NULL),
(22, 'Daimler', NULL, NULL),
(23, 'Dodge', NULL, NULL),
(24, 'Ferrari', NULL, NULL),
(25, 'Fiat', NULL, '2023-06-15 23:27:55'),
(26, 'Ford', NULL, NULL),
(27, 'Galloper', NULL, NULL),
(28, 'Gmc', NULL, NULL),
(29, 'Honda', NULL, NULL),
(30, 'Hummer', NULL, NULL),
(31, 'Hyundai', NULL, NULL),
(32, 'Infiniti', NULL, NULL),
(33, 'Innocenti', NULL, NULL),
(34, 'Isuzu', NULL, NULL),
(35, 'Iveco', NULL, NULL),
(36, 'Iveco-pegaso', NULL, NULL),
(37, 'Jaguar', NULL, NULL),
(38, 'Jeep', NULL, NULL),
(39, 'Kia', NULL, NULL),
(40, 'Lada', NULL, NULL),
(41, 'Lamborghini', NULL, NULL),
(42, 'Lancia', NULL, NULL),
(43, 'Land-rover', NULL, NULL),
(44, 'Ldv', NULL, NULL),
(45, 'Lexus', NULL, NULL),
(46, 'Lotus', NULL, NULL),
(47, 'Mahindra', NULL, NULL),
(48, 'Maserati', NULL, NULL),
(49, 'Maybach', NULL, NULL),
(50, 'Mazda', NULL, NULL),
(51, 'Mercedes-benz', NULL, NULL),
(52, 'Mg', NULL, NULL),
(53, 'Mini', NULL, NULL),
(54, 'Mitsubishi', NULL, NULL),
(55, 'Morgan', NULL, NULL),
(56, 'Nissan', NULL, NULL),
(57, 'Opel', NULL, NULL),
(58, 'Peugeot', NULL, NULL),
(59, 'Pontiac', NULL, NULL),
(60, 'Porsche', NULL, NULL),
(61, 'Renault', NULL, NULL),
(62, 'Rolls-royce', NULL, NULL),
(63, 'Rover', NULL, NULL),
(64, 'Saab', NULL, NULL),
(65, 'Santana', NULL, NULL),
(66, 'Seat', NULL, NULL),
(67, 'Skoda', NULL, NULL),
(68, 'Smart', NULL, NULL),
(69, 'Ssangyong', NULL, NULL),
(70, 'Subaru', NULL, NULL),
(71, 'Suzuki', NULL, NULL),
(72, 'Talbot', NULL, NULL),
(73, 'Tata', NULL, NULL),
(74, 'Toyota', NULL, NULL),
(75, 'Umm', NULL, NULL),
(76, 'Vaz', NULL, NULL),
(77, 'Volkswagen', NULL, NULL),
(78, 'Volvo', NULL, NULL),
(79, 'Wartburg', NULL, NULL),
(80, 'Aiways', NULL, NULL),
(81, 'Alpine', NULL, NULL),
(82, 'Bugatti', NULL, NULL),
(83, 'BYD', NULL, NULL),
(84, 'Caterham', NULL, NULL),
(85, 'Cupra', NULL, NULL),
(86, 'DFSK', NULL, NULL),
(87, 'DS', NULL, NULL),
(88, 'Genesis', NULL, NULL),
(89, 'Hispano Suiza', NULL, NULL),
(90, 'Ineos', NULL, NULL),
(91, 'IONIQ', NULL, NULL),
(92, 'Liux', NULL, NULL),
(93, 'Lucid', NULL, NULL),
(94, 'Lynk&Co', NULL, NULL),
(95, 'Mclaren', NULL, NULL),
(96, 'Microlino', NULL, NULL),
(97, 'Mustang', NULL, NULL),
(98, 'Nio', NULL, NULL),
(99, 'Pagani', NULL, NULL),
(100, 'Pininfarina', NULL, NULL),
(101, 'Polestar', NULL, NULL),
(102, 'Ram', NULL, NULL),
(103, 'Rimac', NULL, NULL),
(104, 'Swm', NULL, NULL),
(105, 'Tesla', NULL, NULL),
(106, 'Wey', NULL, NULL),
(108, 'DR', NULL, NULL),
(109, 'EVO', NULL, NULL),
(110, 'Invicta', NULL, NULL),
(111, 'Jiayuan', NULL, NULL),
(112, 'MAN', NULL, NULL),
(113, 'Maxus', NULL, NULL),
(114, 'Piaggio', NULL, NULL),
(115, 'Renault Trucks', NULL, NULL);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
