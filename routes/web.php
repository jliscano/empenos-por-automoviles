<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MailController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('/home');
    return redirect()->route('home');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/change-password', [App\Http\Controllers\HomeController::class, 'changePassword'])->name('change-password');

Route::post('/change-password', [App\Http\Controllers\HomeController::class, 'updatePassword'])->name('update-password');

Route::resource('bancos', 'BancoController');

Route::resource('clasificados', 'ClasificadoController');

//cobranzas
Route::get('cobranzas/muestraCobranza/{id}/{motivo}', [App\Http\Controllers\CobranzaController::class, 'muestraCobranza'])->name('muestraCobranza');

Route::get('cobranzas/createCobranza/{id}/{motivo}', [App\Http\Controllers\CobranzaController::class,
'createCobranza'])->name('createCobranza');

Route::get('cobranzas/impagos/{imprimir}/{mesesimpago2}/{texto2}', [App\Http\Controllers\CobranzaController::class, 'impagos'])->name('impagos');

//Route::get('cobranzas/reporteImpagos/{cobranzas}', [App\Http\Controllers\CobranzaController::class, 'reporteImpagos'])->name('reporteImpagos');
Route::get('cobranzas/enviaMailCobranzaUno/{id}/{motivo}', [App\Http\Controllers\MailController::class, 'enviaMailCobranzaUno'])->name('enviaMailCobranzaUno');


Route::resource('cobranzas', 'CobranzaController');
Route::resource('empresas', 'EmpresaController');
Route::resource('empseguros', 'EmpseguroController');

Route::resource('marcas', 'MarcaController');
//modelos

Route::get('modelos/creaModelo/{marca}', [App\Http\Controllers\ModeloController::class, 'creaModelo'])->name('creaModelo');

Route::get('modelos/muestraModelos/{marca}', [App\Http\Controllers\ModeloController::class, 'muestraModelos'])->name('muestraModelos');
Route::get('modelos/borramodelos/{id}', [App\Http\Controllers\ModeloController::class, 'destroy'])->name('borramodelos');

Route::resource('modelos', 'ModeloController');



Route::get('clientes/municipiosbyprovincias/{id}', [App\Http\Controllers\ClienteController::class, 'municipiosbyprovincias'])->name('municipiosbyprovincias');

Route::get('clientes/{cliente}/municipiosbyprovincias2/{provincia_id}', [App\Http\Controllers\ClienteController::class, 'municipiosbyprovincias2'])->name('municipiosbyprovincias2');

Route::get('clientes/clientesEspera', [App\Http\Controllers\ClienteController::class, 'clientesEspera'])->name('clientesEspera');
Route::get('clientes/muestraModal', [App\Http\Controllers\ClienteController::class, 'muestraModal'])->name('muestraModal');

Route::resource('clientes', 'ClienteController');
Route::resource('gastos', 'GastoController');

//municipios

Route::get('municipios/creaMunicipio/{provincia}', [App\Http\Controllers\MunicipioController::class, 'creaMunicipio'])->name('creaMunicipio');
Route::get('municipios/muestraLocalidad/{provincia}', [App\Http\Controllers\MunicipioController::class, 'muestraLocalidad'])->name('muestraLocalidad');
Route::resource('municipios', 'MunicipioController');

//pagos
Route::get('pagos/muestraPago/{id}/{retorno}/{padre}', [App\Http\Controllers\PagoController::class, 'muestraPago'])->name('muestraPago');
Route::get('pagos/createPago/{id}/{retorno}/{padre}', [App\Http\Controllers\PagoController::class, 'createPago'])->name('createPago');

Route::delete('pagos/borraPago/{id}/{retorno}', [App\Http\Controllers\PagoController::class, 'borraPago'])->name('borraPago');

Route::get('pagos/editaPago/{id}/{retorno}/{padre}', [App\Http\Controllers\PagoController::class, 'editaPago'])->name('editaPago');
Route::get('pagos/muestraPagoExtension/{id}/{retorno}/{padre}', [App\Http\Controllers\PagoController::class, 'muestraPagoExtension'])->name('muestraPagoExtension');

Route::resource('pagos', 'PagoController');

//prestamos

Route::name('print')->get('/imprimir/{id}/{vehiculo_id}', 'GeneradorController@imprimir');

Route::get('prestamos/editaprestamo/{id}/{retorno}', [App\Http\Controllers\PrestamoController::class, 'editaprestamo'])->name('editaprestamo');
Route::get('prestamos/creaprestamo/{id}/{vehiculo_id}/{retorno}', [App\Http\Controllers\PrestamoController::class, 'creaprestamo'])->name('creaprestamo');

Route::get('prestamos/listaprestamos/{cliente_id}/{vehiculo_id}/{retorno}', [App\Http\Controllers\PrestamoController::class, 'listaprestamos'])->name('listaprestamos');
Route::get('prestamos/listaprestamoscliente/{cliente_id}/{vehiculo_id}/{retorno}', [App\Http\Controllers\PrestamoController::class, 'listaprestamoscliente'])->name('listaprestamoscliente');
Route::get('prestamos/ultimoprestamoscliente/{cliente_id}/{vehiculo_id}/{retorno}', [App\Http\Controllers\PrestamoController::class, 'ultimoprestamoscliente'])->name('ultimoprestamoscliente');
//para mostrar las extensiones 02/10/2024

Route::get('prestamos/creaprestamoextension/{prestamo_id}/{cliente_id}/{vehiculo_id}/{retorno}', [App\Http\Controllers\PrestamoController::class, 'creaprestamoextension'])->name('creaprestamoextension');
Route::get('prestamos/borraExtension/{prestamo_id}/{padre_id}/{retorno}', [App\Http\Controllers\PrestamoController::class, 'borraExtension'])->name('borraExtension');

Route::resource('prestamos', 'PrestamoController');
Route::resource('provincias', 'ProvinciaController');

//vehiculos

Route::get('vehiculos/editavehiculo/{id}/{retorno}', [App\Http\Controllers\VehiculoController::class, 'editavehiculo'])->name('editavehiculo');
Route::get('vehiculos/createcliente/{id}/{retorno}', [App\Http\Controllers\VehiculoController::class, 'createcliente'])->name('createcliente');

Route::get('vehiculos/editavehiculo/{cliente}/modelosxmarcas2/{marca_id}', [App\Http\Controllers\VehiculoController::class, 'modelosxmarcas2'])->name('modelosxmarcas2');

Route::get('vehiculos/createcliente/{cliente}/modelosxmarcas/{marca_id}', [App\Http\Controllers\VehiculoController::class, 'modelosxmarcas'])->name('modelosxmarcas');

Route::get('vehiculos/listavehiculos/{idcliente}/{retorno}', [App\Http\Controllers\VehiculoController::class, 'listavehiculos'])->name('listavehiculos');


//itv
Route::post('itv/muestraitv', [App\Http\Controllers\ItvController::class, 'muestraItv'])->name('muestraItv');
//Route::get('itv/elije', [App\Http\Controllers\ItvController::class, 'listaItv'])->name('listaItv');;
Route::resource('itv', 'ItvController');


//carnet de conducir
Route::post('carnet/muestraCarnet', [App\Http\Controllers\CarnetController::class, 'muestraCarnet'])->name('muestraCarnet');
Route::resource('carnet', 'CarnetController');

//polizas
Route::post('poliza/muestrapolizas', [App\Http\Controllers\PolizaController::class, 'muestraPolizas'])->name('muestraPolizas');
Route::resource('poliza', 'PolizaController');
//multas

Route::get('multa/createMultas/{id}/{vehiculo_id}/{motivo}/{retorno}', [App\Http\Controllers\MultaController::class, 'createMultas'])->name('createMultas');

Route::get('multa/muestraMultas/{id}/{motivo}/{vehiculo_id}/{retorno}', [App\Http\Controllers\MultaController::class, 'muestraMultas'])->name('muestraMultas');

Route::get('multa/general/{motivo}/{retorno}', [App\Http\Controllers\MultaController::class, 'muestraMultasGeneral'])->name('muestraMultasGeneral');

Route::delete('multa/borraMulta/{id}/{cliente_id}/{vehiculo_id}/{motivo}/{retorno}', [App\Http\Controllers\MultaController::class, 'borraMulta'])->name('borraMulta');

Route::get('multa/editaMulta/{id}/{retorno}', [App\Http\Controllers\MultaController::class, 'editaMulta'])->name('editaMulta');

Route::resource('multas', 'MultaController');

Route::resource('vehiculos', 'VehiculoController');
Route::resource('users', 'UserController');

Route::get('muestraContratos/{cliente_id}/{prestamo_id}/{vehiculo_id}/{retorno}', [App\Http\Controllers\ContratoController::class, 'muestraContratos'])->name('muestraContratos');



//-----contratos ----
Route::get('contrato/index', [App\Http\Controllers\ContratoController::class, 'index'])->name('contratos');

    Route::name('CompraVenta')->get('/CompraVenta/{id}/{vehiculo_id}/{prestamo_id}', 'GeneradorController@CompraVenta');
    Route::name('DatosOperacion')->get('/DatosOperacion/{id}/{vehiculo_id}/{prestamo_id}', 'GeneradorController@DatosOperacion');

    Route::name('DocumentoEntrega')->get('/DocumentoEntrega/{id}/{vehiculo_id}/{prestamo_id}', 'GeneradorController@DocumentoEntrega');
    Route::name('EdicionCliente')->get('/EdicionCliente/{id}/{vehiculo_id}/{prestamo_id}', 'GeneradorController@EdicionCliente');
    Route::name('ContratoArrendamiento')->get('/ContratoArrendamiento/{id}/{vehiculo_id}/{prestamo_id}', 'GeneradorController@ContratoArrendamiento');
    Route::name('MandatoEspecifico')->get('/MandatoEspecifico/{id}/{vehiculo_id}/{prestamo_id}', 'GeneradorController@MandatoEspecifico');
    Route::name('ImpagoIndividual')->get('/ImpagoIndividual/{id}/{prestamo_id}', 'GeneradorController@ImpagoIndividual');

//mail

Route::get('mail/enviaMailPoliza', [App\Http\Controllers\MailController::class, 'enviaMailPoliza'])->name('enviaMailPoliza');

Route::get('mail/enviaMailitv', [App\Http\Controllers\MailController::class, 'enviaMailitv'])->name('enviaMailitv');
Route::get('mail/enviaMailCobranza', [App\Http\Controllers\MailController::class, 'enviaMailCobranza'])->name('enviaMailCobranza');

//reportes de rentabilidad

Route::get('rentabilidad/rentageneral/{imprimir}/{mesesimpago2}/{texto2}', [App\Http\Controllers\RentabilidadController::class, 'muestraRentabilidad'])->name('muestraRentabilidad');
Route::get('rentabilidad/individual/{imprimir}/{mesesimpago2}/{texto2}', [App\Http\Controllers\RentabilidadController::class, 'RentabilidadIndividual'])->name('RentabilidadIndividual');

Route::get('rentabilidad/historial/{id}', [App\Http\Controllers\RentabilidadController::class, 'historial'])->name('historial');
//manuales
Route::get('manuales/index', [App\Http\Controllers\ManualesController::class, 'index'])->name('manuales');
Route::get('manuales/download/{archivo}', [App\Http\Controllers\ManualesController::class, 'download'])->name('download');

//documentos de cada cliente
Route::delete('documento/destroyDocumento/{id}/{cliente_id}/{retorno}/{vehiculo_id}', [App\Http\Controllers\DocumentoController::class, 'destroyDocumento'])->name('destroyDocumento');
Route::get('documento/createDocumento/{cliente_id}/{retorno}/{vehiculo_id}', [App\Http\Controllers\DocumentoController::class, 'createDocumento'])->name('createDocumento');
Route::get('documento/muestraDocumentos/{cliente_id}/{retorno}/{vehiculo_id}', [App\Http\Controllers\DocumentoController::class, 'muestraDocumentos'])->name('muestraDocumentos');
Route::get('documento/multiple/{cliente_id}/{retorno}/{vehiculo_id}', [App\Http\Controllers\DocumentoController::class, 'multiple'])->name('multiple');

Route::post('documento/guardamultiple/{cliente_id}/{retorno}/{vehiculo_id}', [App\Http\Controllers\DocumentoController::class, 'guardamultiple'])->name('guardamultiple');
//Route::post('documento/updateDocumento/{vehiculo_id}', [App\Http\Controllers\DocumentoController::class, 'updateDocumento'])->name('updateDocumento');
Route::get('documento/editaDocumento/{cliente_id}/{vehiculo_id}', [App\Http\Controllers\DocumentoController::class, 'editaDocumento'])->name('editaDocumento');
Route::get('documento/storeDocumento/{vehiculo_id}', [App\Http\Controllers\DocumentoController::class, 'storeDocumento'])->name('storeDocumento');

Route::get('documento/crearCarpeta', [App\Http\Controllers\DocumentoController::class, 'crearCarpeta'])->name('crearCarpeta');

Route::resource('documento', 'DocumentoController');

//gastos por vehiculo

Route::get('gastoxvehiculo/miJqueryAjax/{dato}', [App\Http\Controllers\GastoxvehiculoController::class, 'miJqueryAjax'])->name('miJqueryAjax');
Route::post('gastoxvehiculo/updateGastoxVehiculo/{id}/{empresa_id}', [App\Http\Controllers\GastoxvehiculoController::class, 'updateGastoxVehiculo'])->name('updateGastoxVehiculo');
Route::get('gastoxvehiculo/storeGastosxVehiculos/{empresa_id}', [App\Http\Controllers\GastoxvehiculoController::class, 'storeGastosxVehiculos'])->name('storeGastosxVehiculos');
Route::get('gastoxvehiculo/editaGastosxVehiculos/{id}/{empresa_id}', [App\Http\Controllers\GastoxvehiculoController::class, 'editaGastosxVehiculos'])->name('editaGastosxVehiculos');
Route::get('gastoxvehiculo/createGastoVehiculo/{gasto_id}/{empresa_id}', [App\Http\Controllers\GastoxvehiculoController::class, 'createGastoVehiculo'])->name('createGastoVehiculo');
Route::get('gastoxvehiculo/muestraGastosVehiculos/{gasto_id}/{empresa_id}', [App\Http\Controllers\GastoxvehiculoController::class, 'muestraGastosVehiculos'])->name('muestraGastosVehiculos');
Route::delete('gastoxvehiculo/destroyGastoxVehiculo/{id}/{gasto_id}/{empresa_id}', [App\Http\Controllers\GastoxvehiculoController::class, 'destroyGastoxVehiculo'])->name('destroyGastoxVehiculo');
Route::resource('gastoxvehiculos', 'GastoxvehiculoController');

//ingresos e ingreos x vehiculo
Route::delete('ingresoxvehiculo/destroyIngresoxVehiculo/{id}/{gasto_id}/{empresa_id}', [App\Http\Controllers\IngresoxvehiculoController::class, 'destroyIngresoxVehiculo'])->name('destroyIngresoxVehiculo');
Route::get('ingresoxvehiculo/storeIngresosxVehiculos/{empresa_id}', [App\Http\Controllers\IngresoxvehiculoController::class, 'storeIngresosxVehiculos'])->name('storeIngresosxVehiculos');
Route::get('ingresoxvehiculo/editaIngresosxVehiculos/{id}/{empresa_id}', [App\Http\Controllers\IngresoxvehiculoController::class, 'editaIngresosxVehiculos'])->name('editaIngresosxVehiculos');
Route::get('ingresoxvehiculo/createIngresoVehiculo/{gasto_id}/{empresa_id}', [App\Http\Controllers\IngresoxvehiculoController::class, 'createIngresoVehiculo'])->name('createIngresoVehiculo');
Route::post('ingresoxvehiculo/updateIngresoxVehiculo/{id}/{empresa_id}', [App\Http\Controllers\IngresoxvehiculoController::class, 'updateIngresoxVehiculo'])->name('updateIngresoxVehiculo');

Route::get('ingresoxvehiculo/muestraIngresosVehiculos/{ingreso_id}/{empresa_id}', [App\Http\Controllers\IngresoxvehiculoController::class, 'muestraIngresosVehiculos'])->name('muestraIngresosVehiculos');
Route::resource('ingresoxvehiculos', 'IngresoxvehiculoController');
Route::resource('ingresos', 'IngresoController');

//impuestos

Route::get('impuesto/listaImpuestos/{vehiculo_id}/{retorno}',
[App\Http\Controllers\ImpuestoController::class, 'listaImpuestos'])->name('listaImpuestos');

Route::get('impuesto/creaImpuesto/{vehiculo_id}/{retorno}',
[App\Http\Controllers\ImpuestoController::class, 'creaImpuesto'])->name('creaImpuesto');

Route::post('impuesto/storeImpuesto/{retorno}',
[App\Http\Controllers\ImpuestoController::class, 'storeImpuesto'])->name('storeImpuesto');

Route::get('impuesto/editaImpuesto/{id}/{retorno}',
[App\Http\Controllers\ImpuestoController::class, 'editaImpuesto'])->name('editaImpuesto');

Route::get('impuesto/updateImpuesto/{id}/{retorno}',
[App\Http\Controllers\ImpuestoController::class, 'updateImpuesto'])->name('updateImpuesto');

Route::delete('impuesto/borrarImpuesto/{id}/{vehiculo_id}/{retorno}',
[App\Http\Controllers\ImpuestoController::class, 'borrarImpuesto'])->name('borrarImpuesto');

Route::resource('impuestos', 'ImpuestoController');

Route::resource('tipoimpuestos', 'TipoimpuestoController');

Route::get('extensione/creaExtension/{prestamo_id}/{retorno}',
[App\Http\Controllers\ExtensioneController::class, 'creaExtension'])->name('creaExtension');
Route::get('extensione/editaExtension/{extension_id}/{retorno}',
[App\Http\Controllers\ExtensioneController::class, 'editaExtension'])->name('editaExtension');

/*Route::get('extensione/borraExtension/{extension_id}/{prestamo_id}/{retorno}',
[App\Http\Controllers\ExtensioneController::class, 'borraExtension'])->name('borraExtension');
*/
Route::post('extensione/storeExtension/{retorno}',
[App\Http\Controllers\ExtensioneController::class, 'storeExtension'])->name('storeExtension');
Route::post('extensione/updateExtension/{id}/{retorno}',
[App\Http\Controllers\ExtensioneController::class, 'updateExtension'])->name('updateExtension');

Route::resource('extensiones', 'ExtensioneController');
//reporte individual de impagos


