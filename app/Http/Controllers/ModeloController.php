<?php

namespace App\Http\Controllers;

use App\Models\Modelo;
use App\Models\Marca;
use App\Models\Vehiculo;
use Illuminate\Http\Request;

/**
 * Class ModeloController
 * @package App\Http\Controllers
 */
class ModeloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelos = Modelo::paginate();

        return view('modelo.index', compact('modelos'))
            ->with('i', (request()->input('page', 1) - 1) * $modelos->perPage());
    }
    public function muestraModelos(Request $request,$id)
    {

        $modelo=$request->modelo;
        //$modelos = Modelo::paginate();
        $modelos = Marca::where('marcas.id','=',$id)
        ->where('modelos.nombre','like','%'.$modelo.'%')
        ->leftjoin('modelos','modelos.marca_id','=','marcas.id')
        ->select('modelos.*','marcas.id as mar','marcas.nombre as nmar','modelos.nombre as nmod')
        ->orderby('modelos.nombre','asc')
        ->paginate();
        $i=0;

        return view('modelo.index', compact('modelos','id'))
            ->with('i', (request()->input('page', 1) - 1) * $modelos->perPage());
    }
    public function creaModelo($marca)
    {
        $modelo = new Modelo();
        $modelo->marca_id=$marca;
        return view('modelo.create', compact('modelo','marca'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $modelo = new Modelo();
        return view('modelo.create', compact('modelo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Modelo::$rules);

        $modelo = Modelo::create($request->all());
        $id=$request->marca_id;
            /* return redirect()
    ->action('ModeloController@muestraModelos', $id)
    ->with('success', 'Modelo creado con Exito.'); */
    return redirect()
    ->action('MarcaController@edit', $id)
    ->with('success', 'Modelo creado con Exito.');

        //return redirect()->route('modelos.index')
          //  ->with('i','success', 'Modelo creado con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modelo = Modelo::find($id);

        return view('modelo.show', compact('modelo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modelo = Modelo::find($id);
        $marca=$modelo->marca_id;
        return view('modelo.edit', compact('modelo','marca'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Modelo $modelo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Modelo $modelo)
    {
        request()->validate(Modelo::$rules);
        $modelo->update($request->all());
        $id=$request->marca_id;
        return redirect()
        ->action('MarcaController@edit', $id)
        ->with('success', 'Modelo modificado con Exito.');

    /* return redirect()
    ->action('ModeloController@muestraModelos', $id)
    ->with('success', 'Modelo modificado con Exito.'); */
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $modelo=Modelo::find($id);
        $marca_id=$modelo->marca_id;
        $vehiculos=Vehiculo::where('vehiculos.modelo_id','=',$id)->selectRaw('count(*) as tveh')->get();
        $tvehiculos=0;

        foreach($vehiculos as $tveh){
            $tvehiculos=$tveh->tveh;
        }
        if($tvehiculos==0){
            $modelo = Modelo::find($id)->delete();
            $smj='Modelo eliminado con Exito.';
        }else{
            $smj='No se puede eliminar porque tiene Vehiculos relacionados.';
        }
        return redirect()
        ->action('MarcaController@edit', $marca_id)
        ->with('success', $smj);
        /* return redirect()
        ->action('ModeloController@muestraModelos', $marca_id)
        ->with('success', $smj); */

        //return redirect()->route('modelos.index')
          //  ->with('success', 'Modelo eliminado con Exito');
    }
}
