<?php

namespace App\Http\Controllers;

use App\Models\Empseguro;
use App\Models\Vehiculo;
use Illuminate\Http\Request;

/**
 * Class EmpseguroController
 * @package App\Http\Controllers
 */
class EmpseguroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empseguros = Empseguro::paginate();

        return view('empseguro.index', compact('empseguros'))
            ->with('i', (request()->input('page', 1) - 1) * $empseguros->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empseguro = new Empseguro();
        return view('empseguro.create', compact('empseguro'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Empseguro::$rules);

        $empseguro = Empseguro::create($request->all());

        return redirect()->route('empseguros.index')
            ->with('success', 'Empseguro creada con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empseguro = Empseguro::find($id);

        return view('empseguro.show', compact('empseguro'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empseguro = Empseguro::find($id);

        return view('empseguro.edit', compact('empseguro'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Empseguro $empseguro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empseguro $empseguro)
    {
        request()->validate(Empseguro::$rules);

        $empseguro->update($request->all());

        return redirect()->route('empseguros.index')
            ->with('success', 'Empseguro modificada con Exito');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $vehiculos=Vehiculo::where('vehiculos.empseguro_id','=',$id)->selectRaw('count(*) as total')->get();
        $tvehiculos=0;
        foreach ($vehiculos as $gas) {
            $tvehiculos=$gas->total;
        }
        
        if($tvehiculos==0){
            $empseguro = Empseguro::find($id)->delete();
            $smj='Empresa de Seguro eliminada con Exito';
        }else{
            $smj='No se puede eliminar porque tiene Vehículos asociados';
        }
        
        return redirect()->route('empseguros.index')
            ->with('success', $smj);
    }
}
