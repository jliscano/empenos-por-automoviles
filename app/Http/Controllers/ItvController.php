<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Vehiculo;
use Illuminate\Http\Request;
use function PHPUnit\Framework\isNull;

class ItvController extends Controller
{

    //private $dataF;
    //
     public function index(Request $request)
    {
        return view('itv.index');
        
    }
    public function muestraItv( Request $request)
    {
        if(isset($request->startDate)){
            $data=explode('/', $request->startDate);
            Config('global', $data);
        }
        $clientes = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->select('clientes.id as idcliente','clientes.*','marcas.nombre as mar','modelos.nombre as mod','vehiculos.*')
        ->whereYear('itv','=', $data[1])
        ->whereMonth('itv','<=', $data[0])
        ->orwhereYear('itv','<', $data[1])
        ->paginate();
        return view('itv.muestraitv', compact('clientes','data'))
            ->with('i', (request()->input('page', 1) - 1) * $clientes->perPage());
    }
    public function show($id){
        $data=array(config('global.mes'),config('global.anno'));
        $clientes = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->select('clientes.id as idcliente','clientes.*','marcas.nombre as mar','modelos.nombre as mod','vehiculos.*')
        ->whereYear('itv','=', $data[1])
        ->whereMonth('itv','<=', $data[0])
        ->orwhereYear('itv','<', $data[1])
        ->paginate();
        
        return view('itv.muestraitv', compact('clientes','data'))
            ->with('i', (request()->input('page', 1) - 1) * $clientes->perPage());
    }    

}
