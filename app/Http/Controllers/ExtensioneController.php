<?php

namespace App\Http\Controllers;

use App\Models\Extensione;
use App\Models\Prestamo;
use Illuminate\Http\Request;

/**
 * Class ExtensioneController
 * @package App\Http\Controllers
 */
class ExtensioneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $extensiones = Prestamo::paginate();

        return view('extensione.index', compact('extensiones'))
            ->with('i', (request()->input('page', 1) - 1) * $extensiones->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $extensione = new Extensione();
        return view('extensione.create', compact('extensione'));
    }

    public function creaExtension($prestamo_id,$retorno)
    {
        $extensione = new Extensione();
        $extensione->prestamo_id=$prestamo_id;
        $extensione->piva=21;
        return view('extensione.create', compact('extensione','retorno'));
    }
    /**
     * Store a newly created rempenosesource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function storeExtension(Request $request,$retorno)
    {
        request()->validate(Extensione::$rules);

        $extensione = Extensione::create($request->all());
        $prestamo_id=$extensione->prestamo_id;
        return redirect()->route('editaprestamo',[$prestamo_id,$retorno])
            ->with('success', 'Extensión de empeño creada con Exito!!!.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $extensione = Extensione::find($id);

        return view('extensione.show', compact('extensione'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function editaExtension($id,$retorno)
    {
        $extensione = Prestamo::find($id);

        return view('extensione.edit', compact('extensione','retorno'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Extensione $extensione
     * @return \Illuminate\Http\Response
     */
    public function updateExtension(Request $request, Extensione $extensione,$id,$retorno)
    {
        request()->validate(Extensione::$rules);

        //$extensione->update($request->all());
        $extension=Extensione::where('extensiones.id','=',$id)
        ->update(['prestamo_id' => $request->prestamo_id,
                  'monto'=> $request->monto,
                  'fecha'=> $request->fecha,
                  'tcuota'=> $request->tcuota,
                  'cuota'=> $request->cuota,
                  'iva'=> $request->iva,
                  'piva'=> $request->piva
                ]);

        //echo $extensione->monto;exit();
        $prestamo_id=$request->prestamo_id;

        return redirect()->route('editaprestamo',[$prestamo_id,$retorno])
            ->with('success', 'Extensión de empeño actualizada con Exito!!!');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function borraExtension($id,$prestamo_id,$retorno)
    {

        $extensione = Extensione::find($id)->delete();

        return redirect()->route('editaprestamo',[$prestamo_id,$retorno])
            ->with('success', 'Extensión de empeño borrada con Exito!!!');
    }
}
