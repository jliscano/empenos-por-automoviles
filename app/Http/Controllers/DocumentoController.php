<?php

namespace App\Http\Controllers;

use App\Models\Documento;
use App\Models\Cliente;
use App\Models\Vehiculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class DocumentoController
 * @package App\Http\Controllers
 */
class DocumentoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $documentos = Documento::paginate();
        
        return view('documento.index', compact('documentos'))
            ->with('i', (request()->input('page', 1) - 1) * $documentos->perPage());
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $documento = new Documento();
        return view('documento.create', compact('documento'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $retorno=1;
        $vehiculo_id=$request->vehiculo_id;
        request()->validate(Documento::$rules);
        $NuevoNombre=$request->nombre;
        $documento = Documento::create($request->all());
        $nombre=$documento->nombre;
        $cliente_id=$documento->cliente_id;
        /**Verificando si existe el archivo */
        $micarpeta = public_path('documentos/'.$cliente_id.'/'.$vehiculo_id);
        
        if(!file_exists($micarpeta)) {
            mkdir(public_path('documentos/'.$cliente_id.'/'.$vehiculo_id), 0777, true);
        }
        $errors= array();
      $file_name = $NuevoNombre;//$_FILES['archivo']['name'];
      $file_size = $_FILES['archivo']['size'];
      $file_tmp = $_FILES['archivo']['tmp_name'];
      $file_type = $_FILES['archivo']['type'];
     
    $mensaje='';
    
    if(empty($errors)==true) {
         move_uploaded_file($file_tmp,$micarpeta.'/'.$file_name);
         $mensaje='Documento creado con Exito. Y el Archivo fué cargado con Exito';
      }else{
         $mensaje='Documento creado con Exito. Pero el Archivo NO se cargó';
      }
         return redirect()
        ->action('DocumentoController@muestraDocumentos', [$cliente_id,$retorno,$vehiculo_id])
        ->with('success', $mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $documento = Documento::find($id);

        return view('documento.show', compact('documento'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $documento = Documento::find($id);
        $vehiculo_id=$documento->vehiculo_id;
        return view('documento.edit', compact('documento','vehiculo_id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Documento $documento
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Documento $documento)
    {
        request()->validate(Documento::$rules);
        $vehiculo_id=$request->vehiculo_id;
        $NuevoNombre=$request->nombre;
        $documento->update($request->all());
        $cliente_id=$documento->cliente_id;
        $micarpeta = public_path('documentos/'.$cliente_id.'/'.$vehiculo_id);
        
        if(!file_exists($micarpeta)) {
            mkdir(public_path('documentos/'.$cliente_id.'/'.$vehiculo_id), 0777, true);
        }
        $errors= array();
        $ruta=$_SERVER['DOCUMENT_ROOT'];

        //$ruta = str_replace('http://','',$ruta);
        //$ruta=asset('/public/documentos/'.$request->cliente_id.'/'.$request->nombreant);
        //echo $ruta;exit();
        $mensaje='Documento creado con Exito. Y el Archivo fué cargado con Exito';
        if($request->nombreant!='' and $request->nombreant!=$request->nombre){
            $resultado=rename($micarpeta.'/'.$request->nombreant, $micarpeta.'/'.$NuevoNombre);
        }else{
            if($file_tmp = $_FILES['archivo']['tmp_name']!=''){
                $file_name =$NuevoNombre;// $_FILES['archivo']['name'];
                $file_size = $_FILES['archivo']['size'];
                $file_tmp = $_FILES['archivo']['tmp_name'];
                $file_type = $_FILES['archivo']['type'];
                
                $exito=move_uploaded_file($file_tmp,$micarpeta.'/'.$file_name);
                if($exito) {
                    $mensaje='Documento creado con Exito. Y el Archivo fué cargado con Exito';
                }else{
                    $mensaje='Archivo NO se cargó';
                }
            }
        }
      
    
    $retorno=1;
    

         return redirect()
        ->action('DocumentoController@muestraDocumentos', [$cliente_id,$retorno,$vehiculo_id])
        ->with('success', $mensaje);

    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request,$id)
    {
        $cliente_id=$request->cliente_id;

        $documento = Documento::find($id)->delete();
        
         return redirect()
        ->action('DocumentoController@muestraDocumentos', $cliente_id)
        ->with('success', 'Documento eliminado con Exito.');

    }
    
    public function destroyDocumento($id,$cliente_id,$retorno,$vehiculo_id)
    {
        $documento = Documento::find($id);
        
        $cliente_id=$documento->cliente_id;
        $micarpeta = public_path('documentos/'.$cliente_id.'/'.$vehiculo_id);
        if(file_exists($micarpeta.'/'.$documento->nombre)) {
            unlink($micarpeta.'/'.$documento->nombre);
        }
        
        $documento = $documento->delete();
         return redirect()
        ->action('DocumentoController@muestraDocumentos', [$cliente_id,$retorno,$vehiculo_id])
        ->with('success', 'Documento eliminado con Exito.');

    }
    public function muestraDocumentos(Request $request, $cliente_id,$retorno,$vehiculo_id)
    {
        $vehiculos=Vehiculo::leftjoin('marcas','marcas.id','=','vehiculos.marca_id')
        ->leftjoin('modelos','modelos.id','=','vehiculos.modelo_id')
        ->where('cliente_id','=',$cliente_id)->select('marcas.nombre as marca','modelos.nombre as modelo','matricula','vehiculos.id')->get();
        if(!isset($vehiculo_id) or $vehiculo_id==0){
            $i=0;
            foreach($vehiculos as $veh){
                if($i==0){
                    $vehiculo_id=$veh->id;
                    $i++;
                }
            }
        }
        $clientes = Cliente::select('clientes.id','clientes.nombre as nombres','clientes.dni',DB::raw('(select coalesce(count(*),0) from vehiculos where cliente_id=clientes.id) as tvehiculos'),DB::raw('(select coalesce(count(*),0) from prestamos where cliente_id=clientes.id) as tempenos'),'clientes.status')
        ->where('clientes.id','=',$cliente_id)->get();
        
        $documentos = Documento::where('documentos.cliente_id','=',$cliente_id)
        ->where('documentos.vehiculo_id','=',$vehiculo_id)
        //->join('documentos','documentos.cliente_id','=','clientes.id')
        //->select('documentos.*','clientes.nombre','clientes.dni','clientes.id as cli')
        ->select('documentos.id','documentos.cliente_id','documentos.nombre','documentos.vehiculo_id',DB::raw("'tipo' as tipo"))
        ->paginate();
        $archivo=array();
        $archivos[0]=array('audio'=>'ogg','hoja' => 'xls','jpg' =>'jpg','mov' =>'mov','mp3' => 'mp3','mp4'=>'mp4','pdf'=> 'pdf','png'=>'png','presenta'=>'ppt','texto' => 'txt','word' =>'doc');
        $archivos[1]=array('audio'=>'mpg','hoja' => 'xlsx','jpg'=>'jpeg','presenta'=>'pptx','word' =>'docx');
        foreach ($documentos as $doc) {
            
            $tipos=explode('.', strtolower($doc->nombre));
            $tipo=end($tipos);
            //echo "tipo=".$tipo."<br>";
            reset($archivos);
            $clave=false;
            for ($i=0; $i < 2; $i++) { 
                if(!$clave)
                    $clave = array_search(trim($tipo), $archivos[$i]);
            }
            //echo $clave.'<br>';
            if($clave){
                $doc->tipo=$clave.'.png';
            }else{
                $doc->tipo='nodisponible.png';
            }
            
            //echo $doc->tipo;
            
        }   
        return view('documento.index', compact('documentos','cliente_id','clientes','retorno','vehiculos','vehiculo_id'))
            ->with('i', (request()->input('page', 1) - 1) * $documentos->perPage());
    }
    public function createDocumento(Request $request, $cliente_id,$retorno,$vehiculo_id)
    {
        $documento = new Documento();
        $documento->cliente_id=$cliente_id;
        $documento->vehiculo_id=$vehiculo_id;
        return view('documento.create', compact('documento','cliente_id','retorno','vehiculo_id'));
    
    }
    public function multiple(Request $request,$cliente_id,$retorno,$vehiculo_id){
        
        return view('documento.multiple', compact('cliente_id','retorno','vehiculo_id'));
    }

    public function guardamultiple(Request $request,$cliente_id,$retorno,$vehiculo_id){
        $micarpeta = public_path('documentos/'.$cliente_id.'/'.$vehiculo_id);
        $nombre=$_FILES["archivos"]["name"][0];
        if($nombre!=''){
        $conteo = count($_FILES["archivos"]["name"]);
        if(!file_exists($micarpeta)) {
            mkdir(public_path('documentos/'.$cliente_id.'/'.$vehiculo_id), 0777, true);
        }
        for ($i = 0; $i < $conteo; $i++) {
            $ubicacionTemporal = $_FILES['archivos']['tmp_name'][$i];
            $nombreArchivo = $_FILES["archivos"]["name"][$i];
            $extension = pathinfo($nombreArchivo, PATHINFO_EXTENSION);
            $nuevoNombre = sprintf("%s_%d.%s", uniqid(), $i, $extension);
            $exito=move_uploaded_file($ubicacionTemporal, $micarpeta .'/'.$nombreArchivo);
            if($exito){
                $data=array('cliente_id'=>0,'nombre' =>'','vehiculo_id'=>0);
                $documento = new Documento();
                $data['cliente_id']=$cliente_id;
                $data['nombre']=$nombreArchivo;
                $data['vehiculo_id']=$vehiculo_id;
                $documento = Documento::create($data);
                $smj='Archivos cargados con Exito';
            }else{
                $smj='Hubo problemas al cargar los archivo, Por favor revise';
            }
        }
        }else{
            $smj='No hay archivos para cargar';
        }
        return redirect()
        ->action('DocumentoController@muestraDocumentos', [$cliente_id,$retorno,$vehiculo_id])
        ->with('success', $smj);

    }
    public function crearCarpeta(){
        $vehiculos=Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->select('clientes.id as cliente_id','vehiculos.id as vehiculo_id')
        ->orderBy('clientes.id','asc')
        ->get();
        /*
        foreach($vehiculos as $veh){
            $micarpeta = public_path('documentos/'.$veh->cliente_id.'/'.$veh->vehiculo_id);
            $desdecarpeta = public_path('documentos/'.$veh->cliente_id);
            if(!file_exists($micarpeta)) {
                mkdir($micarpeta, 0777, true);
            }
            shell_exec('mv '.$desdecarpeta.'/* '.$micarpeta);
        }
        */
        return view('home')->with('success', 'Proceso completo!!!');
    }
}