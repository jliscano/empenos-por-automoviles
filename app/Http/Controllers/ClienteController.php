<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use App\Models\Cobranza;
use App\Models\Empresa;
use App\Models\Multa;
use App\Models\Provincia;
use App\Models\Municipio;
use App\Models\Prestamo;
use App\Models\Vehiculo;
use App\Models\Documento;
use App\Models\Pago;
use Faker\Documentor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class ClienteController
 * @package App\Http\Controllers
 */
class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto=$request->texto;
        /*
        select a.id,a.nombre,a.dni,b.id,b.matricula from clientes a
        LEFT JOIN vehiculos b on(b.cliente_id=a.id)
        ORDER by a.id
        */
        if($texto!=''){

            $clientes = Cliente::select('clientes.id','clientes.nombre as nombres','clientes.dni','clientes.proempresa')
            ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->where('status','=','0') //en curso
            ->where('clientes.dni','like','%'.$texto.'%')
            ->orwhere('clientes.proempresa','like','%'.$texto.'%')
            ->orwhere('clientes.nombre','like','%'.$texto.'%')
            ->orwhere('vehiculos.matricula','like','%'.$texto.'%')
            ->groupBy('clientes.id','clientes.nombre','clientes.dni','clientes.proempresa')
            //->toSql();
            ->paginate();
            //var_dump($clientes);exit();

        }else{
            $clientes = Cliente::select('clientes.id','clientes.nombre as nombres','clientes.dni','clientes.proempresa')
            //->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->where('status','=','0') //en curso
            ->groupBy('clientes.id','clientes.nombre','clientes.dni','clientes.proempresa')
            ->paginate();
        }
        $vehiculosa=array();
        foreach($clientes as $cliente){
            $vehiculos=Vehiculo::where('vehiculos.cliente_id','=',$cliente->id)
            ->select('id as idveh','matricula')
            ->get();
            foreach($vehiculos as $veh){

                $vehiculosa[]=array(
                'id'=> $cliente->id,
                'matricula'=> $veh->matricula,
                'idveh' => $veh->idveh
                );
            }
        }
        //var_dump($vehiculosa);exit();
        $status='';

        return view('cliente.index', compact('clientes','status','vehiculosa'))
            ->with('i', (request()->input('page', 1) - 1) * $clientes->perPage());
    }

    public function clientesEspera(Request $request)
    {
        $texto=$request->texto;
        if($texto!=''){
            $clientes = Cliente::select('clientes.id','clientes.nombre as nombres','clientes.dni',DB::raw('(select coalesce(count(*),0) from vehiculos where cliente_id=clientes.id) as tvehiculos'),DB::raw('(select coalesce(count(*),0) from prestamos where cliente_id=clientes.id) as tempenos'),'clientes.status')
            ->where('status','=','1') //en espera
            ->where('clientes.dni','like','%'.$texto.'%')
            ->orwhere('clientes.nombre','like','%'.$texto.'%')
            //->orwhere('vehiculos.matricula','like','%'.$texto.'%')
            ->paginate();
        }else{
          $clientes = Cliente::select('clientes.id','clientes.nombre as nombres','clientes.dni',DB::raw('(select coalesce(count(*),0) from vehiculos where cliente_id=clientes.id) as tvehiculos'),DB::raw('(select coalesce(count(*),0) from prestamos where cliente_id=clientes.id) as tempenos'),'clientes.status')
          ->where('status','=','1') //en espera
          ->paginate();

        }

        $status=' En Espera';

        return view('cliente.index', compact('clientes','status'))
            ->with('i', (request()->input('page', 1) - 1) * $clientes->perPage());
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cliente = new Cliente();
        $cliente->empresa_id=1;
        $cliente->tipo_tutor='--';
        $cliente->status=0;
        $sexo=['F'=>'Femenino','M'=>'Másculino'];
        $tipo_tutor=['--'=>'--','Tutor'=>'Tutor/Padre','Apoderado'=>'Apoderado'];
        $empresa=Empresa::all()->pluck('nombre','id');
        $provincia=Provincia::all()->pluck('nombre','id');
        $municipio=['Seleccione una Provincia ..'];
        $status=['En Curso','En Espera'];
        $retorno=1;
        //Municipio::all()->pluck('nombre','id');
        return view('cliente.create', compact('cliente','empresa','provincia','municipio','retorno','status','sexo','tipo_tutor'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Cliente::$rules);

        $cliente = Cliente::create($request->all());
        $id=$cliente->id;
        return redirect()
    ->action('ClienteController@edit', $id)
    ->with('success', 'Cliente creado con Exito.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id);

        return view('cliente.show', compact('cliente'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //$cliente = Cliente::find($id);
        $cliente = Cliente::select('clientes.id','clientes.*',
        DB::raw('(select coalesce(count(*),0) from vehiculos where cliente_id=clientes.id)
        as tvehiculos'),
        DB::raw('(select coalesce(count(*),0) from prestamos
        where cliente_id=clientes.id) as tempenos'),'clientes.status'
            )
        ->find($id);
        $status=['En Curso','En Espera'];
        $sexo=['F'=>'Femenino','M'=>'Másculino'];
        $tipo_tutor=['--'=>'--','Tutor'=>'Tutor/Padre','Apoderado'=>'Apoderado'];
        $retorno=1;
        $provincia=Provincia::all()->pluck('nombre','id');
        if(!empty($cliente->provincia_id)){
        $municipio=Municipio::select('nombre','id')->where('provincia_id','=',$cliente->provincia_id)->orderBy('nombre','asc')->pluck('nombre','id');
        }else{
            $municipio=['Seleccione una Provincia'];
        }
         $empresa=Empresa::all()->pluck('nombre','id');
        return view('cliente.edit', compact('cliente','provincia','municipio','empresa','retorno','status','sexo','tipo_tutor'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Cliente $cliente
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cliente $cliente)
    {
        request()->validate(Cliente::$rules);
        $cliente->update($request->all());
        $id=$cliente->id;
        return redirect()
        ->action('ClienteController@edit', $id)
        ->with('success', 'Cliente modificado con Exito.');

        /*return redirect()->route('clientes.index')
            ->with('success', 'Cliente updated successfully');*/
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        //primero verifica que no tengan empeños y vehiculos
        //echo $id;exit();
        $prestamos=Prestamo::where('prestamos.cliente_id','=',$id)->selectRaw('count(*) as tprestamos')->get();
        $vehiculos=Vehiculo::where('vehiculos.cliente_id',$id)->selectRaw('count(*) as tvehiculos')->get();
        $cobranzas=Cobranza::where('cobranzas.cliente_id',$id)->selectRaw('count(*) as tcobros')->get();
        $multas=Multa::where('multas.cliente_id','=',$id)->where('multas.motivo','=','0')->selectRaw('count(*) as tmultas')->get();
        $gastos=Multa::where('multas.cliente_id','=',$id)->where('multas.motivo','=','1')->selectRaw('count(*) as tgastos')->get();
        //$documentos=Documento::where('documentos.cliente_id','=',$id)->selectRaw('count(*) as tdocumentos')->get();
        $tprestamos=0;$tvehiculos=0;$tcobros=0;$tmultas=0;$tgastos=0;$tdocumentos=0;

        foreach ($prestamos as $presta) {
            $tprestamos=$presta->tprestamos;
        }
        foreach ($vehiculos as $veh) {
            $tvehiculos=$veh->tvehiculos;
        }
        foreach ($cobranzas as $cob) {
            $tcobros=$cob->tcobros;
        }
        foreach ($multas as $mul) {
            $tmultas=$mul->tmultas;
        }
        foreach ($gastos as $gas) {
            $tgastos=$gas->tgastos;
        }
        /*
        foreach ($documentos as $doc) {
            $tdocumentos=$doc->tdocumentos;
        }
        */

        $msj="No se puede eliminar el cliente porque: ";
        if($tprestamos>0){
            $msj.=" Tiene prestamos asociados. ";
        }
        if($tvehiculos>0){
            $msj.=' Tiene Vehículos asociados. ';
        }
        if($tcobros>0){
            $msj.=' Tiene cobranza asociada. ';
        }
        if($tmultas>0){
            $msj.=' Tiene multas asociadas. ';
        }
        if($tgastos>0){
            $msj.=' Tiene gastos asociados. ';
        }
        if($tdocumentos>0){
            $msj.=' Tiene documentos asociados. ';
        }
        if($tvehiculos==0 and $tprestamos==0 and $tcobros==0 and $tmultas==0 and $tgastos==0 and $tdocumentos==0){
            $vehiculo = Cliente::find($id)->delete();
            $msj='Cliente eliminado con Exito';
        }

        return redirect()->route('clientes.index')
            ->with('success', $msj);
    }
    public function municipiosbyprovincias($id){
        $subCate = Municipio::where('provincia_id','=',$id)
        ->orderby('nombre')
        ->get();
        return with(["subCate" => $subCate]);

    }
    public function municipiosbyprovincias2($id, $provincia_id){
        $cMuni = Municipio::where('provincia_id','=',$provincia_id)
        ->orderby('nombre')
        ->get();
        return with(["cMuni" => $cMuni]);
    }
}
