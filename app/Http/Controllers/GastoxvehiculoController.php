<?php

namespace App\Http\Controllers;

use App\Models\Gastoxvehiculo;
use App\Models\Cliente;
use App\Models\Gasto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
/**
 * Class GastoxvehiculoController
 * @package App\Http\Controllers
 */
class GastoxvehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gastoxvehiculos = Gastoxvehiculo::paginate();

        return view('gastoxvehiculo.index', compact('gastoxvehiculos'))
            ->with('i', (request()->input('page', 1) - 1) * $gastoxvehiculos->perPage());
    }
    public function muestraGastosVehiculos($gasto_id,$empresa_id)
    {
        $gastoxvehiculos = Gastoxvehiculo::join('vehiculos','vehiculos.id','=','gastoxvehiculos.vehiculo_id')
        ->join('clientes','clientes.id','=','vehiculos.cliente_id')
        ->join('marcas','marcas.id','=','vehiculos.marca_id')
        ->join('modelos','modelos.id','=','vehiculos.modelo_id')
        ->where('gastoxvehiculos.gasto_id','=',$gasto_id)
        ->where('clientes.empresa_id','=',$empresa_id)
        ->select('gastoxvehiculos.*','marcas.nombre as mar','modelos.nombre as mod','clientes.nombre as nom','clientes.dni','vehiculos.matricula')
        ->paginate();
        $totalGastos=Gasto::where('gastos.id','=',$gasto_id)
        ->select(DB::raw('coalesce(SUM(monto),0) as total_gasto'))
        ->get();
        foreach ($totalGastos as $tot) {
            $totalg=$tot->total_gasto;
        }
        $totalVeh=Gastoxvehiculo::where('gastoxvehiculos.gasto_id','=',$gasto_id)
        ->select(DB::raw('coalesce(SUM(monto),0) as total_veh'))
        ->get();
        foreach ($totalVeh as $tot) {
            $totalv=$tot->total_veh;
        }
        
        return view('gastoxvehiculo.index', compact('gastoxvehiculos','gasto_id','empresa_id','totalg','totalv'))
            ->with('i', (request()->input('page', 1) - 1) * $gastoxvehiculos->perPage());
    }
    public function createGastoVehiculo($gasto_id,$empresa_id)
    {
        $gastoxvehiculo = new Gastoxvehiculo();
        $gastoxvehiculo->gasto_id=$gasto_id;
        return view('gastoxvehiculo.create', compact('gastoxvehiculo','empresa_id'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gastoxvehiculo = new Gastoxvehiculo();
        return view('gastoxvehiculo.create', compact('gastoxvehiculo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Gastoxvehiculo::$rules);
        $gastoxvehiculo = Gastoxvehiculo::create($request->all());
        $id=$gastoxvehiculo->id;
        return redirect()
    ->action('GastoxvehiculoController@edit', $id)
    ->with('success', 'Vinculación de Vehículo a gasto modificada con Exito.');
    }
    public function storeGastosxVehiculos(Request $request,$empresa_id)
    {
        $gasto_id=$request->gasto_id;
        $totalGastos=Gasto::where('gastos.id','=',$gasto_id)
        ->select(DB::raw('coalesce(SUM(monto),0) as total_gasto'))
        ->get();
        foreach ($totalGastos as $tot) {
            $totalg=$tot->total_gasto;
        }
        $totalVeh=Gastoxvehiculo::where('gastoxvehiculos.gasto_id','=',$gasto_id)
        ->select(DB::raw('coalesce(SUM(monto),0) as total_veh'))
        ->get();

        foreach ($totalVeh as $tot1) {
            $totalv=$tot1->total_veh;
        }
        $totalv=$totalv+$request->monto;
        //echo "monto=".$totalv." monto=".$totalg;exit();

        if($totalv<=$totalg){
            request()->validate(Gastoxvehiculo::$rules);
            $gastoxvehiculo = Gastoxvehiculo::create($request->all());
            $id=$gastoxvehiculo->id;

            return redirect()
            ->action('GastoxvehiculoController@editaGastosxVehiculos', [$id,$empresa_id])
            ->with('success', 'Vinculación de Vehículo a gasto creada con Exito.');
        }else{
            return back()->with('success', 'La suma de los montos de los Vehículos vinculados supera el monto del <b>gasto general</b>, Verifique y modifique el monto .');
        }

    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gastoxvehiculo = Gastoxvehiculo::find($id);
        return view('gastoxvehiculo.show', compact('gastoxvehiculo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function editaGastosxVehiculos($id,$empresa_id)
    {
        $gastoxvehiculo = Gastoxvehiculo::find($id);
        return view('gastoxvehiculo.edit', compact('gastoxvehiculo','empresa_id','id'));
    }
    public function updateGastoxVehiculo(Request $request, Gastoxvehiculo $gastoxvehiculo, $id, $empresa_id)
    {   
        request()->validate(Gastoxvehiculo::$rules);
        $gastoxvehiculo = Gastoxvehiculo::where('id','=', $id)
              ->update(['monto' => $request->monto,'vehiculo_id' => $request->vehiculo_id]);
        //$gastoxvehiculo->update();
        //$gastoxvehiculo->update($request->all());
        
        return redirect()->action('GastoxvehiculoController@editaGastosxVehiculos', [$id,$empresa_id])->with('success', 'Vinculo de Vehículo a gasto modificado con Exito.');
    }

    public function edit($id)
    {
        $gastoxvehiculo = Gastoxvehiculo::find($id);
        return view('gastoxvehiculo.edit', compact('gastoxvehiculo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Gastoxvehiculo $gastoxvehiculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Gastoxvehiculo $gastoxvehiculo)
    {
        request()->validate(Gastoxvehiculo::$rules);
        $gastoxvehiculo->update($request->all());
        $id=$gastoxvehiculo->id;
        
        return redirect()
    ->action('GastoxvehiculoController@edit', $id,compact('gastoxvehiculo'))
    ->with('success', 'Vehículo vinculado a gasto con Exito.');
    }
    
    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $gastoxvehiculo = Gastoxvehiculo::find($id)->delete();

        return redirect()->route('gastoxvehiculos.index')
            ->with('success', 'Gastoxvehiculo deleted successfully');
    }
    public function destroyGastoxVehiculo($id,$gasto_id,$empresa_id)
    {
        $gastoxvehiculo = Gastoxvehiculo::find($id)->delete();
        return redirect()
    ->action('GastoxvehiculoController@muestraGastosVehiculos', [$gasto_id,$empresa_id])
    ->with('success', 'Vinculo de Vehículo a gasto Eliminado con Exito.');
    }
    
    public function miJqueryAjax(Request $request, $dato){
        if (isset($_SERVER['HTTP_ORIGIN'])) {  
            header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");  
            header('Access-Control-Allow-Credentials: true');  
            header('Access-Control-Max-Age: 86400');   
        }
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {  
  
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))  
                header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE,OPTIONS");  
            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))  
                header("Access-Control-Allow-Headers: 
                {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");  
        } 
        $datos=explode('|', $dato);
        
        $clientes=Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->join('condicionvehiculos','condicionvehiculos.id','vehiculos.situacion_id')
        ->orwhere('vehiculos.id','=',$datos[0])
        ->orwhere('clientes.dni','=',$datos[0])
        ->orwhere('vehiculos.matricula','=',$datos[0])
        ->select('vehiculos.id as veh','vehiculos.matricula','clientes.nombre','clientes.dni','clientes.empresa_id','vehiculos.situacion_id','condicionvehiculos.nombre as cond')
        ->get();
        $data=array();
        $data[0]=0;
        foreach($clientes as $cliente){
            if($cliente->empresa_id==$datos[1]){
                //verifica que la condicion es en deposito o en reparacion
                if($cliente->situacion_id==1 or $cliente->situacion_id==4){
                    if(!is_null($cliente->veh)&&!empty($cliente->veh)){
                        $data[0] = 1;
                        $data[1] = $cliente->veh;
                        $data[2] = $cliente->nombre;
                        $data[3] = $cliente->dni;
                        $data[4] = $cliente->matricula;
                    }
                }else{
                    $data[0] = 2;
                    $data[5] = $cliente->cond;
                }
            }else{
                $data[0] = 3;
                $data[5] = 'el vehículo no le pertenece a esta Empresa por favor verifique!!!';
            }
        }
        if($data[0]==0){
            $data[5]='Matrícula no encontrada';
        }
        //$data[5]='Aqui';
        return $data;
    }
}
