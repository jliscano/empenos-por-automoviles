<?php

namespace App\Http\Controllers;

use App\Models\Multa;
use App\Models\Cliente;
use App\Models\Prestamo;
use App\Models\Vehiculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
/**
 * Class MultaController
 * @package App\Http\Controllers
 */
class MultaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $multas = Multa::paginate();

        return view('multa.index', compact('multas'))
            ->with('i', (request()->input('page', 1) - 1) * $multas->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $multa = new Multa();
        return view('multa.create', compact('multa'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Multa::$rules);

        $multa = Multa::create($request->all());
        $cliente_id=$multa->cliente_id;
        $vehiculo_id=$multa->vehiculo_id;
        $retorno=$request->retorno;
        $pagada=array('No','Si');
        $motivo=$request->motivo;
        return redirect()
    ->action('MultaController@muestraMultas', [$cliente_id,$motivo,$vehiculo_id,$retorno])
    ->with('success', 'Multa creada con Exito.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $multa = Multa::find($id);

        return view('multa.show', compact('multa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $multa = Multa::find($id);
        $motivo = $multa->motivo;
        $pagada=array('No','Si');
        $motivos=array('Multas','Gastos de Recuperación' );
        $cliente=Cliente::find($multa->cliente_id);
        $nombre=$cliente->nombre;
        return view('multa.edit', compact('multa','pagada','motivos','nombre','motivo'));
    }

    public function editaMulta($id,$retorno)
    {
        $multa = Multa::find($id);
        $motivo = $multa->motivo;
        $vehiculo_id=$multa->vehiculo_id;
        $pagada=array('No','Si');
        $motivos=array('Multas','Gastos de Recuperación' );
        $cliente=Cliente::find($multa->cliente_id);
        $nombre=$cliente->nombre;

        return view('multa.edit', compact('multa','pagada','motivos','nombre','motivo','vehiculo_id','retorno'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Multa $multa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Multa $multa)
    {
        request()->validate(Multa::$rules);

        $multa->update($request->all());
        $motivo=$request->motivo;
        $vehiculo_id=$request->vehiculo_id;
        $retorno=$request->retorno;

        return redirect()
    ->action('MultaController@muestraMultas', [$multa->cliente_id,$motivo,$vehiculo_id,$retorno])
    ->with('success', 'Multa modificada con Exito.');
        //return redirect()->route('multas.index')            ->with('success', 'Multa updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request,$id)
    {
        $motivo=$request->motivo;
        $vehiculo_id=$request->vehiculo_id;
        $retorno=$request->vehiculo_id;
        $cliente_id=$request->cliente_id;
        $multa = Multa::find($id)->delete();

        return redirect()
        ->action('MultaController@muestraMultas', [$cliente_id,$motivo,$vehiculo_id,$retorno])
    ->with('success', 'Multa eliminada con Exito.');

    }
    public function borraMulta($id,$cliente_id,$vehiculo_id,$motivo,$retorno)
    {
        $multa = Multa::find($id)->delete();
         $motivos=array('Multas','Gastos de Recuperación' );
        return redirect()
        ->action('MultaController@muestraMultas', [$cliente_id,$motivo,$vehiculo_id,$retorno])
    ->with('success', $motivos[$motivo].' eliminada con Exito.');

    }

    public function muestraMultas($id,$motivo,$vehiculo_id,$retorno){
        $multas = Cliente::leftjoin('multas','clientes.id','=','multas.cliente_id')
        ->where('multas.cliente_id','=',$id)
        ->where('multas.motivo','=',$motivo)
        ->where('vehiculo_id','=',$vehiculo_id)
        ->paginate();
        $cliente=Cliente::find($id);
        $nombre=$cliente->nombre;
        $pagada=array('No','Si');
        $cliente_id=$id;
        $motivos=array('Multas','Gastos de Recuperación' );

        return view('multa.index', compact('multas','pagada','cliente_id','nombre','motivo','motivos','retorno','vehiculo_id'))
            ->with('i', (request()->input('page', 1) - 1) * $multas->perPage());
    }

    public function muestraMultasGeneral(Request $request,$motivo,$retorno){
        $multas = Cliente::leftjoin('vehiculos','clientes.id','=','vehiculos.cliente_id')
        ->leftjoin('marcas','marcas.id','=','vehiculos.marca_id')
        ->leftjoin('modelos','modelos.id','=','vehiculos.modelo_id')
        ->select('clientes.id as cliente_id','clientes.nombre as nom','clientes.proempresa','vehiculos.matricula',
                 'marcas.nombre as marca','modelos.nombre as modelo','vehiculos.id as vehiculo_id',
            DB::raw("(select coalesce(count(*),0) from multas where motivo='0'
            and cliente_id=clientes.id and pagado='0') as total")
        );


        $texto=$request->texto;
        if($texto!=''){
            $multas=$multas->where('clientes.nombre','like','%'.$texto.'%')
            ->orWhere('clientes.nombre2','like','%'.$texto.'%')
            ->orWhere('vehiculos.matricula','like','%'.$texto.'%')
            ->orWhere('clientes.proempresa','like','%'.$texto.'%');
        }
        $multas=$multas->paginate();

        $pagada=array('No','Si');
        $motivos=array('Multas','Gastos de Recuperación' );

        return view('multa.general', compact('multas','pagada','motivo','motivos','retorno'))
            ->with('i', (request()->input('page', 1) - 1) * $multas->perPage());

    }
    public function createMultas($id,$vehiculo_id,$motivo,$retorno)
    {
        $multa = new Multa();
        $multa->cliente_id=$id;
        $multa->vehiculo_id=$vehiculo_id;

        $multa->motivo=$motivo;
        $pagada=array('No','Si');
        $motivos=array('Multas','Gastos de Recuperación' );
        $cliente= Cliente::find($id);
        $nombre=$cliente->nombre;
        return view('multa.create', compact('multa','pagada','motivos','nombre','motivo','retorno'));
    }
}
