<?php

namespace App\Http\Controllers;
use App\Models\Cliente;
use App\Models\Vehiculo;
use Illuminate\Http\Request;
use Mail;

class PolizaController extends Controller
{
    //
    public function index()
    {
        
        return view('poliza.index');
    }
    public function muestraPolizas( Request $request)
    {
        if(isset($request->startDate)){
            $data=explode('/', $request->startDate);
            Config('global', $data);
        }
        
        $clientes = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->rightjoin('empseguros','vehiculos.empseguro_id','=','empseguros.id')
        ->select('clientes.id as idcliente','clientes.*',
        'marcas.nombre as mar','modelos.nombre as mod','vehiculos.*',
        'empseguros.nombre as empseg')
        ->whereYear('fvencseguro','=', $data[1])
        ->whereMonth('fvencseguro','<=', $data[0])
        ->orwhereYear('fvencseguro','<', $data[1])
        ->paginate();

        return view('poliza.muestrapolizas', compact('clientes','data'))
            ->with('i', (request()->input('page', 1) - 1) * $clientes->perPage());
    }
    
    public function show($id)
    {
        $data=array(config('global.mes'),config('global.anno'));
        $clientes = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->rightjoin('empseguros','vehiculos.empseguro_id','=','empseguros.id')
        ->select('clientes.id as idcliente','clientes.*','marcas.nombre as mar','modelos.nombre as mod','vehiculos.*',
        'empseguros.nombre as empseg')
        ->whereYear('fvencseguro','=', $data[1])
        ->whereMonth('fvencseguro','<=', $data[0])
        ->orwhereYear('fvencseguro','<', $data[1])
        ->paginate();

        return view('poliza.muestrapolizas', compact('clientes','data'))
            ->with('i', (request()->input('page', 1) - 1) * $clientes->perPage());
    }
}
