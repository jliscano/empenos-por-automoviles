<?php

namespace App\Http\Controllers;

use App\Models\Banco;
use App\Models\Empresa;
use Illuminate\Http\Request;

/**
 * Class BancoController
 * @package App\Http\Controllers
 */
class BancoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bancos = Banco::paginate();

        return view('banco.index', compact('bancos'))
            ->with('i', (request()->input('page', 1) - 1) * $bancos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $banco = new Banco();
        return view('banco.create', compact('banco'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Banco::$rules);

        $banco = Banco::create($request->all());

        return redirect()->route('bancos.index')
            ->with('success', 'Banco creado con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $banco = Banco::find($id);

        return view('banco.show', compact('banco'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $banco = Banco::find($id);

        return view('banco.edit', compact('banco'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Banco $banco
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Banco $banco)
    {
        request()->validate(Banco::$rules);

        $banco->update($request->all());

        return redirect()->route('bancos.index')
            ->with('success', 'Banco Modificado con Exito');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $empresas=Empresa::where('empresas.banco_id','=',$id)->selectRaw('count(*) as total')->get();
        $tbancos=0;
        foreach ($empresas as $gas) {
            $tbancos=$gas->total;
        }
        
        if($tbancos==0){
            $banco = Banco::find($id)->delete();
            $smj='Banco eliminado con Exito';
        }else{
            $smj='No se puede eliminar porque tiene Empresas asociadas';
        }
        return redirect()->route('bancos.index')
            ->with('success', $smj);
    }
}
