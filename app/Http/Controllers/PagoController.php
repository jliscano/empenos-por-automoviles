<?php

namespace App\Http\Controllers;

use App\Models\Pago;
use App\Models\Prestamo;
use App\Models\Cliente;
use App\Models\Vehiculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
/**
 * Class PagoController
 * @package App\Http\Controllers
 */
class PagoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pagos = Pago::paginate();

        return view('pago.index', compact('pagos'))
            ->with('i', (request()->input('page', 1) - 1) * $pagos->perPage());
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   /* public function create()
    {
        $pago = new Pago();
        return view('pago.create', compact('pago'));
    }
*/
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pago = Pago::find($id);

            return view('pago.show', compact('pago'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    /*
    public function edit($id)
    {
        $pago = Pago::find($id);
        //$prestamo_id=$pago->prestamo_id;

        $prestamo = Prestamo::find($pago->prestamo_id);
        $cliente=Cliente::find($prestamo->cliente_id);
        //$pago->monto=number_format($prestamo->tcuota,2,',','.');
        $cuota=$prestamo->tcuota;

        $nombre=$cliente->nombre;
        $conciliado=array('No','Si');
        if($prestamo->tipo==1 and $pago->conciliado==1){
            $motivos=array('Cuotas'=>'0','Pago final Empeño'=>1);
            $importe=$prestamo->importe;
        }else{
            $motivos=array('Cuotas'=>0);
            $importe=$prestamo->importe;
        }

        return view('pago.edit', compact('pago','conciliado','motivos','nombre','importe','cuota'));
    }
        */
     public function editaPago($id,$retorno,$padre)
    {
        $pago = Pago::find($id);

        //buscamos sus extensiones
        $prestamo = Prestamo::find($pago->prestamo_id);
        $piva=$prestamo->piva;
        $iva=$prestamo->iva;
        $cuota=$prestamo->tcuota;

        $cliente=Cliente::find($prestamo->cliente_id);

        $cuotaprincipal=$prestamo->tcuota;
        $finiciopago=$prestamo->finiciopago;
        $residual=$prestamo->residual;
        /*if($prestamo->tipo==0)
            $cuotas=$prestamo->cuotas;
        else*/
            $cuotas=200;
        $nombre=$cliente->nombre;
        $cuota_nro=acomoda_cuotas($pago->prestamo_id,$finiciopago,$cuotas);
        $conciliado=array('No','Si');
        //and $pago->conciliado==1
        if($prestamo->tipo==1 ){
            $motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño','2'=>'Gastos de recogida',
            '3'=>'Gastos por impago','4'=>'Ivtm','5'=>'Pago a cuenta del principal del principal');
            $importe=$prestamo->importe;
        }else{
            $motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño','2'=>'Gastos de recogida',
            '3'=>'Gastos por impago','4'=>'Ivtm','5'=>'Pago a cuenta del principal del principal');
            $importe=$prestamo->importe;
        }

        return view('pago.edit', compact('pago','conciliado','motivos','nombre','importe','cuota','retorno','piva','iva','cuota_nro','padre'));
    }



    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function borraPago($id, $retorno)
    {
        $pago = Pago::find($id);
        $prestamo_id=$pago->prestamo_id;
        $importe=$pago->importe;
        //$pago = Pago::find($id)->delete();
        $pagos=Pago::where('prestamo_id','=',$prestamo_id)
        ->where('motivo','=','0')
        ->inWhere('motivo',['0','1'])
        ->sum('pagos.monto as total')
        ->get();
        $monto=0;
        foreach($pagos as $pago){
            $monto=$pago->total;
        }
        echo $monto;
        exit();
        return redirect()
        ->action('PagoController@muestraPago', [$prestamo_id,$retorno])
    ->with('success', 'Pago eliminado con Exito.');
    }

    public function destroy(Request $request, $id, Pago $pagos)
    {
        $retorno=$request->retorno;
        $padre=$request->padre;
        $pago = Pago::find($id);
        $prestamo_idx=$pago->prestamo_id;
        $condicion=$pago->condicion_id;
        $prestamo=Prestamo::find($prestamo_idx);
        $importe=$prestamo->importe;
        $pago = Pago::find($id)->delete();
        $pagosum=Pago::where('prestamo_id','=',$prestamo_idx)
        ->WhereIn('motivo',['0','1'])
        ->select(DB::raw("COALESCE(SUM(monto),0) as total"))
        ->get();
        $monto=0;
        foreach($pagosum as $pag){
            $monto=$pag->total;
        }
        if($monto>=$importe and $condicion==1)
            $prestamo=Prestamo::where('prestamos.id','=',$prestamo_idx)->update(['prestamos.condicion_id'=>2]);
        elseif($monto<$importe)
            $prestamo=Prestamo::where('prestamos.id','=',$prestamo_idx)->update(['prestamos.condicion_id'=>1]);

        return redirect()
        ->action('PagoController@muestraPago', [$prestamo_idx,$retorno,$padre])
    ->with('success', 'Pago eliminado con Exito.');
    }

    public function muestraPago($prestamo_id,$retorno,$padre)
    {
        $pagos = Pago::where('prestamo_id','=',$prestamo_id)->paginate();
        $prestamo_idx=$prestamo_id;
        //$motivos=array('Cuotas','Pago Total Empeño');

        $motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño','2'=>'Gastos de recogida',
            '3'=>'Gastos por impago','4'=>'Ivtm','5'=>'Pago a cuenta del principal del principal');

        $prestamo = Prestamo::find($prestamo_id);
        $vehiculo_id=$prestamo->vehiculo_id;
        $ffirma=$prestamo->ffirma;
        $condicion=$prestamo->condicion_id;
        $cliente=Cliente::find($prestamo->cliente_id);
        $nombre=$cliente->nombre;
        $dni=$cliente->dni;

        $cliente_id=$cliente->id;
        //leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        $vehiculo=Vehiculo::leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->select('vehiculos.*','marcas.nombre as mar','modelos.nombre as mod')
        ->where('cliente_id','=',$cliente->id)
        ->where('vehiculos.id','=',$vehiculo_id)
        ->where('situacion_id','=',2)
        ->orwhere('situacion_id','=',3)
        ->get();
        //en circulacion o en espera;
        $sitveh=0;
        foreach($vehiculo as $veh) {
            //if($veh->situacion_id)
                $sitveh=$veh->situacion_id;
                $matricula=$veh->matricula;
                $mar=$veh->mar;
                $mod=$veh->mod;
        }

        return view('pago.index', compact('pagos','prestamo_id','motivos','nombre','sitveh','cliente_id','vehiculo_id','retorno','mar','mod','matricula','dni','ffirma','condicion','padre'))
            ->with('i', (request()->input('page', 1) - 1) * $pagos->perPage());
    }

    public function muestraPagoExtension($prestamo_id,$retorno,$padre)
    {
        $pagos = Pago::where('prestamo_id','=',$prestamo_id)->paginate();
        $prestamo_idx=$prestamo_id;
        //$motivos=array('Cuotas','Pago Total Empeño');

        $motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño','2'=>'Gastos de recogida',
            '3'=>'Gastos por impago','4'=>'Ivtm','5'=>'Pago a cuenta del principal del principal');

        $prestamo = Prestamo::find($prestamo_id);
        $vehiculo_id=$prestamo->vehiculo_id;
        $ffirma=$prestamo->ffirma;
        $condicion=$prestamo->condicion_id;
        $cliente=Cliente::find($prestamo->cliente_id);
        $nombre=$cliente->nombre;
        $dni=$cliente->dni;

        $cliente_id=$cliente->id;
        //leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        $vehiculo=Vehiculo::leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->select('vehiculos.*','marcas.nombre as mar','modelos.nombre as mod')
        ->where('cliente_id','=',$cliente->id)
        ->where('vehiculos.id','=',$vehiculo_id)
        ->where('situacion_id','=',2)
        ->orwhere('situacion_id','=',3)
        ->get();
        //en circulacion o en espera;
        $sitveh=0;
        foreach($vehiculo as $veh) {
            //if($veh->situacion_id)
                $sitveh=$veh->situacion_id;
                $matricula=$veh->matricula;
                $mar=$veh->mar;
                $mod=$veh->mod;
        }

        return view('pago.index', compact('pagos','prestamo_id','motivos','nombre','sitveh','cliente_id','vehiculo_id','retorno','mar','mod','matricula','dni','ffirma','condicion','padre'))
            ->with('i', (request()->input('page', 1) - 1) * $pagos->perPage());
    }

    public function createPago($id,$retorno,$padre)
    {
        $conciliado=array('No','Si');
        $pago=Pago::select(DB::raw("COALESCE(COUNT(*),0) as numpagos"))
        ->where('prestamo_id','=',$id)->get();
        $numpagos=0;
        foreach($pago as $pag){
            $numpagos=$pag->numpagos;
        }
        $pago = new Pago();
        $pago->prestamo_id=$id;
        $extension_id=0;
        $piva=0;
        $iva=0;
        $cuotaprincipal=0;
        $finiciopago='';
        $residual=0;
        $cuotas=0;
        $motivos=array();
        $nombre='';
        $importe=0;
        $cuota=0;
        $cuota_nro=array();
        //busco el empeño principal
            $prestamo = Prestamo::find($id);
            $piva=$prestamo->piva;
            $iva=$prestamo->iva;
            $cuotaprincipal=$prestamo->tcuota;
            $finiciopago=$prestamo->finiciopago;
            $residual=$prestamo->residual;
            $cuotas=$prestamo->cuotas;
            /*
            if($prestamo->tipo==0)
                $cuotas=$prestamo->cuotas;
            else
            */
                $cuotas=120;
                $cliente=Cliente::find($prestamo->cliente_id);
                $cuota_nro=acomoda_cuotas($pago->prestamo_id,$finiciopago,$cuotas);
                $nombre=$cliente->nombre;
            if($prestamo->tipo=='1'){
                //$motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño');
                $motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño','2'=>'Gastos de recogida',
                '3'=>'Gastos por impago','4'=>'Ivtm','5'=>'Pago a cuenta del principal del principal');
                $importe=$prestamo->importe;
                if($numpagos>0)
                    $cuota=$prestamo->tcuota;
                else{
                    if($prestamo->residual==0){
                        $cuota=$prestamo->tcuota;
                    }else{
                        $cuota=$prestamo->residual;
                    }
                }
            }else{
                $motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño','2'=>'Gastos de recogida',
                '3'=>'Gastos por impago','4'=>'Ivtm','5'=>'Pago a cuenta del principal del principal');
                $importe=$prestamo->importe;
                $cuota=$prestamo->tcuota;
            }
            $pago->monto=$cuota;

            /* $extension_id=$this->busca_cuota_extensiones($id,$pago,
            $piva,
            $iva,
            $cuotaprincipal,
            $finiciopago,
            $residual,
            $cuotas,
            $motivos,
            $nombre,
            $importe,
            $cuota,$numpagos); */

        return view('pago.create', compact('pago','conciliado','motivos','nombre','importe','cuota','retorno','piva','iva','cuota_nro','padre'));
    }

    private function busca_cuota_extensiones($id,&$pago,&$piva,
    &$iva,
    &$cuotaprincipal,
    &$finiciopago,
    &$residual,
    &$cuotas,&$motivos,&$nombre,&$importe,&$cuota,$numpagos){
        $extensiones=Prestamo::where('prestamos.padre','=',$id)
        ->select('prestamos.*')
        ->orderBy('prestamos.finiciopago','Asc')
        ->get();
        $extension_id=0;
        foreach ($extensiones as $exten) {
            $extension_id=$exten->id;
            $piva=$exten->piva;
            $iva=$exten->iva;
            $cuotaprincipal=$exten->tcuota;
            $finiciopago=$exten->finiciopago;
            $residual=$exten->residual;
            $cuotas=$exten->cuotas;
            if($exten->tipo==0)
                $cuotas=$exten->cuotas;
            else
                $cuotas=50;
                $cliente=Cliente::find($exten->cliente_id);
                //$cuota_nro=acomoda_cuotas($pago->prestamo_id,$finiciopago,$cuotas);
                $nombre=$cliente->nombre;
            if($exten->tipo=='1'){
                //$motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño');
                $motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño','2'=>'Gastos de recogida',
                '3'=>'Gastos por impago','4'=>'Ivtm','5'=>'Pago a cuenta del principal del principal');
                $importe=$exten->importe;
                if($numpagos>0)
                    $cuota=$exten->tcuota;
                else{
                    if($exten->residual==0){
                        $cuota=$exten->tcuota;
                    }else{
                        $cuota=$exten->residual;
                    }
                }
            }else{
                $motivos=array('0'=>'Cuotas','1'=>'Pago final Empeño','2'=>'Gastos de recogida',
                '3'=>'Gastos por impago','4'=>'Ivtm','5'=>'Pago a cuenta del principal del principal');
                $importe=$exten->exten;
                $cuota=$exten->tcuota;
            }
        }
        $pago->monto=$cuota;
        return $extension_id;
    }
    public function store(Request $request)
    {
        request()->validate(Pago::$rules);
        $retorno=$request->retorno;
        $padre=$request->padre;
        $pago = Pago::create($request->all());

        $msg_adicional='';
        $prestamo = Prestamo::find($pago->prestamo_id);
        $importePrestamo=$prestamo->importe;
        $tipo=$prestamo->tipo;
        $tiene_extension='N';
        $importe=$this->busca_monto_ultima_extension($prestamo->id,$importePrestamo,$tipo,$tiene_extension);
        if($request->motivo=='1' and $request->conciliado==1){
            $prestamo=Prestamo::where('prestamos.id','=',$pago->prestamo_id)->update(['prestamos.condicion_id'=>2]);
            $msg_adicional='Empeño marcado como cancelado';
        }
        if($tipo=='0' and $request->motivo=='0' and $request->conciliado==1){
            $PagoTotal=Pago::where('pagos.prestamo_id','=',$pago->prestamo_id)->where('pagos.motivo','=','0')->selectRaw('sum(pagos.monto) as suma ')->get();
            $total=0;
            foreach($PagoTotal as $pag){
                $total=$pag->suma;
            }

            if($total>=$importe){
                $prestamo=Prestamo::where('prestamos.id','=',$pago->prestamo_id)->update(['prestamos.condicion_id'=>2]);
                    $msg_adicional='Empeño marcado como cancelado';
            }else{
                $prestamo=Prestamo::where('prestamos.id','=',$pago->prestamo_id)->update(['prestamos.condicion_id'=>1]);
            }
        }
        return redirect()
    ->action('PagoController@muestraPago', [$pago->prestamo_id,$retorno,$padre])
    ->with('success', 'Pago creado con Exito. '.$msg_adicional);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Pago $pago
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pago $pago)
    {
        request()->validate(Pago::$rules);
        $padre=$request->padre;
        $pago->update($request->all());
        $msg_adicional='';
        $prestamo=Prestamo::find($request->prestamo_id);
        $importePrestamo=$prestamo->importe;
        $tipo=$prestamo->tipo;
        $tiene_extension='N';
        $importe=$this->busca_monto_ultima_extension($prestamo->id,$importePrestamo,$tipo,$tiene_extension);
        $retorno=$request->retorno;
        //if($tipo=='1'){
            if($request->motivo=='1'){
                if($request->conciliado==1){
                    $prestamo=Prestamo::where('prestamos.id','=',$pago->prestamo_id)->update(['prestamos.condicion_id'=>2]);
                    $msg_adicional='Empeño marcado como cancelado';
                }else{
                    $prestamo=Prestamo::where('prestamos.id','=',$pago->prestamo_id)->update(['prestamos.condicion_id'=>1]);
                }
            }
        //}else{
        if($tipo=='0' and $request->motivo=='0' and $request->conciliado==1){
            $PagoTotal=Pago::where('pagos.prestamo_id','=',$pago->prestamo_id)->where('pagos.motivo','=','0')->selectRaw('sum(pagos.monto) as suma ')->get();
            $total=0;
            foreach($PagoTotal as $pag){
                $total=$pag->suma;
            }

            if($total>=$importe){
                $prestamo=Prestamo::where('prestamos.id','=',$pago->prestamo_id)->update(['prestamos.condicion_id'=>2]);
                    $msg_adicional='Empeño marcado como cancelado';
            }else{
                $prestamo=Prestamo::where('prestamos.id','=',$pago->prestamo_id)->update(['prestamos.condicion_id'=>1]);
            }
        }

        return redirect()
        ->action('PagoController@muestraPago', [$pago->prestamo_id,$retorno,$padre])
    ->with('success', 'Pago modificado con Exito.'.$msg_adicional);

        /*return redirect()->route('pagos.index')
            ->with('success', 'Pago modificado con Exito');
            */
    }
    private function busca_monto_ultima_extension($id,$importe,&$tipo,&$tiene_extension){
        $extensiones=Prestamo::where('prestamos.padre','=',$id)
        ->select('prestamos.*')
        ->orderBy('prestamos.finiciopago','Asc')
        ->get();
        $monto=0;
        foreach ($extensiones as $exten) {
            $monto=$exten->importe;
            $tipo=$exten->tipo;
        }
        if($monto>0){
            $tiene_extension='S';
            return $monto;
        }else
            return $importe;

    }
}
