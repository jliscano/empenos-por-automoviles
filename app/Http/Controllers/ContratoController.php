<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cliente;
class ContratoController extends Controller
{
     public function index()
    {
        return view('contrato.index');
    }
    public function muestraContratos($cliente_id,$prestamo_id,$vehiculo_id,$retorno)
    {
        $clientes = Cliente::where('clientes.id','=',$cliente_id)
        ->where('prestamos.id','=',$prestamo_id)
        ->where('vehiculos.id','=',$vehiculo_id)
        ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        //join('empresas','clientes.empresa_id','=','empresas.id')
        ->leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->select('clientes.id','clientes.nombre as nombres','clientes.dni','vehiculos.matricula','marcas.nombre as mar','modelos.nombre as mod'
        ,'vehiculos.id as veh','prestamos.id as pre')->paginate();

        return view('contrato.index',compact('cliente_id','clientes','prestamo_id','vehiculo_id','retorno'));
    }
}
