<?php

namespace App\Http\Controllers;

use App\Models\Ingresoxvehiculo;
use App\Models\Cliente;
use App\Models\Ingreso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


/**
 * Class IngresoxvehiculoController
 * @package App\Http\Controllers
 */
class IngresoxvehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ingresoxvehiculos = Ingresoxvehiculo::paginate();

        return view('ingresoxvehiculo.index', compact('ingresoxvehiculos'))
            ->with('i', (request()->input('page', 1) - 1) * $ingresoxvehiculos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ingresoxvehiculo = new Ingresoxvehiculo();
        return view('ingresoxvehiculo.create', compact('ingresoxvehiculo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Ingresoxvehiculo::$rules);

        $ingresoxvehiculo = Ingresoxvehiculo::create($request->all());

        return redirect()->route('ingresoxvehiculos.index')
            ->with('success', 'Ingresoxvehiculo created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ingresoxvehiculo = Ingresoxvehiculo::find($id);

        return view('ingresoxvehiculo.show', compact('ingresoxvehiculo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ingresoxvehiculo = Ingresoxvehiculo::find($id);

        return view('ingresoxvehiculo.edit', compact('ingresoxvehiculo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Ingresoxvehiculo $ingresoxvehiculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ingresoxvehiculo $ingresoxvehiculo)
    {
        request()->validate(Ingresoxvehiculo::$rules);

        $ingresoxvehiculo->update($request->all());

        return redirect()->route('ingresoxvehiculos.index')
            ->with('success', 'Ingresoxvehiculo updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $ingresoxvehiculo = Ingresoxvehiculo::find($id)->delete();

        return redirect()->route('ingresoxvehiculos.index')
            ->with('success', 'Ingresoxvehiculo deleted successfully');
    }

    public function muestraIngresosVehiculos($ingreso_id,$empresa_id)
    {

        $ingresoxvehiculos = Ingresoxvehiculo::join('vehiculos','vehiculos.id','=','ingresoxvehiculos.vehiculo_id')
        ->join('clientes','clientes.id','=','vehiculos.cliente_id')
        ->join('marcas','marcas.id','=','vehiculos.marca_id')
        ->join('modelos','modelos.id','=','vehiculos.modelo_id')
        ->where('ingresoxvehiculos.ingreso_id','=',$ingreso_id)
        ->where('clientes.empresa_id','=',$empresa_id)
        ->select('ingresoxvehiculos.*','marcas.nombre as mar','modelos.nombre as mod','clientes.nombre as nom','clientes.dni','vehiculos.matricula')
        ->paginate();
        $totalIngresos=Ingreso::where('ingresos.id','=',$ingreso_id)
        ->select(DB::raw('coalesce(SUM(monto),0) as total_ingreso'))
        ->get();
        foreach ($totalIngresos as $tot) {
            $totali=$tot->total_ingreso;
        }
        $totalVeh=Ingresoxvehiculo::where('ingresoxvehiculos.ingreso_id','=',$ingreso_id)
        ->select(DB::raw('coalesce(SUM(monto),0) as total_veh'))
        ->get();
        $totalv=0;
        foreach ($totalVeh as $tot) {
            $totalv=$tot->total_veh;
        }
        
        return view('ingresoxvehiculo.index', compact('ingresoxvehiculos','ingreso_id','empresa_id','totali','totalv'))
            ->with('i', (request()->input('page', 1) - 1) * $ingresoxvehiculos->perPage());
    }

    public function createIngresoVehiculo($ingreso_id,$empresa_id)
    {
        $ingresoxvehiculo = new Ingresoxvehiculo();
        $ingresoxvehiculo->ingreso_id=$ingreso_id;
        return view('ingresoxvehiculo.create', compact('ingresoxvehiculo','empresa_id'));
    }
    
    public function storeIngresosxVehiculos(Request $request,$empresa_id)
    {
        $ingreso_id=$request->ingreso_id;
        $totalIngresos=Ingreso::where('ingresos.id','=',$ingreso_id)
        ->select(DB::raw('coalesce(SUM(monto),0) as total_ingreso'))
        ->get();
        foreach ($totalIngresos as $tot) {
            $totali=$tot->total_ingreso;
        }
        $totalVeh=Ingresoxvehiculo::where('ingresoxvehiculos.ingreso_id','=',$ingreso_id)
        ->select(DB::raw('coalesce(SUM(monto),0) as total_veh'))
        ->get();

        foreach ($totalVeh as $tot1) {
            $totalv=$tot1->total_veh;
        }
        $totalv=$totalv+$request->monto;
        //echo "monto=".$totalv." monto=".$totalg;exit();

        if($totalv<=$totali){
            request()->validate(Ingresoxvehiculo::$rules);
            $ingresoxvehiculo = Ingresoxvehiculo::create($request->all());
            $id=$ingresoxvehiculo->id;

            return redirect()
            ->action('IngresoxvehiculoController@editaIngresosxVehiculos', [$id,$empresa_id])
            ->with('success', 'Vinculación de Vehículo a Ingresos creada con Exito.');
        }else{
            return back()->with('success', 'La suma de los montos de los Vehículos vinculados supera el monto del <b>Ingreso general</b>, Verifique y modifique el monto .');
        }
    }
    public function editaIngresosxVehiculos($id,$empresa_id)
    {
        $ingresoxvehiculo = Ingresoxvehiculo::find($id);
        return view('ingresoxvehiculo.edit', compact('ingresoxvehiculo','empresa_id','id'));
    }

    public function updateIngresoxVehiculo(Request $request, Ingresoxvehiculo $ingresoxvehiculo, $id, $empresa_id)
    {   
        request()->validate(Ingresoxvehiculo::$rules);
        $ingresoxvehiculo = Ingresoxvehiculo::where('id','=', $id)
              ->update(['monto' => $request->monto,'vehiculo_id' => $request->vehiculo_id]);
        //$gastoxvehiculo->update();
        //$gastoxvehiculo->update($request->all());
        
        return redirect()->action('IngresoxvehiculoController@editaIngresosxVehiculos', [$id,$empresa_id])->with('success', 'Vinculo de Vehículo a ingreso modificado con Exito.');
    }

    public function destroyIngresoxVehiculo($id,$ingreso_id,$empresa_id)
    {
        $ingresoxvehiculo = Ingresoxvehiculo::find($id)->delete();
        return redirect()
    ->action('IngresoxvehiculoController@muestraIngresosVehiculos', [$ingreso_id,$empresa_id])
    ->with('success', 'Vinculo de Vehículo a ingreso Eliminado con Exito.');
    }

}
