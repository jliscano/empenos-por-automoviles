<?php

namespace App\Http\Controllers;

use App\Models\Vehiculo;
use App\Models\Cliente;
use App\Models\Prestamo;
use App\Models\Impuesto;
use App\Models\Marca;
use App\Models\Multa;
use App\Models\Modelo;
use App\Models\Empseguro;
use App\Models\Cobertura;
use App\Models\Condicionvehiculo;
use App\Models\Documento;
use App\Models\Gastoxvehiculo;
use App\Models\Ingresoxvehiculo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * Class VehiculoController
 * @package App\Http\Controllers
 */
class VehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request,$idcliente,$retorno){

        $texto=$request->texto;
        if($texto!=''){
            $vehiculos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->select('vehiculos.*','vehiculos.id as idveh','clientes.nombre','clientes.dni','marcas.nombre as mar','modelos.nombre as mod','vehiculos.matricula','clientes.id as idcliente')
            ->where('clientes.dni','like','%'.$texto.'%')
            ->orwhere('clientes.nombre','like','%'.$texto.'%')
            ->orwhere('vehiculos.matricula','like','%'.$texto.'%')
            ->where('status','=','0') //en curso
        ->paginate();
        }elseif($idcliente>0){
            $vehiculos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->select('vehiculos.*','vehiculos.id as idveh','clientes.nombre','clientes.dni','marcas.nombre as mar','modelos.nombre as mod','vehiculos.matricula','clientes.id as idcliente')
            ->where('clientes.id','=',$idcliente)
            ->where('status','=','0') //en curso
        ->paginate();
        }else{
            $vehiculos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->select('vehiculos.*','vehiculos.id as idveh','clientes.nombre','clientes.dni','marcas.nombre as mar','modelos.nombre as mod','vehiculos.matricula','clientes.id as idcliente')
            ->where('status','=','0') //en curso
            ->paginate();

        }
        

        return view('vehiculo.index', compact('vehiculos','retorno','idcliente'))
            ->with('i', (request()->input('page', 1) - 1) * $vehiculos->perPage());
    }

    public function listavehiculos(Request $request,$idcliente,$retorno)
    {
        $texto=$request->texto;
        if($texto!=''){
            $vehiculos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->select('vehiculos.*','vehiculos.id as idveh','clientes.nombre','clientes.dni','marcas.nombre as mar','modelos.nombre as mod','vehiculos.matricula','clientes.id as idcliente')
            ->where('clientes.dni','like','%'.$texto.'%')
            ->orwhere('clientes.nombre','like','%'.$texto.'%')
            ->orwhere('vehiculos.matricula','like','%'.$texto.'%')
            //->where('status','=','0') //en curso
        ->paginate();
        }elseif($idcliente>0){
            $vehiculos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->select('vehiculos.*','vehiculos.id as idveh','clientes.nombre','clientes.dni','marcas.nombre as mar','modelos.nombre as mod','vehiculos.matricula','clientes.id as idcliente')
            ->where('clientes.id','=',$idcliente)
            //->where('status','=','0') //en curso
        ->paginate();
        }else{
            $vehiculos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->select('vehiculos.*','vehiculos.id as idveh','clientes.nombre','clientes.dni','marcas.nombre as mar','modelos.nombre as mod','vehiculos.matricula','clientes.id as idcliente')
            //->where('status','=','0') //en curso
            ->paginate();

        }

        return view('vehiculo.index', compact('vehiculos','retorno','idcliente'))
            ->with('i', (request()->input('page', 1) - 1) * $vehiculos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehiculo = new Vehiculo();
        return view('vehiculo.create', compact('vehiculo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Vehiculo::$rules);

        $vehiculo = Vehiculo::create($request->all());
        $id=$vehiculo->id;
        $retorno=$request->retorno;
        return redirect()
        ->action('VehiculoController@editavehiculo', [$id,$retorno])
        ->with('success', 'Vehiculo creado con Exito.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehiculo = Vehiculo::find($id);

        return view('vehiculo.show', compact('vehiculo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehiculo = Vehiculo::find($id);
        $marca=Marca::all()->sortBy('nombre')->pluck('nombre','id');
        $modelo=Modelo::where('marca_id','=',$vehiculo->marca_id)->pluck('nombre','id');

        $tiposeguro = Cobertura::all()->pluck('nombre','id');
        $empseguro = Empseguro::all()->pluck('nombre','id');
        
        $frecuencia=['Semanal','Mensual', 'Trimestral', 'Semestral','Anual'];
        $situacion= Condicionvehiculo::all()->pluck('nombre','id');

        return view('vehiculo.edit', compact('vehiculo','marca','modelo','empseguro','tiposeguro','frecuencia','situacion'));
    
    }

    public function editavehiculo(Request $request,$id,$retorno)
    {
        
        //if(!isset($retorno))

        /*if($totalVeh<=1){
            */
            
            $vehiculo = Vehiculo::find($id);

            $prestamo=Prestamo::where('vehiculo_id','=',$id)
        ->select(DB::raw('count(*) as tpresta'))
        ->get();
            $prestamo_id=0;
            foreach ($prestamo as $pre) {
                $prestamo_id=$pre->tpresta;
            }

            $marca=Marca::all()->sortBy('nombre')->pluck('nombre','id');
            
            $modelo=Modelo::where('marca_id','=',$vehiculo->marca_id)->pluck('nombre','id');

            $tiposeguro = Cobertura::all()->pluck('nombre','id');
            $empseguro = Empseguro::all()->pluck('nombre','id');
            
            $frecuencia=['Semanal','Mensual', 'Trimestral', 'Semestral','Anual'];
            $situacion= Condicionvehiculo::all()->pluck('nombre','id');
            $tcombustible=['Diesel'=>'Diesel','Gasolina'=>'Gasolina','Hidrógeno'=>'Hidrógeno','Biodiesel'=>'Biodiesel','Eléctrico'=>'Eléctrico','Híbrido'=>'Híbrido','GLP'=>'GLP'];
            return view('vehiculo.edit', compact('vehiculo','marca','modelo','empseguro','tiposeguro','frecuencia','situacion','retorno','prestamo_id','tcombustible'));
            /*
        }else{
            $retorno=1;
            $idcliente=$id;
            return redirect()
        ->action('VehiculoController@listavehiculos', [$idcliente,$retorno]);
        }*/
    
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Vehiculo $vehiculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehiculo $vehiculo)
    {
        request()->validate(Vehiculo::$rules);
        $vehiculo->update($request->all());
        $id=$vehiculo->id;
        $retorno=$request->retorno;
        return redirect()
        ->action('VehiculoController@editavehiculo', [$id,$retorno])
        ->with('success', 'Vehiculo modificado con Exito.');
        
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request,$id)
    {
        $idcliente=0;
        $retorno=$request->retorno;

        if($retorno!=0){
            $vehiculo=Vehiculo::find($id);
            $idcliente=$vehiculo->cliente_id;
        }
        
        $gastos=Gastoxvehiculo::where('gastoxvehiculos.vehiculo_id','=', $id)->selectRaw('sum(monto) as tgastos')->get();
        $multas=Multa::where('multas.vehiculo_id','=', $id)->where('multas.cliente_id','=', $idcliente)->selectRaw('sum(monto) as total')->get();
        $ingresos=Ingresoxvehiculo::where('ingresoxvehiculos.vehiculo_id','=', $id)->selectRaw('sum(monto) as tingresos')->get();
        $empenos=Prestamo::where('prestamos.vehiculo_id','=', $id)->selectRaw('sum(importe) as tempenos')->get();
        $impuestos=Impuesto::where('impuestos.vehiculo_id','=', $id)->selectRaw('sum(monto) as total')->get();
        $documentos=Documento::where('documentos.vehiculo_id','=', $id)->selectRaw('count(*) as tdocumentos')->get();
        $tgastos=0;$tingresos=0;$tempenos=0;$tmultas=0;$timpuestos=0;$tdocumentos=0;
        
        foreach($gastos as $gas){
            $tgastos=$gas->tgastos;
        }
        foreach($ingresos as $ing){
            $tingresos=$ing->tingresos;
        }
        foreach($empenos as $emp){
            $tempenos=$emp->tempenos;
        }
        foreach($multas as $mul){
            $tmultas=$mul->total;
        }
        foreach($impuestos as $imp){
            $timpuestos=$imp->total;
        }
        foreach($documentos as $doc){
            $tdocumentos=$doc->tdocumentos;
        }
        $msj='No se puede eliminar el vehículo porque';
        if($tgastos>0){
            $msj.='- tiene gastos asociados.';
        }
        if($tingresos>0){
            $msj.='- tiene Ingresos asociados.';
        }
        if($tempenos>0){
            $msj.='- tiene Empeños asociados.';
        }
        if($tmultas>0){
            $msj.='- tiene Multas/Gastos de Rec. asociados.';
        }
        if($timpuestos>0){
            $msj.='- tiene Impuestos asociados.';
        }
        if($tdocumentos>0){
            $msj.='- tiene Documentos asociados.';
        }
        if($tingresos==0 and $tgastos==0 and $tempenos==0 and $tmultas==0 and $timpuestos==0 and $tdocumentos==0){
            $vehiculo = Vehiculo::find($id)->delete();
            $msj='Vehiculo eliminado con Exito';
        }
        
        return redirect()
        ->action('VehiculoController@listavehiculos', [$idcliente,$retorno])->with('success', $msj);

            
    }
     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function createcliente(Request $request, $id,$retorno)
    {
        
        /*
        $tvehiculosXcliente=Vehiculo::where('cliente_id','=',$id)
        ->select(DB::raw('count(*) as veh_count'))
        ->get();
        $totalVeh=0;
        foreach($tvehiculosXcliente as $tveh) {
            $totalVeh=$tveh->veh_count;
        }
        if($totalVeh<1){
        */
            $vehiculo = new Vehiculo();
            //$retorno=$request->retorno;
            $vehiculo->cliente_id=$id;
            $prestamo=Prestamo::where('prestamos.cliente_id','=',$id);
            $prestamo_id=0;
            foreach ($prestamo as $pre) {
                $prestamo_id=$pre->id;
            }
            $vehiculo->situacion_id=2;//en circulacion
            $vehiculo->nano=date('Y');
            $vehiculo->fcompra='1990-01-01';
            $vehiculo->cuotaunica=0;
            $marca=Marca::all()->sortBy('nombre')->pluck('nombre','id');
            $tiposeguro = Cobertura::all()->pluck('nombre','id');
            $modelo=['Seleccione una Marca...'];
            $empseguro = Empseguro::all()->pluck('nombre','id');
            
            $frecuencia=['Semanal','Mensual', 'Trimestral', 'Semestral','Anual'];
            $situacion= Condicionvehiculo::all()->pluck('nombre','id');
            $tcombustible=['Diesel'=>'Diesel','Gasolina'=>'Gasolina','Hidrógeno'=>'Hidrógeno','Biodiesel'=>'Biodiesel','Eléctrico'=>'Eléctrico','Híbrido'=>'Híbrido','GLP'=>'GLP'];

            return view('vehiculo.create', compact('vehiculo','marca','modelo','empseguro','tiposeguro','frecuencia','situacion','retorno','prestamo_id','tcombustible'));
        /*}else{
            $retorno=1;
            return redirect()
        ->action('VehiculoController@listavehiculos', [$id,$retorno]);

        }*/
    }
    public function modelosxmarcas($id,$marca_id){
        $subCate = Modelo::where('marca_id','=',$marca_id)
        ->orderby('nombre')
        ->get();
        return with(["subCate" => $subCate]);

    }
    public function modelosxmarcas2($id, $marca_id){
        $cMuni = Modelo::where('marca_id','=',$marca_id)
        ->orderby('nombre')
        ->get();
        return with(["cMuni" => $cMuni]);
    }
}
