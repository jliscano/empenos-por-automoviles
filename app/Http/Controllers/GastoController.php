<?php

namespace App\Http\Controllers;

use App\Models\Gasto;
use App\Models\Cliente;
use App\Models\Empresa;
use App\Models\Clasificado;
use App\Models\Gastoxvehiculo;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/**
 * Class GastoController
 * @package App\Http\Controllers
 */
class GastoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->startDate!='' or $request->idclase!=''){
            $data=explode('/', $request->startDate);
            $gastos = Gasto::join('empresas','gastos.empresa_id','=','empresas.id')
        ->leftjoin('clasificados','gastos.clasificado_id','=','clasificados.id')
        ->select('empresas.nombre as emp','gastos.*','clasificados.nombre as clas');
        if($request->startDate!=''){
            $gastos=$gastos->whereYear('fgasto','=', $data[1])
            ->whereMonth('fgasto','=', $data[0]);
        }
        if($request->idclase!=''){
            $gastos=$gastos->where('gastos.clasificado_id','=', $request->idclase);
        }
        $gastos=$gastos->paginate();
        }else{
            $gastos = Gasto::join('empresas','gastos.empresa_id','=','empresas.id')
        ->leftjoin('clasificados','gastos.clasificado_id','=','clasificados.id')
        ->select('empresas.nombre as emp','gastos.*','clasificados.nombre as clas')
        ->paginate();
        }
        
        $clasificados=Clasificado::all()->where('tipo','=','0')->pluck('nombre','id');
        return view('gasto.index', compact('gastos','clasificados'))
            ->with('i', (request()->input('page', 1) - 1) * $gastos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $gasto = new Gasto();
        //$gasto->empresa_id=1;
        $empresa=Empresa::all()->pluck('nombre','id');
        $clasificado=Clasificado::all()->where('tipo','=',0)->pluck('nombre','id');
        $sino=array('No','Si');
        return view('gasto.create', compact('gasto','empresa','clasificado','sino'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Gasto::$rules);
        $gasto = Gasto::create($request->all());
        $id=$gasto->id;
        return redirect()
        ->action('GastoController@edit', $id)
        ->with('success', 'Gasto creado con Exito.');

        //return redirect()->route('gastos.index')->with('success', 'Gasto creada con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $gasto = Gasto::find($id);

        return view('gasto.show', compact('gasto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gasto=Gasto::find($id);
        $empresa=Empresa::all()->pluck('nombre','id');
        $clasificado=Clasificado::all()->where('tipo','=',0)->pluck('nombre','id');
        $sino=array('No','Si');
        return view('gasto.edit', compact('gasto','empresa','clasificado','sino'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Gasto $gasto
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, Gasto $gasto)
    {
        request()->validate(Gasto::$rules);
        $empresa_id=$request->empresa_id;
        $id=$gasto->id;
        //echo $empresa_id."   ".$id.'<br>';
        $gastoxvehiculos=Gastoxvehiculo::rightjoin('vehiculos','vehiculos.id','=','gastoxvehiculos.vehiculo_id')
        ->rightjoin('clientes','clientes.id','=','vehiculos.cliente_id')
        ->select(DB::Raw('coalesce(count(*),0) as total_vehiculos'))
        ->where('gastoxvehiculos.gasto_id','=',$id)
        ->where('clientes.empresa_id','<>',$empresa_id)
        //->  groupBy('gastoxvehiculos.gasto_id')
        ->get(); 
        
        
        //$total_vehiculos=0;
        foreach ($gastoxvehiculos as $gastoxvehiculo) {
            $total_vehiculos=$gastoxvehiculo->total_vehiculos;
        }
        $msj='';
        //echo $total_vehiculos;
        //exit();
        if($total_vehiculos==0){

            $gasto->update($request->all());
            $msj='Gasto modificado con Exito.';
            $succes='success';
        }else{
            $msj='No se puede cambiar la empresa, por que tiene vehículos vinculados!!!.';
            $succes='warmning';
        }
        return redirect()
        ->action('GastoController@edit', $id)
        ->with($succes, $msj);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $gastoxvehiculos=Gastoxvehiculo::where('gastoxvehiculos.gasto_id','=',$id)->selectRaw('count(*) as total')->get();
        $tgastoxvehiculos=0;
        foreach ($gastoxvehiculos as $tgasto) {
            $tgastoxvehiculos=$tgasto->total;
        }
        if($tgastoxvehiculos==0){
            $gasto = Gasto::find($id)->delete();
            $smj='Gasto eliminado con Exito';
        }else{
            $smj='No se puede eliminar porque tiene Vehículos asociados';
        }

        return redirect()->route('gastos.index')
            ->with('success', $smj);
    }
}
