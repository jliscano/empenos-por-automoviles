<?php

namespace App\Http\Controllers;

use App\Models\Cobertura;
use Illuminate\Http\Request;

/**
 * Class CoberturaController
 * @package App\Http\Controllers
 */
class CoberturaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $coberturas = Cobertura::paginate();

        return view('cobertura.index', compact('coberturas'))
            ->with('i', (request()->input('page', 1) - 1) * $coberturas->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cobertura = new Cobertura();
        return view('cobertura.create', compact('cobertura'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Cobertura::$rules);

        $cobertura = Cobertura::create($request->all());

        return redirect()->route('coberturas.index')
            ->with('success', 'Cobertura created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cobertura = Cobertura::find($id);

        return view('cobertura.show', compact('cobertura'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cobertura = Cobertura::find($id);

        return view('cobertura.edit', compact('cobertura'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Cobertura $cobertura
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Cobertura $cobertura)
    {
        request()->validate(Cobertura::$rules);

        $cobertura->update($request->all());

        return redirect()->route('coberturas.index')
            ->with('success', 'Cobertura updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $cobertura = Cobertura::find($id)->delete();

        return redirect()->route('coberturas.index')
            ->with('success', 'Cobertura deleted successfully');
    }
}
