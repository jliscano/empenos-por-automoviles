<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Models\Cliente;
use App\Models\Cobranza;
use Mail;
use App\Mail\EnviaMail;
use App\Mail\EnviaMailitv;
use App\Mail\EnviaMailCobranza;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail as FacadesMail;

class MailController extends Controller
{
    /**
     * Write code on Method
     *
     * @return response()
     */
    public function index()
    {
        $mailData = [
            'title' => 'Mail from ItSolutionStuff.com',
            'body' => 'This is for testing email using smtp.'
        ];
         
        Mail::to('your_email@gmail.com')->send(new DemoMail($mailData));
           
        dd("el(los) correos fueron enviados.");
    }

    public function enviaMailPoliza(Request $request){

        $data=explode('/', $request->startDate);
        $mes=$data[0];
        $nano=$data[1];
        //echo $mes.'/'.$nano;exit();
        $clientes = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->select('clientes.*','marcas.nombre as mar','modelos.nombre as mod','vehiculos.*')
        ->whereYear('fvencseguro','=', $nano)
        ->whereMonth('fvencseguro','<=', $mes)
        ->orwhereYear('fvencseguro','<', $nano)
        ->get();
        
        foreach ($clientes as $cliente) {
            $mailData = [
            'title' => 'Grupo Confiacar',
            'body' => 'Aviso de vencimiento de la Póliza de Seguro.'
        ];
         
        Mail::to($cliente->email)->send(new EnviaMail($mailData));
        }
        $data=array($mes,$nano);
        //dd("Email is sent successfully.");

        return redirect()->action('PolizaController@index')->with('success', 'Correos enviado con Exito!!!.');
    }
    public function enviaMailitv(Request $request){

        $data=explode('/', $request->startDate);
        $mes=$data[0];
        $nano=$data[1];
        //echo $mes.'/'.$nano;exit();
        $clientes = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->select('clientes.*','marcas.nombre as mar','modelos.nombre as mod','vehiculos.*')
        ->whereYear('itv','=', $nano)
        ->whereMonth('itv','<=', $mes)
        ->orwhereYear('itv','<', $nano)
        ->get();
        
        foreach ($clientes as $cliente) {
            $mailData = [
            'title' => 'Grupo Confiacar',
            'body' => 'Aviso de vencimiento del ITV .'
        ];
         
        Mail::to($cliente->email)->send(new EnviaMailitv($mailData));
        }
        $data=array($mes,$nano);
        //dd("Email is sent successfully.");

        return redirect()->action('ItvController@index')->with('success', 'Correos enviado con Exito!!!.');
    }
    public function enviaMailCobranza(){
        $cobranzas = Cliente::leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->leftjoin('pagos','pagos.prestamo_id','=','prestamos.id')
        ->rightjoin('vehiculos','vehiculos.id','=','prestamos.vehiculo_id')
        ->where('clientes.id','<>','Null')
        //->where('prestamos.condicion_id','=',1)
        ->select('clientes.id','clientes.nombre','prestamos.id as pre','clientes.dni','clientes.telefono','clientes.email','prestamos.ffirma',
        'prestamos.tipo','prestamos.finiciopago','prestamos.importe','prestamos.tcuota','prestamos.residual'
        ,DB::raw("(SELECT coalesce(SUM(multas.monto),0) FROM multas
            WHERE multas.cliente_id = clientes.id and motivo='0' and pagado='0'
                    ) as tmultas"),
                DB::raw("(SELECT coalesce(SUM(multas.monto),0) FROM multas
                WHERE multas.cliente_id = clientes.id and motivo='1' and pagado='0'
                    ) as tgastos"),
            
            DB::raw("(SELECT coalesce(SUM(pagos.monto),0) FROM pagos
            WHERE pagos.prestamo_id = prestamos.id  and conciliado='1'
                ) as pago_cuota"),
            DB::raw("(SELECT coalesce(SUM(gastoxvehiculos.monto),0) FROM gastoxvehiculos right join gastos on(gastoxvehiculos.gasto_id=gastos.id)
            WHERE gastoxvehiculos.vehiculo_id = vehiculos.id and gastos.conciliado='1'
            ) as tgasto1"),
            DB::Raw(
                "(SELECT max(pagos.fpago) as ultima FROM pagos WHERE pagos.conciliado='1' and pagos.prestamo_id=prestamos.id) AS ultima"),
            DB::Raw(
             "(SELECT coalesce(SUM(pagos.monto),0) FROM pagos WHERE motivo='1' and pagos.conciliado='1') AS tpagosempeno"),
            DB::raw('0 as muestra','0 as total','0 as impagos','0 as timpagos','0 as cuantospagos')

        )
        //->selectRaw("max(pagos.fpago) as ultima")
        ->groupBy('clientes.id','clientes.nombre','prestamos.id','vehiculos.id','clientes.dni','clientes.telefono','clientes.email','prestamos.ffirma','prestamos.finiciopago','prestamos.importe','prestamos.tcuota','prestamos.residual','prestamos.tipo')
        ->paginate();
        

        foreach ($cobranzas as $cobranza) {
            $cuantospagos=0;
            //amortizando
            if($cobranza->tipo==0){
            $cobranza->impagos=calculaImpago($cobranza->pago_cuota,$cobranza->tcuota,$cobranza->ffirma,$cuantospagos,$cobranza->finiciopago,$cobranza->residual);
            $cobranza->timpagos=$cobranza->impagos*$cobranza->tcuota;
            $pagot=$cobranza->pago_cuota;
            
            $cobranza->total=($cobranza->tgastos+$cobranza->tgasto1+$cobranza->tmultas)+
                ($cuantospagos-$pagot);
            }else{ //empeño tradicional
                $cobranza->impagos=calculaImpago($cobranza->pago_cuota,$cobranza->tcuota,$cobranza->ffirma,$cuantospagos,$cobranza->finiciopago,$cobranza->residual);
                if($cobranza->tpagosempeno==0)
                    $cobranza->total=$cobranza->importe+$cobranza->tgastos+$cobranza->tgasto1+$cobranza->tmultas;
                else
                    $cobranza->total=$cobranza->tgastos+$cobranza->tgasto1+$cobranza->tmultas;

            }
            $mailData = [
                'title' => 'Grupo Confiacar',
                'body' => 'Aviso de impagos. por la cantidad de:'.$cobranza->total
                ];
            if($cobranza->total>0){
                Mail::to($cobranza->email)->send(new EnviaMailCobranza($mailData));
            }
        }   
        return redirect()
        ->action('CobranzaController@impagos')
        ->with('success', 'Correos enviado con Exito!!!.');

    }

    public function enviaMailCobranzaUno($id,$motivo){
        $cobranzas = Cliente::where('clientes.id','=',$id)
        ->select('clientes.id','clientes.nombre','clientes.dni','clientes.telefono','clientes.email')
        ->get();
    
        foreach ($cobranzas as $cobranza) {
            $mailData = [
                'title' => 'Grupo Confiacar',
                'body' => 'Aviso de impagos. '
                ];
                $correo=Mail::to($cobranza->email)->send(new EnviaMailCobranza($mailData));
        }
        if($correo){
            
            $cobro=new Cobranza();

            $data=array(
            'cliente_id' => $id,
            'motivo'=> $motivo,
            'fcobranza'=>date('Y-m-d'),
            'observacion'=>'Email enviado al cliente',
            );
            $cobro->create($data);

            return redirect()
            ->action('CobranzaController@muestraCobranza',compact('id','motivo'))
            ->with('success', 'Correos enviado con Exito!!!.');    
        }else{
            return redirect()
            ->action('CobranzaController@muestraCobranza',compact('id','motivo'))
            ->with('warning', 'Hubo un problema para enviar el correo!!!');    
        }


    }
}
