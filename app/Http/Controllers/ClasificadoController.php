<?php

namespace App\Http\Controllers;

use App\Models\Clasificado;
use App\Models\Gasto;
use App\Models\Ingreso;
use Illuminate\Http\Request;

/**
 * Class ClasificadoController
 * @package App\Http\Controllers
 */
class ClasificadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clasificados = Clasificado::paginate();

        return view('clasificado.index', compact('clasificados'))
            ->with('i', (request()->input('page', 1) - 1) * $clasificados->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $clasificado = new Clasificado();
        $tipos=array('Gastos','Ingresos');
        return view('clasificado.create', compact('clasificado','tipos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Clasificado::$rules);

        $clasificado = Clasificado::create($request->all());

        return redirect()->route('clasificados.index')
            ->with('success', 'Clasificado creado con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $clasificado = Clasificado::find($id);

        return view('clasificado.show', compact('clasificado'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $clasificado = Clasificado::find($id);
        $tipos=array('Gastos','Ingresos');
        return view('clasificado.edit', compact('clasificado','tipos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Clasificado $clasificado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Clasificado $clasificado)
    {
        request()->validate(Clasificado::$rules);

        $clasificado->update($request->all());

        return redirect()->route('clasificados.index')
            ->with('success', 'Clasificado modificado con Exito');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $gastos=Gasto::where('gastos.clasificado_id','=',$id)->selectRaw('count(*) as total')->get();
        $tgastos=0;
        foreach ($gastos as $gas) {
            $tgastos=$gas->total;
        }
        $ingresos=Ingreso::where('ingresos.clasificado_id','=',$id)->selectRaw('count(*) as total')->get();
        $tingresos=0;
        foreach ($ingresos as $gas) {
            $tingresos=$gas->total;
        }
        if($tgastos==0 and $tingresos==0){
            $clasificado = Clasificado::find($id)->delete();
            $smj='Clasificado eliminado con Exito';
        }elseif($tgastos>0){
            $smj='No se puede eliminar porque tiene Gastos asociados';
        }elseif($tingresos>0){
            $smj='No se puede eliminar porque tiene Ingresos asociados';
        }
        return redirect()->route('clasificados.index')
            ->with('success', $smj);
    }
}
