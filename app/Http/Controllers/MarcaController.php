<?php

namespace App\Http\Controllers;

use App\Models\Marca;
use App\Models\Modelo;
use App\Models\Vehiculo;
use Illuminate\Http\Request;

/**
 * Class MarcaController
 * @package App\Http\Controllers
 */
class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto=$request->texto;

        $marcas = Marca::orderBy('nombre', 'asc')
        ->where('nombre','like',''.$texto.'%')
        ->paginate();
        return view('marca.index', compact('marcas'))
            ->with('i', (request()->input('page', 1) - 1) * $marcas->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marca = new Marca();
        return view('marca.create', compact('marca'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Marca::$rules);

        $marca = Marca::create($request->all());
        $id=$marca->id;

        return redirect()->route('marcas.edit',$id)
            ->with('success', 'Marca creada con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $marca = Marca::find($id);

        return view('marca.show', compact('marca'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marca = Marca::find($id);
        $modelos=Modelo::select('id','nombre')
        ->where('modelos.marca_id','=',$id)
        ->paginate();
        return view('marca.edit', compact('marca','modelos'))
        ->with('i', (request()->input('page', 1) - 1) * $modelos->perPage());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Marca $marca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marca $marca)
    {
        request()->validate(Marca::$rules);

        $marca->update($request->all());

        return redirect()->route('marcas.index')
            ->with('success', 'Marca modificada con Exito');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {

        $modelos=Modelo::where('modelos.marca_id','=',$id)->selectRaw('count(*) as tmodelos')->get();
        $tmodelos=0;

        foreach($modelos as $mod){
            $tmodelos=$mod->tmodelos;
        }
        $vehiculos=Vehiculo::where('vehiculos.marca_id','=',$id)->selectRaw('count(*) as tveh')->get();
        $tvehiculos=0;

        foreach($vehiculos as $tveh){
            $tvehiculos=$tveh->tveh;
        }
        if($tmodelos==0 and $tvehiculos==0 ){
            $marca = Marca::find($id)->delete();
            $smj='Marca eliminada con Exito';
        }elseif($tmodelos>0){
            $smj='Por favor elimine todos los modelos de esta marca';
        }elseif($tvehiculos>0){
            $smj='No se puede eliminar porque tiene Vehículos asociados';
        }
        return redirect()->route('marcas.index')
            ->with('success', $smj);
    }
}
