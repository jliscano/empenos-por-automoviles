<?php

namespace App\Http\Controllers;

use App\Models\Prestamo;
use App\Models\Condicione;
use App\Models\Cliente;
use App\Models\Extensione;
use App\Models\Vehiculo;
use App\Models\Pago;
use Faker\Extension\Extension;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
Use Illuminate\Database\Eloquent\Builder;
use PDF;

use function PHPUnit\Framework\isNull;

/**
 * Class PrestamoController
 * @package App\Http\Controllers
 */
class PrestamoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $texto=$request->texto;
        $tipo=['Amortizando','Empeño Tradicional'];
        if($texto!=''){
            $prestamos = Cliente::leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
            ->select('prestamos.*','clientes.nombre','clientes.dni','clientes.id as idcliente','prestamos.id as pres')
            ->where('clientes.dni','like','%'.$texto.'%')
            ->orwhere('clientes.nombre','like','%'.$texto.'%')
            //->orwhere('vehiculos.matricula','like','%'.$texto.'%')
            //->where('status','=','0') //en curso
            ->paginate();
        }else{
            $prestamos = Cliente::leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
            ->select('prestamos.*','clientes.nombre','clientes.dni','clientes.id as idcliente','prestamos.id as pres')
            //->orwhere('vehiculos.matricula','like','%'.$texto.'%')
            //->where('status','=','0') //en curso
            ->paginate();
        }


        return view('prestamo.index', compact('prestamos','tipo'))
            ->with('i', (request()->input('page', 1) - 1) * $prestamos->perPage());
    }
    public function listaprestamos(Request $request,$cliente_id,$vehiculo_id,$retorno)
    {

        $texto=$request->texto;
        $tipo=['Amortizando','Empeño Tradicional'];
        if($texto!=''){
            $prestamos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->select('prestamos.*','clientes.nombre','clientes.dni','clientes.id as idcliente','prestamos.id as pres','vehiculos.matricula','vehiculos.id as idveh','modelos.nombre as mod','marcas.nombre as mar')
            ->where('prestamos.extension','=','N')
            ->Where(function (Builder $query) use($texto) {
                $query->where('clientes.dni','like','%'.$texto.'%');
                $query->orwhere('clientes.nombre','like','%'.$texto.'%');
            })
            //->orwhere('vehiculos.matricula','like','%'.$texto.'%')
            //->where('status','=','0') //en curso
            ->paginate();
        }elseif($vehiculo_id>0){
            $prestamos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->select('prestamos.*','clientes.nombre','clientes.dni','clientes.id as idcliente','prestamos.id as pres','vehiculos.id as idveh','vehiculos.matricula','marcas.nombre as mar','modelos.nombre as mod')
            ->where('prestamos.extension','=','N')
            ->where('prestamos.vehiculo_id','=',$vehiculo_id)
            //->where('status','=','0') //en curso
            ->paginate();
        }else{
            $prestamos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->select('prestamos.*','clientes.nombre','clientes.dni','clientes.id as idcliente','prestamos.id as pres','vehiculos.id as idveh','vehiculos.matricula','marcas.nombre as mar','modelos.nombre as mod')
            ->where('prestamos.extension','=','N')
            //->whereNotNull('prestamos.id')
            //->where('status','=','0') //en curso
            ->paginate();
        }


        return view('prestamo.index', compact('prestamos','tipo','cliente_id','vehiculo_id','retorno'))
            ->with('i', (request()->input('page', 1) - 1) * $prestamos->perPage());
    }
    public function listaprestamoscliente(Request $request,$cliente_id,$vehiculo_id,$retorno)
    {

        $texto=$request->texto;

        $tipo=['A','T'];
        //$cliente_id=$request->cliente_id;
        $busca_vehiculo=0;
        if($texto!=''){
            $prestamos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->leftjoin('condiciones','prestamos.condicion_id','=','condiciones.id')
            ->select('prestamos.*','clientes.nombre','clientes.dni',
            'clientes.id as idcliente','prestamos.id as pres','vehiculos.matricula',
            'vehiculos.id as idveh','modelos.nombre as mod','marcas.nombre as mar'
            ,'vehiculos.matricula','prestamos.extension','condiciones.nombre as condi')
            ->where('prestamos.extension','=','N')
            ->Where(function (Builder $query) use($texto) {
                $query->where('clientes.dni','like','%'.$texto.'%');
                $query->orwhere('clientes.nombre','like','%'.$texto.'%');
            })
            //->orwhere('vehiculos.matricula','like','%'.$texto.'%')
            //->where('status','=','0') //en curso
            ->paginate();
        }elseif($cliente_id>0){
            $busca_vehiculo=1;
            $prestamos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->leftjoin('condiciones','prestamos.condicion_id','=','condiciones.id')
            ->select('prestamos.*','clientes.nombre','clientes.dni',
            'clientes.id as idcliente','prestamos.id as pres',
            'vehiculos.id as idveh','vehiculos.matricula',
            'marcas.nombre as mar','modelos.nombre as mod',
            'prestamos.extension','condiciones.nombre as condi')
            ->where('prestamos.extension','=','N')
            ->where('vehiculos.cliente_id','=',$cliente_id)
            //->where('status','=','0') //en curso
            ->paginate();
            $cliente=Cliente::find($cliente_id);
            $dni=$cliente->dni;
            $nombre=$cliente->nombre;
        }else{
            //$cliente_id=$request->cliente_id;
            $prestamos = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->leftjoin('condiciones','prestamos.condicion_id','=','condiciones.id')
            ->select('prestamos.*','clientes.nombre','clientes.dni','clientes.id as idcliente',
            'prestamos.id as pres','vehiculos.id as idveh','vehiculos.matricula',
            'marcas.nombre as mar','modelos.nombre as mod',
            'prestamos.extension','condiciones.nombre as condi')
            ->where('prestamos.extension','=','N')
            //->whereNotNull('prestamos.id')
                //->where('status','=','0') //en curso
            ->paginate();
            $dni='';
            $nombre='';
        }
        $tvehiculos=0;
        if($busca_vehiculo==1){
            $vehiculos=Vehiculo::selectRaw('count(*) as tveh')
            ->where('vehiculos.cliente_id','=',$cliente_id)
            ->get();
            foreach($vehiculos as $veh){
                $tvehiculos=$veh->tveh;
            }
        }

        return view('prestamo.empenos', compact('prestamos','tipo','cliente_id','retorno','dni','nombre','vehiculo_id','tvehiculos'))
            ->with('i', (request()->input('page', 1) - 1) * $prestamos->perPage());
    }

    public function creaprestamoextension($prestamo_id,$cliente_id,$vehiculo_id,$retorno)
    {
        $cliente=Cliente::find($cliente_id);
        $dni=$cliente->dni;
        $nombre=$cliente->nombre;
        $vehiculo=Vehiculo::where('vehiculos.id','=',$vehiculo_id)
        //$vehiculo=Vehiculo::find($prestamo->vehiculo_id)
        ->rightjoin('marcas','marcas.id','=','vehiculos.marca_id')
        ->rightjoin('modelos','modelos.id','=','vehiculos.modelo_id')
        ->select('marcas.nombre as mar','vehiculos.matricula as matricula','vehiculos.cliente_id','modelos.nombre as mod')->get();
        $marca='--';$modelo='--';$matricula='--';
        foreach($vehiculo as $veh){
            $marca=$veh->mar;
            $modelo=$veh->mod;
            $matricula=$veh->matricula;

        }

        $prestamo = new Prestamo();
        $prestamo->cliente_id=$cliente_id;
        $prestamo->vehiculo_id=$vehiculo_id;
        $prestamo->padre=$prestamo_id;
        $prestamo->extension='S';

        $vehiculo=Vehiculo::where('vehiculo.cliente_id','=',$cliente_id);
        $vehiculo_id=0;
        foreach ($vehiculo as $veh) {
            $vehiculo_id=$veh->id;
        }
        $prestamo->condicion_id=1;//en vigor
        $tipo=['Amortizando','Empeño Tradicional'];

        $ncuota=array();
        for ($i=0; $i<=24 ; $i++) {
            // code...
            $ncuota[$i]=$i;
        }
        //print_r($ncuota);exit();
        //parametrizar
        $prestamo->piva=21;
        $frecuencia=[52=>'Semanal',12=>'Mensual', 4=>'Trimestral', 2=>'Semestral',1=>'Anual'];
        $prestamo->frecuencia='12';
        $situacion= Condicione::all()->pluck('nombre','id');

        $extensiones = Prestamo::where('prestamos.padre','=',0)
        ->get();

        $nextensiones=0;
        return view('prestamo.create', compact('prestamo','tipo','ncuota','frecuencia','situacion','retorno','vehiculo_id','matricula','dni','marca','modelo','nombre','extensiones','nextensiones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
/*     public function create()
    {
        $prestamo = new Prestamo();
        return view('prestamo.create', compact('prestamo'));
    }
 */
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if(!isset($request->porcentaje) or isNull($request->porcentaje) or $request->porcentaje=='NaN')
            $request->porcentaje=0;
        request()->validate(Prestamo::$rules);
        $prestamo = Prestamo::create($request->all());
        $id=$prestamo->id;
        $retorno=$request->retorno;
        if($retorno==1){
            return redirect()
            ->action('PrestamoController@editaprestamo', [$id,$retorno])
            ->with('success', 'Empeño modificado con Exito.');
        }else{
            return redirect()
            ->action('PrestamoController@editaprestamo', [$id,$retorno])
            ->with('success', 'Empeño Creado con Exito.');
        }

        //return redirect()->action('PrestamoController@edit', $id)->with('success', 'Empeño creado con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $prestamo = Prestamo::find($id);

        return view('prestamo.show', compact('prestamo'));
    }
    public function createPDF($id) {
      // retreive all records from db
      $data = Prestamo::find($id);
      $view = \view::make('repamortiza',compact('data'))->render();
      $pdf = \App::make('dompdf.wrapper');
      $pdf = LoadHTML($view);
      return $pdf->stream('informe'.('.pdf'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prestamo = Prestamo::find($id);
        $vehiculo=Vehiculo::where('vehiculos.cliente_id','=',$prestamo->cliente_id)->get();
        $vehiculo_id=0;
        foreach ($vehiculo as $veh) {
            $vehiculo_id=$veh->id;
        }
        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=array();
        for ($i=1; $i<=24 ; $i++) {
            // code...
            $ncuota[$i]=$i;
        }
        $retorno=1;
        $frecuencia=[52=>'Semanal',12=>'Mensual', 4=>'Trimestral', 2=>'Semestral',1=>'Anual'];

        $situacion= Condicione::where('status','=','1')->pluck('nombre','id');

        return view('prestamo.edit', compact('prestamo','tipo','ncuota','frecuencia','situacion','retorno','vehiculo_id'));
    }
    public function editaprestamo(Request $request,$id,$retorno)
    {
        $prestamo = Prestamo::find($id);
        $cliente=Cliente::find($prestamo->cliente_id);
        $dni=$cliente->dni;
        $nombre=$cliente->nombre;
        $vehiculo=Vehiculo::where('vehiculos.id','=',$prestamo->vehiculo_id)
        //$vehiculo=Vehiculo::find($prestamo->vehiculo_id)
        ->rightjoin('marcas','marcas.id','=','vehiculos.marca_id')
        ->rightjoin('modelos','modelos.id','=','vehiculos.modelo_id')
        ->select('marcas.nombre as mar','vehiculos.matricula as matricula','vehiculos.cliente_id','modelos.nombre as mod')->get();
        $marca='--';$modelo='--';$matricula='--';
        foreach($vehiculo as $veh){
            $marca=$veh->mar;
            $modelo=$veh->mod;
            $matricula=$veh->matricula;
        }

        //$extensiones = Extensione::where('extensiones.prestamo_id','=',$prestamo->id)
        $extensiones = Prestamo::where('prestamos.padre','=',$prestamo->id)
        ->get();
        $nextensiones=0;
        foreach($extensiones as $extension){
            $nextensiones++;
        }

        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=array();
        for ($i=1; $i<=24 ; $i++) {
            // code...
            $ncuota[$i]=$i;
        }
        //$retorno=2;

        $frecuencia=[52=>'Semanal',12=>'Mensual', 4=>'Trimestral', 2=>'Semestral',1=>'Anual'];
        $situacion= Condicione::all()->pluck('nombre','id');

        return view('prestamo.edit', compact('prestamo','tipo','ncuota','frecuencia','situacion','retorno','matricula','dni','marca','modelo','nombre','extensiones','nextensiones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Prestamo $prestamo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Prestamo $prestamo)
    {
          //  echo $request->porcentaje; exit();//echo $request->interes;exit();
        if(!isset($request->porcentaje) or isNull($request->porcentaje)
        or $request->porcentaje=='NaN' or $request->porcentaje==''
        or !is_numeric($request->porcentaje))
            $request->porcentaje=0;
        request()->validate(Prestamo::$rules);

        $prestamo->update($request->all());
        $id=$prestamo->id;
        $retorno=$request->retorno;
        if($retorno==1){
            return redirect()
            ->action('PrestamoController@edit', $id)
            ->with('success', 'Empeño modificado con Exito.');
        }else{
            return redirect()
            ->action('PrestamoController@editaprestamo', [$id,$retorno])
            ->with('success', 'Empeño modificado con Exito.');
        }

    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Request $request,$id)
    {

        $idcliente=$request->cliente_id;
        $vehiculo_id=$request->vehiculo_id;
        $retorno=$request->retorno;

        if($retorno==1 or $retorno==2 ){
            $prestamo=Prestamo::find($id);
            $vehiculo=Vehiculo::find($prestamo->vehiculo_id);
            $idcliente=$vehiculo->cliente_id;
            $vehiculo_id=$vehiculo->id;
        }else{
            $vehiculo=0;
            $idcliente=0;
            $vehiculo_id=0;
        }

        $pagos=Pago::where('pagos.prestamo_id','=', $id)->selectRaw('sum(monto) as tpagos')->get();
        $tpagos=0;
        foreach ($pagos as $pago) {
            $tpagos=$pago->tpagos;
        }

        if($tpagos>0){
            $msj='No se puede eliminar este Empeño porque tiene pagos asociados';
        }else{
            $prestamo = Prestamo::find($id)->delete();
            $msj='Empeño eliminado con Exito';
        }

        return redirect()
        ->action('PrestamoController@listaprestamoscliente', [$idcliente,$vehiculo_id,$retorno])->with('success', $msj);
    }
    /*public function amortiza($id){
      $prestamos = Prestamo::find($id);
      return view('prestamos.repamortiza', compact('prestamos'));

    }
    */
    public function creaprestamo($id,$vehiculo_id,$retorno){
        $cliente=Cliente::find($id);
        $dni=$cliente->dni;
        $nombre=$cliente->nombre;
        $vehiculo=Vehiculo::where('vehiculos.id','=',$vehiculo_id)
        //$vehiculo=Vehiculo::find($prestamo->vehiculo_id)
        ->rightjoin('marcas','marcas.id','=','vehiculos.marca_id')
        ->rightjoin('modelos','modelos.id','=','vehiculos.modelo_id')
        ->select('marcas.nombre as mar','vehiculos.matricula as matricula','vehiculos.cliente_id','modelos.nombre as mod')->get();
        $marca='--';$modelo='--';$matricula='--';
        foreach($vehiculo as $veh){
            $marca=$veh->mar;
            $modelo=$veh->mod;
            $matricula=$veh->matricula;

        }

        $prestamo = new Prestamo();
        $prestamo->cliente_id=$id;
        $prestamo->vehiculo_id=$vehiculo_id;
        $prestamo->extension='N';
        $prestamo->padre=0;
        $prestamo->porcentaje=0;

        $vehiculo=Vehiculo::where('vehiculo.cliente_id','=',$id);
        $vehiculo_id=0;
        foreach ($vehiculo as $veh) {
            $vehiculo_id=$veh->id;
        }
        $prestamo->condicion_id=1;//en vigor
        $tipo=['Amortizando','Empeño Tradicional'];

        $ncuota=array();
        for ($i=0; $i<=24 ; $i++) {
            // code...
            $ncuota[$i]=$i;
        }
        //print_r($ncuota);exit();
        //parametrizar
        $prestamo->piva=21;
        $frecuencia=[52=>'Semanal',12=>'Mensual', 4=>'Trimestral', 2=>'Semestral',1=>'Anual'];
        $prestamo->frecuencia='12';
        $situacion= Condicione::all()->pluck('nombre','id');

        $extensiones = Prestamo::where('prestamos.padre','=',0)
        ->get();
        $nextensiones=0;
        /*
        foreach($extensiones as $extension){
            $nextensiones++;
        }
        */
        return view('prestamo.create', compact('prestamo','tipo','ncuota','frecuencia','situacion','retorno','vehiculo_id','matricula','dni','marca','modelo','nombre','extensiones','nextensiones'));
    }
    public function borraExtension(Request $request,$id,$padre,$retorno)
    {

        /*$idcliente=$request->cliente_id;
        $vehiculo_id=$request->vehiculo_id;
        $retorno=$request->retorno;
        */

        if($retorno==1){
            $prestamo=Prestamo::find($padre);
            $vehiculo=Vehiculo::find($prestamo->vehiculo_id);
            $idcliente=$vehiculo->cliente_id;
            $vehiculo_id=$vehiculo->id;
        }else{
            $vehiculo=0;
            $idcliente=0;
            $vehiculo_id=0;
        }

        $pagos=Pago::where('pagos.prestamo_id','=', $padre)->selectRaw('sum(monto) as tpagos')->get();
        $tpagos=0;
        foreach ($pagos as $pago) {
            //$tpagos=$pago->tpagos;
        }

        if($tpagos>0){
            $msj='No se puede eliminar este Empeño porque tiene pagos asociados';
        }else{
            $prestamo = Prestamo::find($id)->delete();
            $msj='Extensión eliminada con Exito';
        }

        return redirect()
        ->action('PrestamoController@editaprestamo', [$padre,$retorno])->with('success', $msj);

    }

}
