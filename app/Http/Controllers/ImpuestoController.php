<?php

namespace App\Http\Controllers;

use App\Models\Impuesto;
use App\Models\Vehiculo;
use App\Models\Tipoimpuesto;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;

/**
 * Class ImpuestoController
 * @package App\Http\Controllers
 */
class ImpuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $impuestos = Impuesto::paginate();

        return view('impuesto.index', compact('impuestos'))
            ->with('i', (request()->input('page', 1) - 1) * $impuestos->perPage());
    }
    public function listaImpuestos($vehiculo_id,$retorno)
    {
        //$impuestos = Impuesto::paginate();
        $impuestos = Impuesto::rightjoin('tipoimpuestos','tipoimpuestos.id','=','impuestos.tipoimpuesto_id')
        ->select('impuestos.*','tipoimpuestos.nombre as tipo')
        ->where('vehiculo_id','=',$vehiculo_id)->paginate();
        
        $vehiculos=Vehiculo::where('vehiculos.id','=',$vehiculo_id)
        ->select('vehiculos.*','marcas.nombre as mar','modelos.nombre as mod')
        ->rightjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->rightjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->get();
        $concilia=['No','Si'];
        return view('impuesto.index', compact('impuestos','retorno','vehiculos','vehiculo_id','concilia'))
            ->with('i', (request()->input('page', 1) - 1) * $impuestos->perPage());
        
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $impuesto = new Impuesto();
        return view('impuesto.create', compact('impuesto'));
    }

    public function creaImpuesto($vehiculo_id,$retorno)
    {
        $impuesto = new Impuesto();
        $impuesto->vehiculo_id=$vehiculo_id;
        $impuesto->piva=0;
        $tipo=Tipoimpuesto::all()->pluck('nombre','id');
        $concilia=['No','Si'];

        return view('impuesto.create', compact('impuesto','retorno','tipo','concilia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Impuesto::$rules);

        $impuesto = Impuesto::create($request->all());

        return redirect()->route('impuestos.index')
            ->with('success', 'Impuesto created successfully.');
    }
    public function storeImpuesto(Request $request, $retorno)
    {
        request()->validate(Impuesto::$rules);
        $vehiculo_id=$request->vehiculo_id;
        $impuesto = Impuesto::create($request->all());
        return redirect()
            ->action('ImpuestoController@listaImpuestos', [$vehiculo_id,$retorno])
            ->with('success', 'Registro de Impuesto creado con Exito.');

        
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $impuesto = Impuesto::find($id);

        return view('impuesto.show', compact('impuesto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $impuesto = Impuesto::find($id);

        return view('impuesto.edit', compact('impuesto'));
    }

    public function editaImpuesto($id,$retorno)
    {
        $impuesto = Impuesto::find($id);
        $tipo=Tipoimpuesto::all()->pluck('nombre','id');
        $concilia=['No','Si'];

        return view('impuesto.edit', compact('impuesto','retorno','tipo','concilia'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Impuesto $impuesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Impuesto $impuesto)
    {
        request()->validate(Impuesto::$rules);

        $impuesto->update($request->all());
        $vehiculo_id=$request->vehiculo_id;
        $retorno=$request->retorno;
        return redirect()
            ->action('ImpuestoController@listaImpuestos', [$vehiculo_id,$retorno])
            ->with('success', 'Registro de Impuesto modificado con Exito.');
    }
    public function updateImpuesto(Request $request, Impuesto $impuesto, $id, $retorno)
    {
        request()->validate(Impuesto::$rules);
        $impuesto->update($request->all());
        echo $impuesto->conciliado;exit();  
        
        //echo $request->conciliado;exit();
        return redirect()
            ->action('ImpuestoController@listaImpuestos', [$vehiculo_id,$retorno])
            ->with('success', 'Registro de Impuesto modificado con Exito.');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $impuesto = Impuesto::find($id)->delete();

        return redirect()->route('impuestos.index')
            ->with('success', 'Registro de Impuesto eliminado con Exito.');
    }
    public function borrarImpuesto($id,$vehiculo_id, $retorno)
    {
        $impuesto = Impuesto::find($id)->delete();

        return redirect()
            ->action('ImpuestoController@listaImpuestos', [$vehiculo_id,$retorno])
            ->with('success', 'Registro de Impuesto eliminado con Exito.');
        
    }
}
