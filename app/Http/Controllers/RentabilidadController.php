<?php

namespace App\Http\Controllers;

use App\Models\Prestamo;
use App\Models\Pago;
use App\Models\Cliente;
use App\Models\Vehiculo;
use App\Models\Tipoimpuesto;
use App\Models\Impuesto;
use App\Models\Empresa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
Use Illuminate\Database\Eloquent\Builder;
use PDF;



class RentabilidadController extends Controller
{
    //
    public function muestraRentabilidad(Request $request,$imprimir,$mesesimpago2,$texto2)
    {
            $bandera=0;
            $mes=date('m');
            $ano=date('Y');
            $tipos=array('A','T');
            //$mesesimpago2=$request->mesesimpago2;
            $empresa_id=0;
            if($request->empresa_id!='' and $request->empresa_id>0)
                $empresa_id=$request->empresa_id;
            if($imprimir==0){
                $mesesimpago=-1;
                if($request->mesesimpago!='') {
                    $mesesimpago=$request->mesesimpago;
                }
                $texto=$request->texto;
            }

            if($texto!=''){
                $bandera=1;
                //echo $bandera;exit();
                $cobranzas = Cliente::rightjoin('empresas','empresas.id','=','clientes.empresa_id')
                ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
                ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
                ->where('prestamos.condicion_id','=',1)
                //->where('prestamos.extension','=','N')
                ->whereNotNull('prestamos.id')
                ->Where(function (Builder $query2 ) use ($texto) {
                    $query2->orWhere('clientes.proempresa','like','%'.$texto.'%');
                    $query2->orWhere('clientes.nombre','like','%'.$texto.'%');
                    $query2->orWhere('clientes.dni','like','%'.$texto.'%');
                    $query2->orWhere('vehiculos.matricula','like','%'.$texto.'%');
                })
                ->Where(function (Builder $query3) use ($empresa_id) {
                    if($empresa_id>0)
                        $query3->where('clientes.empresa_id','=',$empresa_id);
                    else
                        $query3->where('clientes.empresa_id','>',$empresa_id);
                })
                ->select('clientes.id','clientes.nombre','prestamos.id as pre','clientes.dni',
                'clientes.telefono','clientes.email','prestamos.ffirma','prestamos.piva',
                'prestamos.tipo','prestamos.finiciopago','prestamos.importe','prestamos.tcuota','prestamos.porcentaje',
                'prestamos.residual','vehiculos.matricula','empresas.nombre as emp',
                'clientes.proempresa','dnicif','cuotas','prestamos.padre',
                DB::raw('coalesce(prestamos.cuotas,0) as cuotas'),
                DB::raw('0 as impagos'),
                DB::raw('0 as pagado'),
                DB::raw('0 as debe'),
                DB::raw('0 as debe_cuotas'),
                DB::raw('0 as monto_extensiones'),
                DB::raw('0 as numero_extensiones'),
                )
                ->orderBy('clientes.nombre','asc')
                ->get();
            }else{
                $cobranzas = Cliente::rightjoin('empresas','empresas.id','=','clientes.empresa_id')
                ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
                ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
                ->where('prestamos.condicion_id','=',1)
                //->where('prestamos.extension','=','N')
               // ->whereNotNull('prestamos.id')
                //->whereMonth('prestamos.finiciopago','<=',$mes)
                /*
                ->Where(function (Builder $query) use($ano) {
                    $query->WhereYear('prestamos.finiciopago','=',$ano);
                    $query->orWhereYear('prestamos.finiciopago','<',$ano);
                })*/
                /*->Where(function (Builder $query3) use ($empresa_id) {
                    if($empresa_id>0)
                        $query3->where('clientes.empresa_id','=',$empresa_id);
                })*/
                ->select('clientes.id','clientes.nombre','prestamos.id as pre','clientes.dni',
                'clientes.telefono','clientes.email','prestamos.ffirma','prestamos.piva',
                'prestamos.tipo','prestamos.finiciopago','prestamos.importe','prestamos.tcuota','prestamos.porcentaje',
                'prestamos.residual','vehiculos.matricula','empresas.nombre as emp',
                'clientes.proempresa','dnicif','cuotas','prestamos.padre',
                DB::raw('coalesce(prestamos.cuotas,0) as cuotas'),
                DB::raw('0 as impagos'),
                DB::raw('0 as pagado'),
                DB::raw('0 as debe'),
                DB::raw('0 as debe_cuotas'),
                DB::raw('0 as monto_extensiones'),
                DB::raw('0 as numero_extensiones'),
                )
                //->orderBy('clientes.nombre','asc')
                ->get();
            }

            $arr_pagos=array();
            $total_empenos=0;
            $total_deuda=0;
            $total_extensiones=0;
            $arr_Gdebe_cuotas=array();
            $a=0;
            $arr_extensiones=array();
            $monto_extension=0;
            $fecha_extension='';
            foreach($cobranzas as $cobranza) {
                $arr_debe_cuotas=array();
                //extensiones al empeño
                $extensiones=Prestamo::where('prestamos.padre','=',$cobranza->pre)
                ->where('prestamos.extension','=','S')
                ->select('id','padre','importe','finiciopago','tcuota','piva','iva','cuota')
                ->orderBy('finiciopago','asc')
                ->get();
                $monto_extension=0;

                $nextension=0;
                foreach($extensiones as $extension) {
                    $arr_extensiones[]=array(
                        'prestamo_id'  => $extension->padre,
                        'extension_id' => $extension->id,
                        'fecha'        => $extension->finiciopago,
                        'monto'        => $extension->importe,
                        'tcuota'       => $extension->tcuota,
                        'cuota'        => $extension->cuota,
                        'iva'          => $extension->iva,
                        'piva'         => $extension->piva,
                        'ultimo'       => 0
                    );
                    $monto_extension=$monto_extension+$extension->importe;
                    $fecha_extension=$extension->finiciopago;
                    $nextension++;
                }

                $cobranza->monto_extensiones=$monto_extension;
                $cobranza->numero_extensiones=$nextension;
                $nmes=date('m',strtotime($cobranza->finiciopago));
                $nano=date('Y',strtotime($cobranza->finiciopago));

                $total_anos=($ano-$nano);


                if($total_anos==0)
                    $ndebe_cuotas=($mes-$nmes);
                elseif($total_anos==1)
                    $ndebe_cuotas=(12-$nmes)+($mes);
                else
                    $ndebe_cuotas=(12-$nmes)+(($total_anos-1)*12)+($mes);

                $cuotas=$cobranza->cuotas;


                if($cobranza->tipo==0 and $ndebe_cuotas>$cuotas-1 )
                    $ndebe_cuotas=$cuotas;

                else
                    $ndebe_cuotas=$ndebe_cuotas+1;
                //echo $ndebe_cuotas.'<br>';
                /* lleno la matriz de cuotas historicas por pagar */
                $debe_cuotas=0;

                indicames($nmes,$nano);
                for ($i=0; $i<$ndebe_cuotas; $i++){
                    $tcuota=0;
                    $arr_debe_cuotas[$i]['periodo']=$nano.$nmes;
                    $arr_debe_cuotas[$i]['prestamo_id']=$cobranza->pre;
                    $arr_debe_cuotas[$i]['nano']=$nano;
                    $arr_debe_cuotas[$i]['nmes']=$nmes;
                    $nano2=$nano;$nmes2=$nmes;
                    sumames($nano2,$nmes2);
                    $arr_debe_cuotas[$i]['fecha']=$nano2.'-'.$nmes2.'-01';
                    $arr_debe_cuotas[$i]['descripcion']='Cuota '.$nmes.'/'.$nano;
                    $arr_debe_cuotas[$i]['mpago']=0;
                    $arr_debe_cuotas[$i]['piva']=0;
                    $arr_debe_cuotas[$i]['balance']=0;
                        if($cobranza->tipo==1){
                            if($i==0 and $cobranza->residual>0 )
                                $tcuota=$cobranza->residual;
                            else
                                $tcuota=$cobranza->tcuota;
                        }else{
                            $tcuota=$cobranza->tcuota;
                        }
                    $arr_debe_cuotas[$i]['tcuota']=$tcuota;
                    $debe_cuotas=$debe_cuotas+$tcuota;
                    sumames($nano,$nmes);
                }
                //echo $debe_cuotas;
                //pagos realizados
                $pagos=Pago::where('pagos.prestamo_id','=',$cobranza->pre)
                ->whereIn('pagos.motivo', ['0', '1'])
                ->where('pagos.conciliado','=','1')
                ->select('id','prestamo_id','monto','fpago','referencia','cuota_nro')
                ->orderBy('fpago','asc')
                ->get();
                $pagado=0;
                $debe=0;
                $cuantos_pagos=0;
                $debe=0;
                foreach ($pagos as $pago) {
                    $pagado=$pagado+$pago->monto;
                    $cuantos_pagos++;
                    if($pago->cuota_nro!='')
                        $digito=explode('/',$pago->cuota_nro);
                    else
                        $digito=array('','');
                    $arr_pagos[]=array(
                        'periodo'    => $digito[1].$digito[0],
                        'prestamo_id'=> $pago->prestamo_id,
                        'pago_id'    => $pago->id,
                        'fecha'      => $pago->fpago,
                        'nano'       => $digito[1],
                        'nmes'       => $digito[0],
                        'monto'      => $pago->monto,
                        'referencia' => $pago->referencia
                    );
                }
                calcula_deuda_extensiones($arr_Gdebe_cuotas,$debe,$ndebe_cuotas,
                $cuantos_pagos,$arr_debe_cuotas,$arr_pagos,
                $cobranza->pre,$cobranza->piva);
                $contpagos=count($arr_Gdebe_cuotas);
                $pagos_empeno=array();
                for($j=1;$j<=$contpagos;$j++){
                    if($arr_Gdebe_cuotas[$j]['prestamo_id']==$cobranza->pre){
                        $cuantos_pagos++;
                        $pagos_empeno[]=array(
                            'periodo'      => $arr_Gdebe_cuotas[$j]['periodo'],
                            'prestamo_id'  => $arr_Gdebe_cuotas[$j]['prestamo_id'],
                            'fecha'        => $arr_Gdebe_cuotas[$j]['fecha'],
                            'descripcion'  => $arr_Gdebe_cuotas[$j]['descripcion'],
                            'cuota'        => $arr_Gdebe_cuotas[$j]['tcuota'],
                            'pago'         => $arr_Gdebe_cuotas[$j]['mpago'] ,
                            'balance'      => $arr_Gdebe_cuotas[$j]['balance'],
                            'piva'         => $arr_Gdebe_cuotas[$j]['piva'],
                        );
                    }
                }
                usort($pagos_empeno, 'compara_fecha');
                if($cobranza->monto_extensiones>0){
                    arregla_extensiones($pagos_empeno,$arr_extensiones,$cobranza->pre,$cobranza->numero_extensiones);
                }
                $impago_d=0;
                $balance=calcula_balance($pagos_empeno,$impago_d);
                $cobranza->debe=$balance;
                $cobranza->pagado=$pagado;
                $cobranza->impagos=$impago_d;
                $cobranza->debe_cuotas=$ndebe_cuotas;
                $total_empenos=$total_empenos+$cobranza->importe;
                $total_deuda=$total_deuda+$cobranza->debe;
                $total_extensiones=$total_extensiones+$cobranza->monto_extensiones;

            }//end foreach de cobranza
            $empresa=Empresa::all()->pluck('nombre','id');
            $cont=count($arr_pagos);
            $customPaper = [0, 0, 567.00, 500.80];
            //IMPUESTOS
            $impuestos=Impuesto::select('impuestos.tipoimpuesto_id','tipoimpuestos.nombre',
            DB::raw("SUM(monto) as monto"),
            DB::raw("SUM(iva) as iva"))
            ->where('impuestos.conciliado','=','1')
            ->rightjoin('tipoimpuestos','tipoimpuestos.id','=','impuestos.tipoimpuesto_id')
            ->groupBy('impuestos.tipoimpuesto_id','tipoimpuestos.nombre')
                ->get();

            if($mesesimpago>-1){
                $cobranzas = $cobranzas->where('impagos','=',$mesesimpago);
                if($imprimir==0)

                    return view('rentabilidad.rentageneral', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','texto','empresa_id','empresa','arr_Gdebe_cuotas','monto_extension','arr_extensiones','fecha_extension'))
                    ->with('i');
                else{
                    $i=0;
                    $pdf = PDF::loadView('cobranza.reporte', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','i'))
                    ->setPaper($customPaper, 'landscape');
                    $pdf->set_option('isPhpEnabled',true);
                    return $pdf->stream('reporteimpagos.pdf');
                }
            }
            elseif($bandera==0){
                //$cobranzas = $cobranzas->where('debe','>',0);
                if($imprimir==0)
                    return view('rentabilidad.rentageneral', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','texto','empresa_id','empresa','arr_Gdebe_cuotas','monto_extension','arr_extensiones','fecha_extension'))
                    ->with('i');
                else{
                    $i=0;
                    $pdf = PDF::loadView('rentabilidad.rentageneral', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','i'))
                    ->setPaper($customPaper, 'landscape');
                    $pdf->set_option('isPhpEnabled',true);
                    return $pdf->stream('reporteimpagos.pdf');
                }
            }elseif($bandera==1){
                if($imprimir==0)
                    return view('rentabilidad.rentageneral', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','texto','empresa_id','empresa','arr_Gdebe_cuotas','monto_extension','arr_extensiones','fecha_extension'))
                    ->with('i');
                else{
                    $i=0;
                    $pdf = PDF::loadView('cobranza.reporte', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','i'))
                    ->setPaper($customPaper, 'landscape');
                    $pdf->set_option('isPhpEnabled',true);
                    return $pdf->stream('reporteimpagos.pdf');
                }
                // (request()->input('page', 1) - 1) * $cobranzas->perPage()
            }

    }


    public function RentabilidadIndividual(Request $request,$imprimir,$mesesimpago2,$texto2){
        {
            $bandera=0;
            $mes=date('m');
            $ano=date('Y');
            $tipos=array('A','T');
            //$mesesimpago2=$request->mesesimpago2;
            $empresa_id=0;
            if($request->empresa_id!='' and $request->empresa_id>0)
                $empresa_id=$request->empresa_id;
            if($imprimir==0){
                $mesesimpago=-1;
                if($request->mesesimpago!='') {
                    $mesesimpago=$request->mesesimpago;
                }
                $texto=$request->texto;
            }

            if($texto!=''){
                $bandera=1;
                //echo $bandera;exit();
                $cobranzas = Cliente::rightjoin('empresas','empresas.id','=','clientes.empresa_id')
                ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
                ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
                ->where('prestamos.condicion_id','=',1)
                //->where('prestamos.extension','=','N')
                ->whereNotNull('prestamos.id')
                ->Where(function (Builder $query2 ) use ($texto) {
                    $query2->orWhere('clientes.proempresa','like','%'.$texto.'%');
                    $query2->orWhere('clientes.nombre','like','%'.$texto.'%');
                    $query2->orWhere('clientes.dni','like','%'.$texto.'%');
                    $query2->orWhere('vehiculos.matricula','like','%'.$texto.'%');
                })
                ->Where(function (Builder $query3) use ($empresa_id) {
                    if($empresa_id>0)
                        $query3->where('clientes.empresa_id','=',$empresa_id);

                    /*else
                        $query3->where('clientes.empresa_id','>',$empresa_id);*/
                })
                ->select('clientes.id','clientes.nombre','prestamos.id as pre','clientes.dni',
                'clientes.telefono','clientes.email','prestamos.ffirma','prestamos.piva',
                'prestamos.tipo','prestamos.finiciopago','prestamos.importe','prestamos.tcuota','prestamos.porcentaje',
                'prestamos.residual','vehiculos.matricula','empresas.nombre as emp',
                'clientes.proempresa','dnicif','cuotas','prestamos.padre',
                DB::raw('coalesce(prestamos.cuotas,0) as cuotas'),
                DB::raw('0 as impagos'),
                DB::raw('0 as pagado'),
                DB::raw('0 as debe'),
                DB::raw('0 as debe_cuotas'),
                DB::raw('0 as monto_extensiones'),
                DB::raw('0 as numero_extensiones'),
                )
                ->orderBy('clientes.nombre','asc')
                ->get();
            }else{
                $cobranzas = Cliente::rightjoin('empresas','empresas.id','=','clientes.empresa_id')
                ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
                ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
                ->where('prestamos.condicion_id','=',1)
                //->where('prestamos.extension','=','N')
                ->whereNotNull('prestamos.id')
                //->whereMonth('prestamos.finiciopago','<=',$mes)
                /*->Where(function (Builder $query) use($ano) {
                    $query->WhereYear('prestamos.finiciopago','=',$ano);
                    $query->orWhereYear('prestamos.finiciopago','<',$ano);
                })*/
                ->Where(function (Builder $query3) use ($empresa_id) {
                    if($empresa_id>0)
                        $query3->where('clientes.empresa_id','=',$empresa_id);
                })
                ->select('clientes.id','clientes.nombre','prestamos.id as pre','clientes.dni',
                'clientes.telefono','clientes.email','prestamos.ffirma','prestamos.piva',
                'prestamos.tipo','prestamos.finiciopago','prestamos.importe','prestamos.tcuota','prestamos.porcentaje',
                'prestamos.residual','vehiculos.matricula','empresas.nombre as emp',
                'clientes.proempresa','dnicif','cuotas','prestamos.padre',
                DB::raw('coalesce(prestamos.cuotas,0) as cuotas'),
                DB::raw('0 as impagos'),
                DB::raw('0 as pagado'),
                DB::raw('0 as debe'),
                DB::raw('0 as debe_cuotas'),
                DB::raw('0 as monto_extensiones'),
                DB::raw('0 as numero_extensiones'),
                )
                ->orderBy('clientes.nombre','asc')
                ->get();
            }
            $arr_pagos=array();
            $total_empenos=0;
            $total_deuda=0;
            $total_extensiones=0;
            $arr_Gdebe_cuotas=array();
            $a=0;
            $arr_extensiones=array();
            $monto_extension=0;
            $fecha_extension='';
            foreach($cobranzas as $cobranza) {
                $arr_debe_cuotas=array();
                //extensiones al empeño
                $extensiones=Prestamo::where('prestamos.padre','=',$cobranza->pre)
                ->where('prestamos.extension','=','S')
                ->select('id','padre','importe','finiciopago','tcuota','piva','iva','cuota')
                ->orderBy('finiciopago','asc')
                ->get();
                $monto_extension=0;

                $nextension=0;
                foreach($extensiones as $extension) {
                    $arr_extensiones[]=array(
                        'prestamo_id'  => $extension->padre,
                        'extension_id' => $extension->id,
                        'fecha'        => $extension->finiciopago,
                        'monto'        => $extension->importe,
                        'tcuota'       => $extension->tcuota,
                        'cuota'        => $extension->cuota,
                        'iva'          => $extension->iva,
                        'piva'         => $extension->piva,
                        'ultimo'       => 0
                    );
                    $monto_extension=$monto_extension+$extension->importe;
                    $fecha_extension=$extension->finiciopago;
                    $nextension++;
                }

                $cobranza->monto_extensiones=$monto_extension;
                $cobranza->numero_extensiones=$nextension;
                $nmes=date('m',strtotime($cobranza->finiciopago));
                $nano=date('Y',strtotime($cobranza->finiciopago));

                $total_anos=($ano-$nano);


                if($total_anos==0)
                    $ndebe_cuotas=($mes-$nmes);
                elseif($total_anos==1)
                    $ndebe_cuotas=(12-$nmes)+($mes);
                else
                    $ndebe_cuotas=(12-$nmes)+(($total_anos-1)*12)+($mes);

                $cuotas=$cobranza->cuotas;


                if($cobranza->tipo==0 and $ndebe_cuotas>$cuotas-1 )
                    $ndebe_cuotas=$cuotas;

                else
                    $ndebe_cuotas=$ndebe_cuotas+1;
                //echo $ndebe_cuotas.'<br>';
                /* lleno la matriz de cuotas historicas por pagar */
                $debe_cuotas=0;

                indicames($nmes,$nano);
                for ($i=0; $i<$ndebe_cuotas; $i++){
                    $tcuota=0;
                    $arr_debe_cuotas[$i]['periodo']=$nano.$nmes;
                    $arr_debe_cuotas[$i]['prestamo_id']=$cobranza->pre;
                    $arr_debe_cuotas[$i]['nano']=$nano;
                    $arr_debe_cuotas[$i]['nmes']=$nmes;
                    $nano2=$nano;$nmes2=$nmes;
                    sumames($nano2,$nmes2);
                    $arr_debe_cuotas[$i]['fecha']=$nano2.'-'.$nmes2.'-01';
                    $arr_debe_cuotas[$i]['descripcion']='Cuota '.$nmes.'/'.$nano;
                    $arr_debe_cuotas[$i]['mpago']=0;
                    $arr_debe_cuotas[$i]['piva']=0;
                    $arr_debe_cuotas[$i]['balance']=0;
                        if($cobranza->tipo==1){
                            if($i==0 and $cobranza->residual>0 )
                                $tcuota=$cobranza->residual;
                            else
                                $tcuota=$cobranza->tcuota;
                        }else{
                            $tcuota=$cobranza->tcuota;
                        }
                    $arr_debe_cuotas[$i]['tcuota']=$tcuota;
                    $debe_cuotas=$debe_cuotas+$tcuota;
                    sumames($nano,$nmes);
                }
                //echo $debe_cuotas;
                //pagos realizados
                $pagos=Pago::where('pagos.prestamo_id','=',$cobranza->pre)
                ->whereIn('pagos.motivo', ['0', '1'])
                ->where('pagos.conciliado','=','1')
                ->select('id','prestamo_id','monto','fpago','referencia','cuota_nro')
                ->orderBy('fpago','asc')
                ->get();
                $pagado=0;
                $debe=0;
                $cuantos_pagos=0;
                $debe=0;
                foreach ($pagos as $pago) {
                    $pagado=$pagado+$pago->monto;
                    $cuantos_pagos++;
                    if($pago->cuota_nro!='')
                        $digito=explode('/',$pago->cuota_nro);
                    else
                        $digito=array('','');
                    $arr_pagos[]=array(
                        'periodo'    => $digito[1].$digito[0],
                        'prestamo_id'=> $pago->prestamo_id,
                        'pago_id'    => $pago->id,
                        'fecha'      => $pago->fpago,
                        'nano'       => $digito[1],
                        'nmes'       => $digito[0],
                        'monto'      => $pago->monto,
                        'referencia' => $pago->referencia
                    );
                }
                calcula_deuda_extensiones($arr_Gdebe_cuotas,$debe,$ndebe_cuotas,
                $cuantos_pagos,$arr_debe_cuotas,$arr_pagos,
                $cobranza->pre,$cobranza->piva);
                $contpagos=count($arr_Gdebe_cuotas);
                $pagos_empeno=array();
                for($j=1;$j<=$contpagos;$j++){
                    if($arr_Gdebe_cuotas[$j]['prestamo_id']==$cobranza->pre){
                        $cuantos_pagos++;
                        $pagos_empeno[]=array(
                            'periodo'      => $arr_Gdebe_cuotas[$j]['periodo'],
                            'prestamo_id'  => $arr_Gdebe_cuotas[$j]['prestamo_id'],
                            'fecha'        => $arr_Gdebe_cuotas[$j]['fecha'],
                            'descripcion'  => $arr_Gdebe_cuotas[$j]['descripcion'],
                            'cuota'        => $arr_Gdebe_cuotas[$j]['tcuota'],
                            'pago'         => $arr_Gdebe_cuotas[$j]['mpago'] ,
                            'balance'      => $arr_Gdebe_cuotas[$j]['balance'],
                            'piva'         => $arr_Gdebe_cuotas[$j]['piva'],
                        );
                    }
                }
                usort($pagos_empeno, 'compara_fecha');
                if($cobranza->monto_extensiones>0){
                    arregla_extensiones($pagos_empeno,$arr_extensiones,$cobranza->pre,$cobranza->numero_extensiones);
                }
                $impago_d=0;
                $balance=calcula_balance($pagos_empeno,$impago_d);
                $cobranza->debe=$balance;
                $cobranza->pagado=$pagado;
                $cobranza->impagos=$impago_d;
                $cobranza->debe_cuotas=$ndebe_cuotas;
                $total_empenos=$total_empenos+$cobranza->importe;
                $total_deuda=$total_deuda+$cobranza->debe;
                $total_extensiones=$total_extensiones+$cobranza->monto_extensiones;

            }//end foreach de cobranza
            $empresa=Empresa::all()->pluck('nombre','id');
            $cont=count($arr_pagos);
            $customPaper = [0, 0, 567.00, 500.80];
            if($mesesimpago>-1){
                //$cobranzas = $cobranzas->where('impagos','=',$mesesimpago);
                if($imprimir==0)
                    return view('rentabilidad.individual', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','texto','empresa_id','empresa','arr_Gdebe_cuotas','monto_extension','arr_extensiones','fecha_extension'))
                    ->with('i');
                else{
                    $i=0;
                    $pdf = PDF::loadView('cobranza.reporte', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','i'))
                    ->setPaper($customPaper, 'landscape');
                    $pdf->set_option('isPhpEnabled',true);
                    return $pdf->stream('reporteimpagos.pdf');
                }
            }
            elseif($bandera==0){
                //$cobranzas = $cobranzas->where('debe','>',0);
                if($imprimir==0)
                    return view('rentabilidad.individual', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','texto','empresa_id','empresa','arr_Gdebe_cuotas','monto_extension','arr_extensiones','fecha_extension'))
                    ->with('i');
                else{
                    $i=0;
                    $pdf = PDF::loadView('rentabilidad.individual', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','i'))
                    ->setPaper($customPaper, 'landscape');
                    $pdf->set_option('isPhpEnabled',true);
                    return $pdf->stream('reporteimpagos.pdf');
                }
            }elseif($bandera==1){
                if($imprimir==0)
                    return view('rentabilidad.individual', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','texto','empresa_id','empresa','arr_Gdebe_cuotas','monto_extension','arr_extensiones','fecha_extension'))
                    ->with('i');
                else{
                    $i=0;
                    $pdf = PDF::loadView('cobranza.reporte', compact('cobranzas','tipos','arr_pagos','bandera','total_extensiones','total_deuda','total_empenos','mesesimpago','i'))
                    ->setPaper($customPaper, 'landscape');
                    $pdf->set_option('isPhpEnabled',true);
                    return $pdf->stream('reporteimpagos.pdf');
                }
                // (request()->input('page', 1) - 1) * $cobranzas->perPage()
            }

        }

    }


    public function historial($id){
        $clientes = Cliente::where('clientes.id','=',$id)
        ->join('empresas','clientes.empresa_id','=','empresas.id')
        ->leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->select('clientes.*','clientes.id as idcliente','prestamos.*','prestamos.id as pre',DB::Raw(
                    "(SELECT coalesce(SUM(pagos.monto),0) FROM pagos WHERE pagos.prestamo_id=prestamos.id and motivo='0' and pagos.conciliado='1') AS tpagos"),DB::Raw('0 as capital_pagado,0 as interes_pagado,0 as iva_pagado'))
        ->paginate();

        $capital_pagado=array();
        $interes_pagado=array();
        $iva_pagado=array();
        $interes_pagado[0]=0;
        $capital_pagado[0]=0;
        $iva_pagado[0]=0;
        $i=1;
        foreach ($clientes as $cliente) {
            $pagosdesgloses=Pago::where('pagos.prestamo_id','=',$cliente->pre) //->where('pagos.motivo','=','0')
                ->where('conciliado','=','1')
                ->select('pagos.*',DB::Raw(
                "(SELECT coalesce(SUM(pagos.monto),0) FROM pagos WHERE pagos.prestamo_id=".$cliente->pre." and motivo='0') AS tpagos"),DB::Raw('0 as capital_pagado,0 as interes_pagado, 0 as iva_pagado') )->get();
            $datas=array(0,0,0);
            if($cliente->tipo=='0'){ //amortizacion
                $j=$cliente->porcentaje/100;
                $cuota_calculada=($cliente->importe*$j)/(1-((1+$j)**-$cliente->cuotas));
                $diferencia_cuota=$cliente->cuota-$cuota_calculada;
                $capital_resta=$cliente->importe;
                foreach ($pagosdesgloses as $pago) {
                    $iva_pagado[$i]=$pago->monto-($pago->monto/(1+$cliente->piva/100));
                    $pagoneto=$pago->monto-$iva_pagado[$i];
                    if($pago->monto<$cliente->tcuota){ //monto menor a la cuota
                        $interes_calculado=($capital_resta*$j)+$diferencia_cuota;
                            if($interes_calculado>=$pagoneto){
                                $interes_pagado[$i]=$pagoneto;
                                $capital_pagado[$i]=0;
                            }else{
                                $interes_pagado[$i]=$interes_calculado;
                                $capital_pagado[$i]=$pagoneto-$interes_pagado[$i];
                                //$capital_pagado[$i]=0;
                            }
                    }elseif($pago->monto>$cliente->tcuota){ //monto mayor a la cuota
                        $interes_calculado=($capital_resta*$j)+$diferencia_cuota;
                            if($interes_calculado>=$pagoneto){
                                $interes_pagado[$i]=$pagoneto;
                                $capital_pagado[$i]=0;
                            }else{
                                $interes_pagado[$i]=$interes_calculado;
                                $capital_pagado[$i]=$pagoneto-$interes_calculado;
                            }
                    }else{
                        $interes_pagado[$i]=($capital_resta*$j)+$diferencia_cuota;
                        $capital_pagado[$i]=($pagoneto)-$interes_pagado[$i];
                    }
                    $iva_pagado[$i]=$interes_pagado[$i]*($cliente->piva/100);

                    $subtotal=$iva_pagado[$i]+$capital_pagado[$i]+$interes_pagado[$i];
                    $subtotal=$cliente->tcuota-$subtotal;
                    $interes_pagado[$i]=$interes_pagado[$i]+($subtotal/(1+($cliente->piva/100)));
                    $iva_pagado[$i]=($interes_pagado[$i]) * $cliente->piva/100;
                    $capital_resta-=$capital_pagado[$i];

                    $i++;
                }
            }else{ //empeño tradicional
                foreach ($pagosdesgloses as $pago) {
                    if($pago->motivo=='0'){
                        $iva_pagado[$i]=$pago->monto-($pago->monto/(1+$cliente->piva/100));
                        $interes_pagado[$i]=$pago->monto-$iva_pagado[$i];
                        $capital_pagado[$i]=0;
                    }else{
                        $iva_pagado[$i]=0;
                        $interes_pagado[$i]=0;
                        $capital_pagado[$i]=$pago->monto;//-$iva_pagado[$i];
                    }

                    $i++;
                }

            }
        }
        $ncuota=[12,24,12];
        //IMPUESTOS
        $vehiculos=Vehiculo::select('vehiculos.id as idvehiculo','matricula',
        'marcas.nombre as mar','modelos.nombre as mod', DB::raw('0 as pagoImpuesto'),
        DB::raw('0 as pagoIva'),DB::raw("'' as impuesto"))
        ->rightjoin('marcas','marcas.id','=','vehiculos.marca_id')
        ->rightjoin('modelos','modelos.id','=','vehiculos.modelo_id')
        ->where('vehiculos.cliente_id','=',$id)
        ->get();
        foreach ($vehiculos as $veh) {
            $impuestos=Impuesto::select('impuestos.tipoimpuesto_id','tipoimpuestos.nombre','impuestos.vehiculo_id',
            DB::raw("SUM(monto) as monto"),
            DB::raw("SUM(iva) as iva"))
            ->where('impuestos.conciliado','=','1')
            ->where('impuestos.vehiculo_id','=',$veh->idvehiculo)
            ->rightjoin('tipoimpuestos','tipoimpuestos.id','=','impuestos.tipoimpuesto_id')
            ->groupBy('impuestos.tipoimpuesto_id','tipoimpuestos.nombre','impuestos.vehiculo_id')
            ->orderBy('impuestos.vehiculo_id')
            ->get();
            foreach($impuestos as $imp){
                $veh->pagoImpuesto=$imp->monto;
                $veh->pagoIva=$imp->iva;
                $veh->impuesto=$imp->nombre;
            }
        }


        $tipo=['Amortizando','Empeño Tradicional'];
        return view('rentabilidad.historial',compact('clientes','pagosdesgloses','ncuota','tipo','capital_pagado','interes_pagado','iva_pagado','impuestos','vehiculos'));
    }

    private function calculaCapitalInteres($cursor2,$modo,&$data,&$cursor){
        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=[12,24,12];
        foreach ($cursor as $pagosdesglose) {
                 if($pagosdesglose->tpagos>0){

                $cuotas=$pagosdesglose->tpagos/$cursor2->tcuota;
                $ncuotas=intval($cuotas);
            if($cursor2->tipo==0){
            $capital_resta=$cursor2->importe;
            }
            $cuotas=$cursor2->cuotas;
            $capital_pagado=0;
            if($cursor2->tipo==0){
                $j=$cursor2->porcentaje/100;
                $cuota_calculada=($cursor2->importe*$j)/(1-((1+$j)**-$cursor2->cuotas));
                $diferencia_cuota=$cursor2->cuota-$cuota_calculada;
            }
            for($i=1;$i<=$ncuotas;$i++){
                if($cursor2->tipo==1){
                    if($i==1){
                            $iva=$cursor2->residual/1.21;
                            $interes=$cursor2->residual-$iva;
                        }else{
                            $iva=$cursor2->iva;
                            $interes=$cursor2->cuota-$iva;
                        }
                $capital=0;
                $capital_pagado=0;
                $capital_resta=$cursor2->importe;
            }else{
                //C = S(1 + i)n

                //$interes=$capital_resta*$j;
                $interes=($capital_resta*$j)+$diferencia_cuota;
                $capital=$cursor2->cuota-$interes;
                if($i==$cuotas and !$capital_resta==0){
                    $capital=$cap_ant;
                    $capital_pagado=$capital_pagado+$capital;
                    $interes=$cuota-$capital;
                    $capital_resta=$capital_resta-$capital;
                }else{
                    $capital_pagado=$capital_pagado+$capital;
                    $capital_resta=$capital_resta-$capital;
                }
                $iva=$cursor2->iva;
                $cuota=$cursor2->cuota;
            }
            if(($interes+$capital_pagado+$iva)<$cursor2->tcuota){

                $interes=$interes+($cursor2->tcuota-($interes+$capital_pagado+$iva));
                           // $total=$capital_pagado[$i-1]+$interes_pagado[$i-1]+$iva;
            }
            $data[0]=$data[0]+$interes;
            $data[1]=$data[1]+$capital_pagado;
            $data[2]=$data[2]+$iva;
            $cap_ant=$capital_resta;
            if($modo==1){
                $cursor->interes_pagado=$data[0];
                $cursor->capital_pagado=$data[1];
            }
        }
        }
        }
        //unset($data);

  }
}
