<?php

namespace App\Http\Controllers;

use App\Models\Prestamo;
use App\Models\Cliente;
use App\Models\Extensione;
use App\Models\Pago;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
Use Illuminate\Database\Eloquent\Builder;
use PDF;


class GeneradorController extends Controller
{
    //
    public function imprimir($id,$vehiculo_id){
     $clientes = Cliente::where('prestamos.id','=',$id)
        ->where('vehiculos.id','=',$vehiculo_id)
        ->join('empresas','clientes.empresa_id','=','empresas.id')
        ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->select('empresas.nombre as emp','clientes.id','clientes.nombre','clientes.dni',
        'marcas.nombre as mar','modelos.nombre as mod',
        'vehiculos.id as veh','prestamos.id as pre','prestamos.*')
        ->get();
        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=[12,24,0];
        $frecuencia=[52,12,4, 2,1];
     $pdf = PDF::loadView('repamortiza',compact('clientes','tipo','ncuota','frecuencia'))->setPaper('letter', 'landscape');

     $pdf->set_option('isPhpEnabled',true);
     return $pdf->stream('repamortiza.pdf');
}

public function CompraVenta($id,$vehiculo_id,$prestamo_id){
        $clientes = Cliente::where('clientes.id','=',$id)
        ->where('vehiculos.id','=',$vehiculo_id)
        ->where('prestamos.id','=',$prestamo_id)
        ->join('empresas','clientes.empresa_id','=','empresas.id')
        ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->leftjoin('provincias','clientes.provincia_id','=','provincias.id')
        ->leftjoin('municipios','clientes.municipio_id','=','municipios.id')
        ->select('empresas.nombre as emp','clientes.*','marcas.nombre as mar',
        'empresas.*','clientes.nombre as nom','modelos.nombre as mod',
        'vehiculos.id as veh','prestamos.id as pre','prestamos.*',
        'provincias.nombre as prov','municipios.nombre as loc',
        'vehiculos.*', 'clientes.proempresa','clientes.dnicif',
        'clientes.nombre_tutor','clientes.dni_tutor','clientes.sexo_tutor','clientes.pnotarial','clientes.pespecial','clientes.dir_tutor')
        ->get();
        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=[12,24,0];
        $frecuencia=[52,12,4, 2,1];
        $tutor='';
        foreach($clientes as $cliente){
            $tutor=$cliente->tipo_tutor;
        }
        if($tutor=='--'){
            $pdf = PDF::loadView('CompraVenta' , compact('clientes','tipo','ncuota','frecuencia'))->setPaper('letter', 'portrait');
            $pdf->set_option('isPhpEnabled',true);
            return $pdf->stream('CompraVenta.pdf');
        }else{
            $pdf = PDF::loadView('CompraventaTutor' , compact('clientes','tipo','ncuota','frecuencia'))->setPaper('letter', 'portrait');
            $pdf->set_option('isPhpEnabled',true);
            return $pdf->stream('CompraventaTutor.pdf');
        }
    }
    public function DatosOperacion($id,$vehiculo_id,$prestamo_id){
        $clientes = Cliente::where('clientes.id','=',$id)
        ->where('vehiculos.id','=',$vehiculo_id)
        ->where('prestamos.id','=',$prestamo_id)
        ->leftjoin('empresas','clientes.empresa_id','=','empresas.id')
        ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->leftjoin('provincias','clientes.provincia_id','=','provincias.id')
        ->leftjoin('municipios','clientes.municipio_id','=','municipios.id')
        ->leftjoin('coberturas','vehiculos.tipo_id','=','coberturas.id')
        ->leftjoin('empseguros','vehiculos.empseguro_id','=','empseguros.id')
        ->select('empresas.nombre as emp','clientes.*','clientes.observacion as observa',
        'marcas.nombre as mar','clientes.nombre as nom','modelos.nombre as mod',
        'vehiculos.id as veh','prestamos.id as pre','prestamos.*','provincias.nombre as prov',
        'municipios.nombre as loc','vehiculos.*','vehiculos.observacion as observav','coberturas.nombre as cobertura',
        'empseguros.nombre as empseg','clientes.email as emailcliente')
        ->get();
        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=[12,24,0];
        $frecuencia=[52,12,4, 2,1];
        $pdf = PDF::loadView('DatosOperacion',compact('clientes','tipo','ncuota','frecuencia'))->setPaper('letter', 'portrait');
        $pdf->set_option('isPhpEnabled',true);
        return $pdf->stream('DatosOperacion.pdf');
    }
    public function DocumentoEntrega($id,$vehiculo_id,$prestamo_id){
        $clientes = Cliente::where('clientes.id','=',$id)->join('empresas','clientes.empresa_id','=','empresas.id')
        ->where('vehiculos.id','=',$vehiculo_id)
        ->where('prestamos.id','=',$prestamo_id)
        ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->leftjoin('provincias','clientes.provincia_id','=','provincias.id')
        ->leftjoin('municipios','clientes.municipio_id','=','municipios.id')
        ->select('empresas.nombre as emp','clientes.*',
            'marcas.nombre as mar','empresas.*',
            'clientes.nombre as nom','modelos.nombre as mod',
            'vehiculos.id as veh','prestamos.id as pre','prestamos.*',
            'provincias.nombre as prov','municipios.nombre as loc','vehiculos.*',
            'clientes.proempresa','clientes.dnicif',
            'clientes.nombre_tutor','clientes.dni_tutor','clientes.sexo_tutor','clientes.pnotarial','clientes.pespecial','clientes.dir_tutor')
        ->get();
        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=[12,24,0];
        $frecuencia=[52,12,4, 2,1];
        $tutor='';
        foreach($clientes as $cliente){
            $tutor=$cliente->tipo_tutor;
        }
        if($tutor=='--'){
            $pdf = PDF::loadView('DocumentoEntrega',compact('clientes','tipo','ncuota','frecuencia'))->setPaper('letter', 'portrait');
            $pdf->set_option('isPhpEnabled',true);
            return $pdf->stream('DocumentoEntrega.pdf');
        }else{
            $pdf = PDF::loadView('DocumentoEntregaTutor',compact('clientes','tipo','ncuota','frecuencia'))->setPaper('letter', 'portrait');
            $pdf->set_option('isPhpEnabled',true);
            return $pdf->stream('DocumentoEntregaTutor.pdf');
        }
    }
    public function EdicionCliente($id,$vehiculo_id,$prestamo_id){
        $clientes = Cliente::where('clientes.id','=',$id)
        ->where('vehiculos.id','=',$vehiculo_id)
        ->where('prestamos.id','=',$prestamo_id)
        ->join('empresas','clientes.empresa_id','=','empresas.id')
        ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->leftjoin('provincias','clientes.provincia_id','=','provincias.id')
        ->leftjoin('municipios','clientes.municipio_id','=','municipios.id')
        ->leftjoin('coberturas','vehiculos.tipo_id','=','coberturas.id')
        ->select('empresas.nombre as emp','clientes.*','marcas.nombre as mar',
        'empresas.domicilio as domiempresa','empresas.email as emailempresa','clientes.nombre as nom',
        'modelos.nombre as mod','vehiculos.id as veh','prestamos.id as pre',
        'prestamos.*','provincias.nombre as prov','municipios.nombre as mun',
        'vehiculos.*','coberturas.nombre as cobertura')
        ->get();
        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=[12,24,0];
        $frecuencia=[52,12,4, 2,1];
        $pdf = PDF::loadView('EdicionCliente',compact('clientes','tipo','ncuota','frecuencia'))->setPaper('letter', 'portrait');
        $pdf->set_option('isPhpEnabled',true);
        return $pdf->stream('EdicionCliente.pdf');
    }
    public function ContratoArrendamiento($id,$vehiculo_id,$prestamo_id){
        $clientes = Cliente::where('clientes.id','=',$id)->join('empresas','clientes.empresa_id','=','empresas.id')
        ->where('vehiculos.id','=',$vehiculo_id)
        ->where('prestamos.id','=',$prestamo_id)
        ->join('bancos','empresas.banco_id','=','bancos.id')
        ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->leftjoin('provincias','clientes.provincia_id','=','provincias.id')
        ->leftjoin('municipios','clientes.municipio_id','=','municipios.id')
        ->select('empresas.nombre as emp','clientes.*','marcas.nombre as mar',
        'empresas.*','clientes.nombre as nom','modelos.nombre as mod',
        'vehiculos.id as veh','prestamos.id as pre','prestamos.*',
        'provincias.nombre as prov','municipios.nombre as loc',
        'vehiculos.*','bancos.nombre as banconombre',
        'clientes.proempresa','clientes.dnicif',
        'clientes.nombre_tutor','clientes.dni_tutor','clientes.sexo_tutor','clientes.pnotarial',
        'clientes.pespecial','clientes.dir_tutor')
        ->get();
        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=[12,24,0];
        $frecuencia=[52,12,4, 2,1];
        $tutor='';
        foreach($clientes as $cliente){
            $tutor=$cliente->tipo_tutor;
        }
        if($tutor=='--'){
            $pdf = PDF::loadView('ContratoArrendamiento',compact('clientes','tipo','ncuota','frecuencia'))->setPaper('letter', 'portrait');
            $pdf->set_option('isPhpEnabled',true);
            return $pdf->stream('ContratoArrendamiento.pdf');
        }else{
            $pdf = PDF::loadView('ContratoArrendamientoTutor',compact('clientes','tipo','ncuota','frecuencia'))->setPaper('letter', 'portrait');
            $pdf->set_option('isPhpEnabled',true);
            return $pdf->stream('ContratoArrendamientoTutor.pdf');
        }

    }
    public function MandatoEspecifico($id,$vehiculo_id,$prestamo_id){
        $clientes = Cliente::where('clientes.id','=',$id)->join('empresas','clientes.empresa_id','=','empresas.id')
        ->where('vehiculos.id','=',$vehiculo_id)
        ->where('prestamos.id','=',$prestamo_id)
        ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
        ->leftjoin('prestamos','prestamos.cliente_id','=','clientes.id')
        ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
        ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
        ->leftjoin('provincias','clientes.provincia_id','=','provincias.id')
        ->leftjoin('municipios','clientes.municipio_id','=','municipios.id')
        ->select('empresas.nombre as emp','clientes.*','marcas.nombre as mar',
        'empresas.*','clientes.nombre as nom','clientes.*','modelos.nombre as mod',
        'vehiculos.id as veh','prestamos.id as pre','prestamos.*',
        'provincias.nombre as prov','municipios.nombre as mun','vehiculos.*')
        ->get();
        $tipo=['Amortizando','Empeño Tradicional'];
        $ncuota=[12,24,0];
        $frecuencia=[52,12,4, 2,1];
        $pdf = PDF::loadView('MandatoEspecifico',compact('clientes','tipo','ncuota','frecuencia'))->setPaper('A4', 'portrait');
        $pdf->set_option('isPhpEnabled',true);
        return $pdf->stream('MandatoEspecifico.pdf');
    }

    //reporte de impagos individual
    public function ImpagoIndividual($id,$prestamo_id){
        $cobranzas = Cliente::rightjoin('empresas','empresas.id','=','clientes.empresa_id')
            ->leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
            ->leftjoin('marcas','vehiculos.marca_id','=','marcas.id')
            ->leftjoin('modelos','vehiculos.modelo_id','=','modelos.id')
            ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
            ->where('clientes.id','=',$id)
            ->where('prestamos.id','=',$prestamo_id)
            ->where('prestamos.condicion_id','=',1)
            ->whereNotNull('prestamos.id')
            ->select('clientes.id','clientes.nombre','prestamos.id as pre','clientes.dni',
            'clientes.telefono','clientes.email','prestamos.ffirma','prestamos.piva','prestamos.porcentaje',
            'prestamos.tipo','prestamos.finiciopago','prestamos.importe','prestamos.tcuota',
            'prestamos.residual','vehiculos.matricula','empresas.nombre as emp',
            'clientes.proempresa','dnicif','cuotas',
            'modelos.nombre as mod',
            'marcas.nombre as mar',
            DB::raw('coalesce(prestamos.cuotas,0) as cuotas')
            )
            ->orderBy('clientes.nombre','asc')
            ->get();
            // inicio de for each cobranza
            $mes=date('m');
            $ano=date('Y');
            $arr_pagos=array();
            $total_empenos=0;
            $total_deuda=0;
            $total_extensiones=0;
            $arr_Gdebe_cuotas=array();
            $a=0;
            $arr_extensiones=array();
            $monto_extension=0;
            $fecha_extension='';
            foreach($cobranzas as $cobranza) {
                $arr_debe_cuotas=array();
                //extensiones al empeño
                $extensiones=Prestamo::where('prestamos.padre','=',$cobranza->pre)
                ->where('prestamos.extension','=','S')
                ->select('id','padre','importe','finiciopago','tcuota','piva','iva','cuota')
                ->orderBy('finiciopago','asc')
                ->get();
                $monto_extension=0;

                $nextension=0;
                foreach($extensiones as $extension) {
                    $arr_extensiones[]=array(
                        'prestamo_id'  => $extension->padre,
                        'extension_id' => $extension->id,
                        'fecha'        => $extension->finiciopago,
                        'monto'        => $extension->importe,
                        'tcuota'       => $extension->tcuota,
                        'cuota'        => $extension->cuota,
                        'iva'          => $extension->iva,
                        'piva'         => $extension->piva,
                        'ultimo'       => 0
                    );
                    $monto_extension=$monto_extension+$extension->importe;
                    $fecha_extension=$extension->finiciopago;
                    $nextension++;
                }

                $cobranza->monto_extensiones=$monto_extension;
                $cobranza->numero_extensiones=$nextension;
                $nmes=date('m',strtotime($cobranza->finiciopago));
                $nano=date('Y',strtotime($cobranza->finiciopago));
                $total_anos=($ano-$nano);
                if($total_anos==0)
                    $ndebe_cuotas=($mes-$nmes);
                elseif($total_anos==1)
                    $ndebe_cuotas=(12-$nmes)+($mes);
                else
                    $ndebe_cuotas=(12-$nmes)+(($total_anos-1)*12)+($mes);
                $cuotas=$cobranza->cuotas;
                if($cobranza->tipo==0 and $ndebe_cuotas>$cuotas-1 )
                    $ndebe_cuotas=$cuotas;
                else
                    $ndebe_cuotas=$ndebe_cuotas+1;
                //echo $ndebe_cuotas.'<br>';
                /* lleno la matriz de cuotas historicas por pagar */
                $debe_cuotas=0;
                indicames($nmes,$nano);
                for ($i=0; $i<$ndebe_cuotas; $i++){
                    $tcuota=0;
                    $arr_debe_cuotas[$i]['periodo']=$nano.$nmes;
                    $arr_debe_cuotas[$i]['prestamo_id']=$cobranza->pre;
                    $arr_debe_cuotas[$i]['nano']=$nano;
                    $arr_debe_cuotas[$i]['nmes']=$nmes;
                    $nano2=$nano;$nmes2=$nmes;
                    sumames($nano2,$nmes2);
                    $arr_debe_cuotas[$i]['fecha']=$nano2.'-'.$nmes2.'-01';
                    $arr_debe_cuotas[$i]['descripcion']='Cuota '.$nmes.'/'.$nano;
                    $arr_debe_cuotas[$i]['mpago']=0;
                    $arr_debe_cuotas[$i]['balance']=0;
                        if($cobranza->tipo==1){
                            if($i==0 and $cobranza->residual>0 )
                                $tcuota=$cobranza->residual;
                            else
                                $tcuota=$cobranza->tcuota;
                        }else{
                            $tcuota=$cobranza->tcuota;
                        }
                    $arr_debe_cuotas[$i]['tcuota']=$tcuota;
                    $debe_cuotas=$debe_cuotas+$tcuota;
                    sumames($nano,$nmes);
                }
                //echo $debe_cuotas;
                //pagos realizados
                $pagos=Pago::where('pagos.prestamo_id','=',$cobranza->pre)
                ->whereIn('pagos.motivo', ['0', '1'])
                ->where('pagos.conciliado','=','1')
                ->select('id','prestamo_id','monto','fpago','referencia','cuota_nro')
                ->orderBy('fpago','asc')
                ->get();
                $pagado=0;
                $debe=0;
                $cuantos_pagos=0;
                $debe=0;
                foreach ($pagos as $pago) {
                    $pagado=$pagado+$pago->monto;
                    $cuantos_pagos++;
                    if($pago->cuota_nro!='')
                        $digito=explode('/',$pago->cuota_nro);
                    else
                        $digito=array('','');
                    $arr_pagos[]=array(
                        'periodo'    => $digito[1].$digito[0],
                        'prestamo_id'=> $pago->prestamo_id,
                        'pago_id'    => $pago->id,
                        'fecha'      => $pago->fpago,
                        'nano'       => $digito[1],
                        'nmes'       => $digito[0],
                        'monto'      => $pago->monto,
                        'referencia' => $pago->referencia
                    );

                }
                calcula_deuda_extensiones($arr_Gdebe_cuotas,$debe,$ndebe_cuotas,$cuantos_pagos,
                $arr_debe_cuotas,$arr_pagos,$cobranza->pre,$cobranza->piva);


                $contpagos=count($arr_Gdebe_cuotas);
                $pagos_empeno=array();
                for($j=1;$j<=$contpagos;$j++){
                    if($arr_Gdebe_cuotas[$j]['prestamo_id']==$cobranza->pre){
                        $cuantos_pagos++;
                        $pagos_empeno[]=array(
                            'periodo'      => $arr_Gdebe_cuotas[$j]['periodo'],
                            'prestamo_id'  => $arr_Gdebe_cuotas[$j]['prestamo_id'],
                            'fecha'        => $arr_Gdebe_cuotas[$j]['fecha'],
                            'descripcion'  => $arr_Gdebe_cuotas[$j]['descripcion'],
                            'cuota'        => $arr_Gdebe_cuotas[$j]['tcuota'],
                            'pago'         => $arr_Gdebe_cuotas[$j]['mpago'] ,
                            'balance'      => $arr_Gdebe_cuotas[$j]['balance']
                        );
                    }
                }
                /*
                usort($pagos_empeno, 'compara_fecha');
                //if($cobranza->monto_extensiones>0){
                    arregla_extensiones($pagos_empeno,$arr_extensiones,$cobranza->pre,$cobranza->numero_extensiones);
                //}
                $impago_d=0;
                $balance=calcula_balance($pagos_empeno,$impago_d);
                */
                //*************************** */
                /*$cobranza->debe=$balance;
                $cobranza->pagado=$pagado;
                $cobranza->impagos=$impago_d;
                $cobranza->debe_cuotas=$ndebe_cuotas;
                $total_empenos=$total_empenos+$cobranza->importe;
                $total_deuda=$total_deuda+$cobranza->debe;
                $total_extensiones=$total_extensiones+$cobranza->monto_extensiones;
                */
            }//end foreach de cobranza
            // fin for each
        $tipos=['Amortizando','Empeño Tradicional'];
        $pdf = PDF::loadView('ImpagoIndividual',compact('cobranzas','tipos','arr_extensiones','pagos_empeno','arr_Gdebe_cuotas'))->setPaper('letter', 'portrait');
        $pdf->set_option('isPhpEnabled',true);
        return $pdf->stream('ImpagoIndividual.pdf');
    }
}
