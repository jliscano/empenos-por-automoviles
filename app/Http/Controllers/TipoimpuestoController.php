<?php

namespace App\Http\Controllers;

use App\Models\Tipoimpuesto;
use App\Models\Impuesto;
use Illuminate\Http\Request;

/**
 * Class TipoimpuestoController
 * @package App\Http\Controllers
 */
class TipoimpuestoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tipoimpuestos = Tipoimpuesto::paginate();

        return view('tipoimpuesto.index', compact('tipoimpuestos'))
            ->with('i', (request()->input('page', 1) - 1) * $tipoimpuestos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipoimpuesto = new Tipoimpuesto();
        return view('tipoimpuesto.create', compact('tipoimpuesto'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Tipoimpuesto::$rules);

        $tipoimpuesto = Tipoimpuesto::create($request->all());

        return redirect()->route('tipoimpuestos.index')
            ->with('success', 'Tipo de impuesto Creado con Exito!!!.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tipoimpuesto = Tipoimpuesto::find($id);

        return view('tipoimpuesto.show', compact('tipoimpuesto'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipoimpuesto = Tipoimpuesto::find($id);

        return view('tipoimpuesto.edit', compact('tipoimpuesto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Tipoimpuesto $tipoimpuesto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tipoimpuesto $tipoimpuesto)
    {
        request()->validate(Tipoimpuesto::$rules);

        $tipoimpuesto->update($request->all());

        return redirect()->route('tipoimpuestos.index')
            ->with('success', 'Tipo de impuesto modificado con Exito!!!');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        
        $impuestos=Impuesto::where('impuestos.tipoimpuesto_id','=',$id)->selectRaw('count(*) as total')->get();
        $ttipos=0;
        foreach ($impuestos as $gas) {
            $ttipos=$gas->total;
        }
        
        if($ttipos==0){
            $tipoimpuesto = Tipoimpuesto::find($id)->delete();
            $smj='Tipo de impuesto eliminado con Exito';
        }else{
            $smj='No se puede eliminar porque tiene Impuestos asociados';
        }
        return redirect()->route('tipoimpuestos.index')
            ->with('success', $smj);
    }
}
