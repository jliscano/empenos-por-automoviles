<?php

namespace App\Http\Controllers;

use App\Models\Condicionvehiculo;
use Illuminate\Http\Request;

/**
 * Class CondicionvehiculoController
 * @package App\Http\Controllers
 */
class CondicionvehiculoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $condicionvehiculos = Condicionvehiculo::paginate();

        return view('condicionvehiculo.index', compact('condicionvehiculos'))
            ->with('i', (request()->input('page', 1) - 1) * $condicionvehiculos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $condicionvehiculo = new Condicionvehiculo();
        return view('condicionvehiculo.create', compact('condicionvehiculo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Condicionvehiculo::$rules);

        $condicionvehiculo = Condicionvehiculo::create($request->all());

        return redirect()->route('condicionvehiculos.index')
            ->with('success', 'Condicionvehiculo created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $condicionvehiculo = Condicionvehiculo::find($id);

        return view('condicionvehiculo.show', compact('condicionvehiculo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $condicionvehiculo = Condicionvehiculo::find($id);

        return view('condicionvehiculo.edit', compact('condicionvehiculo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Condicionvehiculo $condicionvehiculo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Condicionvehiculo $condicionvehiculo)
    {
        request()->validate(Condicionvehiculo::$rules);

        $condicionvehiculo->update($request->all());

        return redirect()->route('condicionvehiculos.index')
            ->with('success', 'Condicionvehiculo updated successfully');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $condicionvehiculo = Condicionvehiculo::find($id)->delete();

        return redirect()->route('condicionvehiculos.index')
            ->with('success', 'Condicionvehiculo deleted successfully');
    }
}
