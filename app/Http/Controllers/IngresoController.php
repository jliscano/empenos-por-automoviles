<?php

namespace App\Http\Controllers;

use App\Models\Ingreso;
use App\Models\Cliente;
use App\Models\Empresa;
use App\Models\Clasificado;
use App\Models\Ingresoxvehiculo;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

/**
 * Class IngresoController
 * @package App\Http\Controllers
 */
class IngresoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $ingresos = Ingreso::paginate();

        if($request->startDate!='' or $request->idclase!=''){
            $data=explode('/', $request->startDate);
            $ingresos = Ingreso::join('empresas','ingresos.empresa_id','=','empresas.id')
        ->leftjoin('clasificados','ingresos.clasificado_id','=','clasificados.id')
        ->select('empresas.nombre as emp','ingresos.*','clasificados.nombre as clas');
        
        if($request->startDate!=''){
            $ingresos=$ingresos->whereYear('fingreso','=', $data[1])
            ->whereMonth('fingreso','=', $data[0]);
        }
        if($request->idclase!=''){
            $ingresos=$ingresos->where('ingresos.clasificado_id','=', $request->idclase);
        }

        $ingresos=$ingresos->paginate();
        }else{
            $ingresos = Ingreso::join('empresas','ingresos.empresa_id','=','empresas.id')
        ->leftjoin('clasificados','ingresos.clasificado_id','=','clasificados.id')
        ->select('empresas.nombre as emp','ingresos.*','clasificados.nombre as clas')
        ->paginate();
        }
        $clasificados=Clasificado::all()->where('tipo','=','1')->pluck('nombre','id');
        return view('ingreso.index', compact('ingresos','clasificados'))
            ->with('i', (request()->input('page', 1) - 1) * $ingresos->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ingreso = new Ingreso();
        //$ingreso->empresa_id=1;
        $empresa=Empresa::all()->pluck('nombre','id');
        $clasificado=Clasificado::all()->where('tipo','=',1)->pluck('nombre','id');
        $sino=array('No','Si');
        return view('ingreso.create', compact('ingreso','empresa','clasificado','sino'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Ingreso::$rules);

        $ingreso = Ingreso::create($request->all());
        $id=$ingreso->id;
        return redirect()
        ->action('IngresoController@edit', $id)
        ->with('success', 'Ingreso creado con Exito.');        
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $ingreso = Ingreso::find($id);
        return view('ingreso.show', compact('ingreso'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ingreso = Ingreso::find($id);
        $empresa=Empresa::all()->pluck('nombre','id');
        $clasificado=Clasificado::all()->where('tipo','=',1)->pluck('nombre','id');
        $sino=array('No','Si');
        return view('ingreso.edit', compact('ingreso','empresa','clasificado','sino'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Ingreso $ingreso
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ingreso $ingreso)
    {
        request()->validate(Ingreso::$rules);
        $empresa_id=$request->empresa_id;
        $id=$ingreso->id;
        //echo $empresa_id."   ".$id.'<br>';
        $ingresoxvehiculos=Ingresoxvehiculo::rightjoin('vehiculos','vehiculos.id','=','ingresoxvehiculos.vehiculo_id')
        ->rightjoin('clientes','clientes.id','=','vehiculos.cliente_id')
        ->select(DB::Raw('coalesce(count(*),0) as total_vehiculos'))
        ->where('ingresoxvehiculos.gasto_id','=',$id)
        ->where('clientes.empresa_id','<>',$empresa_id)
        //->  groupBy('ingresoxvehiculos.gasto_id')
        ->get(); 
        
        
        //$total_vehiculos=0;
        foreach ($ingresoxvehiculos as $ingresoxvehiculo) {
            $total_vehiculos=$ingresoxvehiculo->total_vehiculos;
        }
        $msj='';
        //echo $total_vehiculos;
        //exit();
        if($total_vehiculos==0){
            $ingreso->update($request->all());
            $msj='Ingreso modificado con Exito.';
            $succes='success';
        }else{
            $msj='No se puede cambiar la empresa, por que tiene vehículos vinculados!!!.';
            $succes='warmning';
        }
        return redirect()
        ->action('Ingreso@edit', $id)
        ->with($succes, $msj);
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $ingreso = Ingreso::find($id)->delete();

        return redirect()->route('ingresos.index')
            ->with('success', 'Ingreso eliminada con Exito');
    }

    
}
