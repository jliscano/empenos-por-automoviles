<?php

namespace App\Http\Controllers;

use App\Models\Municipio;
use App\Models\Provincia;
use App\Models\Cliente;
use Illuminate\Http\Request;

/**
 * Class MunicipioController
 * @package App\Http\Controllers
 */
class MunicipioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $municipios = Municipio::paginate();

        return view('municipio.index', compact('municipios'))
            ->with('i', (request()->input('page', 1) - 1) * $municipios->perPage());
    }
    public function muestraLocalidad(Request $request, $provincia)
    {
        $buscar=$request->buscar;
        if($buscar==''){
        //$municipios = Municipio::paginate();
            $municipios=Municipio::join('provincias','municipios.provincia_id','=','provincias.id')
            ->where('provincias.id','=',$provincia)
            ->select('provincias.id as provincia','provincias.nombre as nom','municipios.*')
            ->paginate();
        }else{
            $municipios=Municipio::join('provincias','municipios.provincia_id','=','provincias.id')
            ->where('provincias.id','=',$provincia)
            ->where('municipios.nombre','like','%'.$buscar.'%')
            ->select('provincias.id as provincia','provincias.nombre as nom','municipios.*')
            ->paginate();
        }
        return view('municipio.index', compact('municipios','provincia'))
            ->with('i', (request()->input('page', 1) - 1) * $municipios->perPage());
    }
     public function creaMunicipio($provincia)
    {
        $municipio = new Municipio();
        $municipio->provincia_id=$provincia;
        return view('municipio.create', compact('municipio','provincia'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $municipio = new Municipio();
        return view('municipio.create', compact('municipio'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Municipio::$rules);

        $municipio = Municipio::create($request->all());
        $provincia=$request->provincia_id;

        return redirect()->route('muestraLocalidad',compact('provincia'))
            ->with('success', 'Localidad creada con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $municipio = Municipio::find($id);

        return view('municipio.show', compact('municipio'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $municipio = Municipio::find($id);
        $provincia=$municipio->provincia_id;

        return view('municipio.edit', compact('municipio','provincia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Municipio $municipio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Municipio $municipio)
    {
        request()->validate(Municipio::$rules);
        $municipio->update($request->all());
        $provincia=$municipio->provincia_id;

        return redirect()
    ->action('MunicipioController@muestraLocalidad', $provincia)
    ->with('success', 'Localidad modificada con Exito.');


    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $clientes=Cliente::where('clientes.municipio_id','=',$id)->selectRaw('count(*) as total')->get();
        $tclientes=0;
        foreach ($clientes as $gas) {
            $tclientes=$gas->total;
        }
        $municipio = Municipio::find($id);
        $provincia=$municipio->provincia_id;
        if($tclientes==0){
            $municipio=$municipio->delete();
            $smj='Localidad eliminada con Exito';
        }else{
            $smj='No se puede eliminar porque tiene Clientes asociados';
        }
        
        return redirect()
    ->action('MunicipioController@muestraLocalidad', $provincia)
    ->with('success', $smj);

        /*return redirect()->route('municipios.index')
            ->with('success', $smj);
            */
    }
}
