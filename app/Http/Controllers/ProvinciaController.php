<?php

namespace App\Http\Controllers;

use App\Models\Provincia;
use App\Models\Cliente;
use App\Models\Municipio;
use Illuminate\Http\Request;

/**
 * Class ProvinciaController
 * @package App\Http\Controllers
 */
class ProvinciaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $provincias = Provincia::paginate();

        return view('provincia.index', compact('provincias'))
            ->with('i', (request()->input('page', 1) - 1) * $provincias->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $provincia = new Provincia();
        return view('provincia.create', compact('provincia'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Provincia::$rules);
        $provincia = Provincia::create($request->all());

        return redirect()->route('provincias.index')
            ->with('success', 'Provincia creada con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $provincia = Provincia::find($id);

        return view('provincia.show', compact('provincia'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $provincia = Provincia::find($id);

        return view('provincia.edit', compact('provincia'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Provincia $provincia
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Provincia $provincia)
    {
        request()->validate(Provincia::$rules);

        $provincia->update($request->all());

        return redirect()->route('provincias.index')
            ->with('success', 'Provincia modificada con Exito');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $localidades=Municipio::where('municipios.provincia_id','=',$id)->selectRaw('count(*) as total')->get();
        $tprovincias=0;
        foreach ($localidades as $gas) {
            $tprovincias=$gas->total;
        }
        
        if($tprovincias==0){
            $provincia = Provincia::find($id)->delete();
            $smj='Provincia eliminada con Exito';
        }else{
            $smj='Por favor elimine primero sus localidades';
        }
        
        return redirect()->route('provincias.index')
            ->with('success', $smj);
    }
}
