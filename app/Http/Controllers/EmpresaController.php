<?php

namespace App\Http\Controllers;

use App\Models\Empresa;
use App\Models\Banco;
use App\Models\Cliente;
use App\Models\Gasto;
use Illuminate\Http\Request;

/**
 * Class EmpresaController
 * @package App\Http\Controllers
 */
class EmpresaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $empresas = Empresa::paginate();

        return view('empresa.index', compact('empresas'))
            ->with('i', (request()->input('page', 1) - 1) * $empresas->perPage());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empresa = new Empresa();
        $bancos=Banco::all()->pluck('nombre','id');
        return view('empresa.create', compact('empresa','bancos'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate(Empresa::$rules);

        $empresa = Empresa::create($request->all());

        return redirect()->route('empresas.index')
            ->with('success', 'Empresa creada con Exito.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $empresa = Empresa::find($id);

        return view('empresa.show', compact('empresa'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $empresa = Empresa::find($id);
        $bancos=Banco::all()->pluck('nombre','id');
        return view('empresa.edit', compact('empresa','bancos'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  Empresa $empresa
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Empresa $empresa)
    {
        request()->validate(Empresa::$rules);

        $empresa->update($request->all());

        return redirect()->route('empresas.index')
            ->with('success', 'Empresa modificada con Exito');
    }

    /**
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy($id)
    {
        $clientes=Cliente::where('clientes.empresa_id','=',$id)->selectRaw('count(*) as tclientes')->get();
        $tclientes=0;
        foreach ($clientes as $cli) {
            $tclientes=$cli->tclientes;
        }
        
        $gastos=Gasto::where('gastos.empresa_id','=',$id)->selectRaw('count(*) as total')->get();
        $tgastos=0;
        foreach($gastos as $gas) {
            $tgastos=$cli->total;
        }
        if($tclientes==0 and $tgastos==0){
            $empresa = Empresa::find($id)->delete();
            $smj='Empresa eliminada con Exito';
        }elseif($tclientes>0){
            $smj='No se puede eliminar porque tiene Clientes asociados';
        }elseif($tgastos>0){
            $smj='No se puede eliminar porque tiene Gastos asociados';
        }

        return redirect()->route('empresas.index')
            ->with('success', $smj);
    }
}
