<?php

use App\Models\Prestamo;
use App\Models\Vehiculo;
use App\Models\Cliente;
use App\Models\Pago;
use App\Models\Extensione;
use Illuminate\Support\Facades\DB;
Use Illuminate\Database\Query\Builder;

if (! function_exists('current_user')) {
    function current_user()
    {
        return auth()->user();
    }
}

 function traducir($num)
    {
    $partes=explode('.',$num);
    $s=$partes[0];
    if (strlen($s)>12)
      die('Hasta 12 dígitos');
    $entera=traduccionParcial($s);
    if (count($partes)>1)
    {
        $deci='';

        if(substr($partes[1],0,2)!='00'){
        $deci=traduccionParcial(substr($partes[1],0,2));
        $entera=$entera.' con '.$deci;
        }
    }
    return ucfirst($entera);
  }

  function traduccionParcial($s)
  {
    settype($s,'string');
    $faltan=strlen($s) % 3;
    $cad='';
    if ($faltan!=0)
      $faltan=3-$faltan;
    for($f=1;$f<=$faltan;$f++)
    {
      $cad.='0';
    }
    $s=$cad.$s;
    if (strlen($s)<=3 && $s[0]==0 && $s[1]==0 && $s[2]==0)
      $resu='cero';
    else
    {
      $cad1=substr($s,strlen($s)-3,3);
      $resu=convertir($cad1);
    }
    if (strlen($s)>3)
    {
      $resu2='';
      $cad2=substr($s,strlen($s)-6,3);
      if ($cad2[0]==0 && $cad2[1]==0 && $cad2[2] ==1)
  $resu2='mil ';
      else
        if ($cad2[0]!=0 || $cad2[1]!=0 || $cad2[2] !=0)
          $resu2=convertir($cad2,2).'mil ';
      $resu=$resu2.$resu;
    }
    if (strlen($s)>6)
    {
      $resu2='';
      $cad3=substr($s,strlen($s)-9,3);
      if ($cad3[0]=='0' && $cad3[1]=='0' && $cad3[2]==1)
  $resu2='un millón ';
      else
    if ($cad3[0]!=0 || $cad3[1]!=0 || $cad3[2] !=0)
          $resu2=convertir($cad3,2).'millones ';
      $resu=$resu2.$resu;
    }

    if (strlen($s)>9)
    {
      $resu2='';
      $cad4=substr($s,strlen($s)-12,3);

      if ($cad4[0]=='0' && $cad4[1]=='0' && $cad4[2]==1)
      {
  if ($cad3[0]==0 && $cad3[1]==0 && $cad3[2]==0)
    $resu2='mil millones ';
  else
    $resu2='mil ';
      }
      else
  $resu2=convertir($cad4,2).'mil millones ';
      $resu=$resu2.$resu;
    }
    return trim($resu);
  }

  function convertir($num,$ind=1)
  {
    $cad='';
    if ($num[0]==1 && $num[1]==0 && $num[2]==0)
    {
       return 'cien ';
    }
    switch ($num[0]){
           case 1:$cad.='ciento ';break;
     case 2:$cad.='doscientos ';break;
     case 3:$cad.='trescientos ';break;
     case 4:$cad.='cuatrocientos ';break;
     case 5:$cad.='quinientos ';break;
     case 6:$cad.='seiscientos ';break;
     case 7:$cad.='setecientos ';break;
     case 8:$cad.='ochocientos ';break;
     case 9:$cad.='novecientos ';break;
    }
    switch ($num[1]) {
    case 3:$cad.='treinta ';break;
    case 4:$cad.='cuarenta ';break;
    case 5:$cad.='cincuenta ';break;
    case 6:$cad.='sesenta ';break;
    case 7:$cad.='setenta ';break;
    case 8:$cad.='ochenta ';break;
    case 9:$cad.='noventa ';break;
    }
    if ($num[2]>=0 && $num[1]==1)
    {
    switch ($num[1].$num[2]) {
          case 10:$cad.='diez ';break;
          case 11:$cad.='once ';break;
    case 12:$cad.='doce ';break;
    case 13:$cad.='trece ';break;
    case 14:$cad.='catorce ';break;
    case 15:$cad.='quince ';break;
    case 16:$cad.='dieciseis ';break;
    case 17:$cad.='diecisiete ';break;
    case 18:$cad.='dieciocho ';break;
    case 19:$cad.='diecinueve ';break;
    }
    return $cad;
    }
    if ($num[2]>=0 && $num[1]==2)
    {
    switch ($num[1].$num[2]) {
      case 20:$cad.='veinte ';break;
      case 21:if ($ind==1) $cad.='veintiuno '; else $cad.='veintiun ';break;
      case 22:$cad.='veintidos ';break;
      case 23:$cad.='veintitrés ';break;
      case 24:$cad.='veinticuatro ';break;
      case 25:$cad.='veinticinco ';break;
      case 26:$cad.='veintiseis ';break;
      case 27:$cad.='veintisiete ';break;
      case 28:$cad.='veintiocho ';break;
      case 29:$cad.='veintinueve ';break;

    }
    return $cad;
    }
    if ($num[2]!=0 && $num[1]!=0)
    {
      if ($num[0]>0 || $num[1]>0)
  $cad.='y ';
    }
    if ($num[1]!=1)
    {
      switch ($num[2]) {
          case 1:if ($ind==1) $cad.='uno ';else $cad.='un ';break;
    case 2:$cad.='dos ';break;
    case 3:$cad.='tres ';break;
    case 4:$cad.='cuatro ';break;
    case 5:$cad.='cinco ';break;
    case 6:$cad.='seis ';break;
    case 7:$cad.='siete ';break;
    case 8:$cad.='ocho ';break;
    case 9:$cad.='nueve ';break;
      }
    }
    return $cad;
  }

  function calculaImpago($pagado,$cuota,$firma,&$cuantos,$finicio,$residual){
    //calculamos los meses/periodos que han transcurrido
    $pagadot=$pagado;
    $date1=new DateTime($finicio);
    $date2=now();
    $interval=$date2->diff($date1);
    if($date1>$date2)
    $periodos=0;
    else
      $periodos=$interval->format("%m")+1;
    //echo $periodos;
    if($residual==0){
      $cuantos=($periodos*$cuota);
      //$periodos=$periodos-intval(($pagadot/$cuota));
      return $periodos;
    }else{
      if($pagadot==$residual){
        $cuantos=($periodos*$cuota)+$residual;
        //$periodos=$periodos-intval(($pagadot/$cuota));
      }else{
        if($pagadot==0)
          $cuantos=(($periodos-1)*$cuota)+ $residual;
        else{
          //$periodos=$periodos-intval(($pagadot/$cuota));
          $cuantos=$cuantos+$residual;
        }
      }
      return intval($periodos);
    }
  }



function amortizacion($cursor,&$comp){
  $pagos=array();
  $mtotal_pago=buscapagos($cursor->id,$pagos); //monto total en euros de pagos
  $extensiones=array();
  $mtotal_extensiones=buscaextensiones($cursor->id,$extensiones); //monto total en euros de extensiones
  $npagos=count($pagos);
  $nextensiones=count($extensiones);
  $falta_pagar=0;
  $cuotas=$cursor->cuotas;
  //$periodo=$cursor->ffirma;

  $periodo=new Datetime($cursor->finiciopago);
  $periodo=date_add($periodo,date_interval_create_from_date_string("-1 month"));
  $periodo=date_format($periodo,'Y-m-d');
  $capital_pagado=0;
  $capital_resta=$cursor->importe;

  $arranchos=array('25px;','48px;','60px;','60px;','55px;','70px;','110px;','100px;');
  /****** Encabezado de la tabla */
  $cadena='<table  class="amor" >
    <tr>
        <th class="borde-doble" style="width: '. $arranchos[0].'">
            Num
        </th>
        <th class="borde-doble" style="width: '. $arranchos[1].'">
            Período
        </th>
        <th class="borde-doble" style="width: '. $arranchos[2].'">
            Capital
        </th>
        <th class="borde-doble" style="width: '. $arranchos[3].'">
            Interés
        </th>
        <th class="borde-doble" style="width: '. $arranchos[4].'">
            IVA
        </th>
        <th class="borde-doble" style="width: '. $arranchos[5].'">
            Total <br>a pagar
        </th>
        <th class="borde-doble" style="width: '. $arranchos[6].'">
            Cap.<br> Amortizado
        </th>
        <th class="borde-doble" style="width: '. $arranchos[7].'">
            Cap. <br>Pendiente
        </th>
    </tr>
    </table>
  <table class="amor">';
  /****** Fin del Encabezado de la tabla */
        if($cursor->tipo==0){
            $capital_resta=$cursor->importe;
        }
        $suma_interes=0;
        $suma_capital=0;
        $suma_iva=0;
        $suma_cuota=0;
   //calculamos por las formula la cuota
   $j=$cursor->porcentaje/100;
   //Cuota mensual = (P * i) / (1 - (1 + i) ^ (-n))
  if($cursor->tipo==0)
    $cuota_calculada=($cursor->importe*$j)/(1-((1+$j)**-$cursor->cuotas));
  else
    $cuota_calculada=0;
   //sacamos la diferencia entre la cuota asignada por el usuario
   $diferencia_cuota=$cursor->cuota-$cuota_calculada;

   $fecha=date_create($periodo);
  /***** Inicio de las amortizaciones */
   for($i=1;$i<=$cuotas;$i++){
        if($cursor->tipo==1){
            if($i==1){
                $interes=$cursor->residual/(($cursor->piva/100)+1);
                $iva=$cursor->residual-$interes;
            }else{
                $iva=$cursor->iva;
                $interes=$cursor->cuota;
            }
            $capital=0;
            $capital_pagado=0;
            $capital_resta=$cursor->importe;
        }else{
            $interes=($capital_resta*$j)+$diferencia_cuota;
            $capital=$cursor->cuota-$interes;
            $iva=($interes) * $cursor->piva/100;
            //cuadrar el iva para solo el interés
            $subtotal=$capital+$iva+$interes;
            $subtotal=$cursor->tcuota-$subtotal;
            $interes=$interes+($subtotal/(1+($cursor->piva/100)));
            $iva=($interes) * $cursor->piva/100;
            //$capital=$capital+($cursor->tcuota-($capital+$iva+$interes));

            //$capital
            if($i==$cuotas and !$capital_resta==0){
                $capital=$cap_ant;
                $capital_pagado=$capital_pagado+$capital;
                $interes=$cuota-$capital;

                $iva=($interes) * $cursor->piva/100;
            //cuadrar el iva para solo el interés
            $subtotal=$capital+$iva+$interes;
            $subtotal=$cursor->tcuota-$subtotal;
            $interes=$interes+($subtotal/(1+($cursor->piva/100)));
            $iva=($interes) * $cursor->piva/100;



                $capital_resta=$capital_resta-$capital;
            }else{
                $capital_pagado=$capital_pagado+$capital;
                $capital_resta=$capital_resta-$capital;
            }

            //$iva=$cursor->iva;


            $cuota=$cursor->cuota;

            //$capital_resta=$cursor->importe-$capital_pagado;
        }
        $total=$capital+$interes+$iva;
        $claser='';
        $clasec='';
        $clasei='';
      if($capital_resta<0){
        $comp[0]=1;
        $claser=' rojo ';
      }
      if($capital<0){
        $clasec=' rojo ';
        $comp[1]=1;
      }
      if($interes<0){
        $comp[2]=1;
        $clasei=' rojo ';
      }

    $cadena.='
    <tr>
        <td  class="alinea_derecha" style="width:'.$arranchos[0].'">'.
            $i.'
        </td>
        <td  class="alinea_derecha" style="width:'. $arranchos[1].'">'.
         date_format($fecha,'m/Y') .
        '</td>
        <td  class="alinea_derecha'.$clasec.' " style="width:'.$arranchos[2].'">'.
            number_format($capital,2,',','.')
        .'</td>
        <td class="alinea_derecha'.$clasei.'" style="width:'.$arranchos[3].'">'.
            number_format($interes,2,',','.')
        .'</td>
        <td class="alinea_derecha" style="width:'.$arranchos[4].'">'.
            number_format($iva,2,',','.')
        .'</td>
        <td class="alinea_derecha" style="width:'. $arranchos[5].'">'.
            number_format($total,2,',','.')
        .'</td>
        <td class="alinea_derecha" style="width:'. $arranchos[6].'">'.
            number_format($capital_pagado,2,',','.')
        .'</td>
        <td  class="alinea_derecha'.$claser.'" style="width:'. $arranchos[7].'">'.
            number_format($capital_resta,2,',','.');
            $cap_ant=$capital_resta;

        $cadena.='</td>    </tr>';
        /***** Totalizaciones */
        $periodo=date_format($fecha,'Y-m-d');
        $suma_interes=$suma_interes+$interes;
        $suma_capital=$suma_capital+$capital;
        $suma_iva=$suma_iva+$iva;
        $suma_cuota=$suma_cuota+$cursor->tcuota;
        /***** Fin de Totalizaciones */
        $fecha=agrega_periodo($periodo);
    }
    /***** Fin de las amortizaciones */

    /***** Totales */
    $cadena.='<tr>
        <td colspan="2" class="totales">
            Totales:......
        </td>
        <td class="totales">'.
            number_format($suma_capital,2,',','.')
        .'</td>
        <td class="totales">'.
            number_format($suma_interes,2,',','.')
        .'</td>'
        .'<td class="totales">'.
            number_format($suma_iva,2,',','.')
        .'</td>'
        .'<td class="totales">'.
            number_format($suma_cuota,2,',','.')
        .'</td></tr></table>';
        /***** Fin de Totales */
        return $cadena;
}

function agrega_periodo($periodo){
      $fecha=date_create($periodo);
      $fecha=date_add($fecha,date_interval_create_from_date_string("1 month"));
      return $fecha;
}

function cuenta_faltantes($tabla, $id, $completo,&$cadena=array(),$i=0){
$cadena[$i]='';
  $falta=0;
  $vienen=0;
  if($tabla=='prestamos'){
    $prestamos=Prestamo::where('prestamos.id','=',$id)->get();

    foreach($prestamos as $prestamo){
      //0:amortizando 1:empeño
      if(!is_null($prestamo->tipo)){
        if($prestamo->tipo=='0'){
          if(is_null($prestamo->porcentaje) or $prestamo->porcentaje==0)
            {$falta=1;
            $cadena[$i].='Porcentaje<br>';}
          if(is_null($prestamo->interes) or $prestamo->interes==0)
          {$falta=1;
            $cadena[$i].='Interes<br>';}
          if(is_null($prestamo->cuotas) or $prestamo->cuotas==0)
          {$falta=1;
            $cadena[$i].='Cuotas<br>';}
        }else{
          if(is_null($prestamo->residual))
          {$falta=1;
            $cadena[$i].='Cuota Residual<br>';}
        }
        if(is_null($prestamo->ffirma))
        {$falta=1;
            $cadena[$i].='F. de firma<br>';}
        if(is_null($prestamo->finiciopago))
        {$falta=1;
            $cadena[$i].='F. Inicio pago<br>';};
        if(is_null($prestamo->importe) or $prestamo->importe==0)
        {$falta=1;
            $cadena[$i].='Importe<br>';}
        if(is_null($prestamo->gastos))
        {$falta=1;
            $cadena[$i].='Gasto<br>';}
        if(is_null($prestamo->tcuota) or $prestamo->tcuota==0)
        {$falta=1;
            $cadena[$i].='Cuota<br>';}
        if(is_null($prestamo->cuota) or $prestamo->cuota==0)
            $falta=1;
        //if(is_null($prestamo->piva) or $prestamo->piva==0)
          //  $falta=1;
        //if(is_null($prestamo->iva) or $prestamo->iva==0)
            //$falta=1;
        $vienen++;
      }else{
        $falta=1;
        $vienen=0;
      }

    }
  }elseif($tabla=='vehiculos'){
    $vehiculos=Vehiculo::where('vehiculos.id','=',$id)->get();
    foreach($vehiculos as $vehiculo){
    if(!is_null($vehiculo->id)){
      if(is_null($vehiculo->fmatricula))
        {$falta=1;
        $cadena[$i].='F. Matrícula<br>';}
      if(is_null($vehiculo->marca_id))
      {$falta=1;
        $cadena[$i].='Marca<br>';}
      if(is_null($vehiculo->modelo_id))
      {$falta=1;
        $cadena[$i].='Modelo<br>';}
      if(is_null($vehiculo->matricula))
      {$falta=1;
        $cadena[$i].='Matrícula<br>';}

      if($completo==1){
        if(is_null($vehiculo->itv))
        {$falta=1;
            $cadena[$i].='ITV<br>';}
        if(is_null($vehiculo->poliza))
        {$falta=1;
            $cadena[$i].='Póliza<br>';}
        if(is_null($vehiculo->empseguro_id))
        {$falta=1;
            $cadena[$i].='Empresa de Seguro<br>';}

        //if(is_null($vehiculo->tipo_id))
          //$falta=1;
        if(is_null($vehiculo->finicseguro))
        {$falta=1;
            $cadena[$i].='F. Inicio Póliza<br>';}
        if(is_null($vehiculo->fvencseguro))
        {$falta=1;
            $cadena[$i].='F. Venc. Póliza<br>';}
        /* if(is_null($vehiculo->cuotaunica))
          $falta=1; */
        if(is_null($vehiculo->frecuencia))
        {$falta=1;
            $cadena[$i].='Frecuencia de la Póliza<br>';}
      }

      if(is_null($vehiculo->valoracion))
      {$falta=1;
        $cadena[$i].='Valoración<br>';}
      if(is_null($vehiculo->situacion_id))
      {$falta=1;
        $cadena[$i].='Situación del Vehículo<br>';}
      if(is_null($vehiculo->bastidor))
      {$falta=1;
        $cadena[$i].='Bastidor<br>';}
      if(is_null($vehiculo->kilometros))
      {$falta=1;
        $cadena[$i].='Kilometraje<br>';}
        $vienen++;
    }else{
      $vienen=0;
      $falta=1;
    }
    }
    }elseif($tabla=='clientes'){
    $clientes=Cliente::where('clientes.id','=',$id)->get();
    foreach($clientes as $cliente){
      if(is_null($cliente->nombre))
        {$falta=1;
        $cadena[$i].='Nombre<br>';}
        if(is_null($cliente->dni))
        {$falta=1;
            $cadena[$i].='DNI<br>';}
        //if($completo==1){
          if(is_null($cliente->fcarnet))
          {$falta=1;
            $cadena[$i].='Carnet de Conductor<br>';}
        //}
        if(is_null($cliente->sexo))
        {$falta=1;
            $cadena[$i].='Sexo<br>';}
        if(is_null($cliente->provincia_id))
        {$falta=1;
            $cadena[$i].='Provincia<br>';}
        if(is_null($cliente->municipio_id))
        {$falta=1;
            $cadena[$i].='Municipio<br>';}
        if(is_null($cliente->direccion))
        {$falta=1;
            $cadena[$i].='Dirección<br>';}
        if(is_null($cliente->codpostal))
        {$falta=1;
            $cadena[$i].='Código Postal<br>';}
        if(is_null($cliente->telefono))
        {$falta=1;
            $cadena[$i].='Teléfono<br>';}
        if(is_null($cliente->email))
        {    $cadena[$i].='Email<br>';}
      $vienen++;
    }
  }
  if($vienen==0)
    $falta=1;
  return $falta;
}
function resumen_faltante($tabla){
  if($tabla=='clientes'){
    $tablas=Cliente::select('clientes.*','clientes.id as ids')->get();
  }elseif($tabla=='vehiculos'){
    $tablas = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
      ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
      ->select('vehiculos.id as ids')
      ->get();
      /*
        foreach($tablas as $reg){
          echo $reg->ids."<br>";
        }
        exit();
        */
  }elseif($tabla=='prestamos'){
    $tablas = Cliente::leftjoin('vehiculos','vehiculos.cliente_id','=','clientes.id')
      ->leftjoin('prestamos','prestamos.vehiculo_id','=','vehiculos.id')
      ->select('prestamos.id as ids')
      ->get();
  }
  $cuantos=0;
  foreach($tablas as $reg){
      $falta=cuenta_faltantes($tabla, $reg->ids,1);
    if($falta==1)
      $cuantos++;

  }

  return $cuantos;
}
function cuenta_faltantes_cliente($id){
  $clientes=Cliente::where('clientes.id','=',$id)->get();
  $falta=0;
    foreach($clientes as $cliente){
        if(is_null($cliente->nombre) and is_null($cliente->nombre2) and is_null($cliente->proempŕesa))
        $falta=1;
        /*
        if(is_null($cliente->dni) and is_null($cliente->dni2) and is_null($cliente->dnicif))
        $falta=1;
        */
        if(is_null($cliente->provincia_id))
        $falta=1;
        if(is_null($cliente->municipio_id))
        $falta=1;
        if(is_null($cliente->direccion))
        $falta=1;
        if(is_null($cliente->codpostal))
        $falta=1;
        if(is_null($cliente->telefono))
          $falta=1;
        if(is_null($cliente->sexo))
          $falta=1;
        //if(is_null($cliente->fcarnet))
          //$falta=1;
        if(!is_null($cliente->nombre2)){
          if(is_null($cliente->sexo2)){
            $falta=1;
          }
        }

        //if(is_null($cliente->email))
        //$falta=1;
    }
    return $falta;
}
function cuenta_faltantes_vehiculo($id){
  $vehiculos=Vehiculo::where('vehiculos.id','=',$id)->get();
  $falta=0;
    foreach($vehiculos as $vehiculo){
      if(is_null($vehiculo->fmatricula))
        $falta=1;
      if(is_null($vehiculo->marca_id))
        $falta=1;
      if(is_null($vehiculo->modelo_id))
        $falta=1;
      if(is_null($vehiculo->matricula))
        $falta=1;
        /*if(is_null($vehiculo->itv))
          $falta=1;
        if(is_null($vehiculo->poliza))
          $falta=1;
        if(is_null($vehiculo->empseguro_id))
          $falta=1;

        //if(is_null($vehiculo->tipo_id))
          //$falta=1;
        if(is_null($vehiculo->finicseguro))
          $falta=1;
        if(is_null($vehiculo->fvencseguro))
          $falta=1;
        if(is_null($vehiculo->cuotaunica))
          $falta=1;
        if(is_null($vehiculo->frecuencia))
          $falta=1;
      */
      if(is_null($vehiculo->valoracion))
        $falta=1;
      if(is_null($vehiculo->situacion_id))
        $falta=1;
      if(is_null($vehiculo->bastidor))
        $falta=1;
      /*
      if(is_null($vehiculo->kilometros))
        $falta=1;
      */
    }
    return $falta;
}
function cuenta_faltantes_prestamo($id){
 $prestamos=Prestamo::where('prestamos.id','=',$id)->get();
 $falta=0;
    foreach($prestamos as $prestamo){
      //0:amortizando 1:empeño
      if(!is_null($prestamo->tipo)){
        if($prestamo->tipo=='0'){
          if(is_null($prestamo->porcentaje) or $prestamo->porcentaje==0)
            $falta=1;
          if(is_null($prestamo->interes) or $prestamo->interes==0)
            $falta=1;
          if(is_null($prestamo->cuotas) or $prestamo->cuotas==0)
            $falta=1;
        }else{
          if(is_null($prestamo->residual))
            $falta=1;
        }
        if(is_null($prestamo->ffirma))
            $falta=1;
        if(is_null($prestamo->finiciopago))
            $falta=1;
        if(is_null($prestamo->importe) or $prestamo->importe==0)
            $falta=1;
        if(is_null($prestamo->gastos))
            $falta=1;
        if(is_null($prestamo->tcuota) or $prestamo->tcuota==0)
            $falta=1;
        if(is_null($prestamo->cuota) or $prestamo->cuota==0)
            $falta=1;
        //if(is_null($prestamo->piva) or $prestamo->piva==0)
          //  $falta=1;
        if(is_null($prestamo->iva) or $prestamo->iva==0)
            $falta=1;

      }else{
        $falta=1;
      }
    }
    return $falta;
}
function etiqueta_periodos($debe_cuotas,$finicio){
  $arr_periodos=array();
  $meses=array('12','01','02','03','04','05','06','07','08','09','10','11');
  $ano_inicio=date('Y',strtotime($finicio));
  $mes_inicio=date('m',strtotime($finicio));
  $mes=$mes_inicio;
  $ano=$ano_inicio;
  if($mes==0){
    $mes=12;
    $ano=$ano-1;
  }

  for($r=0;$r<$debe_cuotas;$r++){
    $arr_periodos[]=$meses[$mes-1].'/'.$ano;
    $mes=$mes+1;
    if($mes>12){
      $ano=$ano+1;
      $mes=1;
    }
  }
  return $arr_periodos;
}

function indicames(&$nmes,&$nano){
  if((int)$nmes==1){
    $nmes=12;
    $nano=$nano-1;
  }else{
    $nmes=$nmes-1;
  }
  if($nmes<10)
      $nmes='0'.$nmes;
}
function acomoda_cuotas($id,$finiciopago,$cuotas){
    //busco si tiene estensiones
    $extensiones=Prestamo::where('padre','=',$id)
        ->select('finiciopago')->get();
    $fechas=array();
    $nextensiones=0;
    $i=0;
    $meses=0;
    foreach($extensiones as $ext){
        $fechas[$i]=$ext->fecha;
        $nextensiones++;
        if($i>0)
            $meses=$meses+diferencia_meses($fechas[$i-1],$ext->fecha);
        else
            $meses=$meses+diferencia_meses($finiciopago,$ext->fecha);
        $i++;
    }

    $ncuotas=$cuotas+$meses;
    //echo $ncuotas;exit();
    $periodos=array();
    $periodo=new DateTime($finiciopago);
    $periodo=date_add($periodo,date_interval_create_from_date_string("-1 month"));
    $periodo=date_format($periodo,'Y-m-d');
    //echo $periodo;exit();
    $periodos['--/----']='--/----';
    for($i=1;$i<=$ncuotas;$i++){
        $periodo2=date('Y-m-d',strtotime($periodo));
        $periodo3=new Datetime($periodo);
        $periodos[date_format($periodo3,'m/Y')]=date_format($periodo3,'m/Y');
        date_add($periodo3,date_interval_create_from_date_string("1 month"));

        $periodo=date_format($periodo3,'Y-m-d');
    }
    return $periodos;
}
function diferencia_meses($inicio,$fin){
    $datetime1=new DateTime($inicio);
    $datetime2=new DateTime($fin);
    # obtenemos la diferencia entre las dos fechas
    $interval=$datetime2->diff($datetime1);
    $intervalMeses=$interval->format("%m");
    return $intervalMeses;
}
function buscapagos($id,&$cursor){
  $pagos=Pago::where('pagos.prestamo_id','=',$id)
  ->whereIn('motivo', ['0', '1'])
  ->select('fpago','monto')
  ->orderBy('fpago','asc')
  ->get();

  $i=0;
  $total_pago=0;
  foreach($pagos as $pago){
    $cursor[$i]['num']=$i;
    $cursor[$i]['fpago']=$pago->fpago;
    $cursor[$i]['monto']=$pago->monto;
    $cursor[$i]['marca']='';
    $total_pago=$total_pago+$pago->monto;
    $i++;
  }
  return $total_pago;
}
function buscaextensiones($id,&$cursor){

  $extensiones=Extensione::where('extensiones.prestamo_id','=',$id)
  ->Where('extensiones.fecha','=',function (Builder $query) use($id) {
      $query->select(DB::raw('max(fecha)'))->where('prestamo_id','=',$id);
    })
  ->select('fecha','monto','tcuota','cuota','iva')
  ->get();

  $i=1;
  $total=0;
  foreach($extensiones as $exte){
    $cursor[$i]['num']=$i;
    $cursor[$i]['fecha']=$exte->fpago;
    $cursor[$i]['monto']=$exte->monto;
    $cursor[$i]['tcuota']=$exte->tcuota;
    $cursor[$i]['cuota']=$exte->cuota;
    $cursor[$i]['iva']=$exte->iva;
    $cursor[$i]['marca']='';
    $total=$total+$exte->monto;
    $i++;
  }
  return $total;
}

function calcula_balance(&$pagos_empeno,&$impago_d){
    $balance=0;
    $balance_tmp=0;
    $cuantos=0;

    for($i=0;$i<count($pagos_empeno);$i++){
      $balance_tmp=($pagos_empeno[$i]['cuota'])-($pagos_empeno[$i]['pago']);
      $pagos_empeno[$i]['balance']=$balance_tmp;

      if($balance_tmp>0){
        $cuantos++;
      }
      $balance=$balance+$pagos_empeno[$i]['balance'];
    }

    $impago_d=$cuantos;
    return $balance;
  }
  function compara_fecha($a, $b){
    //return strtotime(($a['fecha'])) > strtotime(($b['fecha']));
    return (float)$a['periodo']>(float)$b['periodo'];
  }
  function calcula_deuda_extensiones(&$arr_Gdebe_cuotas,&$debe,&$ndebe_cuotas,&$cuantos_pagos,&$arr_debe_cuotas,
  $arr_pagos,$prestamo_id,$piva){
    //igualamos la cantidad de pagos con las cantidad de cuotas a pagar a la fecha
    $ncuotas=count($arr_debe_cuotas);
    $ncuotas2=count($arr_Gdebe_cuotas);
    $npagos=count($arr_pagos);
    $mcuotas=0;
    $mpagos=0;
    $a=$ncuotas2+1;
    $ndebe_cuotas=0;$cuantos_pagos=0;

    for($i=0;$i<$ncuotas; $i++) {
      if($arr_debe_cuotas[$i]['prestamo_id']==$prestamo_id){
        $arr_Gdebe_cuotas[$a]['periodo']=$arr_debe_cuotas[$i]['periodo'];
        $arr_Gdebe_cuotas[$a]['prestamo_id']=$arr_debe_cuotas[$i]['prestamo_id'];
        $arr_Gdebe_cuotas[$a]['fecha']=$arr_debe_cuotas[$i]['fecha'];
        $arr_Gdebe_cuotas[$a]['nano']=$arr_debe_cuotas[$i]['nano'];
        $arr_Gdebe_cuotas[$a]['nmes']=$arr_debe_cuotas[$i]['nmes'];
        $arr_Gdebe_cuotas[$a]['descripcion']=$arr_debe_cuotas[$i]['descripcion'];
        $arr_Gdebe_cuotas[$a]['tcuota']=$arr_debe_cuotas[$i]['tcuota'];
        $arr_Gdebe_cuotas[$a]['piva']=$piva;

        $arr_Gdebe_cuotas[$a]['mpago']=0;
        $arr_Gdebe_cuotas[$a]['balance']=0;
        $arr_Gdebe_cuotas[$a]['fpago']='';
        $arr_Gdebe_cuotas[$a]['mpago_dt']='';
        $ndebe_cuotas++;
      }
        for ($j=0; $j<$npagos; $j++) {
          if($arr_pagos[$j]['prestamo_id']==$prestamo_id){
              //if( $arr_pagos[$j]['nano']==$arr_debe_cuotas[$i]['nano']
              //and $arr_pagos[$j]['nmes']==$arr_debe_cuotas[$i]['nmes']
              //)
              if($arr_pagos[$j]['periodo']==$arr_debe_cuotas[$i]['periodo']){

                $mcuotas=$mcuotas+$arr_debe_cuotas[$i]['tcuota'];
                $arr_Gdebe_cuotas[$a]['mpago']=$arr_Gdebe_cuotas[$a]['mpago']+$arr_pagos[$j]['monto'];
                $arr_Gdebe_cuotas[$a]['fpago']=$arr_Gdebe_cuotas[$a]['fpago'].date('d/m/Y',strtotime($arr_pagos[$j]['fecha'])).'<br>';
                //.' - '. number_format($arr_pagos[$j]['monto'],2,',','.').'<br>';
                $arr_Gdebe_cuotas[$a]['mpago_dt']=$arr_Gdebe_cuotas[$a]['mpago_dt'].number_format($arr_pagos[$j]['monto'],2,',','.').'<br>';
                $arr_Gdebe_cuotas[$a]['piva']=$piva;
                $mpagos=$mpagos+$arr_pagos[$j]['monto'];
                $cuantos_pagos++;

             }
          }
      }
      $arr_Gdebe_cuotas[$a]['balance']=$arr_Gdebe_cuotas[$a]['tcuota']-$mpagos;
      $mpagos=0;
      $a++;
    //}
  }
//$ndebe_cuotas=$ndebe_cuotas-$cuantos_pagos;
  $debe=$mcuotas-$mpagos;
}


function arregla_extensiones(&$pagos_empeno,$arr_extensiones,$prestamo_id,$numero_extensiones){
    $extensiones=count($arr_extensiones);
    $pagos=count($pagos_empeno);
    for ($i=0; $i<$extensiones; $i++) {
            $nmes=date('m',strtotime($arr_extensiones[$i]['fecha']));
            $nano=date('Y',strtotime($arr_extensiones[$i]['fecha']));
            restames($nano,$nmes);
            $periodo_extension=$nano.$nmes;
        for ($j=0; $j<$pagos; $j++) {
            if($arr_extensiones[$i]['prestamo_id']==$prestamo_id){
                if($pagos_empeno[$j]['periodo']>=$periodo_extension){
                $pagos_empeno[$j]['cuota']=$arr_extensiones[$i]['tcuota'];
                }
            }
        }
    }
    //exit();
}
function restames(&$nano,&$nmes){
    $mes=$nmes-1;
    if($mes==0){
      $nano=$nano-1;
      $nmes='12';
    }else{
      $nmes=$mes;
      if($nmes<10)
        $nmes='0'.$nmes;
    }
  }

function sumames(&$nano,&$nmes){
  $mes=$nmes+1;
  if($mes==13){
    $nano=$nano+1;
    $nmes='01';
  }else{
    $nmes=$mes;
    if($nmes<10)
      $nmes='0'.$nmes;
  }
}
function calcula_deuda_futura($cursor,$pagos_empeno,$balance){
    $ultimo=count($pagos_empeno);
    /* if(($ultimo % 2)==0)
        $ultimo=$ultimo-1; */
    if($cursor->tipo=='1'){
        $monto=$cursor->importe;
    }else{
        $calculado=0;
        amortizacion_cuotas_restante($cursor,$calculado,$ultimo);
        $monto=$calculado-$balance;
        //$monto=$ultimo;
    }
    return $monto;
}
function amortizacion_extension($cursor,&$comp,$extension){
   $cuotas=$cursor->cuotas;
   //$cuotas=16;
    $periodo=$extension->fecha;

    $capitalf=$cuotas*$extension->cuota;
    //$interes=$capitalf-$extension->monto;
    //$capital_resta=$capitalf;
   //tasa = ((capitalf/capitali)**(1/cuotas))-1;
    $j = ((($capitalf/($extension->monto-240))**(1/$cuotas))-1);
    echo $j;
    //Cuota mensual = (P * i) / (1 - (1 + i) ^ (-n))
    //echo $porcentaje;


    $pagos=array();
    //$mtotal_pago=buscapagos($cursor->id,$pagos); //monto total en euros de pagos

    //$npagos=count($pagos);
    //$nextensiones=count($extensiones);
    $falta_pagar=0;
    $capital_pagado=0;
    $capital_resta=$extension->monto-240;

    $arranchos=array('25px;','48px;','60px;','60px;','55px;','70px;','110px;','100px;');
    /****** Encabezado de la tabla */
    $cadena='<table  class="amor" >
      <tr>
          <th class="borde-doble" style="width: '. $arranchos[0].'">
              Num
          </th>
          <th class="borde-doble" style="width: '. $arranchos[1].'">
              Período
          </th>
          <th class="borde-doble" style="width: '. $arranchos[2].'">
              Capital
          </th>
          <th class="borde-doble" style="width: '. $arranchos[3].'">
              Interés
          </th>
          <th class="borde-doble" style="width: '. $arranchos[4].'">
              IVA
          </th>
          <th class="borde-doble" style="width: '. $arranchos[5].'">
              Total <br>a pagar
          </th>
          <th class="borde-doble" style="width: '. $arranchos[6].'">
              Cap.<br> Amortizado
          </th>
          <th class="borde-doble" style="width: '. $arranchos[7].'">
              Cap. <br>Pendiente
          </th>
      </tr>
      </table>
    <table class="amor">';
    /****** Fin del Encabezado de la tabla */
          if($cursor->tipo==0){
              $capital_resta=$extension->monto-240;
          }
          $suma_interes=0;
          $suma_capital=0;
          $suma_iva=0;
          $suma_cuota=0;
     //calculamos por las formula la cuota
     //$j=$cursor->porcentaje/100;
     //Cuota mensual = (P * i) / (1 - (1 + i) ^ (-n))
      $cuota_calculada=(($extension->monto-240)*$j)/(1-((1+$j)**-$cuotas));
     //sacamos la diferencia entre la cuota asignada por el usuario
     $diferencia_cuota=$extension->cuota-$cuota_calculada;

     $fecha=date_create($periodo);
    /***** Inicio de las amortizaciones */
    for($i=1;$i<=$cuotas;$i++){
      $interes=($capital_resta*$j)+$diferencia_cuota;
      $capital=$extension->cuota-$interes;
      $iva=($interes) * $extension->piva/100;
      //cuadrar el iva para solo el interés
      $subtotal=$capital+$iva+$interes;
      $subtotal=$extension->tcuota-$subtotal;
      $interes=$interes+($subtotal/(1+($extension->piva/100)));
      $iva=($interes) * $extension->piva/100;
      $capital=$capital+($extension->tcuota-($capital+$iva+$interes));
      if($i==$cuotas and !$capital_resta==0){
          $capital=$cap_ant;
          $capital_pagado=$capital_pagado+$capital;
          $interes=$extension->cuota-$capital;
          $iva=($interes) * $extension->piva/100;
      //cuadrar el iva para solo el interés
      $subtotal=$extension->$iva+$interes;
      $subtotal=$extension->tcuota-$subtotal;
      $interes=$interes+($subtotal/(1+($extension->piva/100)));
      $iva=($interes) * $extension->piva/100;
      $capital_resta=$capital_resta-$capital;
      }else{
          $capital_pagado=$capital_pagado+$capital;
          $capital_resta=$capital_resta-$capital;
      }
      $cuota=$extension->cuota;
    //$capital_resta=$cursor->importe-$capital_pagado;
      $total=$capital+$interes+$iva;
      $claser='';
      $clasec='';
      $clasei='';
    if($capital_resta<0){
      $comp[0]=1;
      $claser=' rojo ';
    }
    if($capital<0){
      $clasec=' rojo ';
      $comp[1]=1;
    }
    if($interes<0){
      $comp[2]=1;
      $clasei=' rojo ';
    }

      $cadena.='
      <tr>
          <td  class="alinea_derecha" style="width:'.$arranchos[0].'">'.
              $i.'
          </td>
          <td  class="alinea_derecha" style="width:'. $arranchos[1].'">'.
           date_format($fecha,'m/Y') .
          '</td>
          <td  class="alinea_derecha'.$clasec.' " style="width:'.$arranchos[2].'">'.
              number_format($capital,2,',','.')
          .'</td>
          <td class="alinea_derecha'.$clasei.'" style="width:'.$arranchos[3].'">'.
              number_format($interes,2,',','.')
          .'</td>
          <td class="alinea_derecha" style="width:'.$arranchos[4].'">'.
              number_format($iva,2,',','.')
          .'</td>
          <td class="alinea_derecha" style="width:'. $arranchos[5].'">'.
              number_format($total,2,',','.')
          .'</td>
          <td class="alinea_derecha" style="width:'. $arranchos[6].'">'.
              number_format($capital_pagado,2,',','.')
          .'</td>
          <td  class="alinea_derecha'.$claser.'" style="width:'. $arranchos[7].'">'.
              number_format($capital_resta,2,',','.');
              $cap_ant=$capital_resta;

          $cadena.='</td>    </tr>';
          /***** Totalizaciones */
          $periodo=date_format($fecha,'Y-m-d');
          $suma_interes=$suma_interes+$interes;
          $suma_capital=$suma_capital+$capital;
          $suma_iva=$suma_iva+$iva;
          $suma_cuota=$suma_cuota+$extension->tcuota;
          /***** Fin de Totalizaciones */
          $fecha=agrega_periodo($periodo);
      }
      /***** Fin de las amortizaciones */

      /***** Totales */
      $cadena.='<tr>
          <td colspan="2" class="totales">
              Totales:......
          </td>
          <td class="totales">'.
              number_format($suma_capital,2,',','.')
          .'</td>
          <td class="totales">'.
              number_format($suma_interes,2,',','.')
          .'</td>'
          .'<td class="totales">'.
              number_format($suma_iva,2,',','.')
          .'</td>'
          .'<td class="totales">'.
              number_format($suma_cuota,2,',','.')
          .'</td></tr></table>';
          /***** Fin de Totales */
          return $cadena;
  }

function amortizacion_cuotas($cursor,&$precio,&$ivass,&$capitalss,$indice){
  $cuotas=$cursor->cuotas;
  $periodo=new Datetime($cursor->finiciopago);
  $periodo=date_add($periodo,date_interval_create_from_date_string("-1 month"));
  $periodo=date_format($periodo,'Y-m-d');
  $capital_pagado=0;
  $capital_resta=$cursor->importe;
  /****** Encabezado de la tabla */
  /****** Fin del Encabezado de la tabla */
        if($cursor->tipo==0){
            $capital_resta=$cursor->importe;
        }
        $suma_interes=0;
        $suma_capital=0;
        $suma_iva=0;
        $suma_cuota=0;
   //calculamos por las formula la cuota
   $j=$cursor->porcentaje/100;
   //Cuota mensual = (P * i) / (1 - (1 + i) ^ (-n))
  if($cursor->tipo==0)
    $cuota_calculada=($cursor->importe*$j)/(1-((1+$j)**-$cursor->cuotas));
  else
    $cuota_calculada=0;
   //sacamos la diferencia entre la cuota asignada por el usuario
   $diferencia_cuota=$cursor->cuota-$cuota_calculada;
   $fecha=date_create($periodo);
  /***** Inicio de las amortizaciones */
   for($i=1;$i<=$cuotas;$i++){
        if($cursor->tipo==1){
            if($i==1){
                $interes=$cursor->residual/(($cursor->piva/100)+1);
                $iva=$cursor->residual-$interes;
            }else{
                $iva=$cursor->iva;
                $interes=$cursor->cuota;
            }
            $capital=0;
            $capital_pagado=0;
            $capital_resta=$cursor->importe;
        }else{
            $interes=($capital_resta*$j)+$diferencia_cuota;
            $capital=$cursor->cuota-$interes;
            $iva=($interes) * $cursor->piva/100;
            //cuadrar el iva para solo el interés
            $subtotal=$capital+$iva+$interes;
            $subtotal=$cursor->tcuota-$subtotal;
            $interes=$interes+($subtotal/(1+($cursor->piva/100)));
            $iva=($interes) * $cursor->piva/100;
            //$capital=$capital+($cursor->tcuota-($capital+$iva+$interes));
            //$capital
            if($i==$cuotas and !$capital_resta==0){
                $capital=$cap_ant;
                $capital_pagado=$capital_pagado+$capital;
                $interes=$cuota-$capital;
                $iva=($interes) * $cursor->piva/100;
            //cuadrar el iva para solo el interés
            $subtotal=$capital+$iva+$interes;
            $subtotal=$cursor->tcuota-$subtotal;
            $interes=$interes+($subtotal/(1+($cursor->piva/100)));
            $iva=($interes) * $cursor->piva/100;
                $capital_resta=$capital_resta-$capital;
            }else{
                $capital_pagado=$capital_pagado+$capital;
                $capital_resta=$capital_resta-$capital;
            }
            //$iva=$cursor->iva;
            $cuota=$cursor->cuota;
            //$capital_resta=$cursor->importe-$capital_pagado;
        }
        if($i==$indice){
            $ivass=$iva;
            $precio=$interes;
            $capitalss=$capital;
        }
        $total=$capital+$interes+$iva;
       if($capital_resta<0){
        //$comp[0]=1;
      }
      if($capital<0){
        //$comp[1]=1;
      }
      if($interes<0){
        //$comp[2]=1;
      }
        $cap_ant=$capital_resta;
        /***** Totalizaciones */
        $periodo=date_format($fecha,'Y-m-d');
        $suma_interes=$suma_interes+$interes;
        $suma_capital=$suma_capital+$capital;
        $suma_iva=$suma_iva+$iva;
        $suma_cuota=$suma_cuota+$cursor->tcuota;
        /***** Fin de Totalizaciones */
        $fecha=agrega_periodo($periodo);
    }
    /***** Fin de las amortizaciones */
}
function amortizacion_cuotas_restante($cursor,&$total,$indice){
    $cuotas=$cursor->cuotas;
    $periodo=new Datetime($cursor->finiciopago);
    $periodo=date_add($periodo,date_interval_create_from_date_string("-1 month"));
    $periodo=date_format($periodo,'Y-m-d');
    $capital_pagado=0;
    $capital_resta=$cursor->importe;
    /****** Encabezado de la tabla */
    /****** Fin del Encabezado de la tabla */
    $capital_resta=$cursor->importe;

    $suma_interes=0;
    $suma_capital=0;
    $suma_iva=0;
    $suma_cuota=0;
     //calculamos por las formula la cuota
     $j=$cursor->porcentaje/100;
     $cuota_calculada=0;
     //Cuota mensual = (P * i) / (1 - (1 + i) ^ (-n))
        //if((1-((1+$j)**(-$cursor->cuotas)))>0){
            $cuota_calculada=($cursor->importe*$j)/ (1-((1+$j)**(-$cursor->cuotas)));
        //}
     //sacamos la diferencia entre la cuota asignada por el usuario
     $diferencia_cuota=$cursor->cuota-$cuota_calculada;
     $fecha=date_create($periodo);
    /***** Inicio de las amortizaciones */
     for($i=1;$i<=$cuotas;$i++){
               $interes=($capital_resta*$j)+$diferencia_cuota;
              $capital=$cursor->cuota-$interes;
              $iva=($interes) * $cursor->piva/100;
              //cuadrar el iva para solo el interés
              $subtotal=$capital+$iva+$interes;
              $subtotal=$cursor->tcuota-$subtotal;
              $interes=$interes+($subtotal/(1+($cursor->piva/100)));
              $iva=($interes) * $cursor->piva/100;
              //$capital=$capital+($cursor->tcuota-($capital+$iva+$interes));
              //$capital
              if($i==$cuotas and !$capital_resta==0){
                  $capital=$cap_ant;
                  $capital_pagado=$capital_pagado+$capital;
                  $interes=$cuota-$capital;
                  $iva=($interes) * $cursor->piva/100;
              //cuadrar el iva para solo el interés
              $subtotal=$capital+$iva+$interes;
              $subtotal=$cursor->tcuota-$subtotal;
              $interes=$interes+($subtotal/(1+($cursor->piva/100)));
              $iva=($interes) * $cursor->piva/100;
                  $capital_resta=$capital_resta-$capital;
              }else{
                  $capital_pagado=$capital_pagado+$capital;
                  $capital_resta=$capital_resta-$capital;
              }
              //$iva=$cursor->iva;
              $cuota=$cursor->cuota;
              //$capital_resta=$cursor->importe-$capital_pagado;
          //if($i>=$indice){
            /*
              $ivass=$ivass+$iva;
              $precio=$precio+$interes;
              $capitalss=$capitalss+$capital;
              */
              $total=$total+$capital+$interes+$iva;
          //}

         if($capital_resta<0){
          //$comp[0]=1;
        }
        if($capital<0){
          //$comp[1]=1;
        }
        if($interes<0){
          //$comp[2]=1;
        }
          $cap_ant=$capital_resta;
          /***** Totalizaciones */
          $periodo=date_format($fecha,'Y-m-d');
          $suma_interes=$suma_interes+$interes;
          $suma_capital=$suma_capital+$capital;
          $suma_iva=$suma_iva+$iva;
          $suma_cuota=$suma_cuota+$cursor->tcuota;
          /***** Fin de Totalizaciones */
          $fecha=agrega_periodo($periodo);
      }
      /***** Fin de las amortizaciones */
  }
function calcula_precio_iva_parcial($pago,$iva_calculado,$precio_calculado,$interes_calculado,&$iva,&$precio,&$interes){
    $pago_calculado=$iva_calculado+$precio_calculado;
    $porcentaje=$pago*100/$pago_calculado;
    $iva=round($iva_calculado*$porcentaje/100,2);
    $precio=round($precio_calculado*$porcentaje/100,2);
    $interes=round($interes_calculado*$porcentaje/100,2);
}
function busca_pago_principal($prestamo_id){
    $pagos=Pago::select('pagos.id','pagos.fpago','pagos.monto','pagos.referencia')
    ->where('prestamo_id','=',$prestamo_id)
    ->where('conciliado','=','1')
    ->whereIn('motivo',['1','5'])
    ->get();
    return $pagos;
}
