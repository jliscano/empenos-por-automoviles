<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Impuesto
 *
 * @property $id
 * @property $vehiculo_id
 * @property $tipoimpuesto_id
 * @property $fpago
 * @property $referencia
 * @property $monto
 * @property $piva
 * @property $iva
 * @property $observacion
 * @property $conciliado
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Impuesto extends Model
{
    
    static $rules = [
		'vehiculo_id' => 'required',
    'tipoimpuesto_id' => 'required',
		'fpago' => 'required',
		'referencia' => 'required',
		'monto' => 'required',
		'piva' => 'required',
		'iva' => 'required',
    'conciliado'=>'required'
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['vehiculo_id','tipoimpuesto_id','fpago','referencia','monto','piva','iva','observacion','conciliado'];



}
