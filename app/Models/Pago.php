<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Pago
 *
 * @property $id
 * @property $prestamo_id
 * @property $motivo
 * @property $fpago
 * @property $referencia
 * @property $cuota_nro
 * @property $monto
 * @property $observacion
 * @property $conciliado
 * @property $created_at
 * @property $updated_at
 *
 * @property Clasificado $clasificado
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Pago extends Model
{

    static $rules = [
		'prestamo_id' => 'required',
		'fpago' => 'required',
        'motivo' => 'required',
		'referencia' => 'required',
        'cuota_nro' =>'required',
		'monto' => 'required',
		'observacion' => '',
        'conciliado' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['prestamo_id','motivo','fpago','referencia','cuota_nro','monto','piva','iva','observacion','conciliado'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function clasificado()
    {
        return $this->hasOne('App\Models\Prestamo', 'id', 'prestamo_id');
    }


}
