<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Ingreso
 *
 * @property $id
 * @property $empresa_id
 * @property $clasificado_id
 * @property $monto
 * @property $referencia
 * @property $fingreso
 * @property $observacion
 * @property $conciliado
 * @property $created_at
 * @property $updated_at
 *
 * @property Clasificado $clasificado
 * @property Empresa $empresa
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Ingreso extends Model
{
    
    static $rules = [
		'empresa_id' => 'required',
		'clasificado_id' => 'required',
		'monto' => 'required',
		'fingreso' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['empresa_id','clasificado_id','monto','referencia','fingreso','observacion','conciliado'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function clasificado()
    {
        return $this->hasOne('App\Models\Clasificado', 'id', 'clasificado_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresa()
    {
        return $this->hasOne('App\Models\Empresa', 'id', 'empresa_id');
    }
    

}
