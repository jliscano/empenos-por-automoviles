<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Extensione
 *
 * @property $id
 * @property $prestamo_id
 * @property $fecha
 * @property $monto
 * @property $tcuota
 * @property $cuota
 * @property $iva
 * @property $piva
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Extensione extends Model
{
    
    static $rules = [
		'prestamo_id' => 'required',
		'fecha' => 'required',
		'monto' => 'required',
		'tcuota' => 'required',
		'cuota' => 'required',
		'iva' => 'required',
		'piva' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['prestamo_id','fecha','monto','tcuota','cuota','iva','piva'];



}
