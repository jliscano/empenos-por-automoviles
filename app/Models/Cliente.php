<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cliente
 *
 * @property $id
 * @property $empresa_id
 * @property $nombre
 * @property $dni
 * @property $sexo
 * @property $provincia_id
 * @property $municipio_id
 * @property $direccion
 * @property $telefono
 * @property $email
 * @property $status
 * @property $nombre2
 * @property $dni2
 * @property $sexo2
 * @property $nombre_tutor
 * @property $tipo_tutor
 * @property $dni_tutor
 * @property $pespecial
 * @property $sexo_tutor
 * @property $dir_tutor
 * @property $pespecial
 * @property $propempresa
 * @property $dnicif
 * @property $codpostal
 * @property $fcarnet
 * @property $observacion
 * @property $created_at
 * @property $updated_at
 * @property $pnotarial
 * @property Empresa $empresa
 * @property Municipio $municipio
 * @property Prestamo[] $prestamos
 * @property Provincia $provincia
 * @property Vehiculo[] $vehiculos
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Cliente extends Model
{

    static $rules = [
        /*
		'empresa_id' => 'required',
		'nombre' => 'required',
		'dni' => 'required',
		'provincia_id' => 'required',
		'municipio_id' => 'required',
		'direccion' => 'required',
		'telefono' => 'required',
		'status' => 'required',
        */
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['empresa_id','nombre',
    'dni','sexo','provincia_id','municipio_id','direccion','telefono',
    'email','status','nombre2','dni2','sexo2','propempresa','dnicif','codpostal',
    'proempresa','observacion','fcarnet','nombre_tutor','tipo_tutor','dni_tutor','pespecial','sexo_tutor','pnotarial','dir_tutor','pespecial' ];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresa()
    {
        //return $this->hasOne('App\Models\Empresa', 'id', 'empresa_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function municipio()
    {
        //return $this->hasOne('App\Models\Municipio', 'id', 'municipio_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prestamos()
    {
        return $this->hasMany('App\Models\Prestamo', 'cliente_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function provincia()
    {
        //return $this->hasOne('App\Models\Provincia', 'id', 'provincia_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehiculos()
    {
        return $this->hasMany('App\Models\Vehiculo', 'cliente_id', 'id');
    }


}
