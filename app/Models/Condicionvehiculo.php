<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Condicionvehiculo
 *
 * @property $id
 * @property $nombre
 * @property $comentario
 * @property $created_at
 * @property $updated_at
 *
 * @property Vehiculo[] $vehiculos
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Condicionvehiculo extends Model
{
    
    static $rules = [
		'nombre' => 'required',
		'comentario' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','comentario'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehiculos()
    {
        return $this->hasMany('App\Models\Vehiculo', 'situacion_id', 'id');
    }
    

}
