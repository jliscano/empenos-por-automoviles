<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Condicione
 *
 * @property $id
 * @property $nombre
 * @property $comentario
 * @property $created_at
 * @property $updated_at
 *
 * @property Prestamo[] $prestamos
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Condicione extends Model
{
    
    static $rules = [
		'nombre' => 'required',
		'comentario' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','comentario'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function prestamos()
    {
        return $this->hasMany('App\Models\Prestamo', 'condicion_id', 'id');
    }
    

}
