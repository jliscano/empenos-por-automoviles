<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Ingresoxvehiculo
 *
 * @property $id
 * @property $ingreso_id
 * @property $vehiculo_id
 * @property $monto
 * @property $created_at
 * @property $updated_at
 *
 * @property Gasto $gasto
 * @property Vehiculo $vehiculo
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Ingresoxvehiculo extends Model
{
    
    static $rules = [
		'ingreso_id' => 'required',
		'vehiculo_id' => 'required',
		'monto' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['ingreso_id','vehiculo_id','monto'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function gasto()
    {
        return $this->hasOne('App\Models\Gasto', 'id', 'ingreso_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function vehiculo()
    {
        return $this->hasOne('App\Models\Vehiculo', 'id', 'vehiculo_id');
    }
    

}
