<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Multa
 *
 * @property $id
 * @property $cliente_id
 * @property $vehiculo_id
 * @property $motivo
 * @property $fmulta
 * @property $monto
 * @property $pagado
 * @property $observacion
 * @property $created_at
 * @property $updated_at
 *
 * @property Cliente $cliente
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Multa extends Model
{
    
    static $rules = [
		'cliente_id' => 'required',
        'vehiculo_id'=>'required',
		'fmulta' => 'required',
		'monto' => 'required',
		'pagado' => 'required',
		//'observacion' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cliente_id','motivo','fmulta','monto','pagado','observacion','vehiculo_id'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cliente()
    {
        return $this->hasOne('App\Models\Cliente', 'id', 'cliente_id');
    }
     /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function vehiculo()
    {
        return $this->hasOne('App\Models\Vehiculo', 'id', 'vehiculo_id');
    }

}
