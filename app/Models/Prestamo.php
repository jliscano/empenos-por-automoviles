<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Prestamo
 *
 * @property $id
 * @property $cliente_id
 * @property $ffirma
 * @property $importe
 * @property $tipo
 * @property $cuotas
 * @property $porcentaje
 * @property $frecuencia
 * @property $cuota
 * @property $iva
 * @property $piva
 * @property $tcuota
 * @property $residual
 * @property $gastos
 * @property $observacion
 * @property $condicion_id
 * @property $interes
 * @property $finiciopago
 * @property $vehiculo_id
 * @property $extension
 * @property $padre
 * @property $created_at
 * @property $updated_at
 *
 * @property Cliente $cliente
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Prestamo extends Model
{


    static $rules = [
		'cliente_id' => 'required',
        'vehiculo_id' => 'required',
		'ffirma' => 'required',
        'finiciopago'=>'required',
        'tipo' => 'required',
		'importe' => 'required|numeric|gt:0|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
        /*
		'porcentaje' => 'required|numeric|gt:0|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',

        'frecuencia' => 'required',
        'cuotas' => 'required|min:0',
        'porcentaje' => 'required|numeric|max:100|gt:-1|regex:/^-?[0-9]+(?:\.[0-9]{1,9})?$/',
		'cuota' => 'required',
		'iva' => 'required|numeric',
		'tcuota' => 'required|numeric',
		'residual' => 'required|numeric|gt:-1|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
        'gastos' => 'required|numeric|gt:0|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
		'interes'=>'required|numeric|gt:-1|regex:/^-?[0-9]+(?:\.[0-9]{1,2})?$/',
        */
        'condicion_id' =>'required',
    ];

    protected $perPage = 20;
    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cliente_id','ffirma','importe','tipo','frecuencia','cuotas','porcentaje','cuota','iva','piva','tcuota','residual','observacion','gastos','interes','finiciopago','condicion_id','vehiculo_id','extension','padre'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cliente()
    {
        return $this->hasOne('App\Models\Cliente', 'id', 'cliente_id');
    }
    public function vehiculo()
    {
        return $this->hasOne('App\Models\Vehiculo', 'id', 'vehiculo_id');
    }

}
