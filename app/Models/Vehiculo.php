<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Vehiculo
 *
 * @property $id
 * @property $cliente_id
 * @property $marca_id
 * @property $modelo_id
 * @property $empseguro_id
 * @property $nano
 * @property $itv
 * @property $fcompra
 * @property $fvencseguro
 * @property $finicseguro
 * @property $tipo_id
 * @property $frecuencia
 * @property $valoracion
 * @property $cuotaunica
 * @property $situacion_id
 * @property $poliza
 * @property $matricula
 * @property $bastidor
 * @property $kilometros
 * @property $fmatricula
 * @property $tcombustible
 * @property $observacion
 * @property $created_at
 * @property $updated_at
 *
 * @property Cliente $cliente
 * @property Empseguro $empseguro
 * @property Marca $marca
 * @property Modelo $modelo
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Vehiculo extends Model
{
    
    static $rules = [
        /*
		'cliente_id' => 'required',
		'marca_id' => 'required',
		'modelo_id' => 'required|gt:0',
		'empseguro_id' => 'required',
        'situacion_id' => 'required',
		'nano' => 'required',
		'itv' => 'required|max:10',
		'fvencseguro' => 'required|max:10',
		'finicseguro' => 'required|max:10',
        'fcarnet' => 'required|max:10',
		'valoracion' => 'required',
		'cuotaunica' => 'required|gt:-1',
		'poliza' => 'required',
		'matricula' => 'required',
        'bastidor' => 'required|MAX:20',
        'kilometros' => 'required',
        'fmatricula' => 'required|max:10',
        */
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cliente_id','marca_id','modelo_id','empseguro_id','nano','itv','fcompra','fvencseguro','finicseguro','tipo_id','frecuencia','valoracion','cuotaunica','situacion_id','poliza','matricula','bastidor','kilometros','fmatricula','tcombustible','observacion'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cliente()
    {
        return $this->hasOne('App\Models\Cliente', 'id', 'cliente_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empseguro()
    {
        //return $this->hasOne('App\Models\Empseguro', 'id', 'empseguro_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function marca()
    {
        //return $this->hasOne('App\Models\Marca', 'id', 'marca_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function modelo()
    {
        //return $this->hasOne('App\Models\Modelo', 'id', 'modelo_id');
    }
}
