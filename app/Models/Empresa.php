<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Empresa
 *
 * @property $id
 * @property $nombre
 * @property $replegal
 * @property $domicilio
 * @property $cif
 * @property $banco_id
 * @property $cuenta
 * @property $dnireplega
 * @property $email
 * @property $created_at
 * @property $updated_at
 *
 * @property Cliente[] $clientes
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Empresa extends Model
{

    static $rules = [
		'nombre' => 'required',
        'replegal' => 'required',
        'domicilio' => 'required',
        'cif' => 'required',
        'dnireplegal' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','replegal','domicilio','cif','dnireplegal','banco_id','cuenta','email'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function clientes()
    {
        return $this->hasMany('App\Models\Cliente', 'empresa_id', 'id');
    }
    public function bancos()
    {
        return $this->hasMany('App\Models\Cliente', 'banco_id', 'id');
    }


}
