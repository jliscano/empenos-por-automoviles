<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Cobranza
 *
 * @property $id
 * @property $cliente_id
 * @property $fcobranza
 * @property $motivo
 * @property $fproxpago
 * @property $observacion
 * @property $created_at
 * @property $updated_at
 *
 * @property Cliente $cliente
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Cobranza extends Model
{
    
    static $rules = [
		'cliente_id' => 'required',
		'fcobranza' => 'required',
		'observacion' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['cliente_id','motivo','fproxpago','fcobranza','observacion'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cliente()
    {
        return $this->hasOne('App\Models\Cliente', 'id', 'cliente_id');
    }
    

}
