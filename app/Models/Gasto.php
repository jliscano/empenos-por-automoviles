<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Gasto
 *
 * @property $id
 * @property $empresa_id
 * @property $clasificado_id
 * @property $monto
 * @property $referencia
 * @property $observacion
 * @property $conciliado
 * @property $fgasto
 * @property $created_at
 * @property $updated_at
 *
 * @property Clasificado $clasificado
 * @property Empresa $empresa
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Gasto extends Model
{
    
    static $rules = [
		'empresa_id' => 'required',
		'clasificado_id' => 'required',
		'monto' => 'required',
		'observacion' => '',
		'fgasto' => 'required',
        'conciliado' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['empresa_id','clasificado_id','monto','referencia','observacion','fgasto','conciliado'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function clasificado()
    {
        return $this->hasOne('App\Models\Clasificado', 'id', 'clasificado_id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function empresa()
    {
        return $this->hasOne('App\Models\Empresa', 'id', 'empresa_id');
    }
    

}
