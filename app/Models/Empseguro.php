<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Empseguro
 *
 * @property $id
 * @property $nombre
 * @property $telefonoseg
 * @property $activo
 * @property $created_at
 * @property $updated_at
 *
 * @property Vehiculo[] $vehiculos
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Empseguro extends Model
{
    
    static $rules = [
		'nombre' => 'required',
        'telefonoseg' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['nombre','telefonoseg','activo'];


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function vehiculos()
    {
        return $this->hasMany('App\Models\Vehiculo', 'empseguro_id', 'id');
    }
    

}
