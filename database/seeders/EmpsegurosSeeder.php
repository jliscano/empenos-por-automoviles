<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpsegurosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
          $tests = array(
            [
                'nombre' => 'Empresa Seguros 1',
                'telefonoseg'=>'717171711'
            ],
            [
                'nombre' => 'Empresa Seguros 2',
                'telefonoseg'=>'717171711'
            ]
        );

        foreach ($tests as $key) {
            DB::table('empseguros')->insert($key);
        }
    }
}
