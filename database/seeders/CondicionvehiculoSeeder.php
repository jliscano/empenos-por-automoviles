<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CondicionvehiculoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $tests = array(
        //1:En Depósito 2: En Circulación 3:En espera 4:En Reparación 5:Vendido 6:Devuelto
        [
                'nombre' => 'En Depósito',
                'comentario' =>'El coche se encuentra físicamente en un depósito, en posesión de la empresa.'
            ],
            [
              'nombre' => 'En Circulación',
                'comentario' =>'El coche está en manos del cliente, quien lo tiene en calidad de alquiler.'
            ],
            [
              'nombre' => 'En Espera',
                'comentario' =>'Se trata de un período, en el cual el cliente tiene la oportunidad de de llegar a un acuerdo de pago con la empresa.'
            ],
            [
              'nombre' => 'En Reparación',
                'comentario' =>'El coche fue recuperado y se encuentra en un taller, para reparaciones.'
            ],
            [
              'nombre' => 'Vendido',
                'comentario' =>'El coche fue vendido para recuperar la deuda acarreada por impagos y gastos de recuperación.'
            ],
            [
              'nombre' => 'Devuelto',
                'comentario' =>'El coche fue devuelto al cliente, quien canceló todas las cuotas del préstamo'
            ]
        );
        foreach ($tests as $key) {
            DB::table('condicionvehiculos')->insert($key);
        }

    }
}
