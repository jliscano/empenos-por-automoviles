<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EmpresasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $tests = array(
         
            [
                'nombre' => 'GRUPO CONFIACAR AUTOMOCIÓN, S.L.L',
                'replegal' =>'Rafael Álvarez Marín',
                'domicilio' =>'C/. Soledad 55, de 30.430 Cehegin (Murcia)',
                'cif' =>'B-73.897.555',
                'dnireplegal' => '75.242003-X'
            ],
            [
                'nombre' => 'Empresa 2',
                'replegal' =>'Rafael Álvarez Marín',
                'domicilio' =>'C/. Soledad 55, de 30.430 Cehegin (Murcia)',
                'cif' =>'B-73.897.555',
                'dnireplegal' => '75.242003-X'
            ]
        );

        foreach ($tests as $key) {
            DB::table('empresas')->insert($key);
        }
    }
}
