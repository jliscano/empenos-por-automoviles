<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinciaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $tests = array(
         
            [
                'nombre' => 'Provincia 1'
            ],
            [
                'nombre' => 'Provincia 2'
            ]
        );

        foreach ($tests as $key) {
            DB::table('provincias')->insert($key);
        }
    }
}
