<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         //$this->faker = $faker = Faker\Factory::create();
        $tests = array(
            [
                'name' => 'Administrador',
                'dni' => '1',
                'telefono' => '1',
                'email' => 'admin@confiacar.com',
                'nivel' => 'Admin',
                'password' => bcrypt('12345678')
            ],
            [
                'name' => 'Fina',
                'dni' => '2',
                'telefono' => '2',
                'email' => 'supervisor@confiacar.com',
                'nivel' => 'Supervisor',
                'password' => bcrypt('12345678')
            ],
            [
                'name' => 'Esther',
                'dni' => '3',
                'telefono' => '3',
                'email' => 'operador@confiacar.com',
                'nivel' => 'Operador',
                'password' => bcrypt('12345678')
            ],
            [
                'name' => 'Luis',
                'dni' => '4',
                'telefono' => '4',
                'email' => 'jefeoficina@confiacar.com',
                'nivel' => 'Jefe de Oficina',
                'password' => bcrypt('12345678')
            ],
            [
                'name' => 'Maxi',
                'dni' => '5',
                'telefono' => '5',
                'email' => 'gerente@confiacar.com',
                'nivel' => 'Gerente',
                'password' => bcrypt('12345678')
            ],
            [
                'name' => 'Raul',
                'dni' => '6',
                'telefono' => '6',
                'email' => 'cobrador@confiacar.com',
                'nivel' => 'Cobrador',
                'password' => bcrypt('12345678')
            ]
        );

        foreach ($tests as $key) {
            DB::table('users')->insert($key);
        }
    }
}
