<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModelosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $tests = array(
         
            [
                'nombre' => 'Modelo 1',
                'marca_id' => 1
            ],
            [
                'nombre' => 'Modelo 2',
                'marca_id' => 1
            ],
            [
                'nombre' => 'Modelo 3',
                'marca_id' => 2
            ],
            [
                'nombre' => 'Modelo 4',
                'marca_id' => 2
            ]
        );

        foreach ($tests as $key) {
            DB::table('modelos')->insert($key);
        }
    }
}
