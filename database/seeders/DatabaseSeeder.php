<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

	$this->call(UserSeeder::class);
        $this->call(BancosSeeder::class);
        $this->call(EmpresasSeeder::class);
        $this->call(EmpsegurosSeeder::class);
        $this->call(CondicioneSeeder::class);
        $this->call(CondicionvehiculoSeeder::class);
        $this->call(ClasificadoSeed::class);
        $this->call(CoberturaSeed::class);
    }
}
