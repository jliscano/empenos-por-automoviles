<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MunicipioSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $tests = array(
         
            [
                'nombre' => 'Localidad 1',
                'cp' =>'00001',
                'provincia_id' => 1
            ],
            [
                'nombre' => 'Localidad 2',
                'cp' =>'00002',
                'provincia_id' => 1
            ],
            [
                'nombre' => 'Localidad 3',
                'cp' =>'00003',
                'provincia_id' => 2
            ],
            [
                'nombre' => 'Localidad 4',
                'cp' =>'00004',
                'provincia_id' => 2
            ]
        );

        foreach ($tests as $key) {
            DB::table('municipios')->insert($key);
        }
    }
}
