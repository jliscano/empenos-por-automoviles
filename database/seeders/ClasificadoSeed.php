<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ClasificadoSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $tests = array(
         
            //gastos 0
            ['nombre'=>'Gastos de Personal.','tipo'=>'0'],
            ['nombre'=>'Utiles y Productos de limpieza.','tipo'=>'0'],
            ['nombre'=>'Utiles y Productos de oficina.','tipo'=>'0'],
            ['nombre'=>'Alimentos y bebidas.','tipo'=>'0'],
            ['nombre'=>'Combustibles y lubricantes.','tipo'=>'0'],
            ['nombre'=>'Repuestos para automóviles.','tipo'=>'0'],
            ['nombre'=>'Reparación de automóviles.','tipo'=>'0']
        );

        foreach ($tests as $key) {
            DB::table('clasificados')->insert($key);
        }
    }
}
