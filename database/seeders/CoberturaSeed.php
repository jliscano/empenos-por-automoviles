<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CoberturaSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

$tests = array(
            [
                'nombre' => 'Seguros a todo riesgo de coche'
            ],
            [
                'nombre' => 'Seguro de coche con franquicia a todo riesgo'
            ],
            [
                'nombre' => 'Seguro de coche a todo riesgo sin franquicia'
            ],
            [
                'nombre' => 'Seguro de coche a terceros'
            ],
            [
                'nombre' => 'Seguro a terceros con lunas'
            ],
            [
                'nombre' => 'Seguro a terceros ampliado'
            ]
        );

        foreach ($tests as $key) {
            DB::table('coberturas')->insert($key);
        }
        
    }
}
