<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BancosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $tests = array(
         
            [
                'nombre' => 'Banco 1',
                'activo' => 'S'
            ],
            [
                'nombre' => 'Banco 2',
                'activo' => 'S'
            ]
        );

        foreach ($tests as $key) {
            DB::table('bancos')->insert($key);
        }
    }
}
