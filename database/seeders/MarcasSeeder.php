<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MarcasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $tests = array(
         
            [
                'nombre' => 'Marca 1'
            ],
            [
                'nombre' => 'Marca 2'
            ]
        );

        foreach ($tests as $key) {
            DB::table('marcas')->insert($key);
        }
    }
}
