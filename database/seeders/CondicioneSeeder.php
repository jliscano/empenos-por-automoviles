<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CondicioneSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         $tests = array(
         //1: en vigor 2: Pagado 3:Pendiente Venta de Vehiculo
            [
                'nombre' => 'En Vigor',
                'comentario' =>'El Préstamo está vigente y en cobros'
            ],
            [
              'nombre' => 'Pagado',
                'comentario' =>'El Préstamo fue cancelado en su totaliad por el cliente'
            ],
            [
              'nombre' => 'Suspendido',
                'comentario' =>'El Préstamo fue suspendido por la empresa debido a impagos en espera de recuperación o venta del vehículo'
            ]
        );

        foreach ($tests as $key) {
            DB::table('condiciones')->insert($key);
        }
    }
}
