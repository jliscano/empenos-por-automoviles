<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cobranzas', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('cliente_id')->unsigned();
            $table->enum('motivo',['0','1','2','4']);
            // 0:Pago de Prestamo-Multa-Gastos de recogida  1:Seguro 2:ITV 
            $table->date('fcobranza');
            $table->date('fproxpago')->nullable();
            $table->string('observacion');
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cobranzas');
    }
};
