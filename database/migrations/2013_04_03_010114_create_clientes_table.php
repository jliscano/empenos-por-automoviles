<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('empresa_id')->unsigned();
            $table->string('nombre')->nullable();;
            $table->string('dni')->nullable();
            $table->string('nombre2')->nullable();
            $table->string('dni2')->nullable();
            $table->string('proempresa')->nullable();
            $table->string('dnicif')->nullable();
            $table->string('codpostal',8)->nullable();
            $table->bigInteger('provincia_id')->unsigned()->nullable();
            $table->bigInteger('municipio_id')->unsigned()->nullable();
            $table->string('direccion')->nullable();
            $table->string('telefono',60)->nullable();
            $table->date('fcarnet')->nullable();
            $table->string('email')->nullable();//->unique();
            $table->string('observacion')->nullable();
            //$table->string('status')->nullable();
            $table->enum('status', [0,1])->default(0)->nullable();
            //0:en curso 1:en espera
            /*$table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('restrict');
            $table->foreign('provincia_id')->references('id')->on('provincias')->onDelete('restrict');
             $table->foreign('municipio_id')->references('id')->on('municipios')->onDelete('restrict');
            */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
};
