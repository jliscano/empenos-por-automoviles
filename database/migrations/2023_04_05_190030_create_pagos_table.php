<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pagos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('prestamo_id')->unsigned();
            $table->enum('motivo', ['0','1'])->nullable()->default('0');
            //0:pago cuota de prestamo, 2:pago final del capital en empeños tipo empeño tradicional.
            $table->date('fpago');
            $table->string('referencia',20);
            $table->double('monto',20,7);
            $table->string('observacion')->nullable();
            $table->enum('conciliado', ['0','1'])->nullable()->default('0');
            //0:no 1:si
            $table->foreign('prestamo_id')->references('id')->on('prestamos')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
};
