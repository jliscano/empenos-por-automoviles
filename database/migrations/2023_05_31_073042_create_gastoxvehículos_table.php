<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gastoxvehiculos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('gasto_id')->unsigned();
            $table->bigInteger('vehiculo_id')->unsigned();
            $table->double('monto',20,3)->unsigned()->default(0);
            $table->timestamps();
            $table->foreign('gasto_id')->references('id')->on('gastos')->onDelete('restrict');
            $table->foreign('vehiculo_id')->references('id')->on('vehiculos')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('gastoxvehiculos')) {

        
        Schema::table('gastoxvehiculos', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            /*
            $table->dropForeign('gastos_clasificado_id_foreign');
            $table->dropColumn('clasificado_id');

            $table->dropForeign('gastos_empresa_id_foreign');
            $table->dropColumn('empresa_id');

            */
            Schema::dropIfExists('gastoxvehiculos');
        });
    }
    }
};
