<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingresos', function (Blueprint $table) {
            $table->id();
            $table->engine='InnoDB';
            $table->bigInteger('empresa_id')->unsigned();
            $table->bigInteger('clasificado_id')->unsigned();
            $table->double('monto',20,2);
            $table->string('referencia',20);
            $table->date('fingreso');
            $table->string('observacion')->nullable();
            $table->enum('conciliado', ['0','1'])->nullable()->default('0');
            //0:no 1:si
            $table->timestamps();
            $table->foreign('clasificado_id')->references('id')->on('clasificados')->onDelete('restrict');
            $table->foreign('empresa_id')->references('id')->on('empresas')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('ingresos')) {
            Schema::table('ingresos', function (Blueprint $table) {
                Schema::disableForeignKeyConstraints();
                Schema::dropIfExists('ingresos');
            });
        }
    }
};