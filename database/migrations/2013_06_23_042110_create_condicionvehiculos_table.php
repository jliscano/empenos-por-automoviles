<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('condicionvehiculos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->string('nombre',30)->required();
            $table->string('comentario')->required();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if (Schema::hasTable('condicionvehiculos')) {
            Schema::table('condicionvehiculos', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('condicionvehiculos');
        });
        }
    }
};
