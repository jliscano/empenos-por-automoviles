<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documentos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('cliente_id')->unsigned()->required();
            $table->string('nombre')->required()->nullable(false);
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         if (Schema::hasTable('documentos')) {
            Schema::table('documentos', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('documentos');
        });
    }
    }
};
