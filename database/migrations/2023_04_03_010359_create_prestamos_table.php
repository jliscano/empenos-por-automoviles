<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prestamos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();

            $table->bigInteger('cliente_id')->unsigned()->required();
            $table->bigInteger('vehiculo_id')->unsigned()->required();
            $table->date('ffirma')->nullable();
            $table->double('importe',20,2)->nullable()->default(0);
            $table->enum('tipo', [0,1])->default(0)->nullable();
            // 0: amortizando 1:empeño tradicional
            $table->smallInteger('cuotas')->unsigned()->default(0)->nullable();
            $table->double('porcentaje',20,2)->default(0)->unsigned()->nullable();
            $table->enum('frecuencia', [52,12,4,2,1])->nullable()->default(12);
            //52:semanal 12:Mensual 4:Trimestral 2:Semestral 1:Anual
            $table->double('cuota',20,2)->unsigned()->default(0)->nullable();
            $table->double('piva',20,2)->unsigned()->default(0)->nullable();
            $table->double('iva',20,2)->unsigned()->default(0)->nullable();
            $table->double('tcuota',20,2)->unsigned()->default(0)->nullable();
            $table->double('residual',20,2)->default(0)->unsigned()->nullable();
            $table->double('gastos',20,2)->default(0)->nullable();
            $table->string('observacion')->nullable();
            $table->double('interes',20,2)->default(0)->unsigned()->nullable();
            $table->bigInteger('condicion_id')->unsigned()->required();

            //1: en vigor 2: Pagado 3:Pendiente Venta de Vehiculo
            $table->date('finiciopago')->nullable();
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('restrict');
            $table->foreign('condicion_id')->references('id')->on('condiciones')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('prestamos')) {
            Schema::table('prestamos', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('prestamos');
        });
        }
    }
};
