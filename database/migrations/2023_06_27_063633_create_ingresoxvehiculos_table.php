<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ingresoxvehiculos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('ingreso_id')->unsigned();
            $table->bigInteger('vehiculo_id')->unsigned();
            $table->double('monto',20,3)->unsigned()->default(0);
            $table->timestamps();
            $table->foreign('ingreso_id')->references('id')->on('ingresos')->onDelete('restrict');
            $table->foreign('vehiculo_id')->references('id')->on('vehiculos')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('ingresoxvehiculos')) {
            Schema::table('ingresoxvehiculos', function (Blueprint $table) {
                Schema::disableForeignKeyConstraints();
                Schema::dropIfExists('ingresoxvehiculos');
            });
        }
    }
};
