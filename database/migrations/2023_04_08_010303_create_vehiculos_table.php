<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('cliente_id')->unsigned()->required();
            $table->bigInteger('marca_id')->unsigned()->nullable();
            $table->bigInteger('modelo_id')->unsigned()->nullable();
            $table->bigInteger('empseguro_id')->unsigned()->nullable();
            $table->Integer('nano')->nullable()->default(0);
            $table->date('itv')->nullable();
            $table->date('fcompra')->nullable();
            $table->date('fvencseguro')->nullable();
            $table->date('finicseguro')->nullable();
            $table->bigInteger('tipo_id')->unsigned()->nullable();
            
            $table->enum('frecuencia', [52,12,4,2,1])->nullable()->default(12);
            //0:semanal 1:Mensual 2:Trimestral 3:Semestral 4:Anual
            $table->string('bastidor',30)->nullable();
            $table->Integer('kilometros')->unsigned()->nullable()->default(0);
            $table->double('valoracion',10,2)->default(0)->nullable()->default(0);
            $table->double('cuotaunica',10,2)->default(0)->nullable();
            $table->bigInteger('situacion_id')->required()->unsigned();
            
            $table->string('poliza',30)->nullable();
            $table->string('matricula',20)->nullable();
            $table->date('fmatricula')->nullable();
            $table->string('observacion')->nullable();
            
            $table->foreign('cliente_id')->references('id')->on('clientes')->onDelete('restrict');
            /*
            $table->foreign('marca_id')->references('id')->on('marcas')->onDelete('restrict');
            $table->foreign('modelo_id')->references('id')->on('modelos')->onDelete('restrict');
            $table->foreign('empseguro_id')->references('id')->on('empseguros')->onDelete('restrict');
            $table->foreign('tipo_id')->references('id')->on('coberturas')->onDelete('restrict');
            $table->foreign('situacion_id')->references('id')->on('condicionvehiculos')->onDelete('restrict');
            */
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
     if (Schema::hasTable('vehiculos')) {

        
        Schema::table('vehiculos', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            
            Schema::dropIfExists('vehiculos');
        });
    }
  }
};
