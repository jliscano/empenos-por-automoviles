<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('municipios', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->id();
            $table->bigInteger('provincia_id')->unsigned();
            $table->string('cp',6)->nullable();
            $table->string('nombre');
            $table->timestamps();
            $table->foreign('provincia_id')->references('id')->on('provincias')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('municipios')) {
            Schema::table('municipios', function (Blueprint $table) {
            Schema::disableForeignKeyConstraints();
            Schema::dropIfExists('municipios');
        });
        }
    }
};
