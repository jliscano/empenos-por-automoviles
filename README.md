<br>
## Acerca de NOA 

Es un sistema backend que controla los empeños realizados a los clientes/solicitantes. Quienes utilizan como prenda sus automóviles. Realizado para la empresa española  Confiacar. Está dividido en tres grandes módulos

-Carga de clientes.
-Carga de vehículos por cliente.
-Carga de empeño por vehículo.

## Módulo de empeños
Contempla los siguientes submódulos:
- La carga del empeño.
- La impresión de los contratos legales. 
- El control de pagos.
- El control de multas
- El control de ITV-español.
- El control de poliza de seguro.

