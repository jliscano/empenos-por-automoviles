<?php
header("refresh: 5; https://empenosauto.com/public");
?>
<style type="text/css">

	body{

		height: 5%;
	}
	.contenedor {
		z-index: 0;
 		position: relative;
		width: 100%;
		height: 1%;
	}

	.hijo{
		z-index: 1;
  		display: flex;
		position: static;
  		justify-content: center;
		width: 20%;
		height: 20%;
		margin-top: 50px;
		margin-left: -330px;
	}
    .fondo{
		display: flex;
  		justify-content: center;
        width: 800px;
        height: 600px;
        background-image:url('public/images/fondo.png');
        background-repeat: no-repeat;
        background-size: 60% 60%;
		position: absolute;

  		top: 40%;
  		left: 50%;
  		margin: 25px 0 0 -250px; /* aplicar a top y al margen izquierdo un valor negativo para completar el centrado del elemento hijo */
    }
    @media only screen and (max-width:800px) {
      /* For tablets: */
        .row .mb-3 {
            margin-left: 100px;

        }
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
        .row .mb-3 {
            margin-left: 40px;


        }
    }
    @media only screen and (max-width:800px) {
      /* For tablets: */
        .fondo {
           /* width: 512px;*/
            height: 720px;
        }
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
        .fondo {
            /*width: 330px;*/
            height: 520px;
        }
    }

</style>
<div class="contenedor">
	<div class="fondo" >

		<div class="hijo">
			<img src="public/images/cargando.gif">
		</div>
	</div>
</div>
