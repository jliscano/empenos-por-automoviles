@extends('layouts.app')

@section('template_title')
    Multa
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __($motivos[$motivo].' de: '.$nombre) }}
                            </span>
                            @if($retorno==0)
                                <a href="{{route('listavehiculos',[0,$retorno])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                            @elseif($retorno==11)
                                <a href="{{route('muestraMultasGeneral',[0,$retorno])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                            @else
                                <a href="{{route('listavehiculos',[$cliente_id,$retorno])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                            @endif

                             <div class="float-right">

                                <a href="{{route('createMultas',[$cliente_id,$vehiculo_id,$motivo,$retorno]) }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                    {{ __('Nueva '.$motivos[$motivo])}}
                                </a>

                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! $multas->links() !!}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
                                    <!--
										<th>Cliente Id</th>
                                    -->
										<th>Fecha</th>
										<th>Monto</th>
										<th>Pagado</th>
                                        <!--
										<th>Observacion</th>
                                    -->

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($multas as $multa)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <!--
											<td>{{ $multa->cliente_id }}</td>
                                        -->
											<td>{{ date('d/m/Y',strtotime($multa->fmulta)) }}</td>
											<td>{{ $multa->monto }}</td>
											<td>{{
                                                $pagada[$multa->pagado] }}</td>
                                            <!--
											<td>{{ $multa->observacion }}</td>
                                        -->

                                            <td>

                                        <form action="{{ route('borraMulta',[$multa->id, $multa->cliente_id,$multa->vehiculo_id,$motivo,$retorno]) }}" method="POST" id='borrar'>
                                            <!--
                                            <a class="btn btn-sm btn-primary " href="{{ route('multas.show',$multa->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Show') }}</a>
                                            -->
                                            <input type="hidden" name="retorno" value="{{$retorno}}">

                                            <a class="btn btn-sm btn-success" href="{{ route('editaMulta',[$multa->id,$retorno]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                            @csrf
                                            @method('DELETE')
                                            <a onclick="aviso({{$multa->id, $multa->cliente_id,$multa->vehiculo_id,$motivo,$retorno}});" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i></a>
                                        </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $multas->links() !!}
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar este Vehículo?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            //$id,$cliente_id,$vehiculo_id,$motivo,$retorno
            /*
            url=url +'/borraMulta/'+id;
            document.getElementById("borrar").action = url;
            document.getElementById("borrar").submit();
            */

        }
    }
</script>
