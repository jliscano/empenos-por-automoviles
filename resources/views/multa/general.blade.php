@extends('layouts.app')

@section('template_title')
    Multa
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __($motivos[$motivo])}}
                            </span>
                            <a href="{{route('home')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! $multas->links() !!}
                        <div class="table-responsive">
                            <form action="{{route('muestraMultasGeneral',[0,11])}}">
                                <input type="text" name="texto" class="form-label" style="width:200px;">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </form>
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
										<th>Cliente</th>
										<th>Matrícula</th>
										<th>Marca/Modelo</th>
										<th>Multas<br>Pendientes de Pago</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($multas as $multa)
                                        <tr>
                                            <td>{{ ++$i }}</td>
											<td>{{ $multa->nom }}</td>
											<td>{{ $multa->matricula }}</td>
											<td>{{ $multa->marca.'/'.$multa->modelo }}</td>
                                            <td>{{ $multa->total }}</td>
                                            <td>

                                        <form action="" method="POST" id='borrar'>
                                            <input type="hidden" name="retorno" value="{{$retorno}}">
                                            <a class="btn btn-sm btn-success" href="{{route('muestraMultas',[$multa->cliente_id,$motivo,$multa->vehiculo_id,$retorno])}}"><i class="fa fa-fw fa-edit"></i> {{ __('Cargar Multas') }}</a>
                                        </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $multas->links() !!}
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar este Vehículo?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            //$id,$cliente_id,$vehiculo_id,$motivo,$retorno
            /*
            url=url +'/borraMulta/'+id;
            document.getElementById("borrar").action = url;
            document.getElementById("borrar").submit();
            */

        }
    }
</script>
