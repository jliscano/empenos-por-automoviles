<style type="text/css">
.verde {
    border-radius: 25px;
    background-color: green;
    color: white;
    margin: 10px;
    width: 160px;
    height: 80px;
    padding: 15px 15px 15px 15px;
}

.grid-container {
    display: grid;
    grid-template-rows: 80px 80px;
    /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
    grid-template-columns: 180px 180px  180px 180px;
    grid-gap: 15px;
    padding: 15px 15px 15px 15px;
}
@media only screen and (max-width:800px) {
  /* For tablets: */
 .grid-container {
  display: grid;
  grid-template-columns: 180px 180px;
}
}
@media only screen and (max-width:560px) {
  /* For mobile phones: */
  .grid-container {
  display: grid;
  grid-template-columns: 180px;
  
}
}
</style>
<div class="box box-info padding-1">
    <div class="box-body">
    <div class="grid-container">
        <div class="grid-item1">
        <div class="form-group">
            {{ Form::label('Fecha :') }}
            {{ Form::date('fmulta', $multa->fmulta, ['class' => 'form-control' . ($errors->has('fmulta') ? ' is-invalid' : ''), 'placeholder' => 'Fmulta']) }}
            {!! $errors->first('fmulta', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="grid-item1">
        <div class="form-group">
            {{ Form::label('Monto :') }}
            {{ Form::number('monto', $multa->monto, ['class' => 'form-control' . ($errors->has('monto') ? ' is-invalid' : ''), 'placeholder' => 'Monto']) }}
            {!! $errors->first('monto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="grid-item2">
        <div class="form-group">
            {{ Form::label('Pagado :') }}
            {{ Form::select('pagado', $pagada,$multa->pagado, ['class' => 'form-control' . ($errors->has('pagado') ? ' is-invalid' : '')]) }}
            {!! $errors->first('pagado', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="grid-item3">
        <div class="form-group">
            {{ Form::label('observacion') }}
            {{ Form::text('observacion', $multa->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observacion']) }}
            {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        
        <div class="grid-item4">
        <div class="form-group">
            <!--{{ Form::label('cliente_id') }}-->
            {{ Form::hidden('cliente_id', $multa->cliente_id, ['class' => 'form-control' . ($errors->has('cliente_id') ? ' is-invalid' : ''), 'placeholder' => 'Cliente Id']) }}
            {!! $errors->first('cliente_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="form-group">
            <!--{{ Form::label('cliente_id') }}-->
            {{ Form::hidden('motivo', $multa->motivo, ['class' => 'form-control' . ($errors->has('motivo') ? ' is-invalid' : ''), 'placeholder' => 'motivo']) }}
            {!! $errors->first('motivo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
         <div class="form-group">
            <!--{{ Form::label('cliente_id') }}-->
            {{ Form::hidden('vehiculo_id', $multa->vehiculo_id, ['class' => 'form-control' . ($errors->has('vehiculo_id') ? ' is-invalid' : ''), 'placeholder' => 'vehiculo_id']) }}
            {!! $errors->first('vehiculo_id', '<div class="invalid-feedback">:message</div>') !!}
            <input type="hidden" name="retorno" value="{{$retorno}}">
        </div>
    </div>
    
    <div class="box-footer mt20">
        
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        <a href="{{route('muestraMultas',[$multa->cliente_id,$multa->motivo,$multa->vehiculo_id,$retorno])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    </div>

    <div class="box-footer mt20">
    
    </div>

    </div>
</div>