@extends('layouts.app')
    <style type="text/css">
        .menu{
            display: grid;
            text-align: center;
            justify-content: center;
            align-items: center;
            max-height: 200px;
            min-height: 200px;
            min-width: 100%;
            max-width: 100%;
        }
        .verde {
            border-radius: 25px;
            background-color: green;
            color: white;
            margin: 10px;
            width: 160px;
            height: 80px;
            padding: 8px 10px ;
            font-size:12px;
            line-height: 12px;
        }
        .verde img{
            height: 35px;
            width: 35px;
            margin-bottom: 10px;
        }


    .grid-container {
        border-radius: 15px;
        /*background-color:aquamarine;*/
        background-image: url( {{ asset('/images/marco.jpg') }});
        background-repeat: no-repeat;
        background-size: auto;
        background-size: 100% 100%;

        /*margin-top: -px;*/
        display:grid;
        grid-template-rows: 80px 80px;
        /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
        grid-template-columns: 180px 180px  180px 180px;
        grid-gap: 15px;
        padding: 15px 15px 25px 15px;
    }
    @media only screen and (max-width:800px) {
      /* For tablets: */
        .grid-container {
            display: grid;
            grid-template-columns: 180px 180px;
        }
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
        .grid-container {
            display: grid;
            grid-template-columns: 180px;
            padding: 24px 15px 25px 15px;
        }

    }

    </style>

    @section('admin-section')

    <b>Operaciones</b>
    <div class="grid-container">
        <a href="{{url('clientes')}}"><div class="grid-item3 verde"><img src="{{ asset('images/menu/cliente.png') }}"><p>Clientes</p> </div></a>
        <a href="{{route('clientesEspera')}}" ><div class="grid-item3 verde"><img src="{{ asset('images/menu/espera.png') }}"  ><p>Clientes En Espera</p></div></a>
        <a href="{{route('listavehiculos',[0,0])}}" ><div class="grid-item10 verde"><img src="{{ asset('images/menu/vehiculo.png') }}" ><p>Vehículos</p></div></a>
        <a href="{{route('listaprestamos',[0,0,0])}}" ><div class="grid-item11 verde"><img src="{{ asset('images/menu/empenos.png') }}" ><p>Empeños</p></div></a>

        <a href="{{url('gastos')}}" ><div class="grid-item14 verde"><img src="{{ asset('images/menu/egreso.png') }}"> <p>Gastos</p></div></a>
        <a href="{{url('ingresos')}}" ><div class="grid-item5 verde"><img src="{{ asset('images/menu/ingreso.png') }}"> <p>Ingresos</p></div></a>
        <a href="{{route('impagos',[0,-1,'*'])}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/cobro.png') }}"> <p>Impagos</p></div></a>
        <a href="{{route('poliza.index')}}" ><div class="grid-item8 verde"><img src="{{ asset('images/menu/poliza.png') }}"> <p>Polizas</p></div></a>
        <a href="{{route('itv.index')}}" ><div class="grid-item9 verde"><img src="{{ asset('images/menu/itv.png') }}"> <p>ITV</p></div></a>
        <a href="{{route('carnet.index')}}" ><div class="grid-item10 verde"><img src="{{ asset('images/menu/carnet.png') }}"><P>Carnet Conductor</P></div></a>
        <a href="{{route('muestraRentabilidad',[0,-1,'*'])}}" ><div class="grid-item11 verde"><img src="{{ asset('images/menu/general.png') }}"> <p>Reporte General</p></div></a>
        <a href="{{route('RentabilidadIndividual',[0,-1,'*'])}}" ><div class="grid-item12 verde"><img src="{{ asset('images/menu/individual.png') }}"><P>Reporte Individual</P></div></a>
        <a href="{{route('muestraMultasGeneral',[0,11]) }}" ><div class="grid-item13 verde"><img src="{{ asset('images/menu/multas.png') }}"><P>Multas</P></div></a>

    </div>
    <br>

    <b>Configuraciones</b>
    <div class="grid-container">
        <a href="{{url('empresas')}}" ><div class="grid-item1 verde"><img src="{{ asset('images/menu/empresa.png') }}"><p>Empresas</p></div></a>
        <a href="{{url('marcas')}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/marcas.png') }}"><p>Marcas y Modelos</p></div></a>
        <a href="{{route('clasificados.index')}}" ><div class="grid-item4 verde"><img src="{{ asset('images/menu/clasificado.png') }}"><p>Clasificador Ingresos y Gastos</p></div></a>
        <a href="{{url('empseguros')}}" ><div class="grid-item5 verde"><img src="{{ asset('images/menu/seguro.png') }}"><p>Empresas de Seguro</p></div></a>
        <a href="{{url('provincias')}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/localidades.png') }}"><p>Provincias y Localidades</p></div></a>
        <a href="{{url('bancos')}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/bancos.png') }}"><p>Bancos</p></div></a>
        <a href="{{url('tipoimpuestos')}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/impuestos.png') }}"><p>Tipo de Impuestos</p></div></a>

    </div>
    <br><br>
    @endsection

    @section('gerente-section')
    <b>Operaciones</b>
    <div class="grid-container">
        <a href="{{url('clientes')}}"><div class="grid-item3 verde"><img src="{{ asset('images/menu/cliente.png') }}"><p>Clientes</p> </div></a>
        <a href="{{route('clientesEspera')}}" ><div class="grid-item3 verde"><img src="{{ asset('images/menu/espera.png') }}"  ><p>Clientes En Espera</p></div></a>
        <a href="{{route('listavehiculos',[0,0])}}" ><div class="grid-item10 verde"><img src="{{ asset('images/menu/vehiculo.png') }}" ><p>Vehículos</p></div></a>
        <a href="{{route('listaprestamos',[0,0,0])}}" ><div class="grid-item11 verde"><img src="{{ asset('images/menu/empenos.png') }}" ><p>Empeños</p></div></a>

        <a href="{{url('gastos')}}" ><div class="grid-item14 verde"><img src="{{ asset('images/menu/egreso.png') }}"> <p>Gastos</p></div></a>
        <a href="{{url('ingresos')}}" ><div class="grid-item5 verde"><img src="{{ asset('images/menu/ingreso.png') }}"> <p>Ingresos</p></div></a>
        <a href="{{url('empresas')}}" ><div class="grid-item6 verde"><img src="{{ asset('images/menu/empresa.png') }}"> <p>Empresas</p></div></a>
        <a href="{{route('impagos',[0,-1,'*'])}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/cobro.png') }}"> <p>Impagos</p></div></a>

        <a href="{{route('poliza.index')}}" ><div class="grid-item8 verde"><img src="{{ asset('images/menu/poliza.png') }}"> <p>Polizas</p></div></a>
        <a href="{{route('itv.index')}}" ><div class="grid-item9 verde"><img src="{{ asset('images/menu/itv.png') }}"> <p>ITV</p></div></a>
        <a href="{{route('carnet.index')}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/carnet.png') }}"><P>Carnet Conductor</P></div></a>
        <a href="{{route('muestraRentabilidad',[0,-1,'*'])}}" ><div class="grid-item1 verde"><img src="{{ asset('images/menu/general.png') }}"> <p>Reporte General</p></div></a>
        <a href="{{route('RentabilidadIndividual',[0,-1,'*'])}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/individual.png') }}"><P>Reporte Individual</P></div></a>
        <a href="{{route('muestraMultasGeneral',[0,11]) }}" ><div class="grid-item13 verde"><img src="{{ asset('images/menu/multas.png') }}"><P>Multas</P></div></a>
    </div>
    <br>
    <b>Configuraciones</b>
    <div class="grid-container">
        <a href="{{url('empresas')}}" ><div class="grid-item1 verde"><img src="{{ asset('images/menu/empresa.png') }}"><p>Empresas</p></div></a>
        <a href="{{url('marcas')}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/marcas.png') }}"><p>Marcas y Modelos</p></div></a>
        <a href="{{route('clasificados.index')}}" ><div class="grid-item4 verde"><img src="{{ asset('images/menu/clasificado.png') }}"><p>Clasificador Ingresos y Gastos</p></div></a>
        <a href="{{url('empseguros')}}" ><div class="grid-item5 verde"><img src="{{ asset('images/menu/seguro.png') }}"><p>Empresas de Seguro</p></div></a>
        <a href="{{url('provincias')}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/localidades.png') }}"><p>Provincias y Localidades</p></div></a>
    </div>
    <br><br>

    @endsection

    @section('jefeoficina-section')
     <b>Operaciones</b>
    <div class="grid-container">
        <a href="{{url('clientes')}}"><div class="grid-item3 verde"><img src="{{ asset('images/menu/cliente.png') }}"><p>Clientes</p> </div></a>
        <a href="{{route('clientesEspera')}}" ><div class="grid-item3 verde"><img src="{{ asset('images/menu/espera.png') }}"  ><p>Clientes En Espera</p></div></a>
        <a href="{{route('listavehiculos',[0,0])}}" ><div class="grid-item10 verde"><img src="{{ asset('images/menu/vehiculo.png') }}" ><p>Vehículos</p></div></a>
        <a href="{{route('listaprestamos',[0,0,0])}}" ><div class="grid-item11 verde"><img src="{{ asset('images/menu/empenos.png') }}" ><p>Empeños</p></div></a>

        <a href="{{url('gastos')}}" ><div class="grid-item14 verde"><img src="{{ asset('images/menu/egreso.png') }}"> <p>Gastos</p></div></a>
        <a href="{{url('ingresos')}}" ><div class="grid-item5 verde"><img src="{{ asset('images/menu/ingreso.png') }}"> <p>Ingresos</p></div></a>
        <a href="{{url('empresas')}}" ><div class="grid-item6 verde"><img src="{{ asset('images/menu/empresa.png') }}"> <p>Empresas</p></div></a>
        <a href="{{route('impagos',[0,-1,'*'])}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/cobro.png') }}"> <p>Impagos</p></div></a>

        <a href="{{route('poliza.index')}}" ><div class="grid-item8 verde"><img src="{{ asset('images/menu/poliza.png') }}"> <p>Polizas</p></div></a>
        <a href="{{route('itv.index')}}" ><div class="grid-item9 verde"><img src="{{ asset('images/menu/itv.png') }}"> <p>ITV</p></div></a>
        <a href="{{route('carnet.index')}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/carnet.png') }}"><P>Carnet Conductor</P></div></a>
        <a href="{{route('muestraRentabilidad',[0,-1,'*'])}}" ><div class="grid-item1 verde"><img src="{{ asset('images/menu/general.png') }}"> <p>Reporte General</p></div></a>
        <a href="{{route('RentabilidadIndividual',[0,-1,'*'])}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/individual.png') }}"><P>Reporte Individual</P></div></a>
        <a href="{{route('muestraMultasGeneral',[0,11]) }}" ><div class="grid-item13 verde"><img src="{{ asset('images/menu/multas.png') }}"><P>Multas</P></div></a>
    </div>
    <br>

    <b>Configuraciones</b>
    <div class="grid-container">
    <a href="{{url('empresas')}}" ><div class="grid-item1 verde"><img src="{{ asset('images/menu/empresa.png') }}"><p>Empresas</p></div></a>
    <a href="{{url('marcas')}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/marcas.png') }}"><p>Marcas y Modelos</p></div></a>
    <a href="{{route('clasificados.index')}}" ><div class="grid-item4 verde"><img src="{{ asset('images/menu/clasificado.png') }}"><p>Clasificador Ingresos y Gastos</p></div></a>
    <a href="{{url('empseguros')}}" ><div class="grid-item5 verde"><img src="{{ asset('images/menu/seguro.png') }}"><p>Empresas de Seguro</p></div></a>
    <a href="{{url('provincias')}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/localidades.png') }}"><p>Provincias y Localidades</p></div></a>
</div>
    <br><br>
    @endsection

    @section('supervisor-section')
       <b>Operaciones</b>
    <div class="grid-container">

        <a href="{{url('clientes')}}"><div class="grid-item3 verde"><img src="{{ asset('images/menu/cliente.png') }}"><p>Clientes</p> </div></a>
        <a href="{{route('clientesEspera')}}" ><div class="grid-item3 verde"><img src="{{ asset('images/menu/espera.png') }}"  ><p>Clientes En Espera</p></div></a>
        <a href="{{route('listavehiculos',[0,0])}}" ><div class="grid-item10 verde"><img src="{{ asset('images/menu/vehiculo.png') }}" ><p>Vehículos</p></div></a>
        <a href="{{route('listaprestamos',[0,0,0])}}" ><div class="grid-item11 verde"><img src="{{ asset('images/menu/empenos.png') }}" ><p>Empeños</p></div></a>
        <a href="{{route('muestraMultasGeneral',[0,11]) }}" ><div class="grid-item13 verde"><img src="{{ asset('images/menu/multas.png') }}"><P>Multas</P></div></a>


        <a href="{{url('gastos')}}" ><div class="grid-item14 verde"><img src="{{ asset('images/menu/egreso.png') }}"> <p>Gastos</p></div></a>
        <a href="{{url('ingresos')}}" ><div class="grid-item5 verde"><img src="{{ asset('images/menu/ingreso.png') }}"> <p>Ingresos</p></div></a>
        <a href="{{route('impagos',[0,-1,'*'])}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/cobro.png') }}"> <p>Impagos</p></div></a>
        <a href="{{route('poliza.index')}}" ><div class="grid-item8 verde"><img src="{{ asset('images/menu/poliza.png') }}"> <p>Polizas</p></div></a>
        <a href="{{route('itv.index')}}" ><div class="grid-item9 verde"><img src="{{ asset('images/menu/itv.png') }}"> <p>ITV</p></div></a>
        <a href="{{route('carnet.index')}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/carnet.png') }}"><P>Carnet Conductor</P></div></a>
        <a href="{{route('RentabilidadIndividual',[0,-1,'*'])}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/individual.png') }}"><P>Reporte Individual</P></div></a>

    </div>
    <br>
    <br>
    <b>Configuraciones</b>
    <div class="grid-container">
        <a href="{{url('empresas')}}" ><div class="grid-item1 verde"><img src="{{ asset('images/menu/empresa.png') }}"><p>Empresas</p></div></a>
        <!--
        <a href="{{url('marcas')}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/marcas.png') }}"><p>Marcas y Modelos</p></div></a>
        <a href="{{route('clasificados.index')}}" ><div class="grid-item4 verde"><img src="{{ asset('images/menu/clasificado.png') }}"><p>Clasificador Ingresos y Gastos</p></div></a>
        <a href="{{url('empseguros')}}" ><div class="grid-item5 verde"><img src="{{ asset('images/menu/seguro.png') }}"><p>Empresas de Seguro</p></div></a>
        <a href="{{url('provincias')}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/localidades.png') }}"><p>Provincias y Localidades</p></div></a>
        -->
    </div>
    <br><br>
    @endsection
    @section('operador-section')
       <b>Operaciones</b>
    <div class="grid-container">
        <a href="{{url('clientes')}}"><div class="grid-item3 verde"><img src="{{ asset('images/menu/cliente.png') }}"><p>Clientes</p> </div></a>
        <a href="{{route('clientesEspera')}}" ><div class="grid-item3 verde"><img src="{{ asset('images/menu/espera.png') }}"  ><p>Clientes En Espera</p></div></a>
        <a href="{{route('listavehiculos',[0,0])}}" ><div class="grid-item10 verde"><img src="{{ asset('images/menu/vehiculo.png') }}" ><p>Vehículos</p></div></a>
        <a href="{{route('listaprestamos',[0,0,0])}}" ><div class="grid-item11 verde"><img src="{{ asset('images/menu/empenos.png') }}" ><p>Empeños</p></div></a>

        <a href="{{url('gastos')}}" ><div class="grid-item14 verde"><img src="{{ asset('images/menu/egreso.png') }}"> <p>Gastos</p></div></a>
        <a href="{{url('ingresos')}}" ><div class="grid-item5 verde"><img src="{{ asset('images/menu/ingreso.png') }}"> <p>Ingresos</p></div></a>
        <a href="{{route('impagos',[0,-1,'*'])}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/cobro.png') }}"> <p>Impagos</p></div></a>
        <a href="{{route('poliza.index')}}" ><div class="grid-item8 verde"><img src="{{ asset('images/menu/poliza.png') }}"> <p>Polizas</p></div></a>
        <a href="{{route('itv.index')}}" ><div class="grid-item9 verde"><img src="{{ asset('images/menu/itv.png') }}"> <p>ITV</p></div></a>
        <a href="{{route('carnet.index')}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/carnet.png') }}"><P>Carnet Conductor</P></div></a>
        <a href="{{route('RentabilidadIndividual',[0,-1,'*'])}}" ><div class="grid-item2 verde"><img src="{{ asset('images/menu/individual.png') }}"><P>Reporte Individual</P></div></a>
        <a href="{{route('muestraMultasGeneral',[0,11]) }}" ><div class="grid-item13 verde"><img src="{{ asset('images/menu/multas.png') }}"><P>Multas</P></div></a>

    </div>
    <br>
    @endsection

    @section('cobrador-section')
   <b>Operaciones</b>
    <div class="grid-container">
        <a href="{{route('impagos',[0,-1,'*'])}}" ><div class="grid-item7 verde"><img src="{{ asset('images/menu/cobro.png') }}"> <p>Impagos</p></div></a>
    </div>
    <br>
    @endsection

    @section('content')


     @if (Route::has('login'))
        @auth
        <div class="menu">
            @if($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
            @endif
            <div>
            <h2>MENU PRINCIPAL</h2>
            <br>
            @if(in_array(Auth::user()->nivel,['Admin']))
                @yield('admin-section')
            @elseif(in_array(Auth::user()->nivel,['Jefe de Oficina']))
                @yield('jefeoficina-section')
            @elseif(in_array(Auth::user()->nivel,['Gerente']))
                @yield('gerente-section')
            @elseif(in_array(Auth::user()->nivel,['Supervisor']))
                @yield('supervisor-section')
            @elseif(in_array(Auth::user()->nivel,['Operador']))
                @yield('operador-section')
            @elseif(in_array(Auth::user()->nivel,['Cobrador']))
                @yield('cobrador-section')
            @endif
            </div>
        </div>

        @else
        <?php
            return redirect()->route('login');
        ?>
        @endif
    @endif
    @endsection



