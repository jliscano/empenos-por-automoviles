@extends('layouts.app')

@section('template_title')
    {{ __('Update') }} Empseguro
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">{{ __('Modificar') }} Compañía de Seguro</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('empseguros.update', $empseguro->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf

                            @include('empseguro.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
