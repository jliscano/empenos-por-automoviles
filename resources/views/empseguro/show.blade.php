@extends('layouts.app')

@section('template_title')
    {{ $empseguro->name ?? "{{ __('Show') Empseguro" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Empseguro</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('empseguros.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            {{ $empseguro->nombre }}
                        </div>
                        <div class="form-group">
                            <strong>Activo:</strong>
                            {{ $empseguro->activo }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
