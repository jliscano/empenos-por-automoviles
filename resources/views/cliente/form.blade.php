<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>

<style type="text/css">
    .requerido{
        color: red;
        font-size: 15px;
    }

    /*****estilo de pestañas****/
    .grid-container {
        display: grid;
        grid-template-rows: 80px 80px;
        /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
        grid-template-columns: 180px 180px  180px 180px 180px;
        grid-gap: 15px;
        padding: 15px 15px 15px 15px 15px;
        }
        @media only screen and (max-width:800px) {
        /* For tablets: */
            .grid-container {
                display: grid;
                grid-template-columns: 180px 180px;
            }
        }
        @media only screen and (max-width:560px) {
        /* For mobile phones: */
            .grid-container {
                display: grid;
                grid-template-columns: 180px;
            }
        }
        .resaltado{
            background-color: aqua;
        }
        .resaltado_azul{
            background-color: lightblue;
        }

</style>
<button type="submit" disabled hidden aria-hidden="true"></button>
<div class="box box-info padding-1">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
</div>
    <!--botones-->
    @if(!isset($retorno) || $retorno==1)
    @if($nuevo==0)
    <a href="{{ route('clientes.index') }}" ><div class="grid-item2 btn btn-primary">Clientes</div></a>
        @if($cliente->tvehiculos==0)
            <a href="{{ url('vehiculos/createcliente',[$cliente->id,1]) }}" ><div class="grid-item2 btn btn-primary">Agregar Vehículos</div></a>
        @else
            <a href="{{ route('listavehiculos',[$cliente->id,2]) }}" ><div class="grid-item2 btn btn-primary">Vehículos</div></a>
        @endif
        @if($cliente->tvehiculos>0)
        <a href="{{route('listaprestamoscliente',[$cliente->id,0,$retorno])}}" ><div class="grid-item4 btn btn-primary">Empeños</div></a>
        @endif
        <a href="{{route('muestraDocumentos',[$cliente->id,1,0])}}" ><div class="grid-item4 btn btn-primary">Documentos</div></a>
        <br /><br />
    @endif
    @endif
    <div class="box-body">
        <table border="0" >
            <td colspan="5">
            <tr>
                <div class="form-group">
                {{ Form::label('Empresa :') }}
                {{ Form::select('empresa_id', $empresa, $cliente->empresa_id, ['class' => 'form-control' . ($errors->has('empresa_id') ? ' is-invalid' : ''), 'placeholder' => 'Asigne una Empresa...','id' =>'empresa_id']) }}
                {!! $errors->first('empresa_id', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            </tr>
            <tr>
            <td colspan="3">
                <div class="form-group">
                {{ Form::label('1er Propietario/Administrador:') }}<span class="requerido"></span>
                {{ Form::text('nombre', $cliente->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre Completo','id'=>'nombre']) }}
                {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('DNI :') }}<span class="requerido"></span>
                    {{ Form::text('dni', $cliente->dni, ['class' => 'form-control' . ($errors->has('dni') ? ' is-invalid' : ''), 'placeholder' => 'Dni','id'=>'dni']) }}
                    {!! $errors->first('dni', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('Sexo :') }}<br>
                    {{ Form::select('sexo',$sexo, $cliente->sexo, ['class' => 'form-control ' . ($errors->has('sexo') ? ' is-invalid' : ''), 'placeholder' => 'Asigne un Sexo','id' =>'sexo']) }}
                    {!! $errors->first('sexo', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <div class="form-group">
                {{ Form::label('2do. Propietario :') }}
                {{ Form::text('nombre2', $cliente->nombre2, ['class' => 'form-control' . ($errors->has('nombre2') ? ' is-invalid' : ''), 'placeholder' => 'Nombre Completo','id'=>'nombre2']) }}
                {!! $errors->first('nombre2', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('DNI :') }}
                    {{ Form::text('dni2', $cliente->dni2, ['class' => 'form-control' . ($errors->has('dni2') ? ' is-invalid' : ''), 'placeholder' => 'Dni','id'=>'dni2']) }}
                    {!! $errors->first('dni2', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('Sexo :') }}<br>
                    {{ Form::select('sexo2',$sexo, $cliente->sexo2, ['class' => 'form-control ' . ($errors->has('sexo') ? ' is-invalid' : ''), 'placeholder' => 'Asigne un Sexo','id' =>'sexo2']) }}
                    {!! $errors->first('sexo2', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
        </tr>
        <tr class="resaltado_azul">
            <td colspan="2">
                <div class="form-group">
                {{ Form::label('Tutor/Apoderado :') }}
                {{ Form::text('nombre_tutor', $cliente->nombre_tutor, ['class' => 'form-control' . ($errors->has('nombre_tutor') ? ' is-invalid' : ''), 'placeholder' => 'Nombre Completo','id'=>'nombre_tutor']) }}
                {!! $errors->first('nombre_tutor', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('Tipo :') }}<br>
                    {{ Form::select('tipo_tutor',$tipo_tutor, $cliente->tipo_tutor, ['class' => 'form-control ' . ($errors->has('tipo_tutor') ? ' is-invalid' : ''), 'placeholder' => 'Asigne un Tipo','id' =>'tipo_tutor']) }}
                    {!! $errors->first('tipo_tutor', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('DNI :') }}
                    {{ Form::text('dni_tutor', $cliente->dni_tutor, ['class' => 'form-control' . ($errors->has('dni_tutor') ? ' is-invalid' : ''), 'placeholder' => 'Dni','id'=>'dni_tutor']) }}
                    {!! $errors->first('dni_tutor', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('Sexo :') }}<br>
                    {{ Form::select('sexo_tutor',$sexo, $cliente->sexo_tutor, ['class' => 'form-control ' . ($errors->has('sexo_tutor') ? ' is-invalid' : ''), 'placeholder' => 'Asigne un Sexo','id' =>'sexo_tutor']) }}
                    {!! $errors->first('sexo_tutor', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
        </tr>
        <tr class="resaltado_azul">
            <td colspan="3" >
                <div class="form-group">
                    {{ Form::label('Dirección Tutor/Apoderado :') }}
                    {{ Form::text('dir_tutor', $cliente->dir_tutor, ['class' => 'form-control' . ($errors->has('pespecial') ? ' is-invalid' : ''), 'placeholder' => 'Dirección tutor o apoderado','id'=>'dir_tutor','maxlength=400']) }}
                    {!! $errors->first('dir_tutor', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td colspan="2">
                <div class="form-group">
                    {{ Form::label('Poder Especial :') }}
                    {{ Form::text('pespecial', $cliente->pespecial, ['class' => 'form-control' . ($errors->has('pespecial') ? ' is-invalid' : ''), 'placeholder' => 'Poder Especial...','id'=>'pespecial','maxlength=200']) }}
                    {!! $errors->first('pespecial', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
        </tr>
        <tr class="resaltado">
            <td colspan="3">
                <div class="form-group">
                    {{ Form::label('Empresa Propietaria :') }}
                    {{ Form::text('proempresa', $cliente->proempresa, ['class' => 'form-control' . ($errors->has('proempresa') ? ' is-invalid' : ''), 'placeholder' => 'Empresa propietaria','id'=>'proempresa']) }}
                    {!! $errors->first('proempresa', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('Poder Notarial :') }}
                    {{ Form::text('pnotarial', $cliente->pnotarial, ['class' => 'form-control' . ($errors->has('pnotarial') ? ' is-invalid' : ''), 'placeholder' => 'Poder Notarial Nro.','id'=>'pnotarial']) }}
                    {!! $errors->first('pnotarial', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('DNI/CIF nº :') }}
                    {{ Form::text('dnicif', $cliente->dnicif, ['class' => 'form-control' . ($errors->has('dnicif') ? ' is-invalid' : ''), 'placeholder' => 'DNI/CIF Empresa propietaria','id'=>'dnicif']) }}
                    {!! $errors->first('dnicif', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            </tr>
            <tr>
            <td>
            <div class="form-group">
                {{ Form::label('Fecha Carnet de Conducir :') }}
                {{ Form::date('fcarnet', $cliente->fcarnet, ['class' => 'form-control' . ($errors->has('fcarnet') ? ' is-invalid' : ''), 'placeholder' => 'Agrege una fecha','id'=>'fcarnet']) }}
                {!! $errors->first('fcarnet', '<div class="invalid-feedback">:message</div>') !!}
            </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('Provincia :') }}
                    {{ Form::select('provincia_id', $provincia,$cliente->provincia_id, ['class' => 'form-control' . ($errors->has('provincia_id') ? ' is-invalid' : ''), 'placeholder' => 'Asigne una Provincia','id' =>'provincia_id','id'=>'provincia_id']) }}
                    {!! $errors->first('provincia_id', '<div class="invalid-feedback">:message</div>') !!}
                </div>

            </td>
            <td colspan="3">
                <div class="form-group">
                    {{ Form::label('Localidad :') }}<br>
                    {{ Form::select('municipio_id',$municipio, $cliente->municipio_id, ['class' => 'form-control  mi-selector' . ($errors->has('municipio_id') ? ' is-invalid' : ''), 'placeholder' => 'Asigne una Localidad','id' =>'municipio_id']) }}
                    {!! $errors->first('municipio_id', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            </tr>
            <tr>
            <td colspan="4">
                <div class="form-group">
                    {{ Form::label('Dirección completa :') }}<span class="requerido"></span>
                    {{ Form::text('direccion', $cliente->direccion, ['class' => 'form-control' . ($errors->has('direccion') ? ' is-invalid' : ''), 'placeholder' => 'Direccion','id'=>'direccion']) }}
                    {!! $errors->first('direccion', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td>
                <div class="form-group">
                    {{ Form::label('Código Postal :') }}
                    {{ Form::text('codpostal', $cliente->codpostal, ['class' => 'form-control' . ($errors->has('codpostal') ? ' is-invalid' : ''), 'placeholder' => 'Código Postal','id'=>'codpostal']) }}
                    {!! $errors->first('codpostal', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            </tr>
            <tr>
            <td>
                <div class="form-group">
                    {{ Form::label('Teléfono :') }}<span class="requerido"></span>
                    {{ Form::text('telefono', $cliente->telefono, ['class' => 'form-control' . ($errors->has('telefono') ? ' is-invalid' : ''), 'placeholder' => 'Telefono','id'=>'telefono']) }}
                    {!! $errors->first('telefono', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            <td colspan="4">
                <div class="form-group">
                    {{ Form::label('Correo Electrónico :') }}
                    {{ Form::email('email', $cliente->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'Email','id'=>'email','maxlength'=>'250']) }}
                    {!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
                </div>
            </td>
            </tr>
            <tr>

            </tr>
            <tr>
                <td colspan="4">
                    <div class="form-group">
                        {{ Form::label('Observaciones :') }}
                        {{ Form::text('observacion', $cliente->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observaciones','id'=>'observacion','maxlength'=>'250']) }}
                        {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
                        <input type="hidden" value="<?php echo $retorno;?>" name="retorno">
                </div>
                </td>
                <td >
                    <div class="form-group">
                        {{ Form::label('Situación :') }}
                        {{ Form::select('status', $status,$cliente->status, ['class' => 'form-control' . ($errors->has('status') ? ' is-invalid' : ''), 'id'=>'status']) }}
                        {!! $errors->first('status', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <br>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary" onclick="salir();">{{ __('Guardar') }}</button>


        <a href="{{route('clientes.index')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    </div>
<style type="text/css">
    table {
        width: 100%;
    }
    .grid-container {
            display: grid;
            grid-template-rows: 40px 40px;
            /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
            grid-template-columns: 120px 120px  120px 120px 120px 120px;
            grid-gap: 15px;
            padding: 10px 10px 10px 10px 10px 10px;

    }
    @media only screen and (max-width:800px) {
          /* For tablets: */
         .grid-container {
          display: grid;
          grid-template-columns: 80px 80px;
        }
    }
    @media only screen and (max-width:560px) {
          /* For mobile phones: */
          .grid-container {
          display: grid;
          grid-template-columns: 80px;

        }
    }
</style>
<script type="text/javascript">
 jQuery(document).ready(function($){
    $(document).ready(function() {
        $('.mi-selector').select2();
    });
});
  $('#provincia_id').on('change', function(e){
    //console.log(e);

    $('#municipio_id').append('<option value="0">Espere un momento...</option>');
    $('#municipio_id').empty();
    $('#municipio_id').append('<option value="0">Asigne una Localidad...</option>');
        var nuevo = $('#nuevo').val();

        if(nuevo==1){
            var marca = e.target.value;

            $.get('municipiosbyprovincias/' + marca,function(data) {
                $.each(data, function(fetch, subCate){
                    console.log(data);
                    for(i = 0; i < subCate.length; i++){
                        $('#municipio_id').append('<option value="'+ subCate[i].id +'">'+ subCate[i].nombre +'</option>');
                    }
                })
            })
        }else{
            var marca = e.target.value;
            //var cliente = $('#id').val();
            //alert(marca);
            $.get('municipiosbyprovincias2/' + marca,function(data2) {
                $.each(data2, function(fetch, cMuni){
                    console.log(data2);
                    for(i = 0; i < cMuni.length; i++){
                        $('#municipio_id').append('<option value="'+ cMuni[i].id +'">'+ cMuni[i].nombre +'</option>');
                    }
                })
            })
        }
    });
    //chequea que se guarde antes de abandonar el módulo
    var modified;
    const form = document.forms[0];
    function salir(){
        window.removeEventListener('beforeunload', chequea);
    }

    window.addEventListener('beforeunload', chequea);
    function chequea(e) {
        if (modified) {
            e.preventDefault();
            e.returnValue = '';
        }
    }


    $(document).ready(function (){
        $("input, select").change(function () {
            modified = true;
        });
        $("input, select").keyup(function () {
            modified = true;
        });
    });

</script>
