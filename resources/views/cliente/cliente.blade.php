<table border="1" >
    <tr>
    <td colspan="4">
        <div class="form-group">
        {{ Form::label('Empresa :') }}
        {{ Form::select('empresa_id', $empresa, $cliente->empresa_id, ['class' => 'form-control' . ($errors->has('empresa_id') ? ' is-invalid' : ''), 'placeholder' => 'Asigne una Empresa...','id' =>'empresa_id']) }}
        {!! $errors->first('empresa_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    <tr>
    <td>
        <div class="form-group">
        {{ Form::label('Nombre 1er Propietario/Administrador:') }}<span class="requerido"></span>
        {{ Form::text('nombre', $cliente->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre']) }}
        {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    <td>
        <div class="form-group">
            {{ Form::label('DNI :') }}<span class="requerido"></span>
            {{ Form::text('dni', $cliente->dni, ['class' => 'form-control' . ($errors->has('dni') ? ' is-invalid' : ''), 'placeholder' => 'Dni']) }}
            {!! $errors->first('dni', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    <td>
        <div class="form-group">
        {{ Form::label('Nombre 2do. Propietario :') }}
        {{ Form::text('nombre2', $cliente->nombre2, ['class' => 'form-control' . ($errors->has('nombre2') ? ' is-invalid' : ''), 'placeholder' => 'Nombre 2do Propietario']) }}
        {!! $errors->first('nombre2', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    <td>
        <div class="form-group">
            {{ Form::label('DNI :') }}
            {{ Form::text('dni2', $cliente->dni2, ['class' => 'form-control' . ($errors->has('dni2') ? ' is-invalid' : ''), 'placeholder' => 'Dni']) }}
            {!! $errors->first('dni2', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>

    </tr>
    <tr>
    <td colspan="3">
        <div class="form-group">
            {{ Form::label('Empresa Propietaria :') }}
            {{ Form::text('proempresa', $cliente->proempresa, ['class' => 'form-control' . ($errors->has('proempresa') ? ' is-invalid' : ''), 'placeholder' => 'Empresa propietaria']) }}
            {!! $errors->first('proempresa', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    <td>
        <div class="form-group">
            {{ Form::label('DNI/CIF nº :') }}
            {{ Form::text('dnicif', $cliente->dnicif, ['class' => 'form-control' . ($errors->has('dnicif') ? ' is-invalid' : ''), 'placeholder' => 'DNI/CIF Empresa propietaria']) }}
            {!! $errors->first('dnicif', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    </tr>
    <tr>
    <td>
    <div class="form-group">
        {{ Form::label('Fecha Carnet de Conducir :') }}
        {{ Form::date('fcarnet', $cliente->fcarnet, ['class' => 'form-control' . ($errors->has('fcarnet') ? ' is-invalid' : ''), 'placeholder' => 'Agrege una fecha']) }}
        {!! $errors->first('fcarnet', '<div class="invalid-feedback">:message</div>') !!}
    </div>
    </td>
    <td colspan="" >
        <div class="form-group">
            {{ Form::label('Provincia :') }}<span class="requerido"></span>
            {{ Form::select('provincia_id', $provincia,$cliente->provincia_id, ['class' => 'form-control' . ($errors->has('provincia_id') ? ' is-invalid' : ''), 'placeholder' => 'Asigne una Provincia','id' =>'provincia_id']) }}
            {!! $errors->first('provincia_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        
    </td>
    <td colspan="2">
        <div class="form-group">
            {{ Form::label('Localidad :') }}<span class="requerido">*</span>
            {{ Form::select('municipio_id',$municipio, $cliente->municipio_id, ['class' => 'form-control' . ($errors->has('municipio_id') ? ' is-invalid' : ''), 'placeholder' => 'Asigne una Localidad','id' =>'municipio_id']) }}
            {!! $errors->first('municipio_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        
    </td>
    </tr>
    <tr>
    <td colspan="3">
        <div class="form-group">
            {{ Form::label('Dirección :') }}<span class="requerido"></span>
            {{ Form::text('direccion', $cliente->direccion, ['class' => 'form-control' . ($errors->has('direccion') ? ' is-invalid' : ''), 'placeholder' => 'Direccion']) }}
            {!! $errors->first('direccion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    <td>
        <div class="form-group">
            {{ Form::label('Código Postal :') }}
            {{ Form::text('codpostal', $cliente->codpostal, ['class' => 'form-control' . ($errors->has('codpostal') ? ' is-invalid' : ''), 'placeholder' => 'Código Postal']) }}
            {!! $errors->first('codpostal', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    </tr>
    <tr>
    <td>
        <div class="form-group">
            {{ Form::label('Teléfono :') }}<span class="requerido"></span>
            {{ Form::text('telefono', $cliente->telefono, ['class' => 'form-control' . ($errors->has('telefono') ? ' is-invalid' : ''), 'placeholder' => 'Telefono']) }}
            {!! $errors->first('telefono', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    <td colspan="2">
        <div class="form-group">
            {{ Form::label('Correo Electrónico :') }}
            {{ Form::email('email', $cliente->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'Email']) }}
            {!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </td>
    </tr>
    <tr>
        <td colspan="3">
            <div class="form-group">
                <!--{{ Form::label('status') }}-->
                {{ Form::hidden('status', 1, ['class' => 'form-control' . ($errors->has('status') ? ' is-invalid' : ''), 'placeholder' => 'Status']) }}
                {!! $errors->first('status', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </td>
    </tr>
    <tr>
        <td colspan="4">
            <div class="form-group">
                {{ Form::label('Observaciones :') }}
                {{ Form::text('observacion', $cliente->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observaciones']) }}
                {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
                <input type="hidden" value="<?php echo $retorno;?>" name="retorno">
        </div>
        </td>
    </tr>
</table>