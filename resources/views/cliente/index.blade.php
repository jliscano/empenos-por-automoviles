@extends('layouts.app')

@section('template_title')
    Cliente
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            <span id="card_title">
                                {{ __('Clientes: '.$status) }}
                            </span>
                            <a href="{{route('home')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                             <div class="float-right">
                                @if($status=='')
                                <a href="{{ route('clientes.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                    {{ __('Nevo Cliente') }}
                                </a>
                                @endif
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            {{ $message }}
                        </div>
                    @endif
                    <br>
                    <div class="card-body">
                        {!! $clientes->links() !!}
                        <div class="table-responsive">
                          <form action="{{route('clientes.index')}}">
                            <input type="text" name="texto" class="form-label" style="width:200px;">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                          </form>

                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
										<!--<th>Empresa</th>-->
										<th>Nombre</th>
										<th>DNI</th>
										<th>Vehículos</th>

                                        <th>Sit.<br>Carga</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $actual=0;
                                        $cadena=array();
                                        $cad=0;
                                    @endphp
                                    @foreach ($clientes as $cliente)
                                        <tr>
                                            <td>{{ ++$i }}</td>
											<td>
                                                @if($cliente->proempresa=='')
                                                    <a  href="{{ route('clientes.edit',$cliente->id) }}"> {{$cliente->nombres }}</a>
                                                @else
                                                <a  href="{{ route('clientes.edit',$cliente->id) }}"> {{$cliente->proempresa }}</a>
                                                @endif
                                            </td>
											<td>{{$cliente->dni }}</td>

                                            <td>
                                                @php
                                                $veh=array();
                                                $cont=count($vehiculosa);
                                                for ($p=0;$p<$cont;$p++){
                                                    if($cliente->id==$vehiculosa[$p]['id']){
                                                        $veh[]=array(
                                                            'idveh' => $vehiculosa[$p]['idveh'],
                                                            'matricula' => $vehiculosa[$p]['matricula']
                                                        );
                                                    }
                                                }

                                                $cont=count($veh);
                                                for($p=0;$p<$cont;$p++){
                                                    echo '<a  href="'.route('editavehiculo',[$veh[$p]['idveh'],7]) .'">'.$veh[$p]['matricula'].' </a>';
                                                    //echo '<a class="btn btn-sm empenos_btn btnn" href="{{route('listaprestamoscliente',[$cliente->id,1])}}"><i class="fa fa-fw fa-edit"></i> {{ __('Empeños') }}</a>';

                                                    if($cont>1)
                                                    echo '<br />';
                                                }
                                                @endphp
                                            </td>
                                            <td>
                                                @if(is_null($cliente->id))
                                                    <img src="{{ asset('images/rojo.jpg') }}" width="28" height="25">
                                                @else
                                                    <?php
                                                        $cad++;
                                                        $falta=cuenta_faltantes('clientes',$cliente->id,0,$cadena,$cad);
                                                    ?>
                                                    @if($falta==0)
                                                    <img src="{{ asset('images/verde.jpg') }}" width="28" height="25">
                                                    @else
                                                    <a href=""  data-toggle="modal" data-target="#myModal{{$cad}}">
                                                        <img src="{{ asset('images/rojo.jpg') }}" width="28" height="25">
                                                    </a>
                                                    @endif
                                                @endif
                                                <div class="modal " id="myModal{{$cad}}" role="dialog" >
                                                    <div class="modal-dialog">
                                                      <!-- Modal -->
                                                      <div class="modal-content" style="height:300px;width:300px;right: 350px;">
                                                        <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          <h4 class="modal-title">Datos Faltantes</h4>
                                                        </div>
                                                        <div class="modal-body" >
                                                            <div>
                                                                @php
                                                                    echo $cadena[$cad];
                                                                @endphp
                                                              </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                            <form action="{{ route('clientes.destroy',$cliente->id) }}" method="POST" id="borrar">
                                                @csrf
                                                @method('DELETE')
                                                <input type="hidden" name="retorno" value="1">
                                <!--<a class="btn btn-sm btn-success btnn" href="{{ route('clientes.edit',$cliente->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>-->
                                <a class="btn btn-sm vehiculos_btn btnn" href="{{ route('listavehiculos',[$cliente->id,1]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Vehículos') }}</a>
                                <a class="btn btn-sm empenos_btn btnn" href="{{route('listaprestamoscliente',[$cliente->id,0,1])}}"><i class="fa fa-fw fa-edit"></i> {{ __('Empeños') }}</a>
                                <a class="btn btn-sm documentos_btn btnn" href="{{route('muestraDocumentos',[$cliente->id,1,0])}}"><i class="fa fa-fw fa-edit"></i> {{ __('Documentos') }}</a>
                                <a  class="btn btn-danger btn-sm btnn" onclick="aviso({{$cliente->id}});"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i></a>

                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $clientes->links() !!}
            </div>
        </div>
    </div>

@endsection
<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar este Cliente?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            url=url +'/clientes/'+id;
            document.getElementById("borrar").action = url;

            document.getElementById("borrar").submit();
        }
    }
</script>


<script src="{{asset('modal/assets/jquery-1.12.4-jquery.min.js')}}"></script>
<script src="{{asset('modal/assets/jquery.validate.min.js')}}"></script>
<script src="{{asset('modal/dist/js/bootstrap.min.js')}}"></script>
