@extends('layouts.app')

@section('template_title')
    {{ __('Update') }} Cliente
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">{{ __('Editar') }} Cliente</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('clientes.update',$cliente) }}"  role="form" enctype="multipart/form-data" onkeydown="return event.key != 'Enter';">
                            
                            <?php
                                $nuevo=0;
                            ?>
                            <input type="hidden" name="nuevo" id="nuevo" value="<?php echo $nuevo; ?>">
                           

                            {{ method_field('PATCH') }}
                            @csrf
                            @include('cliente.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
