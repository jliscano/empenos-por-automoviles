@extends('layouts.app2')

@section('template_title')
    {{ __('Create') }} Gastoxvehiculo
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">{{ __('Nuevo') }} Vehículo vinculado a gasto</span>
                    </div>
                    <div class="card-body">
                        <form method="GET" action="{{ route('storeGastosxVehiculos',$empresa_id) }}"  role="form" enctype="multipart/form-data">
                            @csrf
                            <?php
                                $nuevo=1;
                            ?>
                            @include('gastoxvehiculo.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
