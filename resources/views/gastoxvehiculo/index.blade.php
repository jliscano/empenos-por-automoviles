@extends('layouts.app2')

@section('template_title')
    Gastoxvehiculo
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header" style="background-color:red;color: white;">
                        <div style="display: flex; justify-content: space-between; align-items: center; ">
                            <span id="card_title">
                                <b>{{ __('Vehiculos vinculados a este gasto') }}</b>
                            </span>
                            <!--onclick="cambia(<?php echo $gasto_id;?>,<?php echo $empresa_id;?>)"-->
                             <div class="float-right">
                                @if($totalv<$totalg)
                                <a onclick="cambiar(1,0,0)" 
                                class="btn btn-primary btn-sm float-right"  data-placement="left" >
                                  {{ __('Nuevo Vehículo') }}
                                </a>
                                @endif

                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Num</th>                    
										<th>Nombre</th>
										<th>DNI</th>
                                        <th>Matrícula</th>
                                        <th>Marca/Modelo</th>
										<th>Monto</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($gastoxvehiculos as $gastoxvehiculo)
                                        <tr>
                                            <td>{{ ++$i }}</td>
											<td>{{ $gastoxvehiculo->nom }}</td>
											<td>{{ $gastoxvehiculo->dni }}</td>
                                            <td>{{ $gastoxvehiculo->matricula }}</td>
                                            <td>{{ $gastoxvehiculo->mar.'/'.$gastoxvehiculo->mod }}</td>
											<td>{{ number_format($gastoxvehiculo->monto,2,','.',') }}</td>
                                            <td>
                                                <form action="{{ route('destroyGastoxVehiculo',[$gastoxvehiculo->id,$gasto_id,$empresa_id]) }}" method="POST">
                                                    <a class="btn btn-sm btn-success" href="{{ route('editaGastosxVehiculos',[$gastoxvehiculo->id,$empresa_id]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Borrar') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $gastoxvehiculos->links() !!}
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function cambiar(numero,id){
            let gasto=( "<?php echo $gasto_id; ?>" );
            let empresa=( "<?php echo $empresa_id; ?>" );
            let urlstr =( "<?php echo $_SERVER['HTTP_HOST']; ?>" );
            if(numero==1){
                var direccion = 'http://'+urlstr+'/createGastoVehiculo/'+gasto+'/'+empresa+'/create';
                parent.document.getElementById("gxv").src="{{route('createGastoVehiculo',[$gasto_id,$empresa_id])}}";
            }else{
                var direccion = 'http://'+urlstr+'/editaGastoVehiculo/'+id+'/'+empresa+'/edit';
                parent.document.getElementById("gxv").src=direccion;//"{{route('editaGastosxVehiculos',["+id+","+empresa+"] )}}";

            }
            //alert(direccion);
            
            //parent.document.body.style.backgroundColor = "lightblue";

        }
    </script>

@endsection
