@extends('layouts.app')

@section('template_title')
    {{ $gastoxvehiculo->name ?? "{{ __('Show') Gastoxvehiculo" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Gastoxvehiculo</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('gastoxvehiculos.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Gasto Id:</strong>
                            {{ $gastoxvehiculo->gasto_id }}
                        </div>
                        <div class="form-group">
                            <strong>Vehiculo Id:</strong>
                            {{ $gastoxvehiculo->vehiculo_id }}
                        </div>
                        <div class="form-group">
                            <strong>Monto:</strong>
                            {{ $gastoxvehiculo->monto }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
