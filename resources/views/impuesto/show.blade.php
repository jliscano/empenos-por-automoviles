@extends('layouts.app')

@section('template_title')
    {{ $impuesto->name ?? "{{ __('Show') Impuesto" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Impuesto</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('impuestos.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Vehiculo Id:</strong>
                            {{ $impuesto->vehiculo_id }}
                        </div>
                        <div class="form-group">
                            <strong>Motivo:</strong>
                            {{ $impuesto->motivo }}
                        </div>
                        <div class="form-group">
                            <strong>Fpago:</strong>
                            {{ $impuesto->fpago }}
                        </div>
                        <div class="form-group">
                            <strong>Referencia:</strong>
                            {{ $impuesto->referencia }}
                        </div>
                        <div class="form-group">
                            <strong>Monto:</strong>
                            {{ $impuesto->monto }}
                        </div>
                        <div class="form-group">
                            <strong>Piva:</strong>
                            {{ $impuesto->piva }}
                        </div>
                        <div class="form-group">
                            <strong>Iva:</strong>
                            {{ $impuesto->iva }}
                        </div>
                        <div class="form-group">
                            <strong>Observacion:</strong>
                            {{ $impuesto->observacion }}
                        </div>
                        <div class="form-group">
                            <strong>Conciliado:</strong>
                            {{ $impuesto->conciliado }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
