<style type="text/css">
.grid-container {
    display: grid;
    grid-template-rows: 80px 80px;
    /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
    grid-template-columns: 180px 180px  180px 180px;
    grid-gap: 15px;
    padding: 15px 15px 15px 15px;
}
@media only screen and (max-width:800px) {
  /* For tablets: */
 .grid-container {
  display: grid;
  grid-template-columns: 180px 180px;
}
}
@media only screen and (max-width:560px) {
  /* For mobile phones: */
  .grid-container {
  display: grid;
  grid-template-columns: 180px;
  
}
}
</style>
<div class="box box-info padding-1">
    
    <div class="grid-container">
        <div class="grid-item2">
            <div class="form-group">
                {{ Form::label('Tipo :') }}
                {{ Form::select('tipoimpuesto_id',$tipo, $impuesto->tipoimpuesto_id, ['class' => 'form-control' . ($errors->has('tipoimpuesto_id') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione una opción...']) }}
                {!! $errors->first('tipoimpuesto_id', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="grid-item3">
            <div class="form-group">
                {{ Form::label('Fecha de Pago :') }}
                {{ Form::date('fpago', $impuesto->fpago, ['class' => 'form-control' . ($errors->has('fpago') ? ' is-invalid' : ''), 'placeholder' => 'Fpago']) }}
                {!! $errors->first('fpago', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="grid-item4">
            <div class="form-group">
                {{ Form::label('Referencia :') }}
                {{ Form::text('referencia', $impuesto->referencia, ['class' => 'form-control' . ($errors->has('referencia') ? ' is-invalid' : ''), 'placeholder' => 'Referencia']) }}
                {!! $errors->first('referencia', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="grid-item5">
            <div class="form-group">
                {{ Form::label('Monto :') }}
                {{ Form::text('monto', $impuesto->monto, ['class' => 'form-control' . ($errors->has('monto') ? ' is-invalid' : ''), 'placeholder' => 'Monto','id'=>'monto']) }}
                {!! $errors->first('monto', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="grid-item6">
            <div class="form-group">
                {{ Form::label('% IVA :') }}
                {{ Form::text('piva', $impuesto->piva, ['class' => 'form-control' . ($errors->has('piva') ? ' is-invalid' : ''), 'placeholder' => 'Piva','id'=>'piva']) }}
                {!! $errors->first('piva', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="grid-item7">
            <div class="form-group">
                {{ Form::label('Monto IVA :') }}
                {{ Form::text('iva', $impuesto->iva, ['class' => 'form-control' . ($errors->has('iva') ? ' is-invalid' : ''), 'placeholder' => 'Iva','id'=>'iva']) }}
                {!! $errors->first('iva', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="grid-item8">
            <div class="form-group">
                {{ Form::label('Observacion :') }}
                {{ Form::text('observacion', $impuesto->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observacion']) }}
                {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="grid-item9">
            <div class="form-group">
                {{ Form::label('Conciliado :') }}
                {{ Form::select('conciliado',$concilia, $impuesto->conciliado, ['class' => 'form-control' . ($errors->has('conciliado') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione una opción...']) }}
                {!! $errors->first('conciliado', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
    </div>
    Para editar el campo Monto IVA, deje el campo de porcentaje IVA en cero (0)
    <div class="form-group">
        <!--{{ Form::label('vehiculo_id') }}-->
        {{ Form::hidden('vehiculo_id', $impuesto->vehiculo_id, ['class' => 'form-control' . ($errors->has('vehiculo_id') ? ' is-invalid' : ''), 'placeholder' => 'Vehiculo Id']) }}
        <input hidden value="{{$retorno}}" name="retorno">
        <!--{!! $errors->first('vehiculo_id', '<div class="invalid-feedback">:message</div>') !!}-->
    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        <a href="{{route('listaImpuestos',[$impuesto->vehiculo_id,$retorno])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    </div>
    
</div>
<script>

/*
    var modified;
    const form = document.forms[0];
    function salir(){
        window.removeEventListener('beforeunload', chequea);
    }

    window.addEventListener('beforeunload', chequea);
    function chequea(e) {
        if (modified) {
            e.preventDefault();
            e.returnValue = '';
        }
    }
    
    
    $(document).ready(function (){  
        $("input, select").change(function () {
            modified = true;
        }); 
        $("input, select").keyup(function () {
            modified = true;
        });  
    });
*/
    const cuerpoDelDocumento = document.body;
    //cuerpoDelDocumento.onload = refri;
    cuerpoDelDocumento.onload = calc;
    $('body').on('change', '#piva', function () {
        return calc();
    });
    $('body').on('change', '#monto', function () {
        return calc();
    });
    
    function calc(){
        var piva=$('#piva').val();
        var monto =$('#monto').val();
        if(piva>0){
            $('#iva').prop('readonly', false);
            var iva=monto*(piva/100);
            $('#iva').val(iva);
            $('#iva').prop('readonly', true);
        }else{ 
            $('#iva').prop('readonly', false);
        }

    }
</script>