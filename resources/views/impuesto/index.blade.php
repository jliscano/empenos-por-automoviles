@extends('layouts.app')

@section('template_title')
    Impuesto
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Impuesto') }}
                                @if($retorno>0)
                                <div id='iden'>
                                    @foreach($vehiculos as $vehiculo)
                                        Matrícula:<b>{{$vehiculo->matricula}}</b> Marca/Modelo: <b>{{$vehiculo->mar}}/{{$vehiculo->mod}}</b>
                                    @endforeach
                                </div>
                                @endif
                            </span>
                            <a onclick="history.back()"><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                             <div class="float-right">
                                <a href="{{ route('creaImpuesto',[$vehiculo_id,$retorno]) }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nuevo Impuesto') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! $impuestos->links() !!}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
										<th>Impuesto</th>
										<th>Fec. Pago</th>
										<th>Referencia</th>
										<th>Monto</th>
										<th>Conciliado</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($impuestos as $impuesto)
                                    
                                        <tr>
                                            <td>{{ ++$i }}</td>
											<td>{{ $impuesto->tipo }}</td>
											<td>{{ date('d/m/Y',strtotime($impuesto->fpago)) }}</td>
											<td>{{ $impuesto->referencia }}</td>
											<td>{{ $impuesto->monto }}</td>
											<td>{{ $concilia[$impuesto->conciliado] }}</td>

                                            <td>
                                                <form action="{{ route('borrarImpuesto',[$impuesto->id,$impuesto->vehiculo_id,$retorno]) }}" method="POST" id="borrar">
                                                    <!--<a class="btn btn-sm btn-primary " href="{{ route('impuestos.show',$impuesto->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Show') }}</a>-->
                                                    <a class="btn btn-sm btn-success" href="{{ route('editaImpuesto',[$impuesto->id,$retorno]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    
                                                    <a onclick="aviso();" class="btn btn-danger btn-sm btnn"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i> </a>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $impuestos->links() !!}
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function aviso(){
        var resp=confirm("¿Está seguro de eliminar?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            document.getElementById("borrar").submit();
        }
    }
</script>