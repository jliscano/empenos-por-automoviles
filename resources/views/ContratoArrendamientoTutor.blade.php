<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Contrato de Arrendamiento</title>
<style type="text/css">
        .pequena{
            font-size: 10px;
        }
        .cabecera{
            width: 100%;
        }
        .cabecera tr{
            width: 100%;
            text-align: center;
        }
        .basico {
            width: 100%;
            border: 2px solid blue;
            padding: 10px;
            border-radius: 25px;
        }
        table.tabla_sin {
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        th.borde-doble{
            text-align: right;
            padding: 0;
            border-bottom:double;
        }
        .amor{
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        td.alinea_derecha {
            text-align: right;
            padding: 0;
        }

        /*.page-break {
            page-break-after: always;
        }*/
        div{
            font-size: 18px;
        }
        div.page_break + div.page_break{
            page-break-before: always;
        }
    .tabs-1{
        margin-left: 40px;
    }
/** Define the margins of your page **/
@page {
    margin: 160px 110px;
    margin-left: 140px;
    margin-bottom: 120px;

}

header {
position: fixed;
top: -60px;
left: 0px;
right: 0px;
/*height: 50px;*/

/** Extra personal styles **/

text-align: left;
line-height: 35px;

}

footer {
position: fixed;
bottom: -75px;
right: 250px;
}
#logopagina{
    width: 30%;
    display: table;
}

</style>

</head>
<body>

<header>
    <img src="{{ asset('images/logo2.png') }}" width="90" height="60">
</header>

@foreach ($clientes as $cliente)
@php
if($cliente->sexo=='M')
    $don="Don";
else
    $don="Doña";
if($cliente->sexo2=='M')
    $don2="Don";
else
    $don2="Doña";
if($cliente->sexo_tutor=='M'){
    $don_tutor="Don";
    $parentesco='padre';
}
else{
    $don_tutor="Doña";
    $parentesco='madre';
}
@endphp

<footer>
    <div id="logopagina">
        <img src="{{ asset('images/emp'.$cliente->empresa_id.'.png') }}" width="120" height="80">
    </div>
</footer>
<main>
    <br>

    <?php
    $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
    $dias=date('d',strtotime($cliente->ffirma));
    $mes=date('m',strtotime($cliente->ffirma));
    $nano=date('Y',strtotime($cliente->ffirma));
    $unMesDespuesfirma=date("d-m-Y",strtotime($cliente->ffirma."+  2 months"));
    $mesfirma=date('m',strtotime($unMesDespuesfirma));
    $nanofirma =date('Y',strtotime($unMesDespuesfirma));

    $dias1=date('d',strtotime($cliente->finiciopago));
    $mes1=date('m',strtotime($cliente->finiciopago));
    $nano1=date('Y',strtotime($cliente->finiciopago));

    //$unMesDespues=date("d-m-Y",strtotime($cliente->ffirma."+ 1 month"));

    $unMesDespues=date("d-m-Y",strtotime($cliente->finiciopago."+ 1 month"));
    $mesDespues=explode('-', $unMesDespues);
    if($cliente->tipo==1){
        $seisMesesDespues=date("d-m-Y",strtotime($unMesDespues."+6 months"));
        $seisMes=explode('-', $seisMesesDespues);
        $fhasta=explode('-', $seisMesesDespues);
    }else {
        if($cliente->cuotas<=12){
            $hasta=12;
        }else{
            $hasta=24;
        }
        $fhasta12=date("d-m-Y",strtotime($unMesDespues."+".$cliente->cuotas." months"));
        $seisMesesDespues=date("d-m-Y",strtotime($fhasta12."+6 months"));
        $seisMes=explode('-', $seisMesesDespues);
        $fhasta=explode('-', $fhasta12);
    }
    //echo "fecha despues: ".$seisMesesDespues;

    $entero=intval($cliente->valoracion);
    $cent = $cliente->valoracion-$entero;
    ?>
    <table class="cabecera">
        <tr>
            <th align="center">
                CONTRATO DE ALQUILER DE VEHICULO CON OPCION A COMPRA
            </th>
        </tr>
        <tr>
            <td colspan="2" align="left">
                <br>
                    En <u>{{ ($cliente->prov) }}</u> a <u>{{($dias)}}</u>  de <u>{{($meses[(int)$mes])}}</u> de <u>{{($nano)}}</u>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <br>
                REUNIDOS
            </td>
        </tr>
    </table>
    <br>
    <div align="justify">
        <b>De una parte</b>, Don. {{$cliente->replegal}}, mayor de edad, con D.N.I. número {{($cliente->dnireplegal)}}, actúa en representación de la empresa {{($cliente->emp)}}, con domicilio en {{($cliente->domicilio)}},  con C.I.F: nº {{($cliente->cif)}}, en su condición de administrador único, en adelante ARRENDADOR Y PROPIETARIO.
    </div>
    <br>
    <div align="justify">
        @php
            $coletilla_empresa=' actúa en su propio nombre y representación.';
            if(!is_null($cliente->proempresa))
            $coletilla_empresa=' quien actúa en nombre y representación de '. $cliente->proempresa.' '.$clientes->dnicif.'.';
            $nombre2='';

            if($cliente->nombre2!='' and !is_null($cliente->nombre2)){
            $coletilla_empresa=' actúan en su propio nombre y representación.';
            if(!is_null($cliente->proempresa))
                $coletilla_empresa=' quienes actúan en nombre y representación de '. $cliente->proempresa.' '.$clientes->dnicif.'.';
                $nombre2=' y '.$don2.' '.$cliente->nombre2.', mayor de edad, con D.N.I número '. $cliente->dni2;
            }
        @endphp
        @if ($cliente->tipo_tutor=='Tutor')
        <b>De otra parte</b>, {{$don_tutor}}
        {{(($cliente->nombre_tutor))}}., mayor de edad, con D.N.I número
        {{(($cliente->dni_tutor))}},
        con domicilio en {{($cliente->direccion)}},
        {{$cliente->loc}} ({{($cliente->prov)}}),
        quien actúa en nombre y representación de {{$don}} {{$cliente->nom}} con D.N.I
        {{$cliente->dni}} en su condición de {{$parentesco}} o tutor legal, en adelante  ARRENDATARIO Y OPTANTE.
        @else
        <b>De otra parte</b>, {{$don_tutor}}
        {{(($cliente->nombre_tutor))}}., mayor de edad, con D.N.I número
        {{(($cliente->dni_tutor))}},
        con domicilio en {{($cliente->dir_tutor)}},
        quien actúa en nombre y representación de {{$don}} {{$cliente->nom}} con D.N.I
        {{$cliente->dni}},
        con domicilio en {{($cliente->direccion)}},
        {{$cliente->loc}} ({{($cliente->prov)}}),
        mediante poder especial número {{$cliente->pespecial}},
        en adelante  ARRENDATARIO Y OPTANTE.
        @endif

    </div>
    <br>
    <div align="center">
        DECLARAN
    </div>
    <br>
    <div align="justify">
        I.- Que la empresa {{($cliente->emp)}}, es propietaria del vehículo tipo TURISMO, Marca:{{($cliente->mar.'/'.$cliente->mod)}}, Año:{{(date('Y',strtotime($cliente->fmatricula)))}}, Número de bastidor:{{($cliente->bastidor)}} y Matricula:{{($cliente->matricula)}}.
    </div>
    <br>
    <div align="justify">
        II.- Que el optante, está interesado en celebrar un contrato de alquiler con opción de compra del citado vehículo.
    </div>
    <br>
    <div align="justify">
        III.– Que reconociéndose ambas parte la mutua capacidad legal necesaria, convienen en celebrar el presente contrato de alquiler con opción de compra del citado vehículo con arreglo a las siguientes:
    </div>
    <br>
    <div align="center">
        CLAUSULAS
    </div>
    <br>
    <div align="justify">
        <b><u>PRIMERA</u></b>: OBJETO DEL CONTRATO.
    </div>
    <br>
    <div align="justify">
        El objeto del presente contrato será el arrendamiento del vehículo descrito en el expositivo I, cuyas características y estado conoce y acepta la parte arrendataria.
    </div>
    <br>
    <div align="justify">
        Así mismo, a la fecha de fin del arrendamiento, el OPTANTE podrá optar por la compra del vehículo, concertando con el PROPIETARIO el correspondiente contrato de compraventa del vehículo.
    </div>
    <br>
    <div align="justify">
        <b><u>SEGUNDA</u></b>: VIGENCIA Y DURACIÓN DEL CONTRATO:
    </div>
    <br>
    @if($cliente->tipo==1)
        <div align="justify">
            El plazo de duración del presente contrato será de 1 MES, surtiendo efectos a partir del día <u>{{$dias}}</u> de <u>{{$meses[(int)$mes]}}</u> de <u>{{$nano}}</u> y hasta el día <u>01</u> de <u>{{$meses[(int)$mesfirma]}}</u>. de <u>{{$nanofirma}}</u>.
        </div>
    @else
    <div align="justify">
        <?php
        $tiempo='';
        $anos1='';
        $ameses1='';
        if($cliente->cuotas<12){
            $ameses1= $cliente->cuotas.' meses';
        }else{
            $modulo=intval($cliente->cuotas/12);
            switch($modulo){
                case 2:
                    $anos1=$modulo.' años';
                break;
                case 1:
                    $anos1=$modulo.' año';
                    if(($cliente->cuotas-12)>0)
                        $ameses1=' y '.($cliente->cuotas-12).' mes';
                    if(($cliente->cuotas-12)>1)
                        $ameses1=$ameses1.'es';
                break;
                case 0:
                    if(($cliente->cuotas)>0)
                        $ameses1=($cliente->cuotas).' mes';
                    if(($cliente->cuotas)>1)
                        $ameses1=$ameses1.'es';
                break;
            }
        }
        $tiempo=$anos1.$ameses1;
        //{{ $tiempo }}, {{$dias1}}
        $anos2despuesfirma =date("d-m-Y",strtotime($cliente->ffirma."+ ".($cliente->cuotas+1)." months"));
        $fhasta[1]=date("m",strtotime($anos2despuesfirma));
        $fhasta[2]=date("Y",strtotime($anos2despuesfirma));
        ?>
        El plazo de duración del presente contrato será de {{$tiempo}} surtiendo efectos a partir del día <u>{{$dias}}</u> de <u>{{$meses[(int)$mes]}}</u> de <u>{{$nano}}</u> y hasta el <u>01</u> de <u>{{$meses[(int)$fhasta[1]]}}</u> de <u>{{$fhasta[2]}}</u>
        , salvo que el ARRENDATARIO notifique por escrito al ARRENDADOR, con una antelación mínima de 10 días a la fecha del vencimiento del periodo inicial del contrato, su voluntad de cancelar el presente contrato.
    </div>
    @endif

    <br>
    @if($cliente->tipo==1)
    <div align="justify">
        Llegado el día de vencimiento del contrato, este podrá prorrogarse automáticamente por mensualidades completas hasta un máximo de seis (6), siendo la fecha de fin de la última de las prórrogas el día <u>01</u>. de <u>{{$meses[(int)$seisMes[1]]}}</u>. de <u>{{$seisMes[2]}}</u>, salvo que el ARRENDATARIO notifique por escrito al ARRENDADOR, con una antelación mínima de 10 días a la fecha del vencimiento del periodo inicial del contrato o de cualquiera de sus prorrogas, su voluntad de cancelar el presente contrato.
    </div>
    <br>
    @endif
    <div align="justify">
        @if($cliente->tipo==1)
        <b><u>TERCERA</u></b>: CUOTA DE ALQUILER Y FORMA DE PAGO.
        @else
        <b><u>TERCERA</u></b>: CUOTA DE ALQUILER, PRECIO, PLAZO DE COMPRA Y FORMA DE PAGO
        @endif
    </div>
    <br>
    <div align="justify">
        La cuota pactada por el alquiler del vehículo mientras se ejercita la opción de compra es de <u>{{ traducir($cliente->tcuota) }}</u> EUROS ({{number_format($cliente->tcuota,2,',','.')}} €) mensuales, IVA INCLUIDO.
    </div>
    <br>
    @if($cliente->tipo==1 and $cliente->residual>0)
    <div align="justify">
        El primer mes se regularizará la cuota desde la fecha de firma del contrato hasta el día 01 del mes siguiente, la cantidad resultante es de <u>{{ traducir($cliente->residual) }}</u>.EUROS ( <u>({{number_format($cliente->residual,2,',','.')}}€</u> ), es el resultante de dividir la cuota mensual entre el total de días del mes y multiplicar por los días restantes  hasta el día 01. A partir de esta regularización los periodos de facturación irán comprendidos entren el primer y último día de cada mes.
    </div>
    <br>
    @endif
    <div align="justify">
        Este importe será abonado mensualmente mediante transferencia o ingreso a
        la cuenta de {{$cliente->banconombre}} {{$cliente->cuenta}},
        teniendo que hacer efectivo el mismo antes del 2 de cada mes,
        a partir de la firma del presente contrato.
        @if($cliente->tipo==1)
        En concepto de la trasferencia hay que indicar la matrícula del vehículo.
        @else
        <div align="justify">En concepto de la trasferencia hay que indicar COMPRA Y ALQUILER Y MATRICULA.
        </div>
        @endif
    </div>
    <br>
    <div align="justify">
        Si no se ingresará la mensualidad en la fecha indicada anteriormente, el optante tendrá diez días naturales para subsanar y hacer efectivo el pago, es decir, hasta el día 12 de cada mes, trascurrido este periodo perderá el derecho de ejercitar la opción de compra.
    </div>
    @if($cliente->tipo==0)

    <ol type="I">
        <li>
            <div align="justify">
            <b>LA OBLIGACION DEL ARRENDADOR-</b> Llegado el día
            <u>01</u> de <u>{{$meses[(int)$fhasta[1]]}}</u> de <u>{{$fhasta[2]}}</u> y
            estando todas las mensualidades al corriente de pago. {{$cliente->emp}} dará por
            finalizado el contrato y tendrá la obligación de transferir el vehículo
            a favor del OPTANTE.
            </div>
        </li>
        <li>
            <div align="justify">
            <b>PLAZO PARA EL EJERCICIO DE LA OPCION-</b> El plazo para ejercer la
            opción de compra sobre el vehículo descrito, se podrá ejecitar siempre Y
            cuando esté en vigor lo estipulado en la clausula 2 del presente contrato.
            </div>
        </li>
        <li>
            <div align="justify">
            <b>VENCIMIENTO DE LA OPCIÓN Y ALQUILER-</b> Vencido el plazo sin que el optante
            haga uso de su opción de compra, este deberá hacer entrega del vehículo en
            las 24 horas siguientes a su caducidad. El OPTANTE pordrá ejercer su opción
            de compra de citado vehículo por la cantidad referida en el punto anterior,
            siempre y cundo esté al corriente de todos los pagos.<br>
            Las partes establecen  de mutuo acuerdo como clausula penal de pago de
            TREINTA EUROS (30,00€) por cada uno que pase sin que se haya producido
            el reintegro del vehículo.<br>
            LLegado que sea el dia <u>01</u> de <u>{{$meses[(int)$fhasta[1]]}}</u> de <u>{{$fhasta[2]}}</u>
            sin que lo anterior se haya verificado, o por el incumplimiento reiterado del pago
            de las mensualidades pactadas, la PROPIETARIA sin necesidad de preaviso alguno,
            podrá optar por recoger el vehículo, del lugar en que este se ecuentre,
            por si o por terceros, debiendo el OPTANTE tenerlo a disposición de la PROPIETARIA
            y libre de pertenencias tal y como expresamente se ha pactado.
            Los gastos estipulados por la reiteración de impago de cuotas de alquiler o la
            recogida de vehículo quedan establecidos en TRESCIENTOS EUROS (300,00€),
            serán por cuenta del ARRENDATARIO y repercutidos al mismo.
            </div>
        </li>

    </ul>
    @endif
    <br>
    <div align="justify">
        <b><u>CUARTA</u></b>: GESTION DE MULTAS:
    </div>
    <br>
    <div align="justify">
        Toda sanción o multa que llegue a nuestras instalaciones, bien en forma de expediente sancionador de la Jefatura Provincial de Tráfico o en forma de providencia de apremio de la Agencia Tributaria, será abonada posteriormente por el optante, a la empresa {{$cliente->emp}} por la cuantía total de dicha sanción, siempre y cuando esta última se vea en la obligación de pagarla ante cualquier organismo que así lo requiera. Se devengará el importe de 32,87€ más el Impuesto sobre el Valor Añadido aplicable, o impuesto equivalente en vigor, en concepto de cargo por Gestión de Sanciones de Tráfico (multas). Dicho cargo se devengará por cada boletín o expediente sancionador instruido por la autoridad competente en relación con el vehículo arrendado y por hechos acaecidos durante la vigencia del arrendamiento.
    </div>
    <br>
    <div align="justify">
        <b><u>QUINTA</u></b>: CONTRATACION DEL MANTENIMIENTO, ITV, SEGURO E IMPUESTO MUNICIPAL:
    </div>
    <br>
    <div align="justify">
        Toda reparación, así como el debido mantenimiento, que se requiera por el uso del vehículo correrá por cuenta del ARRENDATARIO.
    </div>
    <br>
    <div align="justify">
        El impuesto municipal del vehículo será a cuenta del ARRENDATARIO, así como las inspecciones periódicas y el seguro obligatorio del vehículo.
    </div>
    <br>
    <div align="justify">
        <b><u>SEXTA</u></b>: SEGURO DE VEHICULO:
    </div>
    <br>
    <div align="justify">
        El Arrendatario deberá tener en vigor el seguro obligatorio, de no ser así se podrá considerar como incumplimiento del contrato. Cualquier sanción administrativa por no tener el seguro obligatorio en vigor, {{$cliente->emp}}, notificará a tráfico la sanción y el Arrendatario tendrá que hacerse cargo de ella.
    </div>
    <br>
    <div align="justify">
        <b><u>SEPTIMA</u></b>: RESPONSABILIDAD Y USO-
    </div>
    <br>
    <div align="justify">
        El ARRENDATARIO es responsable de cualquier daño ocasionado en el vehículo o a terceros, aunque los mismos tengan carácter imprudente o fortuito, cuyo importe le será repercutido al finalizar el plazo de la opción, sea esta ejercitada o no. En el supuesto de que el vehículo sea declarado siniestro total, mientras se encuentre el arrendatario en posesión del vehículo, este habrá de pagar a {{$cliente->emp}}., la cantidad estipulada en la cláusula referente a la opción de compra que asciende a <u>{{ traducir($cliente->importe) }}</u> EUROS ({{ number_format($cliente->importe,2,',','.') }}€), además de las cuotas impagadas hasta el momento del siniestro más la deuda generada por incumplimiento de contrato.
    </div>
    <br>
    <div align="justify">
        Igualmente, es responsable de la pérdida o deterioro de las pertenencias, cualesquiera que fuere su carácter, que deposite o porte en el vehículo.
    </div>
    <br>
    <div align="justify">
        <b><u>OCTAVA</u></b>: CESION DEL USO DEL VEHICULO-
    </div>
    <br>
    <div align="justify">
        El optante se obliga a igualmente a no ceder el uso del vehículo a persona alguna, quedando excluidos de su conducción cualquier persona que no sea el optante o que no autorice expresamente LA PROPIETARIA.
    </div>
    <br>
    @if($cliente->tipo==1)
    <div align="justify">
        <b><u>NOVENA</u></b>: OPCION DE COMPRA-
    </div>
    <ol type="I" start="1" style="margin-left: -20px;">
    <li>
        <div align="justify" style="">
            PRECIO DE LA OPCION- El precio total de la opción de compra sobre el vehículo citado es la cantidad de <u>{{ traducir($cliente->importe) }}</u>. EUROS ({{number_format($cliente->importe,2,',','.')}}€), siempre y cuando este al corriente de pago en concepto de uso y disfrute del vehículo.
        </div>
    </li>
    <li>
        <div align="justify">
        @if ($cliente->tipo==1)
            FORMA DE PAGO DE LA OPCION- El optante abonará la cantidad de
            <u>{{ traducir($cliente->tcuota) }}</u> EUROS
            ({{number_format($cliente->importe,2,',','.')}}€),
            en el número de cuenta  de {{$cliente->banconombre}}
            {{$cliente->cuenta}}, en concepto de compra del vehículo.
            El optante podrá ejercer su opción de compra del vehículo siempre y
            cuando este al corriente de todos los pagos.
        @else
            FORMA DE PAGO DE LA OPCION- El optante abonará la cantidad de
            <u>{{ traducir($cliente->tcuota) }}</u> EUROS
            ({{number_format($cliente->tcuota,2,',','.')}}€),
            en el número de cuenta  de {{$cliente->banconombre}}
            {{$cliente->cuenta}}, en concepto de compra del vehículo.
            El optante podrá ejercer su opción de compra del vehículo siempre y
            cuando este al corriente de todos los pagos.
        @endif

        </div>
    </li>
    <li>
        <div align="justify">
            PLAZO PARA EL EJERCICIO DE LA OPCION- El plazo para ejercer la opción de compra sobre el vehículo descrito, se podrá ejercitar siempre y cuando esté en vigor lo estipulado en la cláusula 2.1 del presente contrato.
        </div>
    </li>
    <li>
        <div align="justify">
            VENCIMIENTO DE LA OPCION- Vencido el plazo sin que el optante haga uso de la misma, este deberá hacer entrega del vehículo en las 24 horas siguientes a su caducidad.
        </div>
        <br>
        <div align="justify">
            Las partes establecen de mutuo acuerdo como clausula penal el pago de TREINTA (30,00€)/ día, por cada uno que pase sin que se haya producido el reintegro del vehículo.
        </div>
        <br>
        <div align="justify">
            Llegado que sea el día <u>01</u> de <u>{{$meses[(int)$fhasta[1]]}}</u> de <u>{{$fhasta[2]}}</u> sin que lo anterior se haya verificado, o por el incumplimiento reiterado del pago de las mensualidades pactadas se dará por cancelado el contrato, LA PROPIETARIA sin necesidad de preaviso alguno, podrá optar por recoger el vehículo del lugar en que este se encuentre, por si o por terceros, debiendo el optante tenerlo a disposición de LA PROPIETARIA y libre de pertenencias tal y como expresamente se ha pactado. Los gastos derivados de la recogida del vehículo quedan establecidos en TRESCIENTOS EUROS (300,00€), más los ocasionados por la demora y serán por cuenta del y repercutidos al mismo.
        </div>
        <br>
    </li>
    </ol>
    <div align="justify">
        <b><u>DECIMA</u></b>: INCUMPLIMIENTO DEL CONTRATO.
    </div>
    <br>
    <div align="justify">
        Cualquier incumplimiento en algunos de los puntos aquí descritos, negligencia, o actos de mala fe, será motivo más que suficiente para la anulación de la opción de compra, la correspondiente retirada del vehículo y se dará por cancelado el contrato.
    </div>
    <br>
    <div align="justify">
        <b><u>DECIMOPRIMERA</u></b>: COMUNICACIONES
    </div>
    <br>
    <div align="justify">
        A efectos de notificaciones se conviene como domicilio de {{$cliente->emp}} en su oficina sita en {{ $cliente->domicilio }}, y del arrendatario, el que figura en el encabezamiento del presente contrato.
    </div>
    <br>
    <div align="justify">
        Todas las notificaciones que se efectúen a dichos domicilios, se entenderán por bien hechas, salvo que por conducto fehaciente cualquiera de las partes hubiese indicado, a la otra, su cambio de domicilio a estos efectos.
    </div>
    <br>
    <div align="justify">
        <b><u>DECIMOSEGUNDA</u></b>: JURISDICCION
    </div>
    <br>
    <div align="justify">
        Para la resolución de cualquier controversia o discrepancia que pudiera surgir en la interpretación o ejecución del presente contrato, las partes se someten, con expresa renuncia al fuero que pudiera corresponderles, a la jurisdicción y competencia de los Juzgados y Tribunales de Murcia-Capital.
    </div>
    <br>
    <div align="justify">
        En prueba de conformidad con cuanto antecede, ambas partes firman el presente Contrato, en duplicado ejemplar, en el lugar y fecha "ut supra" indicados.
    </div>
    @else
        <div align="justify">
        <b><u>NOVENA</u></b>: INCUMPLIMIENTO DEL CONTRATO.
    </div>
    <br>
    <div align="justify">
        Cualquier incumplimiento en algunos de los puntos aquí descritos, negligencia, o actos de mala fe, será motivo más que suficiente para la anulación de la opción de compra, la correspondiente retirada del vehículo y se dará por cancelado el contrato.
    </div>
    <br>
    <div align="justify">
        <b><u>DECIMA</u></b>: COMUNICACIONES
    </div>
    <br>
    <div align="justify">
        A efectos de notificaciones se conviene como domicilio de {{$cliente->emp}} en su oficina sita en {{ $cliente->domicilio }}, y del arrendatario, el que figura en el encabezamiento del presente contrato.
    </div>
    <br>
    <div align="justify">
        Todas las notificaciones que se efectúen a dichos domicilios, se entenderán por bien hechas, salvo que por conducto fehaciente cualquiera de las partes hubiese indicado, a la otra, su cambio de domicilio a estos efectos.
    </div>
    <br>
    <div align="justify">
        <b><u>DECIMOPRIMERA</u></b>: JURISDICCION
    </div>
    <br>
    <div align="justify">
        Para la resolución de cualquier controversia o discrepancia que pudiera surgir en la interpretación o ejecución del presente contrato, las partes se someten, con expresa renuncia al fuero que pudiera corresponderles, a la jurisdicción y competencia de los Juzgados y Tribunales de Murcia-Capital.
    </div>
    <br>
    <div align="justify">
        En prueba de conformidad con cuanto antecede, ambas partes firman el presente Contrato, en duplicado ejemplar, en el lugar y fecha "ut supra" indicados.
    </div>
    @endif
    <br>
    <table>
        <tr>
            <td>
                LA PROPIETARIA
            </td>
            <td>
                EL OPTANTE
            </td>
        </tr>
        <tr>
            <td>
                {{$cliente->emp}}<br>
                <img src="{{ asset('images/emp'.$cliente->empresa_id.'.png') }}" width="120" height="80">
            </td>
            <td>

            </td>
        </tr>
        <tr>
            <td>
                Fdo.: {{$cliente->replegal}}
                <br>
            </td>
            <td>
                Fdo.:
                @if($cliente->proempresa!='')
                    Por:{{$cliente->proempresa}}<br>
                @endif
                    {{$cliente->nombre_tutor}}
            </td>
        </tr>
    </table>
    <br>
    <br>
    <div align="justify" class="pequena">
        En {{$cliente->emp}}. tratamos la información que aporta para el contrato con la finalidad de gestionar los compromisos derivados del mismo. Los datos personales que tratamos en {{$cliente->emp}}. son facilitados por el propio interesado y las categorías de datos tratadas son: Datos identificativos y datos económicos. Sus datos personales serán mantenidos durante el tiempo que dure la relación contractual y posteriormente 10 años durante el cual pueden ser solicitados por Juzgados y Tribunales. La base legal para el tratamiento de sus datos es la ejecución del contrato. Dependiendo de su forma de pago sus datos serán cedidos a bancos o cajas para la gestión de la misma, los datos personales no serán proporcionados a otros terceros salvo obligación legal. Cualquier persona tiene derecho a obtener confirmación sobre si en {{$cliente->emp}}. estamos tratando datos personales que les conciernan, o no.  Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos. En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones. En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos. En cualquier momento usted que puede presentar reclamación ante la autoridad de control.
    </div>
    <div style="color:blue">
        Teléfono de Contacto: 622 161 442<br>
        www.confiacar.es
    </div>
    <footer>
        <script type="text/php">
            if ( isset($pdf) ) {
                // v.0.7.0 and greater
                $pdf->page_script('

                $x = 462;
                $y = 745;

                $text = "Pág :{PAGE_NUM} de {PAGE_COUNT}" ;
                $font = $fontMetrics->get_font("helvetica", "normal");
                $size = 12;
                $color = array(0,0,0);
                $word_space = 0.5;  //  default
                $char_space = 0.3;  //  default
                $angle = 0.0;   //  default
                $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
                ');
            }
        </script>

    </footer>
</main>

@endforeach

</body>
</html>
