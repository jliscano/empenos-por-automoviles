<?php
$micarpeta =  ('/documentos'.'/'.$documento->cliente_id);
?>
<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            <!--{{ Form::label('cliente_id') }}-->
            {{ Form::hidden('cliente_id', $documento->cliente_id, ['class' => 'form-control' . ($errors->has('cliente_id') ? ' is-invalid' : ''), 'placeholder' => 'Cliente Id']) }}
            {!! $errors->first('cliente_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!--{{ Form::label('cliente_id') }}-->
            {{ Form::hidden('vehiculo_id', $documento->vehiculo_id, ['class' => 'form-control' . ($errors->has('vehiculo_id') ? ' is-invalid' : ''), 'placeholder' => 'Vehículo Id']) }}
            {!! $errors->first('vehiculo_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!-- {{ Form::label('Nombre del Archivo') }} -->
            {{ Form::text('nombre', $documento->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre','id' => 'nombre']) }}
            {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
            {{ Form::hidden('nombreant', $documento->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre','id' => 'nombreant']) }}
            {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        
        {{ csrf_field() }}
        <label for="archivo">Archivo: </label><br>
        <input type="file" name="archivo" id='archivo'  class="form-control" onchange="PreviewImage()">
        <div class="form-group">
            {{ Form::label('Vista Previa') }}<br>
            <div style="clear:both">
                <iframe id="viewer"  frameborder="0" scrolling="yes" width="100%" height="300"></iframe>
            </div>
            
        </div>
    </div>
    <br>
    
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        <a href="{{route('muestraDocumentos',[$documento->cliente_id,$retorno,$vehiculo_id])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    </div>
</div>

<script type="text/javascript">
    const cuerpoDelDocumento = document.body;
    cuerpoDelDocumento.onload = getUrl;
    
    function getUrl(){
        if(!document.getElementById("nombre").value==''){
            var nombre=document.getElementById("nombre").value;
            var extension=getFileExtension(nombre);
            
            if(extension==1){
                //pdffile='./public/documentos/'+<?php echo $documento->cliente_id ?>+'/'+document.getElementById("nombre").value;
                
                
                pdffile="{{asset('/documentos/'.$documento->cliente_id.'/'.$documento->vehiculo_id.'/'.$documento->nombre)}}";
                //alert(pdffile);
                $('#viewer').attr('src',pdffile);
            }else{
                $('#viewer').attr('src','/public/images/nodisponible.png');
            }
        }
    }
    function PreviewImage() {
        pdffile=document.getElementById("archivo").files[0];
        nombre=document.getElementById("archivo").files[0].name;
        document.getElementById("nombre").value=nombre;
        var presenta=getFileExtension(nombre);
        
        if(presenta==1){
            pdffile_url=URL.createObjectURL(pdffile);
            $('#viewer').attr('src',pdffile_url);
        }else{
            $('#viewer').attr('src','/public/images/nodisponible.png');
        }
    }
    function getFileExtension(filename) {
        var extension=filename.split('.').pop();
        if(extension=='xls' | extension=='xlsx' | extension=='doc' | extension=='docs'){
            return 0;
        }else{
            return 1;
        }
    }

</script>