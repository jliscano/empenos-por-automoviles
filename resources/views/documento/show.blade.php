@extends('layouts.app')

@section('template_title')
    {{ $documento->name ?? "{{ __('Show') Documento" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Documento</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('documentos.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Cliente Id:</strong>
                            {{ $documento->cliente_id }}
                        </div>
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            {{ $documento->nombre }}
                        </div>
                        <div class="form-group">
                            <strong>Via:</strong>
                            {{ $documento->via }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
