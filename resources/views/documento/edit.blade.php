@extends('layouts.app')

@section('template_title')
    {{ __('Update') }} Documento
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">{{ __('Modificar ') }} Documento</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('documento.update', $documento->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf
                            <?php
                                $retorno=1;
                                $nuevo=0;
                            ?>

                            @include('documento.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
