@extends('layouts.app')
<style>
    .imagen{
        width:80px;
        height:80px;
    }
</style>

@section('template_title')
    Documento
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                        @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                        @endif
                        
                        @foreach ($clientes as $cliente)
                        
                            <span id="card_title">
                                <a href="{{route('clientes.edit',$cliente->id)}}">
                                
                                {{ __('Documentos de: '.$cliente->nombres) }}
                                <br>
                                {{'DNI: '.$cliente->dni }}
                            </a>
                            </span>
                            @if($retorno==1)
                                <a href="{{route('clientes.index')}}">
                            @elseif($retorno==4 or $retorno==2)
                            <a href="{{route('listavehiculos',[$cliente->id,1]) }}"  >
                            @elseif($retorno==5 )
                            <a href="{{route('listavehiculos',[$cliente->id,1]) }}"  >
                            @else
                            <a href="javascript:window.history.back()"  >
                            @endif
                            <div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>

                             <div class="float-right">
                                <a href="{{ route('multiple',[$cliente->id,1,$vehiculo_id]) }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                    {{ __('Carga Multiple') }}
                                </a>
                                <a href="{{ route('createDocumento',[$cliente->id,1,$vehiculo_id]) }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nuevo Documento') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    <div class="card-body">
                    @if(!is_null($cliente->id))
                    <a href="{{ route('clientes.index') }}" ><div class="grid-item2 btn btn-primary">Clientes</div></a>
                    <a href="{{ route('listavehiculos',[$cliente->id,1]) }}" ><div class="grid-item2 btn btn-primary">Vehículos</div></a>
                    <a href="{{ route('listaprestamoscliente',[$cliente->id,$vehiculo_id,1]) }}" ><div class="grid-item2 btn btn-primary">Empeños</div></a>
                    @endif
                    <br /><br />
                    @endforeach
                        {{"Vehículos :"}}
                        <select name="vehiculo_id" id="vehiculo_id" onchange="cambiar(this.value,{{$retorno}},{{$cliente->id}})">
                            
                            @php
                                $selected='';
                                foreach($vehiculos as $veh){
                                    if($vehiculo_id==$veh->id)
                                        $selected=' selected ';
                                    else {
                                        $selected=' ';
                                    }
                                    echo "<option value=".$veh->id." ".$selected." >".$veh->matricula.'-'.$veh->marca.'/'.$veh->modelo."</option>";
                                }
                            @endphp
                        </select>                        
                    <br /><br />
                    {!! $documentos->links() !!}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
										<th>Nombre</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($documentos as $documento)
                                        <tr>
                                            <td valign='middle'>{{ ++$i }}</td>
											<td valign='middle'><a target="_blank" href="{{asset('/documentos/'.$documento->cliente_id.'/'.$documento->vehiculo_id.'/'.$documento->nombre)}}">{{ $documento->nombre }}</a></td>
                                            <td valign='middle'><a target="_blank" href="{{asset('/documentos/'.$documento->cliente_id.'/'.$documento->vehiculo_id.'/'.$documento->nombre)}}"><img class='imagen' 
                                            src="{{asset('documentos/'.$documento->cliente_id.'/'.$documento->vehiculo_id.'/'.$documento->nombre)}}" alt="" 
                                            onerror="this.onerror=null;
                                            this.src='{{asset('/images/'.$documento->tipo)}}'"></a></td>
                                            <td>
                                                <form action="{{ route('destroyDocumento',[$documento->id,$cliente_id,$retorno,$vehiculo_id]) }}" method="POST">
                                                    <a class="btn btn-sm btn-success" href="{{ route('documento.edit',$documento->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Borrar') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $documentos->links() !!}
            </div>
        </div>
    </div>
@endsection
<script>
    function cambiar(vehiculo_id,retorno,cliente){
        //alert(vehiculo_id+cliente+retorno);
        //var ruta={{public_path('documentos/muestraDocumentos/')}};
        let url ="<?php echo env('APP_URL'); ?>";
        
        document.location=url+'/documento/muestraDocumentos/'+cliente+'/'+retorno+'/'+vehiculo_id;
    }
    
</script>