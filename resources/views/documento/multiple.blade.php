@extends('layouts.app')
<style>
    .imagen{
        width:80px;
        height:80px;
    }
    .centro{
        position: relative;
        left: 20%;
    }
</style>

@section('template_title')
    Multipĺe Documentos
@endsection
@section('content')
<div class="box box-info padding-1">
    <div class="card-body centro">
        <form method="post" enctype="multipart/form-data" action="{{route('guardamultiple',[$cliente_id,$retorno,$vehiculo_id])}}">
            @csrf
            <div class="table-responsive" style="width: 60%;">
                    <div class="alert alert-success" id='mensaje'>
                    </div>
                <table class="table table-striped table-hover">
                <tr><td>
                <input multiple type="file" name="archivos[]">
                <br><br>
                
                Presionar Ctrl + Click para seleccionar varios archivos
                <br><br>
                </td></tr><tr><td>
                </td></tr>
                </table>
            </div>
            <input type="submit" value="Enviar" class="btn btn-primary" onclick="mensaje();">
            
            <a href="{{route('muestraDocumentos',[$cliente_id,$retorno,$vehiculo_id])}}">
                <div class="btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
        </form>
    </div>
</div>
@endsection
<script>
    function mensaje(){
        document.getElementById("mensaje").innerHTML='<p>Procesando por favor Espere</p>';
    }
</script>