@extends('layouts.app')

@section('template_title')
    Cliente
@endsection

@section('content')
<style type="text/css">
    .impuesto{
        width: 100%;
        align-self:center ;
    }
	.der{
		text-align: right;
	}
    td{
		text-align: right;
        border-width: 1px;
        padding: 5px;
    }
    .sinborde{
        text-align: right;
        border-width: 0 ;
        border-bottom: 3px double blue;
        padding: 5px;
    }
    .resumen{
		padding-left: 45px;
		font-size: 12pt;
		font-weight: bold;
		
	}
	.totales{
        border-width: 0 0 0 0;
        border-top: 2px solid blue;
        font-weight: bold;
        text-align: right;
   }
   .bordes{
		border-top: 4px solid black;
    	font-weight: bold;
   }
	.fijo{
		width: 20%;
		
	}
	.cabecera{
		padding-left: 15px;
  		border-bottom: 2px solid blue;
  	}

  

  th{
  	padding-left: 15px;
  	border-bottom: 2px solid blue;
	  text-align: right;
  }
  table{
	width:100%;
  }
</style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    @foreach($clientes as $cliente)
                    <div class="card-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">
                              <span id="card_title">
                                {{ __('Historial de :'.$cliente->nombre) }}
                            </span>
                            <a href="{{route('RentabilidadIndividual')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                             <div class="float-right">
                             	<!--
                                <a href="{{ route('clientes.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nevo Cliente') }}
                                </a>
                               -->
                              </div>
                        </div>
                        <table>
                            <tr>
                                <td class="der">
                                    Fecha de Firma:
                                </td>
                                <td>
                                 {{date('d/m/Y',strtotime($cliente->ffirma))}}   
                                </td>
                                <td class="der">
                                    Importe:
                                </td>
                                <td class="der">
                                 {{number_format($cliente->importe,2,'.',',')}}   
                                </td>
                                <td class="der">
                                    Interes Calculado:
                                </td>
                                <td>
                                 {{number_format($cliente->interes,2,'.',',')}}   
                                </td>
                            </tr>
                            <tr>
                                <td class="der">
                                    Nro. Cuotas:
                                </td>
                                <td>
                                 {{number_format($cliente->cuotas,0,',','.')}}
                                </td>
                                <td class="der">
                                    Importe Cuota (IVA inc.):
                                </td>
                                <td>
                                 {{number_format($cliente->tcuota,2,',','.')}}
                                </td>
                                <td class="der">
                                    Tipo:
                                </td>
                                <td>
                                 {{$tipo[$cliente->tipo]}}   
                                </td>
                            </tr>
                        </table>
                    </div>
                    @endforeach
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover" >
                        		<thead class="thead">
                                    <tr>
                            <th>No</th>
										<!--<th>Empresa</th>-->
										<th>Periodo</th>
										<th class="der">Capital</th>
										<th class="der">Interés</th>
										<th class="der">IVA</th>
                                        <th class="der">Total</th>
										<th class="der">Cap. Pagado</th>
										<th class="der">Cap. Pendiente</th>
                    <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                        $i=0;
                        $periodo=$cliente->ffirma;
                        $cuota=$cliente->tcuota;
                        $capital_amort=0;
                        $capital_resta=0;
                        $tcapital=0;
                        $tinteres=0;
                        $ttotal=0;
                        $tiva=0;
                    ?>
                    @foreach($pagosdesgloses as $desglose)
                    <?php
                       
                    ?>
                    <tr>
                      <td>{{ ++$i }}</td>
                      <?php
                       $fecha1 = date_create($periodo);
                        $fecha = date_add($fecha1, date_interval_create_from_date_string( $i." month"));
                       if($cliente->tipo=='0'){

                            $iva=$iva_pagado[$i];
                            $total=$capital_pagado[$i]+$interes_pagado[$i]+$iva;
                            $capital_amort=$capital_amort+$capital_pagado[$i];
                            $capital_resta=$cliente->importe-$capital_amort;
                        }else{
                            /*
                            if($i==1){
                                $iva=$interes_pagado[$i]/(1+$cliente->piva);
                                $interes_pagado[$i]=$interes_pagado[$i]-$iva;
                                $total=$interes_pagado[$i]+$iva;
                                 $capital_amort=$capital_amort+$capital_pagado[$i];
                                $capital_resta=$cliente->importe-$capital_amort;
                            }else{
                                */
                                $iva=$iva_pagado[$i];
                                //$interes_pagado[$i]=$interes_pagado[$i]-$iva;
                                $total=$capital_pagado[$i]+$interes_pagado[$i]+$iva;
                                $capital_amort=$capital_amort+$capital_pagado[$i];
                                $capital_resta=$cliente->importe-$capital_amort;
                            //}
                            
                        }
                        /*
                        if($total<$cuota){
                            $interes_pagado[$i]=$interes_pagado[$i]+($cuota-$total);
                            $total=$capital_pagado[$i]+$interes_pagado[$i]+$iva;
                        }
                        */
                      ?>
                      
						<td>{{( date_format($fecha,'m/Y') )}}</td>
						<td class="der">
                            {{number_format($capital_pagado[$i],2,',','.')}}
                        </td>
                     	<td align="right">
                            {{number_format($interes_pagado[$i],2,',','.')}}
                     	</td>
            	        <td align="right">
                            {{number_format($iva,2,',','.')}}
                        </td>
            	        <td align="right">
                            {{number_format($total,2,',','.')}}
                        </td>
                      <td align="right">
                            {{ number_format($capital_amort,2,',','.') }}
                      </td>   
                      <td align="right">
                         {{ number_format($capital_resta,2,',','.') }}
                      </td>
                      <td>
                      </td>
                      </tr>
                      <?php
                        $tcapital+=$capital_pagado[$i];
                        $tinteres+=$interes_pagado[$i];
                        $tiva+=$iva_pagado[$i];
                        $ttotal+=$total;
                      ?>
                    
                    @endforeach
                    <tr>
                        <td>
                            
                        </td>
                        <td align="right">
                            Totales:...
                        </td>
                        <td align="right">
                            {{ number_format($tcapital,2,',','.') }}
                        </td>
                        <td align="right">
                            {{ number_format($tinteres,2,',','.') }}
                        </td>
                        <td align="right">
                            {{ number_format($tiva,2,',','.') }}
                        </td>
                        <td align="right">
                            {{ number_format($ttotal,2,',','.') }}
                        </td>
                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $clientes->links() !!}
            </div>
        </div>
    </div>
<div class="container-fluid">
<div class="row">
<div class="col-sm-8">
    <div class="card">
    <div class="card-header" style="padding-left:4em;">
        <b>IMPUESTOS</b>
        @php
            $total = array();$totalMonto= array();$totalSiniva=array();$totalIva = array();$i=0;
        @endphp
        <table border="1">
            @foreach($vehiculos as $veh)
            @php
            $total[$i]=$veh->pagoImpuesto;
            $totalMonto[$i]=$veh->pagoImpuesto;
            $totalSiniva[$i]=$veh->pagoImpuesto-$veh->pagoIva;
            $totalIva[$i]=$veh->pagoIva;
            $i++;
            @endphp
            <thead>
                <tr>
                    <th colspan="4" class="cabecera"  style="text-align: left;">Vehículo:{{$veh->matricula.' - '.$veh->mar.'/'.$veh->mod}}</th>
                </tr>
                <tr>
                    <th class="fijo">Impuesto</th>
                    <th >Monto</th>
                    <th >Impuesto</th>
                    <th >IVA</th>
                </tr>
            </thead> 
            <tr>
                <td class="sinborde">{{ $veh->impuesto}}</td>
                <td class="sinborde">{{number_format($veh->pagoImpuesto,2,',','.')}}</td>
                <td class="sinborde">{{number_format($veh->pagoImpuesto-$veh->pagoIva,2,',','.')}}</td>
                <td class="sinborde">{{number_format($veh->pagoIva,2,',','.')}}</td>
            </tr>
        @endforeach
        <tr class="totales">
            <td class="totales">Total General:...</td>
            <td class="totales">{{number_format(array_sum($total),2,',','.')}}</td>
            <td class="totales">{{number_format(array_sum($totalSiniva),2,',','.')}}</td>
            <td class="totales">{{number_format(array_sum($totalIva),2,',','.')}}</td>
        </tr>
    </table>

        
    </div>
</div>
</div>
</div>
</div>    
    
@endsection
