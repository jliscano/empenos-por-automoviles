@extends('layouts.app')

@section('template_title')
    Pago
@endsection
<?php
    $conciliado=array('No','Si');
?>
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Pagos :') }}<div id='iden'></div>
                            </span>
                            @if($retorno==1)
                                <a href="{{route('listaprestamos',[$cliente_id,$vehiculo_id,$retorno])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                            @elseif($retorno==6)
                            <a href="{{ route('listaprestamoscliente',[$cliente_id,$vehiculo_id,1]) }}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">Volver
                            </a>
                            @elseif($retorno==7)
                                <a href="{{ route('impagos',[0,-1,'*']) }}">
                                    <div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div>
                                </a>
                            @elseif($retorno==10)
                                <a href="{{ route('editaprestamo',[$padre,6]) }}">
                                    {{-- venimos a pagar un empeño extension y volvemos al empeño original --}}
                                    <div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div>
                                </a>
                            @else
                                <a href="{{route('listaprestamos',[0,0,0])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                             @endif
                             <div class="float-right">
                                @if($condicion==2)
                                    </div>Empeño Cancelado  </div>
                                @endif
                                <a href="{{ route('createPago',[$prestamo_id,$retorno,$padre] ) }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                    {{ __('Nuevo Pago') }}
                                  </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! $pagos->links() !!}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
										<th>Fecha</th>
										<th>Referencia</th>
										<th>Monto</th>
                                        <th>Motivo</th>
										<th>Conciliado</th>

                                        <th></th>
                                    </tr>
                                </thead>
                    <tbody>
                        @foreach ($pagos as $pago)
                        <script>
                            $("#iden").html("DNI:<b>{{$dni}}</b> Nombre:<b>{{$nombre}}</b> Matrícula:<b>{{$matricula}} {{$mar.'/'.$mod}}</b> F.Firma<b>{{date('d/m/Y',strtotime($ffirma))}}</b>");
                        </script>
                            <tr>
                                <td>{{ ++$i }}</td>
								<td>{{ date('d/m/Y', strtotime($pago->fpago)) }}</td>
								<td>{{ $pago->referencia }}</td>
								<td>{{ number_format($pago->monto,2,',','.') }}</td>
                                <td>{{ $motivos[$pago->motivo] }}</td>
								<td>{{
                                $conciliado[
                                $pago->conciliado] }}</td>

                                <td>
                                    <form action="{{ route('borraPago',[$pago->id,$retorno]) }}" method="POST" id="borrar">
                                    <a class="btn btn-sm btn-success" href="{{ route('editaPago',[$pago->id,$retorno,$padre]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                    <input type="hidden" name="retorno" value="{{$retorno}}">
                                    <input type="hidden" name="padre" value="{{$padre}}">
                                    @csrf
                                    @method('DELETE')
                                    <a  class="btn btn-danger btn-sm" onclick="aviso({{$pago->id}});"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i></a>
                                    </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $pagos->links() !!}
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar este Pago?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            url=url +'/pagos/'+id;
            document.getElementById("borrar").action = url;
            document.getElementById("borrar").submit();
        }
    }
</script>
