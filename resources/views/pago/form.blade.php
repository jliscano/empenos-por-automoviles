<style type="text/css">
.verde {
    border-radius: 25px;
    background-color: green;
    color: white;
    margin: 10px;
    width: 160px;
    height: 80px;
    padding: 15px 15px 15px 15px;
}

.grid-container {
    display: grid;
    grid-template-rows: 80px 80px;
    /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
    grid-template-columns: 180px 180px  180px 180px;
    grid-gap: 15px;
    padding: 15px 15px 15px 15px;
}
@media only screen and (max-width:800px) {
  /* For tablets: */
 .grid-container {
  display: grid;
  grid-template-columns: 180px 180px;
}
}
@media only screen and (max-width:560px) {
  /* For mobile phones: */
  .grid-container {
  display: grid;
  grid-template-columns: 180px;

}
}
</style>
<div class="box box-info padding-1">
    <div class="box-body">
    <div class="grid-container">
        <div class="grid-item1">
        <div class="form-group">

            {{ Form::label('Motivo del pago:') }}
            {{ Form::select('motivo', $motivos, $pago->motivo, ['class' => 'form-control' . ($errors->has('motivo') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione un Motivo','OnChange'=>'anuncio(this.value)']) }}
            {!! $errors->first('motivo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item2">
        <div class="form-group">
            {{ Form::label('Fecha :') }}
            {{ Form::date('fpago', $pago->fpago, ['class' => 'form-control' . ($errors->has('fpago') ? ' is-invalid' : ''), 'placeholder' => 'Fpago']) }}
            {!! $errors->first('fpago', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="grid-item3">
        <div class="form-group">
            {{ Form::label('Referencia :') }}
            {{ Form::text('referencia', $pago->referencia, ['class' => 'form-control' . ($errors->has('referencia') ? ' is-invalid' : ''), 'placeholder' => 'Referencia', 'maxlength'=>'50']) }}
            {!! $errors->first('referencia', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="grid-item5">
            <div class="form-group">
                {{ Form::label('Corresponde a la Cuota :') }}
                {{ Form::select('cuota_nro', $cuota_nro, $pago->cuota_nro, ['class' => 'form-control' . ($errors->has('cuota_nro') ? ' is-invalid' : ''), 'placeholder' => 'Cuota Nro.']) }}
                {!! $errors->first('cuota_nro', '<div class="invalid-feedback">:message</div>') !!}
            </div>
            </div>
        <div class="grid-item4">
        <div class="form-group">
            {{ Form::label('Monto :') }}
            {{ Form::number('monto', $pago->monto, ['class' => 'form-control' . ($errors->has('monto') ? ' is-invalid' : ''), 'placeholder' => 'Monto','id'=>'monto','pattern'=>"[0-9]+([\.,][0-9]+)?",'step'=>"0.01"]) }}
            {!! $errors->first('monto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item5">
        <div class="form-group">
            {{ Form::label('Conciliado :') }}
            {{ Form::select('conciliado', $conciliado, $pago->conciliado, ['class' => 'form-control' . ($errors->has('conciliado') ? ' is-invalid' : ''), 'placeholder' => 'Conciliado']) }}
            {!! $errors->first('conciliado', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="grid-item6">
        <div class="form-group">
            {{ Form::label('Observacion :') }}
            {{ Form::text('observacion', $pago->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observacion','style'=>'width:400px']) }}
            {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item7">
        <div class="form-group">
            <!--{{ Form::label('clasificado_id') }}-->
            {{ Form::hidden('prestamo_id', $pago->prestamo_id, ['class' => 'form-control' . ($errors->has('prestamo_id') ? ' is-invalid' : ''), 'placeholder' => 'prestamo_id']) }}
            {!! $errors->first('prestamo_id', '<div class="invalid-feedback">:message</div>') !!}
            <input type="hidden" name="retorno" value="{{$retorno}}">
            <input type="hidden" name="padre" value="{{$padre}}">
        </div>
        </div>
    </div> <!-- grid -->
    </div>

    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        {{-- @if($retorno==10)
        <a href="{{route('muestraPagoExtension',[$pago->prestamo_id,$retorno,$padre])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver Extension</div></a>
        @else --}}
        <a href="{{route('muestraPago',[$pago->prestamo_id,$retorno,$padre])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
        {{-- @endif --}}

    </div>
    <div class="box-footer mt20">

    </div>
</div>
<script type="text/javascript">


function anuncio(valor){
    if(valor==1){
        $('#monto').val({{ $importe }});
        $('#monto').prop('readonly',true);
        alert('Este pago cancelará totalmente el empeño una vez que sea conciliado.');
        $('#piva').val(0);
        $('#iva').val(0);
    }else if(valor==0){
        $('#monto').prop('readonly',false);
        $('#monto').val({{ $cuota }});
        $('#piva').val({{ $piva }});
        $('#iva').val({{ $iva }});
    }else{
        $('#monto').val(0);
    }
}
</script>
