@extends('layouts.app')

@section('template_title')
    {{ __('Update') }} Condicionvehiculo
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">{{ __('Update') }} Condicionvehiculo</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('condicionvehiculos.update', $condicionvehiculo->id) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('PATCH') }}
                            @csrf

                            @include('condicionvehiculo.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
