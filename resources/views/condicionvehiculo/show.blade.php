@extends('layouts.app')

@section('template_title')
    {{ $condicionvehiculo->name ?? "{{ __('Show') Condicionvehiculo" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Condicionvehiculo</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('condicionvehiculos.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Nombre:</strong>
                            {{ $condicionvehiculo->nombre }}
                        </div>
                        <div class="form-group">
                            <strong>Comentario:</strong>
                            {{ $condicionvehiculo->comentario }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
