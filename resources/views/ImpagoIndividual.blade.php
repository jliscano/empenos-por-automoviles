@extends('layouts.app2')
@section('content')
<div class="container-fluid">
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped " ">
                <thead class="thead">
                    <tr>
                        <th colspan="6">
                            Reporte de Impagos
                        </th>
                        <th style="text-align: right;">
                            @php
                                echo date('d/m/Y h:i A');
                            @endphp
                        </th>
                    </tr>
                    <tr>
                        <th colspan="2" style="width: 40%;">Cliente</th>
                        <th>DNI</th>
                        <th>Teléfono</th>
                        <th colspan="2" style="width: 40%;">Email</th>
                        <th>Tipo</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                    $p=0;
                    $i=0;
                    @endphp
                   @foreach($cobranzas as $cobranza)
                            <tr>
                            <td colspan="2">
                                @php
                                if($cobranza->proempresa=='')
                                    echo $cobranza->nombre;
                                else
                                    echo $cobranza->proempresa;
                                @endphp
                            </td>
                            <td>
                                @php
                                if($cobranza->proempresa=='')
                                    echo $cobranza->dni;
                                else
                                    echo $cobranza->dnicif;
                                @endphp
                            </td>
                            <td>{{ $cobranza->telefono}}</td>
                            <td colspan="2">{{ $cobranza->email }}</td>
                            <td>
                                @if(isset($cobranza->pre))
                                    {{ $tipos[$cobranza->tipo]}}
                                    @if($cobranza->tipo==0)
                                        {{$cobranza->cuotas}} Cuotas
                                    @endif
                                @endif
                            </td>
                        </tr>
                        <tbody>
                        <thead class="thead">
                        <tr>
                            <th colspan="2">
                                Vehículo:
                            </th>
                            <th>
                                Matrícula:
                            </th>
                            <th colspan="2">
                                Fecha Firma:
                            </th>
                            <th>
                                F. Inicio Pago
                            </th>
                            <th>
                                Importe:
                            </th>
                        </tr>
                    </thead>
                        </tbody>
                    <tr>
                        <td colspan="2">
                            {{$cobranza->mar}}/{{$cobranza->mod}}
                        </td>
                        <td>
                            {{$cobranza->matricula}}
                        </td>
                        <td colspan="2">
                            {{date('d/m/Y',strtotime($cobranza->ffirma))}}
                        </td>
                        <td >
                            {{date('d/m/Y',strtotime($cobranza->finiciopago))}}
                        </td>
                        <td >
                            {{number_format($cobranza->importe,2,',','.')}}
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            @if(count($arr_extensiones)>0)
            <table class="table table-striped table-hover ">
                <thead class="thead">
                    <tr>
                        <th colspan="7">
                            Extensiones
                        </th>
                    </tr>
                    <tr>
                        <th>Núm</th>
                        <th colspan="2" style="width: 40%;">Fecha</th>
                        <th colspan="2">Monto</th>
                        <th colspan="2">Cuota</th>
                    </tr>
                </thead>
                <tbody>
                    @for($i=0;$i<count($arr_extensiones);$i++)
                    <tr>
                    <td>{{$i+1}}</td>
                    <td colspan="2">{{date('d/m/Y',strtotime($arr_extensiones[$i]['fecha']))}}</td>
                    <td colspan="2">{{number_format($arr_extensiones[$i]['monto'],2,',','.')}}</td>
                    <td colspan="2">{{number_format($arr_extensiones[$i]['tcuota'],2,',','.')}}</td>
                    </tr>
                    @endfor
                </tbody>
            </table>
            @endif
            @php
            $contpagos=count($arr_Gdebe_cuotas);
            //pasamos los pagos a una arreglo más pequeño
            $cuantos_pagos=0;
            $pagos_empeno=array();
            for($j=1;$j<=$contpagos;$j++){
                if($arr_Gdebe_cuotas[$j]['prestamo_id']==$cobranza->pre){
                    $cuantos_pagos++;
                    $pagos_empeno[]=array(
                        'periodo'     => $arr_Gdebe_cuotas[$j]['periodo'],
                        'prestamo_id' => $arr_Gdebe_cuotas[$j]['prestamo_id'],
                        'fecha'       => $arr_Gdebe_cuotas[$j]['fecha'],
                        'descripcion' => $arr_Gdebe_cuotas[$j]['descripcion'],
                        'cuota'       => $arr_Gdebe_cuotas[$j]['tcuota'],
                        'pago'        => $arr_Gdebe_cuotas[$j]['mpago'],
                        'piva'        => $arr_Gdebe_cuotas[$j]['piva'],
                        'balance'     => $arr_Gdebe_cuotas[$j]['balance'],
                        'fpago'       => $arr_Gdebe_cuotas[$j]['fpago'],
                        'mpago_dt'    => $arr_Gdebe_cuotas[$j]['mpago_dt']
                    );
                }
            }

            $contperiodos=count($pagos_empeno)+5;
            $contpagos=$cuantos_pagos;
            usort($pagos_empeno, 'compara_fecha');

            arregla_extensiones($pagos_empeno,$arr_extensiones,$cobranza->pre,$cobranza->numero_extensiones);
            $impago_d=0;
            $balance=calcula_balance($pagos_empeno,$impago_d);
            @endphp
            <table class="table table-striped " id="tablaimpagos">
                <thead>
                <tr>
                    <th  style="width: 4%;text-align:right;">
                        Núm
                    </th>
                    <th style="width: 5%;text-align:left;">
                        Fecha
                    </th>
                    <th style="width: 8%;">
                        Descripción
                    </th>
                    <th style="width: 10%;text-align:right;">
                        Cuota
                    </th>
                    <th style="width: 10%;text-align:right;">
                        Precio
                    </th>
                    <th style="width: 10%;text-align:right;">
                        IVA
                    </th>
                    <th style="width: 10%;text-align:right;">
                        TOTAL ALQ.
                    </th>
                    <th style="width: 10%;text-align:right;">
                        Debe
                    </th>
                    <th colspan="2" style="text-align:center;">
                        Pagado Detalle
                    </th>
                </tr>
            </thead>
            <tbody>
                @php

                $cuota_actual=0;
                $deuda=0;
                $deuda_cuotas=0;
                $deuda_iva=0;
                $deuda_capital=0;
                $deuda_interes=0;
                $cobrado=0;
                $cobrado_iva=0;
                $cobrado_capital=0;
                $cobrado_interes=0;
                //falta mostrar los pagos en cada cuota
                    $total_debe=0;$total_cuota=0;$total_precio=0;$total_iva=0;$total_alq=0;
                    $cuota_actual=0;
                    for($j=0;$j<$cuantos_pagos;$j++){
                        if($cobranza->tipo=='1'){
                            if($pagos_empeno[$j]['balance']>0) {
                                $precio=abs($pagos_empeno[$j]['pago']/(1+($pagos_empeno[$j]['piva']/100)));
                                $iva=abs(round($pagos_empeno[$j]['pago']-$precio,2));
                                $precio_iva=$pagos_empeno[$j]['balance']/(1+($cobranza->piva/100));
                                $iva2=round($pagos_empeno[$j]['balance']-$precio_iva,2);
                                $deuda_iva=$deuda_iva+$iva2;
                                $deuda_interes=$deuda_interes+$precio_iva;
                            }else{
                                $precio=abs($pagos_empeno[$j]['pago']/(1+($pagos_empeno[$j]['piva']/100)));
                                $iva=abs(round($pagos_empeno[$j]['pago']-$precio,2));
                            }
                        } else {
                            amortizacion_cuotas($cobranza,$interes_calculado,$iva_calculado,$capital_calculado,$j+1);
                            $precio_calculado=$interes_calculado+$capital_calculado;
                            if($pagos_empeno[$j]['balance']==0) {
                                $iva=$iva_calculado;
                                $precio=$interes_calculado+$capital_calculado;
                            }else{
                                calcula_precio_iva_parcial($pagos_empeno[$j]['pago'],$iva_calculado,$precio_calculado,$interes_calculado,$iva2,$precio2,$interes2);
                                if($pagos_empeno[$j]['pago']>0){
                                    $iva=$iva2;
                                    $precio=$precio2;
                                    $capital2=$capital_calculado-($precio2-$interes2);
                                    $deuda_capital=$deuda_capital+$capital2;
                                    $deuda_interes=$deuda_interes+($interes_calculado-$interes2);
                                    $deuda_iva=$deuda_iva+($iva_calculado-$iva);
                                }else{
                                    $iva=0;$precio=0;
                                    $iva2=$iva_calculado;
                                    $deuda_capital=$deuda_capital+$capital_calculado;
                                    $deuda_interes=$deuda_interes+$interes_calculado;
                                    $deuda_iva=$deuda_iva+$iva_calculado;
                                }
                            }
                        }
                            if($j % 2==0)
                                $clase='par';
                            else
                            $clase='impar';
                            if($cuota_actual!=$pagos_empeno[$j]['cuota'])
                                $cuota_actual=$pagos_empeno[$j]['cuota'];
                            echo '<tr class="'.$clase.'"><td style="width: 4%;text-align:right;">';
                            echo $j+1;
                            echo '</td><td style="width: 5%;text-align:left;">';
                            echo date('d/m/Y',strtotime($pagos_empeno[$j]['fecha']));
                            echo '</td><td style="width: 8%;">';
                            echo $pagos_empeno[$j]['descripcion'];
                            echo '</td><td  style="width: 10%;text-align:right;">';
                            echo number_format($pagos_empeno[$j]['cuota'],2,',','.');
                            echo '</td><td style="width: 10%;text-align:right;"">';
                            //$precio=$pagos_empeno[$j]['pago']/(1+($pagos_empeno[$j]['piva']/100));
                            echo number_format($precio,2,',','.');
                            echo '</td><td style="text-align: right;width:10%;">';
                            //$iva=$pagos_empeno[$j]['pago']-$precio;
                            echo number_format($iva,2,',','.');
                            echo '</td><td style="text-align: right;width:10%;">';
                            echo number_format($pagos_empeno[$j]['pago'],2,',','.');
                            if($pagos_empeno[$j]['balance']>0)
                                echo '</td><td style="width: 10%;text-align:right;color:red"  >';
                            else
                                echo '</td><td style="width: 10%;text-align:right;">';
                            echo number_format($pagos_empeno[$j]['balance'],2,',','.');
                            echo '</td><td> ';
                                echo $pagos_empeno[$j]['fpago'];
                            echo '</td><td>';
                                echo $pagos_empeno[$j]['mpago_dt'];
                            echo '</td></tr>';
                            $total_cuota=$total_cuota+$pagos_empeno[$j]['cuota'];
                            $total_precio=$total_precio+$precio;
                            $total_iva=$total_iva+$iva;
                            $total_alq=$total_alq+$pagos_empeno[$j]['pago'];
                            $total_debe=$total_debe+$pagos_empeno[$j]['balance'];

                    }
                        echo '<tr class="linea"><td colspan="3" style="text-align:right;">Totales :.................</td>';
                        echo '<td  style="text-align:right;">'.number_format($total_cuota,2,',','.').'</td>';
                        echo '<td  style="text-align:right;">'.number_format($total_precio,2,',','.').'</td>';
                        echo '<td  style="text-align:right;">'.number_format($total_iva,2,',','.').'</td>';
                        echo '<td  style="text-align:right;">'.number_format($total_alq,2,',','.').'</td>';
                        echo '<td  style="text-align:right;">'.number_format($total_debe,2,',','.').'</td></tr>';
                @endphp

                <tr>
                    <tr><td colspan="7" style="text-align:right;">
                        @if($cobranza->tipo=='0')
                        Capital por cobrar<br>
                        @endif
                        Intereses por cobrar<br>
                        Iva por cobrar<br>
                    </td>
                    <td style="text-align: right;width:104px;">
                        @if($cobranza->tipo=='0')
                        {{number_format($deuda_capital,2,',','.')}}<br>
                        @endif
                        {{number_format($deuda_interes,2,',','.')}}<br>
                        {{number_format($deuda_iva,2,',','.')}}<br>
                    </td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
</div>
<footer>
    <script type="text/php">
        if ( isset($pdf) ) {
            // v.0.7.0 and greater
            $x = 515;
            $y = 720;
            $text = "Pág :{PAGE_NUM} de {PAGE_COUNT}";
            $font = $fontMetrics->get_font("arial", "normal");
            $size = 12;
            $color = array(0,0,0);
            $word_space = 0.5;  //  default
            $char_space = 0.3;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
    </script>
</footer>
@endsection
<style>

    table thead th {
    font-family: 'arial';
    font-size:12px;
    border-bottom: 2px solid black;
    }
    table td{
        font-family: 'arial';
        font-size:10px;
    }
    .linea{
        border-top: 2px black solid;
    }
    .par{
        background-color:  silver;
        text-align: right;
    }
    .impar{
        background-color:  white;
        text-align: right;
    }
</style>
