<style type="text/css">
        .verde {
            border-radius: 25px;
            background-color: green;
            color: white;
            margin: 10px;
            width: 160px;
            height: 80px;
            padding: 15px 15px 15px 15px;
        }

    .grid-container {
            display: grid;
            grid-template-rows: 80px 80px;
            /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
            grid-template-columns: 180px 180px  180px 180px;
            grid-gap: 15px;
            padding: 15px 15px 15px 15px;

    }
    @media only screen and (max-width:800px) {
      /* For tablets: */
     .grid-container {
      display: grid;
      grid-template-columns: 180px 180px;
    }
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
      .grid-container {
      display: grid;
      grid-template-columns: 180px;
      
    }
    }
    </style>
<div class="box box-info padding-1">
    <div class="box-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('warmning'))
        <div class="alert alert-warning">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="grid-container">        
    <div class="grid-item1">
        <div class="form-group">
            {{ Form::label('Empresa :') }}
            {{ Form::select('empresa_id', $empresa ,$gasto->empresa_id, ['class' => 'form-control' . ($errors->has('empresa_id') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione una Empresa']) }}
            {!! $errors->first('empresa_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item1">
        <div class="form-group">
            {{ Form::label('Clase de Gasto :') }}
            {{ Form::select('clasificado_id', $clasificado,$gasto->clasificado_id, ['class' => 'form-control' . ($errors->has('clasificado_id') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione una clasificación']) }}
            {!! $errors->first('clasificado_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item2">
        <div class="form-group">
            {{ Form::label('Fecha :') }}
            {{ Form::date('fgasto', $gasto->fgasto, ['class' => 'form-control' . ($errors->has('fgasto') ? ' is-invalid' : ''), 'placeholder' => 'Fgasto']) }}
            {!! $errors->first('fgasto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item3">
        <div class="form-group">
            {{ Form::label('Monto :') }}
            {{ Form::text('monto', $gasto->monto, ['class' => 'form-control' . ($errors->has('monto') ? ' is-invalid' : ''), 'placeholder' => 'Monto']) }}
            {!! $errors->first('monto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item4">
        <div class="form-group">
            {{ Form::label('Referencia/Factura :') }}
            {{ Form::text('referencia', $gasto->referencia, ['class' => 'form-control' . ($errors->has('referencia') ? ' is-invalid' : ''), 'placeholder' => 'Escriba el número de Referencia']) }}
            {!! $errors->first('referencia', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>    
    <div class="grid-item5">
        <div class="form-group">
            {{ Form::label('Pagado :') }}
            {{ Form::select('conciliado', $sino,$gasto->conciliado, ['class' => 'form-control' . ($errors->has('conciliado') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione un valor']) }}
            {!! $errors->first('conciliado', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item6">
        <div class="form-group">
            {{ Form::label('Observacion :') }}
            {{ Form::text('observacion', $gasto->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observacion...','style'=>'width:400px;']) }}
            {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    </div>

    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        <a href="{{route('gastos.index')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    </div>
    @if($nuevo==0)

    <iframe id="gxv" src="{{route('muestraGastosVehiculos',[$gasto->id,$gasto->empresa_id])}}" width="100%" height="600"></iframe>

    @endif

</div>