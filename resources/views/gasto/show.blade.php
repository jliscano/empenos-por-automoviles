@extends('layouts.app')

@section('template_title')
    {{ $gasto->name ?? "{{ __('Show') Gasto" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Gasto</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('gastos.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Empresa Id:</strong>
                            {{ $gasto->empresa_id }}
                        </div>
                        <div class="form-group">
                            <strong>Clasificado Id:</strong>
                            {{ $gasto->clasificado_id }}
                        </div>
                        <div class="form-group">
                            <strong>Monto:</strong>
                            {{ $gasto->monto }}
                        </div>
                        <div class="form-group">
                            <strong>Referencia:</strong>
                            {{ $gasto->referencia }}
                        </div>
                        <div class="form-group">
                            <strong>Observacion:</strong>
                            {{ $gasto->observacion }}
                        </div>
                        <div class="form-group">
                            <strong>Fgasto:</strong>
                            {{ $gasto->fgasto }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
