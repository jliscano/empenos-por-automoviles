@extends('layouts.app')

@section('template_title')
    Cobranza
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            <span id="card_title">
                                {{ __('Contactos: ').$nom }}<br>
                                Email:
                                <b>{{$email}}</b>
                                <br>
                                Teléfono:
                                <b>{{$tele}}</b>
                            </span>
                            <!---->
                            @if($motivo==0){
                                <a  href="{{route('impagos',[0,'-1','*'])}}" >
                            @elseif($motivo==1)
                            <a  href="{{route('muestraPolizas')}}" >
                            @elseif($motivo==3)
                            <a  href="{{route('muestraCarnet')}}" >
                            @elseif($motivo==2)
                                <a  href="{{route('muestraItv')}}" >
                            @endif

                                <div class=" btn btn-primary" style="background-color: yellow;color:black;">Volver</div>
                            </a>


                             <div class="float-right">
                                <a href="{{route('enviaMailCobranzaUno',[$id,$motivo])}}" class="btn btn-primary btn-sm float-right btn-danger"  data-placement="left">
                                    {{ __('Enviar Email') }}
                                  </a>
                                <a href="{{ route('createCobranza',[$id,$motivo]) }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nuevo Contacto') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>

										<th>Fecha</th>
										<th>Observacion</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cobranzas as $cobranza)
                                        <tr>
                                            <td>{{ ++$i }}</td>

											<td>{{ date('d/m/Y',strtotime($cobranza->fcobranza)) }}</td>
											<td>{{ $cobranza->observacion }}</td>

                                    <td>
                                    <form action="{{ route('cobranzas.destroy',$cobranza->id) }}" method="POST">
                                    <!--
                                    <a class="btn btn-sm btn-primary " href="{{ route('cobranzas.show',$cobranza->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Show') }}</a>
                                -->
                                    <a class="btn btn-sm btn-success" href="{{ route('cobranzas.edit',$cobranza->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>

                                    @csrf
                                    @method('DELETE')
                                    <!--
                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Delete') }}</button>
                                -->
                                </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        </table>
                        </div>
                    </div>
                </div>
                {!! $cobranzas->links() !!}
            </div>
        </div>
    </div>
@endsection
