@extends('layouts.app')

@section('template_title')
    Cobranza
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Cobranza') }}
                            </span>

                             <div class="float-right">
                                <a href="{{ route('cobranzas.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nuevo Contacto') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Cliente</th>
										<th>Fecha</th>
										<th>Observacion</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($cobranzas as $cobranza)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $cobranza->cliente_id }}</td>
											<td>{{ $cobranza->fcobranza }}</td>
											<td>{{ $cobranza->observacion }}</td>

                                            <td>
                                                <form action="{{ route('cobranzas.destroy',$cobranza->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-primary " href="{{ route('cobranzas.show',$cobranza->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Show') }}</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('cobranzas.edit',$cobranza->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Edit') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Delete') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $cobranzas->links() !!}
            </div>
        </div>
    </div>
@endsection
