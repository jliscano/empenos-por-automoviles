<style type="text/css">
.verde {
    border-radius: 25px;
    background-color: green;
    color: white;
    margin: 10px;
    width: 160px;
    height: 80px;
    padding: 15px 15px 15px 15px;
}

.grid-container {
    display: grid;
    grid-template-rows: 80px 80px;
    /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
    grid-template-columns: 180px 180px  480px 180px;
    grid-gap: 15px;
    padding: 15px 15px 15px 15px;
}
@media only screen and (max-width:800px) {
  /* For tablets: */
 .grid-container {
  display: grid;
  grid-template-columns: 180px 180px;
}
}
@media only screen and (max-width:560px) {
  /* For mobile phones: */
  .grid-container {
  display: grid;
  grid-template-columns: 180px;
  
}
}
</style>
<div class="box box-info padding-1">
    <div class="box-body">
    <div class="grid-container">
        <div class="grid-item1">
        <div class="form-group">
            {{ Form::label('Fecha de contacto :') }}
            {{ Form::date('fcobranza', $cobranza->fcobranza, ['class' => 'form-control' . ($errors->has('fcobranza') ? ' is-invalid' : ''), 'placeholder' => 'Fcobranza']) }}
            {!! $errors->first('fcobranza', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item2">
        <div class="form-group">
            @if($cobranza->motivo=='0' or $cobranza->motivo=='1')
            {{ Form::label('Fecha Posible pago :') }}
            @else
            {{ Form::label('Fecha Posible Renovación :') }}
            @endif
            {{ Form::date('fproxpago', $cobranza->fproxpago, ['class' => 'form-control' . ($errors->has('fproxpago') ? ' is-invalid' : ''), 'placeholder' => 'fproxpago']) }}
            {!! $errors->first('fproxpago', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="grid-item3">
        <div class="form-group">
            {{ Form::label('Observacion :') }}
            {{ Form::text('observacion', $cobranza->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observacion']) }}
            {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item3">
        <div class="form-group">
            <!--{{ Form::label('Motivo :') }}-->
            {{ Form::hidden('motivo', $cobranza->motivo, ['class' => 'form-control' . ($errors->has('motivo') ? ' is-invalid' : ''), 'placeholder' => 'Motivo']) }}
            {!! $errors->first('motivo', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="form-group">
            <!--{{ Form::label('cliente_id') }}-->
            {{ Form::hidden('cliente_id', $cobranza->cliente_id, ['class' => 'form-control' . ($errors->has('cliente_id') ? ' is-invalid' : ''), 'placeholder' => 'Cliente Id']) }}
            {!! $errors->first('cliente_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        
        <a href="{{route('muestraCobranza',[$cobranza->cliente_id,$cobranza->motivo])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    </div>
</div>