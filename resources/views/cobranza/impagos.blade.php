@extends('layouts.app')

@section('template_title')
    Impagos
@endsection
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
            <div class="card-header">
                    <div style="display: flex; justify-content: space-between; align-items: center;">
                        <span id="card_title">
                            {{ __('Informe de Impagos') }}
                        </span>
                        <a href="{{route('home')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>

                         <div class="float-right">

                            <form id="myForm" method="GET" action="{{ route('enviaMailCobranza') }}"  role="form" enctype="multipart/form-data">
                                @csrf
                                {{-- <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Enviar Email') }}</button> --}}
                            </form>
                        </div>
                    </div>
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="centro">
                    <H4><B>RESUMEN</B></H4>
                    <div class="centro" id='total_deuda'>

                    </div>
                </div>

                <form action="{{route('impagos',[0,-1,'*'])}}">
                <div class="form-group ">
                    {{ Form::label('Filtros :') }}
                    {{ Form::select('empresa_id',$empresa, $empresa_id, ['class' => 'form-control  mi-selector' . ($errors->has('municipio_id') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione Empresa...','id' =>'municipio_id']) }}
                    <input type="text" name="texto" class="form-control" placeholder="Nombre, DNI, Matrícula  a buscar..." style='width:250px;' value="{{$texto}}">
                    @php
                        if($mesesimpago==-1) $mesesimpago='';
                    @endphp
                    <input type="number" name="mesesimpago" class="form-control" placeholder="Mese de impago..." style='width:150px;' min="0" value="{{$mesesimpago}}">
                    <input type="hidden" name="mesesimpago2" class="form-control" placeholder="Meses de impago..." style='width:150px;' value="{{$mesesimpago}}">
                    <input type="hidden" name="texto2" class="form-control" placeholder="Mese de impago..." style='width:150px;' value="{{$texto}}">
                </div>
                <div class="lado">
                    <input type="submit" value="Buscar" class="btn btn-sm btn-primary">
                </div>
                <br>
                </form>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover" >
                            <thead class="thead">
                                <tr>
                                    <th>No</th>
                                    <th>Cliente</th>
                                    <th>DNI</th>
                                    <th>Matrícula</th>
                                    <th>Teléfono</th>
                                    <th >Email</th>
                                    <th >Tipo</th>
                                    <th>Meses <br>Impagos</th>
                                    <th>Debe</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                $l=1;
                                $p=0;
                                $total_deuda=0;
                                $total_deuda_iva=0;
                                $total_deuda_capital=0;
                                $total_deuda_interes=0;
                                $total_cobrado=0;
                                $total_cobrado_iva=0;
                                $total_cobrado_capital=0;
                                $total_cobrado_interes=0;
                                @endphp
                                @foreach($cobranzas as $cobranza)
                                        <tr>
                                        <td>{{ $l++ }}</td>
                                        <td>
                                            @php
                                            if($cobranza->proempresa=='')
                                                echo $cobranza->nombre;
                                            else
                                                echo $cobranza->proempresa;
                                            @endphp
                                        </td>
                                        <td>
                                            @php
                                            if($cobranza->proempresa=='')
                                                echo $cobranza->dni;
                                            else
                                                echo $cobranza->dnicif;
                                            @endphp
                                        </td>
                                        <td>
                                            {{ $cobranza->matricula }}
                                        </td>
                                        <td>{{ $cobranza->telefono}}</td>
                                        <td >{{ $cobranza->email }}</td>
                                        <td >
                                            @if(isset($cobranza->pre))
                                            <a href="{{route('editaprestamo',[$cobranza->pre,7])}}">
                                            {{ $tipos[$cobranza->tipo] }}
                                            </a>
                                            @endif
                                        </td>
                                        <td>
                                            {{ $cobranza->impagos }}
                                        </td>
                                        <td align="right">
                                            @php
                                                $contpagos=count($arr_Gdebe_cuotas);

                                                //pasamos los pagos a una arreglo más pequeño
                                                $cuantos_pagos=0;
                                                $pagos_empeno=array();
                                                for($j=1;$j<=$contpagos;$j++){
                                                    if($arr_Gdebe_cuotas[$j]['prestamo_id']==$cobranza->pre){
                                                        $cuantos_pagos++;
                                                        $pagos_empeno[]=array(
                                                            'periodo' => $arr_Gdebe_cuotas[$j]['periodo'],
                                                            'prestamo_id' => $arr_Gdebe_cuotas[$j]['prestamo_id'],
                                                            'fecha'       => $arr_Gdebe_cuotas[$j]['fecha'],
                                                            'nmes'        => $arr_Gdebe_cuotas[$j]['nmes'],
                                                            'nano'        => $arr_Gdebe_cuotas[$j]['nano'],
                                                            'descripcion' => $arr_Gdebe_cuotas[$j]['descripcion'],
                                                            'cuota'       => $arr_Gdebe_cuotas[$j]['tcuota'],
                                                            'pago'        => $arr_Gdebe_cuotas[$j]['mpago'],
                                                            'piva'        => $arr_Gdebe_cuotas[$j]['piva'],
                                                            'balance'     => $arr_Gdebe_cuotas[$j]['balance'],
                                                            'fpago'       => $arr_Gdebe_cuotas[$j]['fpago'],
                                                            'mpago_dt'    => $arr_Gdebe_cuotas[$j]['mpago_dt']
                                                        );
                                                    }
                                                }

                                                usort($pagos_empeno, 'compara_fecha');
                                                arregla_extensiones($pagos_empeno,$arr_extensiones,$cobranza->pre,$cobranza->numero_extensiones);
                                                $impagos=0;
                                                $balance=calcula_balance($pagos_empeno,$impago_d);
                                                $total_deuda=$total_deuda+$balance;

                                            @endphp
                                            <button type="button" class="btn btn-info " data-toggle="modal" data-target="#myModal{{$l}}">
                                            @php
                                                    echo number_format($balance,2,',','.');
                                            @endphp
                                            </button>
                                        </td>

                                        <td>
                                        <form action="{{ route('muestraCobranza',[$cobranza->id,0] ) }}" method="GET">
                                            <!--<a class="btn btn-sm btn-primary" href="{{ route('muestraCobranza',[$cobranza->id,0] ) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Cobranza') }}</a>-->
                                            {{-- <button type="submit" class="btn btn-sm btn-primary"><i class="fa fa-fw fa-trash"></i> {{ __('Cobranza') }}</button> --}}
                                            <input type="hidden" name="mesesimpago3" class="form-control" placeholder="Mese de impago..." style='width:150px;' min="0" value="{{$mesesimpago}}">
                                            <input type="hidden" name="texto3" class="form-control" style='width:150px;' value="{{$texto}}">
                                            <input type="hidden" name="empresa2" class="form-control" style='width:150px;' value="{{$empresa_id}}">
                                            <!--
                                            <a class="btn btn-sm btn-success" href="{{ route('cobranzas.edit',$cobranza->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Edit') }}</a>
                                            -->
                                            <!--

                                            <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Delete') }}</button>

                                            -->
                                        </form>

                                        <div class="modal " id="myModal{{$l}}" role="dialog" >
                                            <div class="modal-dialog">
                                              <!-- Modal -->
                                              @php
                                                $contperiodos=count($pagos_empeno)+5;
                                                $contpagos=$cuantos_pagos;
                                                $alto=($contperiodos * 50);
                                                //if($alto<610)
                                                $alto=760;
                                                $TipoE=array('Amortizando','Tradicional');
                                                $encuotas='';
                                                if ($cobranza->tipo=='0')
                                                    $encuotas=$cobranza->cuotas.' Cuotas';
                                                $esExtension='';
                                                if($cobranza->extension=='S')
                                                    $esExtension='Extensión';
                                              @endphp
                                              <div class="modal-content" style="height: {{ ($alto) }}px;width:1200px;right: 350px;">
                                                <div class="modal-header">
                                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                  <h4 class="modal-title">Datos</h4>
                                                </div>
                                                <div class="modal-body" >
                                                    <b>{{$cobranza->nombre}}</b> DNI: <b>{{$cobranza->dni}}</b>
                                                    Empresa: <b>{{$cobranza->emp}}</b><br>
                                                    Importe: <b>{{number_format($cobranza->importe,2,',','.')}}</b> Tipo: <b>{{$TipoE[$cobranza->tipo].' '.$encuotas}}</b> F. Inicio Pago: <b>{{date('d/m/Y',strtotime($cobranza->finiciopago))}} {{$esExtension}}</b><br>
                                                    {{-- @for ($i = 0; $i < count($arr_extensiones); $i++)
                                                    @if($arr_extensiones[$i]['prestamo_id']==$cobranza->pre)
                                                        Extensión Núm:{{$i+1}}: <b>{{number_format($arr_extensiones[count($arr_extensiones)-1]['monto'],2,',','.')}}</b> F. Inicio Pago: <b>{{date('d/m/Y',strtotime($arr_extensiones[count($arr_extensiones)-1]['fecha']))}}</b><br>
                                                    @endif
                                                    @endfor --}}
                                                    <table class="table table-striped " >
                                                    <tr>
                                                        <td>
                                                            <a href="{{route('muestraPago',[$cobranza->pre,7,0])}}">Cargar/Editar Pago</a>
                                                        </td>
                                                        <td>
                                                            <a href="{{route('ImpagoIndividual',[$cobranza->id,$cobranza->pre])}}" target="_BLANK">Imprimir</a>
                                                        </td>
                                                    </tr>
                                                  </table><!--  -->
                                                    <table class="table table-striped " id="tablaimpagos">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 80px">
                                                                Núm
                                                            </th>
                                                            <th style="width: 94px">
                                                                Fecha
                                                            </th>
                                                            <th style="width:162px;">
                                                                Descripción
                                                            </th>
                                                            <th style="text-align: right;width:94px;">
                                                                Cuota
                                                            </th>
                                                            <th style="text-align: right;width:104px;">
                                                                Precio
                                                            </th>
                                                            <th style="text-align: right;width:84px;">
                                                                IVA
                                                            </th>
                                                            <th style="text-align: right;min-width:164px;width:64px;">
                                                                TOTAL ALQ.
                                                            </th>
                                                            <th style="text-align: right;width:104px;">
                                                                Debe
                                                            </th>
                                                            <th colspan=2 style="text-align: center;width:204px;">
                                                                Pagado Detalle
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @php
                                                            $cuota_actual=0;
                                                            $deuda=0;
                                                            $deuda_cuotas=0;
                                                            $deuda_iva=0;
                                                            $deuda_capital=0;
                                                            $deuda_interes=0;
                                                            $cobrado=0;
                                                            $cobrado_iva=0;
                                                            $cobrado_capital=0;
                                                            $cobrado_interes=0;
                                                            for($j=0;$j<$cuantos_pagos;$j++){
                                                                    if($cobranza->tipo=='1'){
                                                                        if($pagos_empeno[$j]['balance']>0) {
                                                                            $precio=abs($pagos_empeno[$j]['pago']/(1+($pagos_empeno[$j]['piva']/100)));
                                                                            $iva=abs(round($pagos_empeno[$j]['pago']-$precio,2));
                                                                            $precio_iva=$pagos_empeno[$j]['balance']/(1+($cobranza->piva/100));
                                                                            $iva2=round($pagos_empeno[$j]['balance']-$precio_iva,2);
                                                                            $deuda_iva=$deuda_iva+$iva2;
                                                                            $deuda_interes=$deuda_interes+$precio_iva;
                                                                        }else{
                                                                            $precio=abs($pagos_empeno[$j]['pago']/(1+($pagos_empeno[$j]['piva']/100)));
                                                                            $iva=abs(round($pagos_empeno[$j]['pago']-$precio,2));
                                                                        }
                                                                    } else {
                                                                        amortizacion_cuotas($cobranza,$interes_calculado,$iva_calculado,$capital_calculado,$j+1);
                                                                        $precio_calculado=$interes_calculado+$capital_calculado;
                                                                        if($pagos_empeno[$j]['balance']==0) {
                                                                            $iva=$iva_calculado;
                                                                            $precio=$interes_calculado+$capital_calculado;
                                                                        }else{
                                                                            calcula_precio_iva_parcial($pagos_empeno[$j]['pago'],$iva_calculado,$precio_calculado,$interes_calculado,$iva2,$precio2,$interes2);
                                                                            if($pagos_empeno[$j]['pago']>0){
                                                                                $iva=$iva2;
                                                                                $precio=$precio2;
                                                                                $capital2=$capital_calculado-($precio2-$interes2);
                                                                                $deuda_capital=$deuda_capital+$capital2;
                                                                                $deuda_interes=$deuda_interes+($interes_calculado-$interes2);
                                                                                $deuda_iva=$deuda_iva+($iva_calculado-$iva);
                                                                            }else{
                                                                                $iva=0;$precio=0;
                                                                                $iva2=$iva_calculado;
                                                                                $deuda_capital=$deuda_capital+$capital_calculado;
                                                                                $deuda_interes=$deuda_interes+$interes_calculado;
                                                                                $deuda_iva=$deuda_iva+$iva_calculado;
                                                                            }
                                                                        }
                                                                    }
                                                                    echo '<tr><td style="width: 7%">';
                                                                        echo $j+1;
                                                                    echo '</td><td style="width: 5%">';
                                                                        echo date('d/m/Y',strtotime($pagos_empeno[$j]['fecha']));
                                                                    echo '</td><td style="width:150px;">';
                                                                        echo $pagos_empeno[$j]['descripcion'];
                                                                    echo '</td><td style="text-align: right;width:110px;" >';
                                                                        echo number_format($pagos_empeno[$j]['cuota'],2,',','.');
                                                                    echo '</td><td style="text-align: right;width:100px;">';
                                                                        echo number_format($precio,2,',','.');
                                                                    echo '</td><td style="text-align: right;width:84px;">';

                                                                        echo number_format($iva,2,',','.');
                                                                    echo '</td><td style="text-align: right;min-width:104px;width:164px;" >';
                                                                        echo number_format($pagos_empeno[$j]['pago'],2,',','.');
                                                                    if($pagos_empeno[$j]['balance']>0)
                                                                        echo '</td><td style="color:red;text-align: right;width:84px;min-width:104px;" >';
                                                                    else
                                                                        echo '</td><td style="text-align: right;width:104px;min-width:104px;">';
                                                                        echo number_format($pagos_empeno[$j]['balance'],2,',','.');
                                                                    echo '</td><td style="text-align: right;width:104px;">';
                                                                        echo $pagos_empeno[$j]['fpago'];
                                                                    echo '</td><td >';
                                                                        echo $pagos_empeno[$j]['mpago_dt'];
                                                                    echo '</td></tr>';
                                                                    $deuda_cuotas=$deuda_cuotas+$pagos_empeno[$j]['cuota'];
                                                                    $cobrado_capital=$cobrado_capital+$precio;
                                                                    $cobrado_iva=$cobrado_iva+$iva;
                                                                    $cobrado=$cobrado+$pagos_empeno[$j]['pago'];
                                                                    $deuda=$deuda+$pagos_empeno[$j]['balance'];
                                                                    /*
                                                                    if($pagos_empeno[$j]['balance']>0 and $cobranza->tipo=='1'){
                                                                        $precio2=$pagos_empeno[$j]['balance']/(1+($cobranza->piva/100));
                                                                        $iva2=round($pagos_empeno[$j]['balance']-$precio2,2);
                                                                        //$deuda_iva=$deuda_iva+$iva2;
                                                                        $deuda_interes=$deuda_interes+$precio2;
                                                                        $deuda_iva=$deuda_iva+$iva2;
                                                                    }
                                                                        */
                                                            }
                                                        @endphp
                                                    </tbody>
                                                    </table>

                                                    <table id="totales_individual">
                                                        <thead>
                                                        <th>
                                                            <td style="width: 315px;text-align:right">Totales.....</td>
                                                            <td style="text-align: right;width:110px;">{{number_format($deuda_cuotas,2,',','.')}}</td>
                                                            <td style="text-align: right;width:100px;">{{number_format($cobrado_capital,2,',','.')}}</td>
                                                            <td style="text-align: right;width:84px;">{{number_format($cobrado_iva,2,',','.')}}</td>
                                                            <td style="text-align: right;width:164px;">{{number_format($cobrado,2,',','.')}}</td>
                                                            <td style="text-align: right;width:104px;">{{number_format($deuda,2,',','.')}}</td>
                                                        </th>
                                                        </thead>
                                                    </table>
                                                    @php
                                                        if($cobranza->tipo=='1'){
                                                            $deuda_capital=$cobranza->importe;
                                                            $pago_cta_principal=busca_pago_principal($cobranza->pre);
                                                            $pagos_principal=0;
                                                            foreach ($pago_cta_principal as $pagos) {
                                                                $pagos_principal+=$pagos->monto;

                                                            }
                                                            $total_deuda_capital=$deuda_capital-$pagos_principal;
                                                        }
                                                    @endphp
                                                    <table id="totales_deuda">
                                                        <thead>
                                                        <th>
                                                            <td style="width: 315px;text-align:right"></td>
                                                            <td style="text-align: right;width:110px;"></td>
                                                            <td style="text-align: right;width:100px;"></td>
                                                            <td style="text-align: right;width:84px;"></td>
                                                            <td style="text-align: right;width:164px;">
                                                                Capital por cobrar<br>
                                                                @if($cobranza->tipo=='1' AND $pagos_principal>0)
                                                                    Pagos a Cta. Principal<br>
                                                                    <b>Total Deuda Capital</b><br>
                                                                @endif
                                                                Intereses por cobrar<br>
                                                                Iva por cobrar<br>
                                                            </td>
                                                            <td style="text-align: right;width:104px;">
                                                                {{number_format($deuda_capital,2,',','.')}}<br>
                                                                @if($cobranza->tipo=='1' AND $pagos_principal>0)
                                                                    <b><u>{{number_format($pagos_principal,2,',','.')}}</u></b><br>
                                                                    <b>{{number_format($total_deuda_capital,2,',','.')}}</b><br>
                                                                @endif
                                                                {{number_format($deuda_interes,2,',','.')}}<br>
                                                                {{number_format($deuda_iva,2,',','.')}}<br>
                                                            </td>
                                                        </th>
                                                        </thead>
                                                    </table>


                                                        <div class="modal-footer">
                                                            <table  class="table table-striped " >
                                                                <tr>

                                                                    <td>
                                                                        <a href="{{route('muestraPago',[$cobranza->pre,7,0])}}">Cargar/Editar Pago</a>
                                                                    </td>
                                                                    <td>
                                                                        <a href="{{route('muestraPago',[$cobranza->pre,7,0])}}" target="_BLANK">Imprimir</a>
                                                                    </td>
                                                                    <td>
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                                                                    </td>
                                                                </tr>
                                                            </table>

                                                        </div>
                                                </div>
                                              </div>
                                            </div>
                                          </div>
                                        </td>
                                        </tr>
                                @endforeach
                                <script>
                                    $('#total_deuda').html('Total Deuda por cobrar:'+'{{ number_format($total_deuda,2,',','.') }}' );

                                </script>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{asset('modal/assets/jquery-1.12.4-jquery.min.js')}}"></script>
<script src="{{asset('modal/assets/jquery.validate.min.js')}}"></script>
<script src="{{asset('modal/assets/ValidarRegistro.js')}}"></script>
<script src="{{asset('modal/dist/js/bootstrap.min.js')}}"></script>
<style>
    .corto{
        width: 120px;
        position: absolute;
    }
    .lado{
        position: absolute;
        left: 540px;
    }
    .centro{
        text-align: center;

    }
    /* scroll*/
    div .ListaTabla {
        border: 2px solid lightgray !important;
    }
    .ListaTabla th td{
        background-color: #E6E6E6;
        color: #727374;
        font-size: 12px;
        border: 2px solid #D1D0C6;
        border-collapse: collapse;
        font-family: "Century Gothic";
    }
    #tablaimpagos {
        /*table-layout: fixed;*/
        width: 100%;
    }
    #tablaimpagos table td {
        color: #000;
        font-size: 8pt;
        border: 2px solid #D1D0C6;
        border-collapse: collapse;
    }
    #tablaimpagos thead, #tablaimpagos tbody { display: block; }
    #tablaimpagos tbody {
        height: 280px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    #tablaimpagos thead {
        height: 40px;
        overflow-y: auto;
        overflow-x: hidden;
    }
    .blanco{
        color: black;
    }
    .rojo{
        color: crimson;
    }
    #totales_individual {
        font-weight: bolder;
        color: white;
        background-color: green;
    }

</style>
@endsection
