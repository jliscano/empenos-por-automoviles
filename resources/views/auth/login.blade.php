@extends('layouts.app')

@section('content')
<style type="text/css">
    .cajita{
        color:gray;
        font-size:2vmin;
        width: 300px;
        margin-left: -50px;
        text-align: center;
    }
    .fondo{
        width: 100%;
        height: 450px;
        background-image:url( {{ asset('/images/fondo.png') }});
        background-repeat: no-repeat;
        background-size: auto;
        background-size: 100% 100%;
    }
    @media only screen and (max-width:800px) {
      /* For tablets: */
        .row .mb-3 {
            margin-left: 100px;

        }
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
        .row .mb-3 {
            margin-left: 40px;


        }
    }
    @media only screen and (max-width:800px) {
      /* For tablets: */
        .fondo {
           /* width: 512px;*/
            height: 720px;
        }
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
        .fondo {
            /*width: 330px;*/
            height: 520px;
        }
    }

</style>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header"><h3>{{ __('Ingreso al Sistema') }}</h3></div>
                <!--card-body-->
                <div class="fondo" >
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <br><br>
                        <div class="row mb-3">
                            <label for="email" class="col-md-4 col-form-label text-md-end">{{ __('Correo') }}</label>

                            <div class="col-md-6" >
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus style="width:200px;">

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="password" class="col-md-4 col-form-label text-md-end">{{ __('Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" style="width:200px;">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Recuerdame') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Ingresar') }}
                                </button>
                                <br>
                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('No recuerdo mi Contraseña:') }}
                                    </a>
                                @endif

                                <div class='cajita'>
                                    <p style="text-align:center;width: 200px;">
                                    NOA<br>
                                    Versión: 1.04<br>
                                    {{ 'Ultima Act:14/10/2024'  }}
                                    </p>
                                </div>
                            </div>
                            <br><br><br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
