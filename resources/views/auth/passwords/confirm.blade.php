@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Confirme Contraseña') }}</div>

                <div class="card-body">
                    {{ __('Por favor confirme su  Contraseña antes de  continuar.') }}

                    <form method="POST" action="{{ route('Contraseña.confirm') }}">
                        @csrf

                        <div class="row mb-3">
                            <label for="Contraseña" class="col-md-4 col-form-label text-md-end">{{ __('Contraseña') }}</label>

                            <div class="col-md-6">
                                <input id="Contraseña" type="Contraseña" class="form-control @error('Password') is-invalid @enderror" name="Contraseña" required autocomplete="current-Contraseña">

                                @error('Contraseña')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirme  Contraseña') }}
                                </button>

                                @if (Route::has('Password.request'))
                                    <a class="btn btn-link" href="{{ route('Password.request') }}">
                                        {{ __('Olvidé mi Contraseña?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
