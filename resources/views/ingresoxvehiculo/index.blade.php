@extends('layouts.app2')

@section('template_title')
    Ingresoxvehiculo
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Vehículos vinculados a este Ingreso') }}
                            </span>

                             <div class="float-right">
                                @if($totalv<$totali)
                                <a onclick="cambiar(1,0,0)" 
                                class="btn btn-primary btn-sm float-right"  data-placement="left" >
                                  {{ __('Nuevo Vehículo') }}
                                </a>
                                @endif
                                
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Num</th>                    
										<th>Nombre</th>
										<th>DNI</th>
                                        <th>Matrícula</th>
                                        <th>Marca/Modelo</th>
										<th>Monto</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($ingresoxvehiculos as $ingresoxvehiculo)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $ingresoxvehiculo->nom }}</td>
											<td>{{ $ingresoxvehiculo->dni }}</td>
                                            <td>{{ $ingresoxvehiculo->matricula }}</td>
                                            <td>{{ $ingresoxvehiculo->mar.'/'.$ingresoxvehiculo->mod }}</td>
											<td>{{ number_format($ingresoxvehiculo->monto,2,','.',') }}</td>

                                            <td>
                                                <form action="{{ route('destroyIngresoxVehiculo',[$ingresoxvehiculo->id,$ingreso_id,$empresa_id]) }}" method="POST">
                                                    <!--<a class="btn btn-sm btn-primary " href="{{ route('ingresoxvehiculos.show',$ingresoxvehiculo->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Show') }}</a>-->
                                                    <a class="btn btn-sm btn-success" href="{{ route('editaIngresosxVehiculos',[$ingresoxvehiculo->id,$empresa_id]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Borrar') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $ingresoxvehiculos->links() !!}
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function cambiar(numero,id){
            let gasto=( "<?php echo $ingreso_id; ?>" );
            let empresa=( "<?php echo $empresa_id; ?>" );
            let urlstr =( "<?php echo $_SERVER['HTTP_HOST']; ?>" );
            if(numero==1){
                var direccion = 'http://'+urlstr+'/createIngresoVehiculo/'+gasto+'/'+empresa+'/create';
                parent.document.getElementById("gxv").src="{{route('createIngresoVehiculo',[$ingreso_id,$empresa_id])}}";
            }else{
                var direccion = 'http://'+urlstr+'/muestraIngresosVehiculos/'+id+'/'+empresa+'/edit';
                parent.document.getElementById("gxv").src=direccion;//"{{route('editaGastosxVehiculos',["+id+","+empresa+"] )}}";

            }
            //alert(direccion);
            
            //parent.document.body.style.backgroundColor = "lightblue";

        }
    </script>
@endsection
