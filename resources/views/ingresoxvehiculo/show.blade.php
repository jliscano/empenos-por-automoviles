@extends('layouts.app')

@section('template_title')
    {{ $ingresoxvehiculo->name ?? "{{ __('Show') Ingresoxvehiculo" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Ingresoxvehiculo</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('ingresoxvehiculos.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Ingreso Id:</strong>
                            {{ $ingresoxvehiculo->ingreso_id }}
                        </div>
                        <div class="form-group">
                            <strong>Vehiculo Id:</strong>
                            {{ $ingresoxvehiculo->vehiculo_id }}
                        </div>
                        <div class="form-group">
                            <strong>Monto:</strong>
                            {{ $ingresoxvehiculo->monto }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
