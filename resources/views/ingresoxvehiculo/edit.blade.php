@extends('layouts.app2')

@section('template_title')
    {{ __('Update') }} Ingresoxvehiculo
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">{{ __('Modificar') }} Vehículo Vinculado</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('updateIngresoxVehiculo',[$id, $empresa_id]) }}"  role="form" enctype="multipart/form-data">
                            {{ method_field('POST') }}
                            @csrf
                            <?php 
                                $nuevo=0;
                            ?>

                            @include('ingresoxvehiculo.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
