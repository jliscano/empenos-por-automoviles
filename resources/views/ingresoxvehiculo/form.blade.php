<div class="box box-info padding-1">
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
<div class="box box-info padding-1">
    <div class="box-body">
        <div class="" style="margin-left: 200px;">
            {{ Form::label('Buscar Vehículo :(Escriba la Matrícula)') }}
            <input type="text" id='busca'>
            <input type="hidden" id='empresa' value="{{$empresa_id}}">
            <a onclick="buscar(2)"><div class="btn btn-primary" >Buscar</div></a>
        </div>
        <p id='datosvehiculo'>Datos del Vehículo</p>
        
        <div class="form-group">
            <!--{{ Form::label('ingreso_id') }}-->
            {{ Form::hidden('ingreso_id', $ingresoxvehiculo->ingreso_id, ['class' => 'form-control' . ($errors->has('ingreso_id') ? ' is-invalid' : ''), 'placeholder' => 'Ingreso Id']) }}
            {!! $errors->first('ingreso_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!--{{ Form::label('vehiculo_id') }}-->
            {{ Form::hidden('vehiculo_id', $ingresoxvehiculo->vehiculo_id, ['class' => 'form-control' . ($errors->has('vehiculo_id') ? ' is-invalid' : ''), 'placeholder' => 'Vehiculo Id','id'=>'vehiculo_id']) }}
            {!! $errors->first('vehiculo_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('monto') }}
            {{ Form::number('monto', $ingresoxvehiculo->monto, ['class' => 'form-control' . ($errors->has('monto') ? ' is-invalid' : ''), 'placeholder' => 'Monto','style'=>'width:150px;text-align:right;','pattern'=>'[0-9]+([\.,][0-9]+)?']) }}
            {!! $errors->first('monto', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    
    
    
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        <a href="{{route('muestraIngresosVehiculos',[$ingresoxvehiculo->ingreso_id,$empresa_id])}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    </div>
</div>
<script src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script type="text/javascript">
    <?php if($nuevo==0){ ?>
        const cuerpoDelDocumento = document.body;
        cuerpoDelDocumento.onload = buscar(1);
    <?php } ?>
    function buscar(numero) {
        var dato;
        if(numero==1){
            <?php if(isset($ingresoxvehiculo->vehiculo_id)) { ?>;
                dato =  <?php echo $ingresoxvehiculo->vehiculo_id;?>+'|'+  <?php echo $empresa_id;?>;

            <?php }else{ ?>
                dato = '0|'+ <?php echo $empresa_id;?>;
            <?php } ?>
        }else{
            dato =  document.getElementById('busca').value+ '|'+ <?php echo $empresa_id;?>;
        }
        var tabla=1;var datos=0;
        let url ="<?php echo env('APP_URL'); ?>";
        var urlstr= url+'/gastoxvehiculo/miJqueryAjax/'+dato;
        $.ajax({
            url: urlstr,
            method: 'get',
            data:{tabla, datos},
            processData: false,
            contentType: false  
        }).done(function (data) {
            if(data[0]==1){
                $('#vehiculo_id').val(data[1]);
                var datavehiculo='Nombre:<b>'+data[2]+'</b> DNI:<b>'+data[3]+'</b> Matrícula:<b>'+data[4]+'';
                document.getElementById('datosvehiculo').innerHTML=datavehiculo;
            }else{
                alert(data[5]);
                document.getElementById('data').innerHTML='';
            }
        }).fail(function () {
            alert('Error');
        });
    }
</script>