@extends('layouts.app')

@section('template_title')
    Provincia
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Provincias') }}
                            </span>
                            <a href="{{route('home')}}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">Volver</a>

                             <div class="float-right">
                                <a href="{{ route('provincias.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nueva Provincia') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! $provincias->links() !!}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
                                        
										<th>Provincias</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($provincias as $provincia)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $provincia->nombre }}</td>

                                            <td>
                                                <form action="{{ route('provincias.destroy',$provincia->id) }}" method="POST" id="borrar">
                                                    <!--<a class="btn btn-sm btn-primary " href="{{ route('provincias.show',$provincia->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Show') }}</a>-->
                                                    <a class="btn btn-sm btn-success" href="{{ route('provincias.edit',$provincia->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    <a class="btn btn-sm btn-success" href="{{ route('muestraLocalidad',$provincia->id) }}" style="background-color:silver;color: black;"><i class="fa fa-fw fa-edit"></i> {{ __('Localidades') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <a  class="btn btn-danger btn-sm" onclick="aviso({{$provincia->id}});"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i></a>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $provincias->links() !!}
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar esta Provincia?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            
            let url ="<?php echo env('APP_URL'); ?>";
            url=url +'/provincias/'+id;
            document.getElementById("borrar").action = {{ route('provincias.destroy',$provincia->id) }};
            
            
            document.getElementById("borrar").submit();
        }
    }
</script>