@extends('layouts.app')
    <style type="text/css">
        .azul {
            background-color: blue;
            color: white;
            border-radius: 25px;
            margin: 10px;
            width: 160px;
            height: 80px;
            padding: 15px 15px 15px;
        }
        .silver {
            background-color: silver;
            color: black;
            border-radius: 25px;
            margin: 10px;
            width: 160px;
            height: 80px;
            padding: 15px 15px 15px;
        }

    .grid-container {
            display: grid;
            grid-template-rows: 80px 80px;
            /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
            grid-template-columns: 180px 180px  180px;
            grid-gap: 15px;
            padding: 15px 15px 15px;

    }
    @media only screen and (max-width:800px) {
      /* For tablets: */
     .grid-container {
      display: grid;
      grid-template-columns: 180px 180px;
    }
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
      .grid-container {
      display: grid;
      grid-template-columns: 180px;
      
    }
    }

    </style>
    @section('content')
    <div class="box box-info padding-1">
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
    </div>
    
    <?php
    $falta_cliente=cuenta_faltantes_cliente($cliente_id);
    $falta_vehiculo=cuenta_faltantes_vehiculo($vehiculo_id);
    $falta_prestamo=cuenta_faltantes_prestamo($prestamo_id);
    ?>
    @if($falta_cliente>0 or $falta_vehiculo>0 or $falta_prestamo>0)
    
    <div class="alert alert-success">
        <p>
        {{'Carga datos básicos incompleta del Cliente, Vehículo y Empeño' }}<br>
        {{'Algunos reporte No están disponible hasta no completar la carga' }}
        </p>
    </div>
    @endif
    @foreach($clientes as $cliente)
    <div class="card card-default">
    <div class="card-header">
        <!--editar el cliente-->
        <span class="card-title">
            <a href="{{route('clientes.edit',$cliente_id)}}">{{ __('Contratos de: '.$cliente->nombres) }} DNI:{{$cliente->dni}} Vehículo :{{$cliente->mar}}/{{$cliente->mmod}} / {{$cliente->matricula}}</a>
        </span>
    </div>
    
    </div>
    <div style="margin-left:20px;">
    <br>
    
    @if($retorno>0)
    @if(!is_null($cliente->id))
    <a href="{{ route('clientes.index') }}" ><div class="grid-item2 btn btn-primary">Clientes</div></a>
    @endif
    @if(is_null($cliente->veh))
        <a href="{{ url('vehiculos/createcliente',[$cliente->id,1]) }}" ><div class="grid-item2 btn btn-primary">Vehículos</div></a>
    @else
        <a href="{{ route('listavehiculos',[$cliente->id,1]) }}" ><div class="grid-item2 btn btn-primary">Vehículos</div></a>
    @endif
    
    @if(!is_null($cliente->id))
        <a href="{{route('muestraDocumentos',[$cliente->id,1,0])}}" ><div class="grid-item4 btn btn-primary">Documentos</div></a>
    @endif
    @endif

    
    <div class="grid-container">
        @if($falta_cliente>0 or $falta_vehiculo>0 or $falta_prestamo>0)
        <div class="grid-item1 silver">Contrato Compra-Venta</div>
        @else
        <a href="{{route('CompraVenta',[$cliente_id,$cliente->veh,$cliente->pre])}}" target="_blank" ><div class="grid-item1 azul">Contrato Compra-Venta</div></a>
        @endif
        
        @if($falta_cliente>0 or $falta_vehiculo>0 or $falta_prestamo>0)
        <div class="grid-item1 silver">Datos de la Operación</div>
        @else
        <a href="{{route('DatosOperacion',[$cliente_id,$cliente->veh,$cliente->pre])}}" target="_blank" ><div class="grid-item1 azul">Datos de la Operación</div></a>
        @endif

        @if($falta_cliente>0 or $falta_vehiculo>0 or $falta_prestamo>0)
        <div class="grid-item1 silver">Documento de Entrega</div>
        @else
        <a href="{{route('DocumentoEntrega',[$cliente_id,$cliente->veh,$cliente->pre])}}" target="_blank"><div class="grid-item3 azul">Documento de Entrega</div></a>
        @endif

        @if($falta_cliente>0 or $falta_vehiculo>0 or $falta_prestamo>0)
        <div class="grid-item1 silver">Edición Cliente</div>
        @else
        <a href="{{route('EdicionCliente',[$cliente_id,$cliente->veh,$cliente->pre])}}" target="_blank"><div class="grid-item4 azul">Edición Cliente</div></a>
        @endif

        @if($falta_cliente>0 or $falta_vehiculo>0 or $falta_prestamo>0)
        <div class="grid-item1 silver">Contrato Arrendamiento</div>
        @else
        <a href="{{route('ContratoArrendamiento',[$cliente_id,$cliente->veh,$cliente->pre])}}" target="_blank"><div class="grid-item5 azul">Contrato Arrendamiento</div></a>
        @endif

        @if($falta_cliente>0 or $falta_vehiculo>0 or $falta_prestamo>0)
        <div class="grid-item1 silver">Mandato Específico</div>
        @else
        <a href="{{route('MandatoEspecifico',[$cliente_id,$cliente->veh,$cliente->pre])}}" target="_blank"><div class="grid-item6 azul">Mandato Específico</div></a>
        @endif
    </div>
        @endforeach
    </div>
    
    <br>
    <div style="margin-left:20px;">
    @if($retorno==0)
    
    <a href="{{ route('listaprestamos',[0,0,0]) }}"><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    @elseif($retorno==1 )
        <a href="{{ route('editaprestamo',[$cliente->pre,$retorno]) }}"><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    @elseif($retorno==5)
        <a href="{{ route('listaprestamos',[$cliente->id,$cliente->veh,1]) }}"  class="btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    @elseif($retorno==6)
        <a href="{{ route('listaprestamoscliente',[$cliente->id,$cliente->veh,1]) }}"  class="btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
        @elseif($retorno==8)
        <a href="{{ route('editaprestamo',[$cliente->pre,1]) }}"  class="btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
        
    @endif
    </div>
    @endsection