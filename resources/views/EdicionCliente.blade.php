<<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Edición del Cliente</title>
<style type="text/css">
        .pequena{
            font-size: 10px;
        }
        .cabecera{
            width: 100%;
        }
        .cabecera tr{
            width: 100%;
            text-align: center;
        }
        .basico {
            width: 100%;
            border: 2px solid blue;
            padding: 10px;
            border-radius: 25px;
        }
        table.tabla_sin {
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        th.borde-doble{
            text-align: right;
            padding: 0;
            border-bottom:double;
        }
        .amor{
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        td.alinea_derecha {
            text-align: right;
            padding: 0;
        }

        /*.page-break {
            page-break-after: always;
        }*/
        div{
            font-size: 18px;
        }
        div.page_break + div.page_break{
            page-break-before: always;
        }
    .tabs-1{
        margin-left: 40px;
    }
/** Define the margins of your page **/
@page {
    margin: 60px 110px;
    margin-left: 100px;
    margin-bottom: 80px;
}

header {
position: fixed;
top: -50px;
left: 0px;
right: 0px;
/*height: 50px;*/

/** Extra personal styles **/

text-align: left;
line-height: 35px;

}

footer {
position: fixed;
bottom: -40px;
left: 0px;
right: 0px;
height: 50px;

/** Extra personal styles **/
/*background-color: #03a9f4;
color: white;
text-align: center;
*/
line-height: 35px;

}

</style>
</head>
<body>
<header>

</header>
<main>
    <table class="tabla_sin" >
        <tr>
            <th colspan="4" align="right">
                    Teléfono oficina: 622 161 442<br>
                    www.confiacar.es
            </th>
        </tr>
        <tr>
            <td colspan="2">
                <img src="{{ asset('images/logo_confiacar.png') }}" width="180" height="120">
            </td>
            <td colspan="2">
                <h2>PROPUESTA COMERCIAL</h2>
            </td>
        </tr>
    </table>
    @foreach ($clientes as $cliente)
    <table class="tabla_sin" border="1">
        <tr>
            <td align="left">
                LE ATENDIO:
            </td>
            <td align="left">
                {{($cliente->replegal)}}
            </td>
            <td align="right">
                FECHA
            </td>
            <td align="left">
                {{ date( 'd/m/Y',strtotime($cliente->ffirma))}}
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
              <h3> <b>DATOS DEL CLIENTE</b> </h3>
            </td>
            <td colspan="2" align="center">
               <h3> <b>DATOS DEL AUTOMOVIL</b></h3>
            </td>
        </tr>
        <tr>
            <td>
                CIF/ NIF:
            </td>
            <td>
                {{($cliente->dni)}}
            </td>
            <td>
                MATRICULA:
            </td>
            <td>
                {{($cliente->matricula)}}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                NOMBRE:
            </td>
            <td>
                MARCA:
            </td>
            <td>
                {{($cliente->mar)}}
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                {{($cliente->nom)}}
            </td>
            <td>
                MODELO:
            </td>
            <td align="left">
                {{($cliente->mod)}}
            </td>
        </tr>
        <tr>
            <td colspan colspan="2">
                DIRECCION:
            </td>
            <td>
                FECHA MATRICULA:
            </td>
            <td>
                @if(!$cliente->fmatricula)
                __/__/____
                @else
                {{ date('d/m/Y',strtotime($cliente->fmatricula)) }}
                @endif
            </td>
        </tr>
        <tr>
            <td colspan="2" align="left">
                {{($cliente->direccion)}}
            </td>
            <td>
                KMTS REALIZADOS:
            </td>
            <td>
                {{($cliente->kilometros)}}
            </td>
        </tr>
        <tr>
            <td>
                PROVINCIA:
            </td>
            <td>
                {{($cliente->prov)}}
            </td>
            <td rowspan="2">
                FECHA CARNET CONDUCIR
            </td>
            <td align="left" rowspan="2">
                @if(is_null($cliente->fcarnet))
                __/__/____
                @else
                {{ date('d/m/Y',strtotime($cliente->fcarnet)) }}
                @endif
            </td>
        </tr>

        <tr>
            <td>
                MUNICIPIO:
            </td>
            <td align="left">
                {{($cliente->mun)}}
            </td>
        </tr>
        <tr>
            <td colspan="2">
                TELEFONO:<BR />
                {{$cliente->telefono}}
            </td>
            <td colspan="2">
                    CORREO:<BR />
                    {{$cliente->email}}***
            </td>
        </tr>
    </table>

    <table class="tabla_sin" border="0">
        <tr>
            <td colspan="2" align="center">
                <u><h4>DEPOSITO</h4></u>
            </td>
            <td colspan="2" align="center">
                <u><h4>CIRCULANDO</h4></u>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                CUOTA MENSUAL (IVA inc.)
            </td>
            <td colspan="2">
                CUOTA MENSUAL (IVA inc.)
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <?php
                if($cliente->situacion_id==1)
                echo number_format($cliente->tcuota,2,',','.');
                ?>
            </td>
            <td colspan="2">
                <?php
                if($cliente->situacion_id==2)
                echo number_format($cliente->tcuota,2,',','.');
                ?>
            </td>
        </tr>
        <tr>
            <td colspan="4">
                OFERTA SELECCIONADA:
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <?php
                if($cliente->situacion_id==1)
                echo 'X';
                ?>
            </td>
            <td colspan="2" align="center">
                <?php
                if($cliente->situacion_id==2)
                echo 'X';
                ?>
            </td>
        </tr>
    </table>
    <table class="tabla_sin">
        <tr>
            <td valign="top">
                CONFORME CLIENTE<br>
                    {{$cliente->nom}}
            </td>
            <td align="center">
                CONFORME {{($cliente->emp)}}<br>

                <img src="{{ asset('images/emp'.$cliente->empresa_id.'.png') }}" width="120" height="80">
            </td>
        </tr>
    </table>
    <div class="pequena" align="justify">
        <p>
        {{($cliente->emp)}}. le informa que sus datos personales serán tratados con la finalidad de llevar a cabo la correcta gestión de la propuesta comercial. Los datos personales han sido facilitados por el propio interesado, como consecuencia de la relación contractual que mantiene con nosotros.
        Autorizo a {{($cliente->emp)}}. para la realización de envíos publicitarios con el fin de estar informado de las últimas ofertas y servicios.<br>
        SI    <br>    NO <br>
        Autorizo a {{($cliente->emp)}}. para la cesión de mis datos a ALRI SOCIEDAD DE INVERSORES, S.L. con el fin de que éste me pueda informar de ofertas y servicios interesantes.<br>
        SI    <br>    NO<br>
        Sus datos no serán cedidos a otros terceros salvo obligación legal.<br>
        Usted puede acceder, rectificar y suprimir los datos, así como ejercitar otros derechos y obtener información adicional en {{($cliente->emp)}},
        en {{$cliente->domiempresa}} o al email {{$cliente->emailempresa}}<br>
</p>
    </div>
    @endforeach
        <footer>
        <script type="text/php">
            if ( isset($pdf) ) {
                // v.0.7.0 and greater
                $pdf->page_script('

                $x = 462;
                $y = 745;

                $text = "Pág :{PAGE_NUM} de {PAGE_COUNT}" ;
                $font = $fontMetrics->get_font("helvetica", "normal");
                $size = 12;
                $color = array(0,0,0);
                $word_space = 0.5;  //  default
                $char_space = 0.3;  //  default
                $angle = 0.0;   //  default
                $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
                ');
            }
        </script>

    </footer>
</main>
</body>
</html>
