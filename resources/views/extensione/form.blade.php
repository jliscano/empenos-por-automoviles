<div class="box box-info padding-1">
    <div class="box-body">

        <div class="form-group">
            <!--{{ Form::label('prestamo_id') }}-->
            {{ Form::hidden('prestamo_id', $extensione->prestamo_id, ['class' => 'form-control' . ($errors->has('prestamo_id') ? ' is-invalid' : ''), 'placeholder' => 'Prestamo Id']) }}
            {!! $errors->first('prestamo_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('fecha') }}
            {{ Form::date('fecha', $extensione->fecha, ['class' => 'form-control' . ($errors->has('fecha') ? ' is-invalid' : ''), 'placeholder' => 'Fecha']) }}
            {!! $errors->first('fecha', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('monto') }}
            {{ Form::number('monto', $extensione->monto, ['class' => 'form-control' . ($errors->has('monto') ? ' is-invalid' : ''), 'placeholder' => 'Monto','id'=>'monto','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any']) }}
            {!! $errors->first('monto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Cuota Total :') }}
            {{ Form::number('tcuota', $extensione->tcuota, ['class' => 'form-control' . ($errors->has('tcuota') ? ' is-invalid' : ''), 'placeholder' => 'Tcuota','id'=>'tcuota','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any']) }}
            {!! $errors->first('tcuota', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Cuota :') }}
            {{ Form::number('cuota', $extensione->cuota, ['class' => 'form-control' . ($errors->has('cuota') ? ' is-invalid' : ''), 'placeholder' => 'Cuota','id'=>'cuota','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any']) }}
            {!! $errors->first('cuota', '<div class="invalid-feedback">:message</div>') !!}
        </div>

        <div class="form-group">
            {{ Form::label('IVA :') }}
            {{ Form::number('iva', $extensione->iva, ['class' => 'form-control' . ($errors->has('iva') ? ' is-invalid' : ''), 'placeholder' => 'Iva','id'=>'iva','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any']) }}
            {!! $errors->first('iva', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!--{{ Form::label('piva') }}-->
            {{ Form::hidden('piva', $extensione->piva, ['class' => 'form-control' . ($errors->has('piva') ? ' is-invalid' : ''), 'placeholder' => 'Piva']) }}
            {!! $errors->first('piva', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        <a href="{{ route('editaprestamo',[$extensione->id,$retorno]) }}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">Volver
        </a>
    </div>
</div>
<script>
    $('body').on('change', '#tcuota', function () {
        return calc();
    });
    $('body').on('change', '#monto', function () {
        return calc();
    });
    function calc(){
        var tcuota=$('#tcuota').val();
        var monto=$('#monto').val();
        var piva=$('#piva').val();
        var cuota = tcuota/(1+(piva/100));
        var iva =tcuota-cuota;
        $('#cuota').val(Math.round10(cuota,-2));
        $('#iva').val(Math.round10(iva,-2));

    }

    function decimalAdjust(type, value, exp) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    //value=value.tasa.toLocaleString('es-ES');
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back

    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }

function number_format(amount, decimals) {

amount += ''; // por si pasan un numero en vez de un string
amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

decimals = decimals || 0; // por si la variable no fue fue pasada

// si no es un numero o es igual a cero retorno el mismo cero
if (isNaN(amount) || amount === 0)
    return parseFloat(0).toFixed(decimals);

// si es mayor o menor que cero retorno el valor formateado como numero
amount = '' + amount.toFixed(decimals);

var amount_parts = amount.split('.'),
    regexp = /(\d+)(\d{3})/;

while (regexp.test(amount_parts[0]))
    amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

return amount_parts.join('.');
}
</script>
