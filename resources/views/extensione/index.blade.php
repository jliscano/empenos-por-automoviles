
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="card-header">
                <div style="display: flex; justify-content: space-between; align-items: center;">
                    <span id="card_title">
                        {{ __('Extensiones') }}
                    </span>
                </div>
            </div>

        <div style="text-align: right">


        </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-striped table-hover">
                        <thead class="thead">
                            <tr>
                                <th>No</th>

                                <th></th>
                                <th>Fecha</th>
                                <th>Monto</th>
                                <th>Cuota Total</th>
                                <!--
                                <th>Cuota</th>
                                <th>Piva</th>
                                <th>Iva</th>
                                -->
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i=0;
                            @endphp
                            @foreach ($extensiones as $extensione)
                                <tr>
                                    <td>{{ ++$i }}</td>
                                    <td></td>
                                    <td>{{ date('d/m/Y',strtotime($extensione->finiciopago)) }}</td>
                                    <td>{{ number_format($extensione->importe,2,',','.') }}</td>
                                    <td>{{ number_format($extensione->tcuota,2,',','.') }}</td>
                                    <!--
                                    <td>{{ $extensione->cuota }}</td>
                                    <td>{{ $extensione->iva }}</td>
                                    <td>{{ $extensione->piva }}</td>
                                    -->
                                    <td>
                                        <a class="btn btn-sm btn-success" href="{{ route('editaprestamo',[$extensione->id,$retorno]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                        <a class="btn btn-sm pagos_btn btnn" href="{{ route('muestraPagoExtension',[$extensione->id,10,20]) }}" ><i class="fa fa-fw fa-edit"></i> {{ __('Pagos') }}</a>
                                        @csrf
                                        <a class="btn btn-danger btn-sm" href="{{ route('borraExtension',[$extensione->id,$extensione->padre,10]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Borrar') }}</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>


