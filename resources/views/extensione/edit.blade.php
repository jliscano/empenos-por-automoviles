@extends('layouts.app')

@section('template_title')
    {{ __('Editar') }} Extensión
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">{{ __('Editar') }} Extensión</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('updateExtension', [$extensione->id,$retorno]) }}"  role="form" enctype="multipart/form-data">
                            
                            @csrf

                            @include('extensione.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
