@extends('layouts.app')

@section('template_title')
    {{ $extensione->name ?? "{{ __('Show') Extensione" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Extensione</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('extensiones.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Prestamo Id:</strong>
                            {{ $extensione->prestamo_id }}
                        </div>
                        <div class="form-group">
                            <strong>Fecha:</strong>
                            {{ $extensione->fecha }}
                        </div>
                        <div class="form-group">
                            <strong>Monto:</strong>
                            {{ $extensione->monto }}
                        </div>
                        <div class="form-group">
                            <strong>Tcuota:</strong>
                            {{ $extensione->tcuota }}
                        </div>
                        <div class="form-group">
                            <strong>Cuota:</strong>
                            {{ $extensione->cuota }}
                        </div>
                        <div class="form-group">
                            <strong>Iva:</strong>
                            {{ $extensione->iva }}
                        </div>
                        <div class="form-group">
                            <strong>Piva:</strong>
                            {{ $extensione->piva }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
