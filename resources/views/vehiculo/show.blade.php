@extends('layouts.app')

@section('template_title')
    {{ $vehiculo->name ?? "{{ __('Show') Vehiculo" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Vehiculo</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('vehiculos.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Cliente Id:</strong>
                            {{ $vehiculo->cliente_id }}
                        </div>
                        <div class="form-group">
                            <strong>Marca Id:</strong>
                            {{ $vehiculo->marca_id }}
                        </div>
                        <div class="form-group">
                            <strong>Modelo Id:</strong>
                            {{ $vehiculo->modelo_id }}
                        </div>
                        <div class="form-group">
                            <strong>Empseguro Id:</strong>
                            {{ $vehiculo->empseguro_id }}
                        </div>
                        <div class="form-group">
                            <strong>Nano:</strong>
                            {{ $vehiculo->nano }}
                        </div>
                        <div class="form-group">
                            <strong>Itv:</strong>
                            {{ $vehiculo->itv }}
                        </div>
                        <div class="form-group">
                            <strong>Fcompra:</strong>
                            {{ $vehiculo->fcompra }}
                        </div>
                        <div class="form-group">
                            <strong>Fvencseguro:</strong>
                            {{ $vehiculo->fvencseguro }}
                        </div>
                        <div class="form-group">
                            <strong>Finicseguro:</strong>
                            {{ $vehiculo->finicseguro }}
                        </div>
                        <div class="form-group">
                            <strong>Tiposeguro:</strong>
                            {{ $vehiculo->tiposeguro }}
                        </div>
                        <div class="form-group">
                            <strong>Frecuencia:</strong>
                            {{ $vehiculo->frecuencia }}
                        </div>
                        <div class="form-group">
                            <strong>Valoracion:</strong>
                            {{ $vehiculo->valoracion }}
                        </div>
                        <div class="form-group">
                            <strong>Cuotaunica:</strong>
                            {{ $vehiculo->cuotaunica }}
                        </div>
                        <div class="form-group">
                            <strong>Situacion:</strong>
                            {{ $vehiculo->situacion }}
                        </div>
                        <div class="form-group">
                            <strong>Poliza:</strong>
                            {{ $vehiculo->poliza }}
                        </div>
                        <div class="form-group">
                            <strong>Matricula:</strong>
                            {{ $vehiculo->matricula }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
