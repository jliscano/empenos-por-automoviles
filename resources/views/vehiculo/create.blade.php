@extends('layouts.app')

@section('template_title')
    {{ __('Update') }} Vehiculo
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">{{ __('Nuevo') }} Vehiculo</span>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('vehiculos.store') }}"  role="form" enctype="multipart/form-data" onkeydown="return event.key != 'Enter';">
                            
                            @csrf
                            <?php
                                $nuevo=1;
                            ?>
                            <input type="hidden" name="nuevo" id="nuevo" value="<?php echo $nuevo; ?>">

                            @include('vehiculo.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
