@extends('layouts.app')

@section('template_title')
    Vehiculo
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">

                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            <span id="card_title">
                                {{ __('Vehiculos') }}
                                @if($retorno>0)
                                <div id='iden'></div>
                                @endif
                            </span>
                        <div align="center">
                            @if($retorno==0)
                            <a href="{{route('home')}}">
                            @elseif($retorno==1)
                            <a href="{{route('clientes.index')}}">
                                @elseif($retorno==2)
                                <a href="{{route('clientes.edit',$idcliente)}}">
                                <?php //$retorno=4; ?>
                            @else
                                <a href="javascript.document.history.back(-1)">
                            @endif
                                <div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                        </div>
                        <div>

                        </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        <!--botones-->

    @if(!isset($retorno) || $retorno>0)
        <a href="{{ route('clientes.index') }}" ><div class="grid-item2 btn btn-primary">Clientes</div></a>
        <a href="{{route('listaprestamoscliente',[$idcliente,0,4])}}" ><div class="grid-item4 btn btn-primary">Empeños</div></a>
        <a href="{{route('muestraDocumentos',[$idcliente,4,0])}}" ><div class="grid-item4 btn btn-primary">Documentos</div></a>
        <a href="{{ route('createcliente',[$idcliente,4]) }}" ><div class="grid-item5 btn btn-primary">Agregar Vehículo</div></a>
    @endif
                        {!! $vehiculos->links() !!}
                        <div class="table-responsive">
                            @if(!isset($retorno) || $retorno==0)
                            <form action="{{route('listavehiculos',[$idcliente,$retorno])}}">
                                <input type="text" name="texto" class="form-label" style="width:200px;">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                              </form>
                              @endif
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
										<th>Núm</th>
										<!--<th>Empresa</th>
										<th>Nombre</th>
										<th>DNI</th>
                                        -->
										<th>Marca/Modelo</th>
										<th>Matrícula</th>
                                        <th>Sit.<br> Carga</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $cadena=array();
                                    $cad=0;
                                @endphp
                                    @foreach ($vehiculos as $vehiculo)
                                    @php
                                        echo "<script>";
                                        echo '$("#iden").html("<a href=\"'.route('clientes.edit',$idcliente).'\">DNI:<b>'.$vehiculo->dni.'</b>Nombre:<b>'.$vehiculo->nombre.'</b></a>")';
                                        echo "</script>";
                                    @endphp


                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <!--<td>{{ $vehiculo->nombre }}</td>
											<td>{{ $vehiculo->dni }}</td>
                                            -->
                                            <td>{{ $vehiculo->mar }}/{{ $vehiculo->mod }}</td>
                                            <td>{{ $vehiculo->matricula}}</td>
                                            <td>
                                                @if(is_null($vehiculo->idveh))
                                                    <img src="{{ asset('images/rojo.jpg') }}" width="28" height="25">
                                                @else
                                                    <?php
                                                        $cad++;
                                                        $falta=cuenta_faltantes('vehiculos',$vehiculo->id,1,$cadena,$cad);
                                                    ?>
                                                    @if($falta==0)
                                                    <img src="{{ asset('images/verde.jpg') }}" width="28" height="25">
                                                    @else
                                                    <a href="#"  data-toggle="modal" data-target="#myModal{{$cad}}">
                                                        <img src="{{ asset('images/rojo.jpg') }}" width="28" height="25">
                                                    </a>
                                                    @endif
                                                @endif
                                                <div class="modal " id="myModal{{$cad}}" role="dialog" >
                                                    <div class="modal-dialog">
                                                      <!-- Modal -->
                                                      <div class="modal-content" style="height:300px;width:300px;right: 350px;">
                                                        <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          <h4 class="modal-title">Datos Faltantes</h4>
                                                        </div>
                                                        <div class="modal-body" >
                                                            <div>
                                                                @php
                                                                    echo $cadena[$cad];
                                                                @endphp
                                                              </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </td>
											<td>
                                            @if(!is_null($vehiculo->idveh))
                                                <form action="{{ route('vehiculos.destroy',$vehiculo->idveh) }}" method="POST" id="borrar">
                                                    <input type="hidden" name="retorno" value="{{$retorno}}">
                                        <a class="btn btn-sm btn-success btnn" href="{{ route('editavehiculo',[$vehiculo->idveh,$retorno]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                        <a class="btn btn-sm empenos_btn btnn" href="{{ route('listaprestamoscliente',[$vehiculo->cliente_id,$vehiculo->id,$retorno])}}"><i class="fa fa-fw fa-edit"></i> {{ __('Empeños') }}</a>
                                        <a class="btn btn-sm multas_btn btnn"  href="{{ route('muestraMultas',[$vehiculo->idcliente,0,$vehiculo->idveh,$retorno]) }}" ><i class="fa fa-fw fa-edit"></i> {{ __('Multas') }}</a>
                                        <a class="btn btn-sm impuestos_btn btnn"  href="{{ route('listaImpuestos',[$vehiculo->id,8]) }}" ><i class="fa fa-fw fa-edit"></i> {{ __('Impuestos') }}</a>
                                        <a class="btn btn-sm gastos_btn btnn"  href="{{ route('muestraMultas',[$vehiculo->idcliente,1,$vehiculo->idveh,$retorno]) }}" ><i class="fa fa-fw fa-edit"></i> {{ __('Gastos') }}</a>

                                                @csrf
                                                @method('DELETE')
                                                <a onclick="aviso({{$vehiculo->idveh}});" class="btn btn-danger btn-sm btnn"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i> </a>
                                                </form>
                                            @else
                                                    <a class="btn btn-sm btn-success" href="{{ route('createcliente',[$vehiculo->idcliente,2]) }}" style="background-color:blue;color:white;"><i class="fa fa-fw fa-edit" ></i> {{ __('Nuevo') }}</a>
                                            @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $vehiculos->links() !!}
            </div>
        </div>
    </div>
@endsection
<script src="{{asset('modal/assets/jquery-1.12.4-jquery.min.js')}}"></script>
<script src="{{asset('modal/assets/jquery.validate.min.js')}}"></script>
<script src="{{asset('modal/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar este Vehículo?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            url=url +'/vehiculos/'+id;
            document.getElementById("borrar").action = url;
            document.getElementById("borrar").submit();

        }
    }
</script>
