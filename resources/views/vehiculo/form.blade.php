<style type="text/css">
    .grid-container {
        display: grid;
        grid-template-rows: 80px 80px;
        /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
        grid-template-columns: 180px 180px  180px 180px 180px;
        grid-gap: 15px;
        padding: 15px 15px 15px 15px 15px;
        }
        @media only screen and (max-width:800px) {
        /* For tablets: */
            .grid-container {
                display: grid;
                grid-template-columns: 180px 180px;
            }
        }
        @media only screen and (max-width:560px) {
        /* For mobile phones: */
            .grid-container {
                display: grid;
                grid-template-columns: 180px;
            }
        }
</style>

<div class="box box-info padding-1">
    @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
    @endif
</div>
    @if(!isset($retorno) || $retorno>0 )
    @if(!is_null($vehiculo->cliente_id))
        <a href="{{ route('clientes.index') }}" ><div class="grid-item2 btn btn-primary">Clientes</div></a>
    @endif
        <a href="{{route('muestraDocumentos',[$vehiculo->cliente_id,2,0 ])}}" ><div class="grid-item4 btn btn-primary">Documentos</div></a>
        @if($nuevo==0)
            <a href="{{route('listaprestamoscliente',[$vehiculo->cliente_id,$vehiculo->id,1])}}" ><div class="grid-item3 btn btn-primary">Empeños</div></a>
        @endif
    
        @if($nuevo==0)
            <a href="{{ url('vehiculos/createcliente',[$vehiculo->cliente_id,1]) }}" ><div class="grid-item5 btn btn-primary">Agregar Vehículo</div></a>
        @endif
    @endif
    
<div class="box-body">

    <div class="grid-container">
        <div class="grid-item1">
        <div class="form-group">
            {{ Form::label('Fecha de la Matrícula :') }}
            {{ Form::date('fmatricula', $vehiculo->fmatricula, ['class' => 'form-control' . ($errors->has('fmatricula') ? ' is-invalid' : '')]) }}
            {!! $errors->first('fmatricula', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item2">
        <div class="form-group">
        {{ Form::label('Marca :') }}
        {{ Form::select('marca_id', $marca,$vehiculo->marca_id, ['class' => 'form-control' . ($errors->has('marca_id') ? ' is-invalid' : ''), 'placeholder' => 'Asigne una Marca','id' => 'marca_id']) }}
        {!! $errors->first('marca_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item2">
        <div class="form-group">
        {{ Form::label('Modelo :') }}
        {{ Form::select('modelo_id', $modelo, $vehiculo->modelo_id, ['class' => 'form-control' . ($errors->has('modelo_id') ? ' is-invalid' : ''), 'placeholder' => 'Asigne un Modelo', 'id'=>'modelo_id']) }}
        {!! $errors->first('modelo_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item3">
        <div class="form-group">
        {{ Form::label('Matrícula') }}
        {{ Form::text('matricula', $vehiculo->matricula, ['class' => 'form-control' . ($errors->has('matricula') ? ' is-invalid' : ''), 'placeholder' => 'Matricula']) }}
        {!! $errors->first('matricula', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="grid-item12">
        <div class="form-group">
            {{ Form::label('Valoración del Vehículo :') }}
            {{ Form::number('valoracion', $vehiculo->valoracion, ['class' => 'form-control' . ($errors->has('valoracion') ? ' is-invalid' : ''), 'placeholder' => 'Valoracion', 'inputmode'=> 'decimal','step'=>'0.10','min'=>1]) }}
            {!! $errors->first('valoracion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        
        <div class="grid-item4">
        <div class="form-group">
            {{ Form::label('Fecha Venc. ITV :') }}
            {{ Form::date('itv', $vehiculo->itv, ['class' => 'form-control' . ($errors->has('itv') ? ' is-invalid' : ''), 'placeholder' => 'Itv']) }}
            {!! $errors->first('itv', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
    
        <div class="grid-item5">
        <div class="form-group">
            {{ Form::label('Núm. de Póliza de Seguro :') }}
            {{ Form::text('poliza', $vehiculo->poliza, ['class' => 'form-control' . ($errors->has('poliza') ? ' is-invalid' : ''), 'placeholder' => 'Poliza']) }}
            {!! $errors->first('poliza', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item6">
        <div class="form-group">
            {{ Form::label('Empresa de Seguro :') }}
            {{ Form::select('empseguro_id', $empseguro, $vehiculo->empseguro_id, ['class' => 'form-control' . ($errors->has('empseguro_id') ? ' is-invalid' : ''), 'placeholder' => 'Asigne una empresa','id'=>'empseguro_id']) }}
            {!! $errors->first('empseguro_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item7">
        <div class="form-group">
            {{ Form::label('Tipo de seguro :') }}
            {{ Form::select('tipo_id', $tiposeguro,  $vehiculo->tipo_id, ['class' => 'form-control' . ($errors->has('tipo_id') ? ' is-invalid' : ''), 'placeholder' => 'Tipo de Cobertura', 'id'=>'Tipo de seguro']) }}
            {!! $errors->first('tipo_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item8">
        <div class="form-group">
            {{ Form::label('Fecha Inicio Seguro :') }}
            {{ Form::date('finicseguro', $vehiculo->finicseguro, ['class' => 'form-control' . ($errors->has('finicseguro') ? ' is-invalid' : ''), 'placeholder' => 'Agrege una fecha']) }}
            {!! $errors->first('itv', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item9">
        <div class="form-group">
            {{ Form::label('Fecha Vencimiento Seguro :') }}
            {{ Form::date('fvencseguro', $vehiculo->fvencseguro, ['class' => 'form-control' . ($errors->has('fvencseguro') ? ' is-invalid' : ''), 'placeholder' => 'Agrege una fecha']) }}
            {!! $errors->first('fvencseguro', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <!--
        <div class="grid-item10">
        <div class="form-group">
            {{ Form::label('Cuota del Seguro :') }}
            {{ Form::number('cuotaunica', $vehiculo->cuotaunica, ['class' => 'form-control' . ($errors->has('cuotaunica') ? ' is-invalid' : ''), 'placeholder' => 'Cuota del seguro','inputmode'=> 'decimal','step'=>'0.10','min'=>0]) }}
            {!! $errors->first('cuotaunica', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
    -->

        <div class="grid-item11">
        <div class="form-group">
            {{ Form::label('Frecuencia :') }}
            <!--
            <br>
            <select name="frecuencia" id="frecuencia" class="form-control" placeholder="Frecuencia">
                <option value="0">Asigne una frecuancia</option>
                @foreach($frecuencia as $fre)
                @if($fre==$vehiculo->frecuencia)
                    <option value="{{($fre)}}" selected>{{($fre)}}</option>
                @else
                <option value="{{($fre)}}">{{($fre)}}</option>
                @endif
                @endforeach
            </select>
            -->
            {{ Form::select('frecuencia', $frecuencia, $vehiculo->frecuencia, ['class' => 'form-control' . ($errors->has('frecuencia') ? ' is-invalid' : ''), 'placeholder' => 'Frecuencia']) }}
            {!! $errors->first('frecuencia', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item13">
        <div class="form-group">
            {{ Form::label('Situación :') }}
            {{ Form::select('situacion_id', $situacion, $vehiculo->situacion_id, ['class' => 'form-control' . ($errors->has('situacion_id') ? ' is-invalid' : ''), 'placeholder' => 'Situacion']) }}
            {!! $errors->first('situacion_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item14">
        <div class="form-group">
            {{ Form::label('Bastidor :') }}
            {{ Form::text('bastidor', $vehiculo->bastidor, ['class' => 'form-control' . ($errors->has('bastidor') ? ' is-invalid' : ''), 'placeholder' => 'Bastidor']) }}
            {!! $errors->first('bastidor', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

        <div class="grid-item15">
        <div class="form-group">
            {{ Form::label('Kilometraje :') }}
            {{ Form::number('kilometros', $vehiculo->kilometros, ['class' => 'form-control' . ($errors->has('kilometros') ? ' is-invalid' : ''), 'placeholder' => 'Kilometros']) }}
            {!! $errors->first('kilometros', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>
        <div class="grid-item16">
            <div class="form-group">
                {{ Form::label('Combustible :') }}
                {{ Form::select('tcombustible', $tcombustible,$vehiculo->tcombustible, ['class' => 'form-control' . ($errors->has('tcombustible') ? ' is-invalid' : ''), 'placeholder' => 'Tipo de combustible']) }}
                {!! $errors->first('tcombustible', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="grid-item17">
        <div class="form-group">
            {{ Form::label('Observaciones :') }}
            {{ Form::text('observacion', $vehiculo->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observaciones', 'style' =>'width:600px;','maxlength'=>'250']) }}
            {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        </div>

    </div>
    <div class="form-group">
        <!--{{ Form::label('cliente_id') }}-->
        {{ Form::hidden('cliente_id', $vehiculo->cliente_id, ['class' => 'form-control' . ($errors->has('cliente_id') ? ' is-invalid' : ''), 'placeholder' => 'Cliente Id']) }}
        {!! $errors->first('cliente_id', '<div class="invalid-feedback">:message</div>') !!}
    </div>
    <div class="form-group">
        <!--{{ Form::label('Fecha de compra :') }}-->
        {{ Form::hidden('fcompra', $vehiculo->fcompra, ['class' => 'form-control' . ($errors->has('fcompra') ? ' is-invalid' : ''), 'placeholder' => 'Fecha de Compra','value' =>'01/01/1990']) }}
        {!! $errors->first('fcompra', '<div class="invalid-feedback">:message</div>') !!}
    </div>
    <div class="form-group">
      <!--{{ Form::label('Año :') }}-->
      {{ Form::hidden('nano', $vehiculo->nano ,  ['class' => 'form-control' . ($errors->has('nano') ? ' is-invalid' : ''), 'placeholder' => 'Año' ]) }}
      {!! $errors->first('nano', '<div class="invalid-feedback">:message</div>') !!}
      <input type="hidden" value="<?php echo $retorno;?>" name="retorno">
      <input type="hidden" value="<?php echo $nuevo;?>" name="nuevo" id='nuevo'>
</div>

<br>

<div class="box-footer mt20">
        <button type="submit" class="btn btn-primary" onclick="salir();">{{ __('Guardar') }}</button>
        @if($retorno==1 or $retorno==2)
        <a href="{{route('listavehiculos',[$vehiculo->cliente_id,$retorno])}}"  class="btn btn-primary" style="background-color: yellow;color:black;">
        @elseif($retorno==4)
        <a href="{{ route('listavehiculos',[$vehiculo->cliente_id,1]) }}"  class="btn btn-primary" style="background-color: yellow;color:black;">
        @elseif($retorno==6)
        <a href="{{ route('listaprestamoscliente',[$vehiculo->cliente_id,1]) }}"  class="btn btn-primary" style="background-color: yellow;color:black;">
        @elseif($retorno==7)
        <a href="{{route('clientes.index')}}"  class="btn btn-primary" style="background-color: yellow;color:black;">
        @else
        <a href="{{route('listavehiculos',[0,$retorno])}}"  class="btn btn-primary" style="background-color: yellow;color:black;">
        @endif
            Volver
        </a>
        
</div>

<script type="text/javascript">
 
 $('#marca_id').on('change', function(e){
    //console.log(e);
    
    $('#modelo_id').append('<option value="0">Espere un momento...</option>');
    $('#modelo_id').empty();
    $('#modelo_id').append('<option value="0">Asigne un Modelo</option>');
        var nuevo = $('#nuevo').val();
        
        if(nuevo==1){
            var marca = e.target.value;        
            //var marca=$('#marca_id').val();
            $.get('modelosxmarcas/' + marca,function(data) {
                $.each(data, function(fetch, subCate){
                    console.log(data);
                    for(i = 0; i < subCate.length; i++){
                        $('#modelo_id').append('<option value="'+ subCate[i].id +'">'+ subCate[i].nombre +'</option>');
                    }
                })
            })
        }else{
            var marca = e.target.value;
            var cliente = $('#id').val();
            //alert(marca);
            $.get('modelosxmarcas2/' + marca,function(data2) {
                $.each(data2, function(fetch, cMuni){
                    console.log(data2);
                    for(i = 0; i < cMuni.length; i++){
                        $('#modelo_id').append('<option value="'+ cMuni[i].id +'">'+ cMuni[i].nombre +'</option>');
                    }
                })
            })
        }
        
    });
 
 //chequea que se guarde antes de abandonar el módulo
    var modified;
    const form = document.forms[0];
    function salir(){
        window.removeEventListener('beforeunload', chequea);
    }

    window.addEventListener('beforeunload', chequea);
    function chequea(e) {
        if (modified) {
            e.preventDefault();
            e.returnValue = '';
        }
    }
    
    
    $(document).ready(function (){  
        $("input, select").change(function () {
            modified = true;
        }); 
        $("input, select").keyup(function () {
            modified = true;
        }); 
    });
/* No funciona*/
const $nombre = document.querySelector("#nombre");

// Escuchamos el keydown y prevenimos el evento
$nombre.addEventListener("keydown", (evento) => {
	if (evento.key == "Enter") {
		// Prevenir
		evento.preventDefault();
		return false;
	}
}); 
</script>
