@extends('layouts.app')

@section('template_title')
    {{ __('Update') }} Vehiculo
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        <span class="card-title">{{ __('Modificar') }} Vehiculo</span>
                    </div>
                    <div class="card-body">
                        <!--onkeydown="return event.key != 'Enter';"-->
                        <form method="POST" action="{{ route('vehiculos.update', $vehiculo->id) }}"  role="form" enctype="multipart/form-data" onkeydown="return event.key != 'Enter';">
                            
                            {{ method_field('PATCH') }}
                            @csrf
                            <?php
                                $nuevo=0;
                            ?>
                            <button type="submit" disabled hidden aria-hidden="true"></button>
                            <input type="hidden" name="nuevo" id="nuevo" value="<?php echo $nuevo; ?>">

                            @include('vehiculo.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
