<?php
    $tipos=array('Tipo 1','Tipo2');
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Datos de la Operación</title>
<style type="text/css">
        .pequena{
            font-size: 10px;
        }
        .cabecera{
            width: 100%;
        }
        .cabecera tr{
            width: 100%;
            text-align: center;
        }
        .basico {
            width: 100%;
            border: 2px solid blue;
            padding: 10px;
            border-radius: 25px;
        }
        table.tabla_sin {
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        th.borde-doble{
            text-align: right;
            padding: 0;
            border-bottom:double;
        }
        .amor{
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        td.alinea_derecha {
            text-align: right;
            padding: 0;
        }

        /*.page-break {
            page-break-after: always;
        }*/
        div{
            font-size: 18px;
        }
        div.page_break + div.page_break{
            page-break-before: always;
        }
    .tabs-1{
        margin-left: 40px;
    }
/** Define the margins of your page **/
@page {
    margin: 160px 110px;
    margin-left: 140px;
    margin-bottom: 80px;
}

header {
position: fixed;
top: -60px;
left: 0px;
right: 0px;
/*height: 50px;*/

/** Extra personal styles **/

text-align: left;
line-height: 35px;

}

footer {
position: fixed;
bottom: -40px;
left: 0px;
right: 0px;
height: 50px;

/** Extra personal styles **/
/*background-color: #03a9f4;
color: white;
text-align: center;
*/
line-height: 35px;

}

</style>
</head>
<body>
<header>
    <img src="{{ asset('images/logo2.png') }}" width="90" height="60">
</header>
<main>
    <table class="cabecera">
        <tr>
            <th align="center">
                RELLENAR CADA EXPEDIENTE UNA VEZ FIRMADO
            </th>
        </tr>
    </table>
    <br>
    @foreach ($clientes as $cliente)
    <div align="left">
        NOMBRE Y APELLIDOS: {{$cliente->nom}}
        <br>
        DNI: {{$cliente->dni}}
        <br>
        NUMERO DE TELEFONO: {{$cliente->telefono}}
        <br>
        DIRECCION O DIRECCIONES TANTO DEL DNI COMO DE
        LA POLIZA: {{$cliente->direccion}} {{$cliente->loc}} ({{$cliente->prov}})
        <br>
        CORREO ELECTRONICO: {{$cliente->emailcliente}}
        <br>
        MODELO DEL COCHE: {{$cliente->mar}} / {{$cliente->mod}}
        <br>
        MATRICULA DEL COCHE: {{$cliente->matricula}}
        <br>
        FECHA PROXIMA ITV: {{date('d/m/Y',strtotime($cliente->itv))}}
        <br>
        FECHA DE LA FIRMA: {{date('d/m/Y',strtotime($cliente->ffirma))}}
        <br>
    </div>
    <br>
    <div align="left">
        <div style="font-weight: bold;">
        COMPAÑÍA SEGURO:
        </div>
        <div class="tabs-1">
            {{$cliente->empseg}}<br>
            NUMERO DE POLIZA: {{$cliente->poliza}}
            <br>
            VENCIMIENTO DE LA POLIZA: {{date('d/m/Y',strtotime($cliente->fvencseguro))}}
            <br>
            COBERTURAS: {{$cliente->cobertura}}
            <br>
            TELEFONO DEL CORREDOR: {{$cliente->telefonoseg}}
        </div>
    </div>
    <br>
    <div align="left">
        <div style="font-weight: bold;">
        NUMEROS DE LA OPERACIÓN:
        </div>
        <div class="tabs-1">
            VALORACION: {{number_format($cliente->valoracion,2,',','.')}}
            <br>
            TIPO DE EMPEÑO: {{ $tipo[$cliente->tipo]}}
            @if($cliente->tipo=='0')
                {{$cliente->cuotas.' Cuotas'}}
            @endif
            <br>
            SOLICITUD:  {{number_format($cliente->importe-$cliente->gastos,2,',','.')}}
            <br>
            GASTOS:  {{number_format($cliente->gastos,2,',','.')}}
            <br>
            TOTAL:  {{number_format($cliente->importe,2,',','.')}}
            <br>
            CUOTA:  {{number_format($cliente->tcuota,2,',','.')}}
            <br>
            RESIDUAL:  {{number_format($cliente->residual,2,',','.')}}
        </div>
    </div>
    <br>
    <div align="left">
        OTROS DATOS DE INTERES: {{$cliente->observa}}<br>{{$cliente->observav}}
    </div>
    @endforeach
    <footer>
        <script type="text/php">
            if ( isset($pdf) ) {
                // v.0.7.0 and greater
                $pdf->page_script('

                $x = 462;
                $y = 745;

                $text = "Pág :{PAGE_NUM} de {PAGE_COUNT}" ;
                $font = $fontMetrics->get_font("helvetica", "normal");
                $size = 12;
                $color = array(0,0,0);
                $word_space = 0.5;  //  default
                $char_space = 0.3;  //  default
                $angle = 0.0;   //  default
                $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
                ');
            }
        </script>

    </footer>
</main>
</body>
</html>
