<!doctype html>
{{-- <html lang="{{ str_replace('_', '-', app()->getLocale()) }}"> --}}
    <html lang="es_ES">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->

    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.bunny.net/css?family=Nunito" rel="stylesheet">

    <link rel="shortcut icon" sizes="192x192" href="{{ asset('favicon.png') }}">

    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <!-- Scripts -->
    @vite(['resources/sass/app.scss', 'resources/js/app.js'])
</head>
<style>
    .btnn:hover{
        background-color:blueviolet;
    }
    .btnn{
        border: 1px 1px 1px 1px;
        border-color: black;
    }
    .pagos_btn{
        background-color:orange;color:black;
    }
    .contratos_btn{
        background-color:azure;color: black;
    }
    .multas_btn{
        background-color: orange;color: black;
    }
    .impuestos_btn{
        background-color: brown;color: white;
    }
    .documentos_btn{
        background-color:burlywood;color: black;
    }
    .gastos_btn{
        background-color: silver;color: black;
    }
    .empenos_btn{
        background-color:cyan;
        color:black;
    }
    .vehiculos_btn{
        background-color:blue;
        color: beige;
    }
    .renta_btn{
        background-color:bisque;
        color:blue;
        width: 100%;
        text-align: right;
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
        .btn-sm {
            margin: 0px 0px 16px 0px;
            font-size: 10px;
        }
    }

</style>
<body>
<!--onbeforeunload="return ChequeUpdate()"-->
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                    <a  class="navbar-brand" href="{{ url('/home') }}">
                    <img src="{{ asset('images/logo2.png') }}" width="90" height="90">
                    <!--{{ config('app.name', 'Laravel') }}-->
                    </a>
                <div>

                </div>

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">
                        @if (Auth::check())
                            {{ __('Panel del '.Auth::user()->nivel) }}
                        @endif
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                            <!--
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            -->
                            @endif

                            @if (Route::has('register'))
                                <!--
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                                -->
                            @endif
                        @else
                        <div style="margin-left:40mm;">
                            <table>
                                <tr>
                                    <th>


                            <a href="{{url('clientes')}}">
                            <img src="{{ asset('images/cliente.jpg') }}" width="25" height="30">
                            <b style="color: red">
                                {{resumen_faltante('clientes')}}
                            </b>
                            </a>
                                    </th>
                                    <th>
                            <a href="{{route('listavehiculos',[0,0])}}" >
                            <img src="{{ asset('images/vehiculo.jpg') }}" width="25" height="30">
                            <b style="color: red"> {{resumen_faltante('vehiculos')}}</b>
                            </a>
                                    </th>
                                    <th>

                            <a href="{{route('listaprestamos',[0,0,0])}}" >
                            <img src="{{ asset('images/empenos.jpg') }}" width="25" height="30">
                            <b style="color: red"> {{resumen_faltante('prestamos')}}</b>
                            </a>
                                    </th>
                                    </tr>
                            </table>
                        </div>
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();        document.getElementById('logout-form').submit();">
                                        {{ __('Salir') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>

                                    <a class="dropdown-item" href="{{url('users')}}/{{Auth::user()->id}}/edit">

                                        {{ __('Perfil') }}
                                    </a>
                                    <a class="dropdown-item" href="{{url('change-password')}}">

                                        {{ __('Cambiar Password') }}
                                    </a>
                                    <a class="dropdown-item" href="{{route('manuales')}}">

                                        {{ __('Manuales') }}
                                    </a>
                                </div>


                            </li>

                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <?php
            //DateTimeZone('Europe/Madrid');
            setlocale(LC_ALL, 'es_ES.UTF-8');
            ?>
            @yield('content')
        </main>
    </div>
</body>
</html>
