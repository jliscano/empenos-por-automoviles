@extends('layouts.app')
    <style type="text/css">
        .azul {
            background-color: blue;
            color: white;
            border-radius: 25px;
            margin: 10px;
            width: 160px;
            height: 80px;
            padding: 15px 15px 15px;
        }

    .grid-container {
            display: grid;
            grid-template-rows: 80px 80px;
            /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
            grid-template-columns: 180px 180px  180px;
            grid-gap: 15px;
            padding: 15px 15px 15px;

    }
    @media only screen and (max-width:800px) {
      /* For tablets: */
     .grid-container {
      display: grid;
      grid-template-columns: 180px 180px;
    }
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
      .grid-container {
      display: grid;
      grid-template-columns: 180px;

    }
    }

    </style>
    @section('content')
    <?php
        $micarpeta =  ('/documentos/');
    ?>
    <div class="card card-default">
    <div class="card-header">
        <span class="card-title">{{ __('Manuales de Usuario') }}
        </span>
    </div>
    </div>
    <div class="grid-container">
        <a href="{{'/public/documentos/Clientes.pdf'}}" target="_blank" ><div class="grid-item1 azul">Módulo de Clientes</div></a>
        <a href="{{'/public/documentos/Gastos.pdf'}}" target="_blank"><div class="grid-item2 azul">Módulo de Gastos</div></a>
        {{-- <a href="{{'/public/documentos/Cobros.pdf'}}" target="_blank"><div class="grid-item3 azul">Módulo de Cobros</div></a>--}}
        <a href="{{'/public/documentos/ITV-Poliza.pdf'}}" target="_blank"><div class="grid-item4 azul">Módulo de ITV/Polizas/Carnet</div></a>
        <a href="{{'/public/documentos/Usuario.pdf'}}" target="_blank"><div class="grid-item5 azul">Perfil de Usuarios</div></a>
        <a href="{{'/public/documentos/Impagos.pdf'}}" target="_blank"><div class="grid-item5 azul">Impagos</div></a>
        <a href="{{'/public/documentos/Configuraciones.pdf'}}" target="_blank"><div class="grid-item5 azul">Configuraciones Generales</div></a>
        <a href="{{'/public/documentos/Reporte_Individual.pdf'}}" target="_blank"><div class="grid-item6 azul">Rentabilidad Individual</div></a>
    </div>
    <a href="{{route('home')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    @endsection
