@extends('layouts.app')

@section('template_title')
    Clasificado
@endsection

<?php
    $tipos=array('Gastos','Ingresos')
?>
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Clasificador') }}
                            </span>
                            <a href="{{route('home')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>

                             <div class="float-right">
                                <a href="{{ route('clasificados.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nueva Clasificación') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! $clasificados->links() !!}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
                                        
										<th>Nombre</th>
										<th>Tipo</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($clasificados as $clasificado)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            
											<td>{{ $clasificado->nombre }}</td>
											<td>{{ $tipos[$clasificado->tipo] }}</td>

                                            <td>
                                                <form action="{{ route('clasificados.destroy',$clasificado->id) }}" method="POST" id="borrar">
                                                    <!--<a class="btn btn-sm btn-primary " href="{{ route('clasificados.show',$clasificado->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Show') }}</a>-->
                                                    <a class="btn btn-sm btn-success" href="{{ route('clasificados.edit',$clasificado->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <a  class="btn btn-danger btn-sm" onclick="aviso({{$clasificado->id}});"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i></a>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $clasificados->links() !!}
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar esta Clase?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            url=url +'/clasificados/'+id;
            document.getElementById("borrar").action = url;
            document.getElementById("borrar").submit();
        }
    }
</script>