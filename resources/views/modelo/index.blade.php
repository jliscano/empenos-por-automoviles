{{-- @extends('layouts.app')

@section('template_title')
    Modelo
@endsection
 --}}
{{-- @section('content') --}}
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            <span id="card_title">
                                {{ __('Modelos') }}
                            </span>
                            {{-- <a href="{{route('marcas.index')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a> --}}

                             {{-- <div class="float-right">
                                <a href="{{ route('creaModelo',$id) }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nuevo Modelo') }}
                                </a>
                              </div> --}}
                              <div class="float-right">
                                <a href="{{ route('creaModelo',$marca->id) }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nuevo Modelo') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! $modelos->links() !!}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
										{{-- <th>Marca</th> --}}
										<th>Modelo</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <form action="{{url('modelos.destroy',0)}}" method="POST" id ="borrarmodelo">
                                    </form>
                                    @foreach ($modelos as $modelo)
                                        <tr>
                                            <td>{{ ++$i }}</td>
											{{-- <td>{{ $modelo->nmar }}</td> --}}
											<td>{{ $modelo->nombre }}</td>
                                            <td>
                                                <form action="{{url('modelos.destroy',$modelo->id)}}" method="POST" id ="borrarmodelo">
                                                    <a class="btn btn-sm btn-success" href="{{ route('modelos.edit',$modelo->id) }}">
                                                        <i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}
                                                    </a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <a onclick="avisomodelo({{$modelo->id}});" class="btn btn-danger btn-sm btnn"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i> </a>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $modelos->links() !!}
            </div>
        </div>
    </div>
{{-- @endsection --}}
<script type="text/javascript">
    function avisomodelo(id){
        var resp=confirm("¿Está seguro de eliminar este Modelo?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            url1=url+'/modelos/'+id;
            //alert(url);
            document.getElementById("borrarmodelo").action=url1;
            document.getElementById("borrarmodelo").submit();
        }
    }
</script>
