<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Compra Venta</title>

<style type="text/css">


    /*.pagenum:before {
        content: counter(page);
    }*/
        .pequena{
            font-size: 10px;
        }
        .cabecera{
            width: 100%;
        }
        .cabecera tr{
            width: 100%;
            text-align: center;
        }
        .basico {
            width: 100%;
            border: 2px solid blue;
            padding: 10px;
            border-radius: 25px;
        }
        table.tabla_sin {
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        th.borde-doble{
            text-align: right;
            padding: 0;
            border-bottom:double;
        }
        .amor{
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        td.alinea_derecha {
            text-align: right;
            padding: 0;
        }

        /*.page-break {
            page-break-after: always;
        }*/
        div{
            font-size: 18px;
        }
        /*
        div.page_break + div.page_break{
            page-break-before: always;
        }
        */
        .page-break { page-break-after: always; }
/** Define the margins of your page **/
@page {
    margin: 160px 120px;
    margin-left: 160px;
    margin-bottom: 100px;
}

header {
position: fixed;
top: -60px;
left: 0px;
right: 0px;
/*height: 50px;*/

/** Extra personal styles **/

text-align: left;
line-height: 35px;
}

footer {
position: fixed;
bottom: -75px;
right: 250px;
}
#logopagina{
    width: 30%;
    display: table;
}
</style>
</head>
<body>

 <header>
    <img src="{{ asset('images/logo2.png') }}" width="90" height="60">
</header>


    @foreach ($clientes as $cliente)
    <footer>
        <div id="logopagina">
            <img src="{{ asset('images/emp'.$cliente->empresa_id.'.png') }}" width="120" height="80">
        </div>
    </footer>
    <main>

    @php

        if($cliente->sexo=='M')
            $don="Don";
        else
            $don="Doña";
        if($cliente->sexo2=='M')
            $don2="Don";
        else
            $don2="Doña";
        $pnotarial='';
        if($cliente->pnotarial!='')
            $pnotarial=' bajo poder notarial ' .$cliente->pnotarial;

            $dias=date('d',strtotime($cliente->ffirma));
            $mes=date('m',strtotime($cliente->ffirma));
            $nano=date('Y',strtotime($cliente->ffirma));
            $entero=intval($cliente->valoracion);
            $cent = $cliente->valoracion-$entero;
    @endphp
        <table class="cabecera">
            <tr>
                <th align="center" colspan="2">
                    Contrato de Compra Venta
                </th>
            </tr>
            <br>
            <tr>
                <td colspan="2" align="left">
                    En <u>{{ ($cliente->prov) }}</u> a <u>{{($dias)}}</u>  de <u>{{($mes)}}</u> de <u>{{($nano)}}</u>
                </td>
            </tr>
            <br>
            <tr>
                <td colspan="2">
                    REUNIDOS
                </td>
            </tr>
        </table>
        <br>
        <div align="justify" >
            De una parte, Don. {{($cliente->replegal)}}, mayor de edad,
            con D.N.I. número {{($cliente->dnireplegal)}}, actúa en representación
            de la empresa {{($cliente->emp)}}, con domicilio en
            {{($cliente->domicilio)}},  con C.I.F: nº {{($cliente->cif)}},
            en su condición de administrador único.
            {{-- , en adelante ARRENDADOR Y PROPIETARIO --}}
        </div>
        <br>
        @php
        $coletilla_empresa='actúa en su propio nombre y representación.';
        if(!is_null($cliente->proempresa)){
            $coletilla_empresa=' actúa en nombre y representación de '. $cliente->proempresa.' '.$cliente->dnicif.'.'.
        ' en su condición de Administrador único '.$pnotarial.'.';
        }
        $nombre2='';$vendedor='el VENDEDOR';$vende='al VENDEDOR';$ven='VENDEDOR';
        if($cliente->nombre2!='' and !is_null($cliente->nombre2)){
            $coletilla_empresa=' actúan en su propio nombre y representación.';
            if(!is_null($cliente->proempresa))
                $coletilla_empresa=' actúan en nombre y representación de '. $cliente->proempresa.' con C.I.F:'.$cliente->dnicif.'.'.
            ' en su condición de Administradores '.$pnotarial.'.';
            $nombre2=' y '.$don2.' '.$cliente->nombre2.', mayor de edad, con D.N.I número '. $cliente->dni2;
            $vendedor='los VENDEDORES';$vende='a los VENDEDORES';$ven='VENDEDORES';
        }
        @endphp
        <div align="justify" >
                De otra parte, {{$don}} {{(($cliente->nom))}} (en adelante {{$vendedor}}),
                mayor de edad, con D.N.I número {{($cliente->dni)}}
                {{$nombre2}}, con domicilio en  {{($cliente->direccion)}},
                {{$cliente->loc}} ({{($cliente->prov)}}), {{$coletilla_empresa}}
        </div>
        <br>
        <div align="center" >
            EXPONEN
        </div>
        <br>
        <div align="justify" >
                I.- Que {{$don}} {{($cliente->nom)}} ({{$vendedor}}) es dueño del siguiente vehículo:
        </div>

        <div align="left" >
            - Marca: {{($cliente->mar)}}<br>
            - Modelo: {{($cliente->mod)}}<br>
            - Matrícula: {{($cliente->matricula)}}<br>
            - Número de Bastidor: {{($cliente->bastidor)}}<br>
            - Kilómetros: {{(number_format($cliente->kilometros,0,',','.') )}}
        </div>
        <br>
        <div align="justify" >
                II.- Que {{$don}} {{($cliente->nom)}} ({{$vendedor}}) declara que el reseñado vehículo se encuentra libre de cargas y gravámenes y al corriente de pago de todos los impuestos y sin sanción de ningún tipo pendiente de pago.
        </div>
        <br>
        <div align="justify" >
            III.-  Que la mercantil compradora conoce el estado actual del vehículo.
        </div>
        <br>
        <div align="justify" >
            IV.- Así mismo, ({{$vendedor}}) se compromete a firmar cuantos documentos aparte de éste sean necesarios para que el vehículo quede correctamente inscrito a nombre de {{($cliente->emp)}}, en los correspondientes organismos públicos.
        </div>
        <br>
        <div align="justify" >
            V.- Que habiendo llegado a un acuerdo para la compra y venta del vehículo reseñado, articulan la misma conforme a los siguientes:
        </div>
        <br>
        <div align="center">
            ACUERDOS
        </div>
        <div align="justify" >
            @if($cliente->proempresa=='')
                PRIMERO.-  {{$don}} {{($cliente->nom)}} vende a {{($cliente->emp)}}, el vehículo reseñado en el exponendo I, entregando la posesión del mismo en éste momento; el precio de la compraventa es de <u>{{(traducir($cliente->importe)) }}</u>.  EUROS  (<u>{{number_format($cliente->importe,2,',','.')}}</u> €) que se pagan en éste acto, sirviendo el presente documento como eficaz carta de pago por la citada cantidad.
            @else
                PRIMERO.-  {{$don}} {{($cliente->nom)}} vende a {{($cliente->emp)}}, el vehículo reseñado en el exponendo I, entregando la posesión del mismo en éste momento; el precio de la compraventa es de <u>{{(traducir($cliente->importe)) }}</u>.  EUROS  (<u>{{number_format($cliente->importe,2,',','.')}}</u> €) que se pagan en éste acto, sirviendo el presente documento como eficaz carta de pago por la citada cantidad.
            @endif

        </div>
        <br>
        <div align="justify" >
            SEGUNDO.- El vehículo se vende libre de cualquier carga o gravamen de todo tipo, respondiendo el vendedor frente al comprador de las responsabilidades que por cualquier concepto (deudas civiles, embargos, multas administrativas, impuesto de circulación, etc…) surgiesen contra el citado vehículo por actos anteriores a éste contrato. El VENDEDOR declara que no pesa sobre el vehículo ninguna carga o gravamen ni impuesto, deuda o sanción pendientes de abono en la fecha de la firma de este contrato, comprometiéndose en caso contrario a regularizar tal situación a su exclusivo cargo.
        </div>
        <br>
        <div align="justify" >
            TERCERO.- A partir de éste momento {{($cliente->emp)}} se hace cargo de cuantas responsabilidades derivadas de la propiedad y uso pudiere incurrir el vehículo que adquiere.
        </div>
        <br>
        <div align="justify" >
            CUARTO.- Las partes pactan la compensación de cualquier crédito que {{($cliente->emp)}}, u otra entidad perteneciente a su mismo grupo empresarial, ostente frente al VENDEDOR, o frente a una entidad regida por el mismo, sea cual fuere el título de que derive y aun cuando se encuentre pendiente de vencimiento, con el crédito que el VENDEDOR, o una entidad regida por el mismo, pudiera ostentar frente a {{($cliente->emp)}}, o frente a cualquier entidad perteneciente al mismo grupo empresarial. En el caso de existir cualquier crédito a compensar, {{($cliente->emp)}}., se reservará el derecho de no arrendar el vehículo de la presente compraventa {{$vende}}.
            <br>
            Y para que así conste, firman el presente por duplicado y a un solo efecto en el lugar y fecha arriba indicados.
        </div>
        <br>
        <div align="justify" >
        </div>
        <br>
        <div align="justify" >
        </div>
        <table style="width: 100%;">
        <tr>
            <td align="left">
                {{$ven}}
            </td>
            <td align="right">
                COMPRADOR
            </td>
            </tr>
            <tr>
                <td align="left" valign="bottom" >
                    Fdo.:{{$cliente->nom}}
                </td>
                <td align="right" valign="bottom">
                     <img src="{{ asset('images/emp'.$cliente->empresa_id.'.png') }}" width="100" height="80">
                     <br>
                    Fdo.: Rafael Álvarez Marín
                </td>
            </tr>
            <br>
            <br>
            <br>
            <br>
            <tr>
                <td align="justify" class="pequena" colspan="2">
                   En {{($cliente->emp)}}. tratamos la información que aporta para el contrato con la finalidad de gestionar los compromisos derivados del mismo. Los datos personales que tratamos en {{($cliente->emp)}}. son facilitados por el propio interesado y las categorías de datos tratadas son: Datos identificativos y datos económicos. Sus datos personales serán mantenidos durante el tiempo que dure la relación contractual y posteriormente 10 años durante el cual pueden ser solicitados por Juzgados y Tribunales. La base legal para el tratamiento de sus datos es la ejecución del contrato. Sus datos serán cedidos a una gestoría para realizar la tramitación del cambio de titularidad del vehículo, ésta cesión es necesaria para llevar a cabo el objeto del presente contrato, dependiendo de su forma de pago sus datos serán cedidos a bancos o cajas para la gestión de la misma, los datos personales no serán proporcionados a otros terceros salvo obligación legal. Cualquier persona tiene derecho a obtener confirmación sobre si en {{($cliente->emp)}}. estamos tratando datos personales que les conciernan, o no.  Las personas interesadas tienen derecho a acceder a sus datos personales, así como a solicitar la rectificación de los datos inexactos o, en su caso, solicitar su supresión cuando, entre otros motivos, los datos ya no sean necesarios para los fines que fueron recogidos. En determinadas circunstancias, los interesados podrán solicitar la limitación del tratamiento de sus datos, en cuyo caso únicamente los conservaremos para el ejercicio o la defensa de reclamaciones. En determinadas circunstancias y por motivos relacionados con su situación particular, los interesados podrán oponerse al tratamiento de sus datos. En cualquier momento usted que puede presentar reclamación ante la autoridad de control.
                </td>
            </tr>
        </table>
        <div style="color:blue">
        Teléfono de Contacto: 622 161 442<br>
        www.confiacar.es
        </div>

<footer>
    <script type="text/php">
        if ( isset($pdf) ) {
            // v.0.7.0 and greater
            $x = 458;
            $y = 745;
            $text = "Pág :{PAGE_NUM} de {PAGE_COUNT}";
            $font = $fontMetrics->get_font("helvetica", "normal");
            $size = 12;
            $color = array(0,0,0);
            $word_space = 0.5;  //  default
            $char_space = 0.3;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
        }
    </script>
</footer>
</main>
@endforeach
</body>
</html>
