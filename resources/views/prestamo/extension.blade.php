    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Extensiones') }}
                            </span>

                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                     <!--botones-->
                     <a class="btn btn-sm btn-success" href="{{ route('creaprestamo',[$prestamo->idcliente,$prestamo->idveh,$retorno]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Nuevo') }}</a>

                        <div class="table-responsive">

                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
                                        <!--<th>Nombre</th>
										<th>DNI</th>-->
										<th>Firma</th>
										<th>Importe</th>
										<th>Tipo</th>
                                        <th>Sit.<br>Carga</th>

                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($prestamos as $prestamo)

                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            @if(!is_null($prestamo->ffirma))
											<td>{{date('d/m/Y',strtotime($prestamo->ffirma)) }}</td>
                                            @else
                                            <td>{{ '--------' }}</td>
                                            @endif
											<td>{{ number_format($prestamo->importe,2,',','.') }}</td>
											<td>{{ $tipo[$prestamo->tipo] }}</td>
                                            <td>

                                                <form action="{{ route('prestamos.destroy',$prestamo->id) }}" method="POST" id="borrar">
                                                    @csrf
                                                    @method('DELETE')
                                                    <a class="btn btn-sm btn-success btnn" href="{{ route('editaprestamo',[$prestamo->id,$retorno]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>

                                                    <a  class="btn btn-danger btn-sm btnn" onclick="aviso({{$prestamo->id}});"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i></a>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $prestamos->links() !!}
            </div>
        </div>
    </div>

<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar este Empeño?");
        if(!resp){
            //event.preventDefault();
            //event.returnValue = '';
            return false;
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            url=url +'/prestamos/'+id;
            document.getElementById("borrar").action = url;
            document.getElementById("borrar").submit();
        }
    }
</script>
