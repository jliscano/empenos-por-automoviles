@extends('layouts.app')

@section('template_title')
    Empeños
@endsection

@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Empeños') }}
                                @if($retorno>0)
                                    <div id='iden'></div>
                                @endif
                            </span>
                            @if($retorno==0)
                                <a href="{{route('home')}}">
                            @elseif($retorno==1)
                                <a href="{{ route('clientes.edit',$cliente_id) }}">
                            @elseif($retorno==4)
                                <a href="{{ route('listavehiculos',[$cliente_id,1]) }}">
                                    <?php $retorno=5; ?>
                            @else
                                <a href="javascript:window.history.back()">
                            @endif
                            <div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>

                            <div>

                            </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                     <!--botones-->

    @if(!isset($retorno) || $retorno>0)
        <a href="{{ route('clientes.index') }}" ><div class="grid-item2 btn btn-primary">Clientes</div></a>
        <!--
        <a href="{{ route('clientes.edit',$cliente_id) }}" ><div class="grid-item2 btn btn-primary">Clientes</div></a>
        -->
        <a href="{{ route('listavehiculos',[$cliente_id,1]) }}" ><div class="grid-item2 btn btn-primary">Vehículos</div></a>
        <a href="{{route('muestraDocumentos',[$cliente_id,5,0])}}" class="grid-item3 btn btn-primary">Documentos</a>
        <a class="grid-item4 btn btn-primary" href="{{ route('creaprestamo',[$cliente_id,$vehiculo_id,2]) }}" ><i class="fa fa-fw fa-edit" ></i>Agregar Empeño</a>
    @else
    {!! $prestamos->links() !!}
    <form action="{{route('listaprestamos',[$cliente_id,$vehiculo_id,$retorno])}}">
                                <input type="text" name="texto" class="form-label" style="width:200px;">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                              </form>
    @endif
                        <div class="table-responsive">

                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
                                        <!--<th>Nombre</th>
										<th>DNI</th>-->
										<th>Firma</th>
										<th>Importe</th>
										<th>Tipo</th>
                                        <th>Condición</th>
                                        <th>Sit.<br>Carga</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $cadena=array();
                                        $cad=0;
                                    @endphp
                                    @foreach ($prestamos as $prestamo)
                                        @if($retorno==0)
                                        <script>
                                            $("#iden").html("DNI:<b>{{$prestamo->dni}}</b> Nombre:<b>{{$prestamo->nombre}}</b> Matrícula:<b>{{$prestamo->matricula}} {{$prestamo->mar.'/'.$prestamo->mod}}</b>");
                                        </script>
                                        @endif

                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <!--
                                            <td>{{ $prestamo->nombre }}</td>
                                            <td>{{ $prestamo->dni }}</td>
                                            -->
                                            @if(!is_null($prestamo->ffirma))
											<td>{{date('d/m/Y',strtotime($prestamo->ffirma)) }}</td>
                                            @else
                                            <td>{{ '--------' }}</td>
                                            @endif
											<td>{{ number_format($prestamo->importe,2,',','.') }}</td>
                                            @if(!is_null($prestamo->tipo))
											    <td>{{ $tipo[$prestamo->tipo] }}</td>
                                            @else
                                                <td>{{ '--------' }}</td>
                                            @endif
                                            <td>
                                                @if(!is_null($prestamo->condi))
											    <td>{{$prestamo->condi}} </td>
                                            @else
                                                <td>{{ '--------' }}</td>
                                            @endif

                                            </td>
                                            <td>
                                                @if(is_null($prestamo->pres))
                                                    <img src="{{ asset('images/rojo.jpg') }}" width="28" height="25">
                                                @else
                                                    <?php
                                                        $cad++;
                                                        $falta=cuenta_faltantes('prestamos',$prestamo->pres,1,$cadena,$cad);
                                                    ?>
                                                    @if($falta==0)
                                                        <img src="{{ asset('images/verde.jpg') }}" width="28" height="25">
                                                    @else
                                                        <a href=""  data-toggle="modal" data-target="#myModal{{$cad}}">
                                                            <img src="{{ asset('images/rojo.jpg') }}" width="28" height="25">
                                                        </a>
                                                    @endif
                                                @endif
                                                <div class="modal " id="myModal{{$cad}}" role="dialog" >
                                                    <div class="modal-dialog">
                                                      <!-- Modal -->
                                                      <div class="modal-content" style="height:300px;width:300px;right: 350px;">
                                                        <div class="modal-header">
                                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                          <h4 class="modal-title">Datos Faltantes</h4>
                                                        </div>
                                                        <div class="modal-body" >
                                                            <div>
                                                                @php
                                                                    echo $cadena[$cad];
                                                                @endphp
                                                              </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </td>

                                            <td>
                                            @if(!is_null($prestamo->pres))
                                                <form action="{{ route('prestamos.destroy',$prestamo->id) }}" method="POST" id="borrar">
                                                    <input type="hidden" name="retorno" value="{{$retorno}}">
                                                    <input type="hidden" name="ciente_id" value="{{$prestamo->cliente_id}}">
                                                    <input type="hidden" name="vehiculo_id" value="{{$prestamo->idveh}}">
                                                    <a class="btn btn-sm btn-success btnn" href="{{ route('editaprestamo',[$prestamo->id,$retorno]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    <a class="btn btn-sm contratos_btn btnn" href="{{route('muestraContratos',[$prestamo->cliente_id,$prestamo->id,$prestamo->idveh,$retorno] )}}" ><i class="fa fa-fw fa-edit"></i> {{ __('Contratos') }}</a>
                                                    <a class="btn btn-sm pagos_btn btnn" href="{{ route('muestraPago',[$prestamo->id,$retorno,0]) }}" ><i class="fa fa-fw fa-edit"></i> {{ __('Pagos') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <a  class="btn btn-danger btn-sm btnn" onclick="aviso({{$prestamo->id}});"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i></a>
                                                </form>
                                                @else
                                                    @if(isset($prestamo->idveh) and isset($retorno))
                                                    <a class="btn btn-sm btn-success" href="{{ route('creaprestamo',[$prestamo->idcliente,$prestamo->idveh,$retorno]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Nuevo') }}</a>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $prestamos->links() !!}
            </div>
        </div>
    </div>
@endsection
<script src="{{asset('modal/assets/jquery-1.12.4-jquery.min.js')}}"></script>
<script src="{{asset('modal/assets/jquery.validate.min.js')}}"></script>
<script src="{{asset('modal/dist/js/bootstrap.min.js')}}"></script>
<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar este Empeño?");
        if(!resp){
            //event.preventDefault();
            //event.returnValue = '';
            return false;
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            url=url +'/prestamos/'+id;
            document.getElementById("borrar").action = url;
            document.getElementById("borrar").submit();
        }
    }
</script>
