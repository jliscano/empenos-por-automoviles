@extends('layouts.app')

@section('template_title')
    {{ __('Update') }} Empeño
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="">
            <div class="col-md-12">
                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        @if($prestamo->extension=='S')
                        <span class="card-title">{{ __('Modificar') }} Extensión</span>
                        @else
                        <span class="card-title">{{ __('Modificar') }} Empeño</span>
                        @endif
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('prestamos.update', $prestamo->id) }}"  role="form" enctype="multipart/form-data" onkeydown="return event.key != 'Enter';">
                            {{ method_field('PATCH') }}
                            @csrf
                             <?php
                                $nuevo=0;
                            ?>
                            <input type="hidden" name="nuevo" id="nuevo" value="<?php echo $nuevo; ?>">

                            @include('prestamo.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
