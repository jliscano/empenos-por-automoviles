@extends('layouts.app')

@section('template_title')
    Empeños y Vehiculos
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            <span id="card_title">
                                {{ __('Vehículos y Empeños') }}<div id='iden'></div>
                                @if($retorno>0)
                                    <div id='iden'></div>
                                @endif
                                @php
                                    if($cliente_id>0){
                                    echo "<script>";
                                    echo '$("#iden").html("<a href=\"'.route('clientes.edit',$cliente_id).'\">DNI:<b>'.$dni.' </b>Nombre:<b>'.$nombre.'</b></a>")';
                                    echo "</script>";
                                    }
                                @endphp
                            </span>

                            @if($retorno==0)
                                <a href="{{route('home')}}">
                            @elseif($retorno==1)
                                <a href="{{ route('clientes.edit',$cliente_id) }}">
                            @elseif($retorno==4)
                                <a href="{{ route('listavehiculos',[$cliente_id,7]) }}">
                                    <?php $retorno=5; ?>

                            @else
                                <a href="javascript:window.history.back()">
                            @endif
                            <div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                     <!--botones-->
    @if(!isset($retorno) || $retorno>0)
        <a href="{{route('clientes.index') }}" ><div class="grid-item2 btn btn-primary">Clientes</div></a>
        <a href="{{route('listavehiculos',[$cliente_id,6]) }}" ><div class="grid-item2 btn btn-primary">Vehículos</div></a>
        <a href="{{route('muestraDocumentos',[$cliente_id,6,0])}}" class="grid-item3 btn btn-primary">Documentos</a>

        @if($tvehiculos>0)
        <a class="btn btn-sm btn-success" href="{{ route('creaprestamo',[$cliente_id,$vehiculo_id,2]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Nuevo Empeño') }}</a>
        @endif

    @else
    <form action="{{route('listaprestamos',[$cliente_id,$vehiculo_id,$retorno])}}">
        <input type="text" name="texto" class="form-label" style="width:200px;">
        <button type="submit" class="btn btn-primary">Buscar</button>
    </form>
    @endif
                        <div class="table-responsive">

                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
                                        <th>Matrícula</th>
										<th>Marca/Modelo</th>
										<th>Firma</th>
										<th>Importe</th>
										<th>Tipo</th>
                                        <th>Cond.</th>
                                        <th>Sit.<br>Carga</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($prestamos as $prestamo)
                                        <tr>
                                            <td>{{ ++$i }}</td>
                                            <td>{{ $prestamo->matricula }}</td>
                                            <td>{{ $prestamo->mar.'/'.$prestamo->mod }}</td>
                                            @if(!is_null($prestamo->ffirma))
											<td>{{date('d/m/Y',strtotime($prestamo->ffirma)) }}</td>
                                            @else
                                            <td>{{ '--------' }}</td>
                                            @endif
											<td>{{ number_format($prestamo->importe,2,',','.') }}</td>
                                            @if(!is_null($prestamo->tipo))
											    <td>{{ $tipo[$prestamo->tipo] }}
                                                    </td>
                                            @else
                                                <td>{{ '--------' }}</td>
                                            @endif
                                            <td>
                                            @if(!is_null($prestamo->condi))
                                                <div class="parpadea text">
											    {{$prestamo->condi}}
                                                </div>
                                            @else
                                            {{ '--------' }}
                                            @endif
                                            </td>
                                            <td>
                                                @if(is_null($prestamo->pres))
                                                    <img src="{{ asset('images/rojo.jpg') }}" width="28" height="25">
                                                @else
                                                    <?php
                                                        $falta=cuenta_faltantes('prestamos',$prestamo->pres,1);
                                                    ?>
                                                    @if($falta==0)
                                                        <img src="{{ asset('images/verde.jpg') }}" width="28" height="25">
                                                    @else
                                                        <img src="{{ asset('images/rojo.jpg') }}" width="28" height="25">
                                                    @endif
                                                @endif
                                            </td>

                                            <td>
                                            @if(!is_null($prestamo->pres))
                                                <form action="{{ route('prestamos.destroy',$prestamo->id) }}" method="POST" id="borrar">
                                                    <input type="hidden" name="retorno" value="{{$retorno}}">
                                                    <input type="hidden" name="cliente_id" value="{{$prestamo->idcliente}}">
                                                    <input type="hidden" name="vehiculo_id" value="{{$prestamo->idveh}}">
                                                    <!--<a class="btn btn-sm btn-primary " href="{{ route('prestamos.show',$prestamo->id) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Show') }}</a>-->
                                                    <a class="btn btn-sm btn-success btnn" href="{{ route('editaprestamo',[$prestamo->id,6]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    <a class="btn btn-sm empenos_btn btnn" href="{{ route('creaprestamo',[$prestamo->cliente_id,$prestamo->idveh,2]) }}" ><i  ></i>Nuevo Empeño</a>
                                                    <a class="btn btn-sm contratos_btn btnn" href="{{route('muestraContratos',[$prestamo->cliente_id,$prestamo->id,$prestamo->idveh,6] )}}" ><i class="fa fa-fw fa-edit"></i> {{ __('Contratos') }}</a>
                                                    <a class="btn btn-sm pagos_btn btnn" href="{{ route('muestraPago',[$prestamo->id,6,0]) }}" ><i class="fa fa-fw fa-edit"></i> {{ __('Pagos') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <a  class="btn btn-danger btn-sm btnn" onclick="aviso({{$prestamo->id}});"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i></a>
                                                </form>
                                                @else
                            @if(!isset($prestamo->idveh) and isset($retorno))
                            <a class="btn btn-sm btn-success" href="{{ route('creaprestamo',[$prestamo->idcliente,$prestamo->idveh,2]) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Nuevo Empeño') }}</a>
                            @endif
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $prestamos->links() !!}
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function aviso(id){
        var resp=confirm("¿Está seguro de eliminar este Empeño?");
        if(!resp){
            //event.preventDefault();
            //event.returnValue = '';
            return false;
        }else{
            let url ="<?php echo env('APP_URL'); ?>";
            url=url +'/prestamos/'+id;
            document.getElementById("borrar").action = url;
            document.getElementById("borrar").submit();
        }
    }
</script>
<style>
    .text {
        font-size:12px;
        font-family:helvetica;
        font-weight:bold;
        color:#d96f0b;
    }
    .parpadea{
        animation-name: parpadeo;
        animation-duration: 1s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;

        -webkit-animation-name:parpadeo;
        -webkit-animation-duration: 1s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;
    }
    @-moz-keyframes parpadeo{
  0% { opacity: 1.0; }
  50% { opacity: 0.0; }
  100% { opacity: 1.0; }
}

@-webkit-keyframes parpadeo {
  0% { opacity: 1.0; }
  50% { opacity: 0.0; }
   100% { opacity: 1.0; }
}

@keyframes parpadeo {
  0% { opacity: 1.0; }
   50% { opacity: 0.0; }
  100% { opacity: 1.0; }
}
</style>
