@extends('layouts.app')

@section('template_title')
    {{ __('Create') }} Empeño
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">

                @includeif('partials.errors')

                <div class="card card-default">
                    <div class="card-header">
                        @if($prestamo->extension=='S')
                        <span class="card-title">{{ __('Nueva') }} Extensión</span>
                        @else
                        <span class="card-title">{{ __('Nuevo') }} Empeño</span>
                        @endif

                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('prestamos.store') }}"  role="form" enctype="multipart/form-data" onkeydown="return event.key != 'Enter';">
                            @csrf
                             <?php
                                $nuevo=1;
                            ?>
                            <input type="hidden" name="nuevo" id="nuevo" value="<?php echo $nuevo; ?>">

                            @include('prestamo.form')

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
