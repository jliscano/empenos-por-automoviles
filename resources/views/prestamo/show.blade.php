@extends('layouts.app')

@section('template_title')
    {{ $prestamo->name ?? "{{ __('Show') Prestamo" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Prestamo</span>
                        </div>
                        <div class="float-right">
                                <a href="{{ route('print',$prestamo->id) }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Imprimir PDF') }}
                                </a>
                              </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Cliente Id:</strong>
                            {{ $prestamo->cliente_id }}
                        </div>
                        <div class="form-group">
                            <strong>Ffirma:</strong>
                            {{ $prestamo->ffirma }}
                        </div>
                        <div class="form-group">
                            <strong>Importe:</strong>
                            {{ $prestamo->importe }}
                        </div>
                        <div class="form-group">
                            <strong>Tipo:</strong>
                            {{ $prestamo->tipo }}
                        </div>
                        <div class="form-group">
                            <strong>Cuotas:</strong>
                            {{ $prestamo->cuotas }}
                        </div>
                        <div class="form-group">
                            <strong>Porcentaje:</strong>
                            {{ $prestamo->porcentaje }}
                        </div>
                        <div class="form-group">
                            <strong>Frecuencia:</strong>
                            {{ $prestamo->frecuencia }}
                        </div>
                        <div class="form-group">
                            <strong>Cuota:</strong>
                            {{ $prestamo->cuota }}
                        </div>
                        <div class="form-group">
                            <strong>Iva:</strong>
                            {{ $prestamo->iva }}
                        </div>
                        <div class="form-group">
                            <strong>Tcuota:</strong>
                            {{ $prestamo->tcuota }}
                        </div>
                        <div class="form-group">
                            <strong>Residual:</strong>
                            {{ $prestamo->residual }}
                        </div>
                        <div class="form-group">
                            <strong>Observacion:</strong>
                            {{ $prestamo->observacion }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
