<style >
    .totales{
            border-top: 2px solid blue;
            font-weight: bold;
            text-align: right;
        }
        .cabecera{
            width: 100%;
        }
        .cabecera tr{
            width: 100%;
            text-align: center;
        }
        .basico {
            width: 100%;
            border: 2px solid blue;
            padding: 10px;
            border-radius: 25px;
        }
    table.tabla_sin {
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        th.borde-doble{
            text-align: right;
            padding: 0;
            border-bottom:double;
        }
        .amor{
            border-collapse:collapse;
            border: none;
            width: 95%;
        }
        td.alinea_derecha {
            text-align: right;
            padding: 0;
        }
        .rojo{
            background-color: red;
            color: white;
        }
</style>

<div class="box box-info padding-1">
    <div class="box-body">
        @if ($message = Session::get('success'))
           <div class="alert alert-success">
               <p>{{ $message }}</p>
           </div>
       @endif
</div>

    @if(!isset($retorno) || $retorno==1)
    @if(!is_null($prestamo->cliente_id))
    <a href="{{ route('clientes.edit',$prestamo->cliente_id) }}" ><div class="grid-item2 btn btn-primary">Clientes</div></a>
    @endif
    <a href="{{route('muestraDocumentos',[$prestamo->cliente_id,5,0])}}" ><div class="grid-item4 btn btn-primary">Documentos</div></a>
    @endif
    @if($nuevo==0)
        <a href="{{route('muestraContratos',[$prestamo->cliente_id,$prestamo->id,$prestamo->vehiculo_id,8] )}}"  class="grid-item4 btn btn-primary">Contratos</a>
        @if($prestamo->extension=='N' and $nuevo==0)
        <a href="{{ route('creaprestamoextension',[$prestamo->id,$prestamo->cliente_id,$prestamo->vehiculo_id,$retorno]) }}" class="grid-item4 btn btn-primary float-right"  data-placement="left">
            {{ __('Crear Extensión') }}
        </a>

        @endif

    @endif
    @if($nuevo==0 and $prestamo->tipo==0)
        <a href="{{route('print',[$prestamo->id,$prestamo->vehiculo_id])}}"  class="grid-item2 btn btn-primary" target="_blank">
            Tabla de amortización
        </a>
    @endif
    <span class="card-title">
        {{ __('Cliente: '.$nombre) }} DNI:{{$dni}} Vehículo :{{$marca}}/{{$modelo}} / {{$matricula}}
    </span>

        <p id="cuidado" class="invalid-feedback"></p>
        <table>
            <tr>
                  <td>
                    <div class="form-group">
                    {{ Form::label('Fecha de Firma :') }}
                    {{ Form::date('ffirma', $prestamo->ffirma, ['class' => 'form-control' . ($errors->has('ffirma') ? ' is-invalid' : ''), 'placeholder' => 'Ffirma','id'=>'ffirma']) }}
                    {!! $errors->first('ffirma', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ Form::label('Importe :') }}
                        {{ Form::number('importe', $prestamo->importe, ['class' => 'form-control' . ($errors->has('importe') ? ' is-invalid' : ''), 'placeholder' => 'Importe','id'=>'importe','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any' ]) }}
                        {!! $errors->first('importe', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ Form::label('Gastos :') }}
                        {{ Form::number('gastos', $prestamo->gastos, ['class' => 'form-control' . ($errors->has('gastos') ? ' is-invalid' : ''), 'placeholder' => 'Gastos','id'=>'gastos','pattern'=>'^\d*(\,\d{0,2})?$']) }}
                        {!! $errors->first('gastos', '<div class="invalid-feedback">:message</div>') !!}
                    </div>

                </td>
                <td >
                <div class="form-group">
                        {{ Form::label('Tipo :') }}
                        {{ Form::select('tipo', $tipo,$prestamo->tipo, ['class' => 'form-control' . ($errors->has('tipo') ? ' is-invalid' : ''), 'placeholder' => 'Tipo','id'=>'tipo']) }}
                        {!! $errors->first('tipo', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
                <td id='et4'>
                     <div class="form-group">
                        {{ Form::label('Nro. de cuotas :') }}
                        {{ Form::select('cuotas', $ncuota, $prestamo->cuotas, ['class' => 'form-control' . ($errors->has('cuotas') ? ' is-invalid' : ''), 'placeholder' => 'Cuotas','id'=>'cuotas']) }}
                        {!! $errors->first('cuotas', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="form-group">
                        {{ Form::label('Fecha Inicio de Pago :') }}
                        {{ Form::date('finiciopago', $prestamo->finiciopago, ['class' => 'form-control' . ($errors->has('finiciopago') ? ' is-invalid' : ''), 'placeholder' => 'F. inicio','id'=>'finiciopago']) }}
                        {!! $errors->first('finiciopago', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                    <p id='adv' class="invalid-feedback"></p>

                </td>
                <td>
                    <div class="form-group">
                        {{ Form::label('Total cuota :') }}
                        {{ Form::number('tcuota', $prestamo->tcuota, ['class' => 'form-control' . ($errors->has('tcuota') ? ' is-invalid' : ''), 'placeholder' => 'Tcuota','id'=>'tcuota','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any']) }}
                        {!! $errors->first('tcuota', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                    <p id='adv' class="invalid-feedback"></p>

                </td>
                <td id='et1'>
                    <div class="form-group">
                        {{ Form::label('% Interés :') }}
                        <input type="number" id="mostrar_porcentaje"   class="form-control" step="0.01">
                        {{ Form::hidden('porcentaje', $prestamo->porcentaje, ['class' => 'form-control' . ($errors->has('porcentaje') ? ' is-invalid' : ''), 'placeholder' => 'Porcentaje','min'=> '1.00','max'=>'100.00','id'=>'porcentaje','pattern'=>'^\d*(\.\d{0,2})?$','step'=>'any']) }}

                        {!! $errors->first('porcentaje', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ Form::label('Cuota :') }}
                        {{ Form::number('cuota', $prestamo->cuota, ['class' => 'form-control' . ($errors->has('cuota') ? ' is-invalid' : ''), 'placeholder' => 'Cuota','id'=>'cuota','step'=>'any']) }}
                        {!! $errors->first('cuota', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
                <td>
                    <div class="form-group">
                        {{ Form::label('IVA :') }}
                        {{ Form::number('iva', $prestamo->iva, ['class' => 'form-control' . ($errors->has('iva') ? ' is-invalid' : ''), 'placeholder' => 'Iva','id'=>'iva','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any']) }}
                        {!! $errors->first('iva', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
                </tr>
                <tr>


                <td id='et5'>
                    <div class="form-group" >
                        {{ Form::label('Cuota Residual') }}
                        {{ Form::number('residual', $prestamo->residual, ['class' => 'form-control' . ($errors->has('residual') ? ' is-invalid' : ''), 'placeholder' => 'Residual','id'=>'residual','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any']) }}
                        {!! $errors->first('residual', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
                <td id='et2'>
                    <div class="form-group">
                        {{ Form::label('Importe + Interés + IVA:') }}
                    {{ Form::number('pant1', null, ['class' => 'form-control' . ($errors->has('pant1') ? ' is-invalid' : ''), 'placeholder' => '','id'=>'pant1','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any']) }}
                    </div>
                </td>
                <td id='et3'>
                    <div class="form-group">
                    {{ Form::label('Interés :') }}
                    {{ Form::number('interes', $prestamo->interes, ['class' => 'form-control' . ($errors->has('interes') ? ' is-invalid' : ''), 'placeholder' => 'Interés','id'=>'interes','pattern'=>'^\d*(\,\d{0,2})?$','step'=>'any']) }}
                    {!! $errors->first('interes', '<div class="invalid-feedback">:message</div>') !!}
                </div>
                </td>
                <td colspan="">
                    <div class="form-group">
                        {{ Form::label('Situación :') }}
                        {{ Form::select('condicion_id', $situacion,$prestamo->condicion_id, ['class' => 'form-control' . ($errors->has('condicion_id') ? ' is-invalid' : ''), 'placeholder' => 'Condición del Préstamo','id'=>'condicion_id']) }}
                        {!! $errors->first('condicion_id', '<div class="invalid-feedback">:message</div>') !!}
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="5">
                    <div class="form-group">
                        {{ Form::label('Observaciones :') }}
                        {{ Form::text('observacion', $prestamo->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observaciones']) }}
                        {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
                </div>
                </td>
            </tr>
        </table>
        <div class="form-group">
            <!--{{ Form::label('Frecuencia :') }}-->
            {{ Form::hidden('frecuencia', $prestamo->frecuencia, ['class' => 'form-control' . ($errors->has('frecuencia') ? ' is-invalid' : ''), 'placeholder' => 'Frecuencia','id'=>'frecuencia']) }}
            {!! $errors->first('frecuencia', '<div class="invalid-feedback">:message</div>') !!}

            {{ Form::hidden('extension', $prestamo->extension, ['class' => 'form-control' . ($errors->has('extension') ? ' is-invalid' : ''), 'placeholder' => 'Extension','id'=>'extension']) }}
            {!! $errors->first('extension', '<div class="invalid-feedback">:message</div>') !!}
            {{ Form::hidden('padre', $prestamo->padre, ['class' => 'form-control' . ($errors->has('padre') ? ' is-invalid' : ''), 'placeholder' => 'padre','id'=>'padre']) }}
            {!! $errors->first('padre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
            <!--{{ Form::label('cliente_id') }}-->
            {{ Form::hidden('cliente_id', $prestamo->cliente_id, ['class' => 'form-control' . ($errors->has('cliente_id') ? ' is-invalid' : ''), 'placeholder' => 'Cliente Id']) }}
            {!! $errors->first('cliente_id', '<div class="invalid-feedback">:message</div>') !!}

            {{ Form::hidden('vehiculo_id', $prestamo->vehiculo_id, ['class' => 'form-control' . ($errors->has('vehiculo_id') ? ' is-invalid' : ''), 'placeholder' => 'vehiculo_id']) }}
            {!! $errors->first('vehiculo_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            <!--{{ Form::label('$ IVA') }}-->
            {{ Form::hidden('piva', $prestamo->piva, ['class' => 'form-control' . ($errors->has('piva') ? ' is-invalid' : ''), 'placeholder' => '% IVA','id'=>'piva']) }}
            {!! $errors->first('piva', '<div class="invalid-feedback">:message</div>') !!}
            <input type="hidden" value="<?php echo $retorno;?>" name="retorno">
        </div>
    </div>
@if($nuevo==0 and $prestamo->tipo==0 and $prestamo->porcentaje>0 and $prestamo->importe>0 and $prestamo->cuota>0)
<div id='amortización' >
    @php
    $comp=array(0,0,0);
    echo amortizacion($prestamo,$comp);
    @endphp
</div>
@endif
    <div id='extensiones'>
        @if($nextensiones>0)
            @include('extensione.index')
        @endif
    </div>
    <br>
    <div >
        <button type="submit" id="guardar" class="grid-item1 btn btn-primary" >{{ __('Guardar') }}
        </button>
        @if($prestamo->extension=='S')
            <a href="{{route('editaprestamo',[$prestamo->padre,$retorno])}}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">
                Volver
            </a>
        @else
            @if($retorno==1 or $retorno==2 )
                <a href="{{route('listaprestamoscliente',[$prestamo->cliente_id,$prestamo->vehiculo_id,$retorno])}}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">
                Volver
                </a>
            @elseif($retorno==5)
                <a href="{{route('listaprestamos',[$prestamo->cliente_id,$prestamo->vehiculo_id,4])}}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">
                    Volver
                </a>
            @elseif($retorno==6)
                <a href="{{ route('listaprestamoscliente',[$prestamo->cliente_id,$prestamo->vehiculo_id,1]) }}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">Volver
                </a>
            @elseif($retorno==6)
                <a href="{{ route('listaprestamoscliente',[$prestamo->cliente_id,$prestamo->vehiculo_id,1]) }}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">Volver
                </a>
            @elseif($retorno==7)
                <a href="{{ route('impagos',[0,-1,'*']) }}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">Volver
                </a>
            @else
                <a href="{{route('listaprestamos',[0,0,0])}}"  class="grid-item3 btn btn-primary" style="background-color: yellow;color:black;">
                Volver
                </a>
            @endif
        @endif
    </div>

<style type="text/css">

    table {
        width: 100%;
    }
    .grid-container {
            display: grid;
            grid-template-rows: 60px 60px 60px;
            /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
            grid-template-columns: 120px 120px  120px 120px 120px;
            grid-gap: 15px;
            padding: 10px 10px 10px 10px 10px;

    }
    @media only screen and (max-width:800px) {
          /* For tablets: */
         .grid-container {
          display: grid;
          grid-template-columns: 80px 80px;
        }
    }
    @media only screen and (max-width:560px) {
          /* For mobile phones: */
          .grid-container {
          display: grid;
          grid-template-columns: 80px;

        }
    }
</style>
<script type="text/javascript">

const cuerpoDelDocumento = document.body;
//cuerpoDelDocumento.onload = oculta;
cuerpoDelDocumento.onload = refri;
//cuerpoDelDocumento.onload = calc;


function refri(){
    var tipo=$('#tipo').val();

    if(tipo==0){
        var tcuota = parseFloat($('#tcuota').val());
        var cuotas = $('#cuotas option:selected').val();
        var porcentaje=parseFloat($('#porcentaje').val());
        var pant1=tcuota*cuotas;
        $('#pant1').val(Math.round10(pant1,-2));
        $('#mostrar_porcentaje').val(Math.round10(porcentaje,-2));
    }
    oculta();
}
    function calculadias(){

        var ffirma = $("#ffirma").val();
        finiciapago=$("#finiciopago").val();
        if(ffirma==finiciapago || finiciapago==''){
            return 0;
        }else{
            const fecha = ffirma.split('-');
            var ultimoDia = new Date(parseFloat(fecha[0]), parseFloat(fecha[1]) , 0);

            var fechaInicio = new Date(ffirma).getTime();
            var fechaFin1    = new Date(ultimoDia).getTime();
            var fechaFin2    = new Date(finiciapago).getTime();
            if(fechaFin2>fechaFin1){
                fechaFin=fechaFin1;
            }else{
                fechaFin=fechaFin2;
            }

            //var diff =(fechaFin-fechaInicio)/(1000*60*60*24);
            var diff =(fechaFin-fechaInicio)/(1000*60*60*24);
            //alert(diff.toFixed(0));
            return diff.toFixed(0);
        }

    }

    function formatoFecha(fecha){
        const today = new Date(fecha);
        const yyyy = today.getFullYear();
        let mm = today.getMonth() + 1; // Months start at 0!
        let dd = today.getDate();
        if (dd < 10) dd = '0' + dd;
        if (mm < 10) mm = '0' + mm;
        fecha2=dd+"/"+mm+"/"+yyyy
        return fecha2;
    }
    function calc() {
        var tipo = parseFloat($('#tipo').val());
        //if(tipo !=undefined){
            const tip = [0,1];
            const frec= [52,12,4,2,1];
            const arrcuota=[12,24,0];
            var tcuota=parseFloat($('#tcuota').val());
            var porcen=parseFloat($('#mostrar_porcentaje').val());

            var gastos=parseFloat($('#gastos').val());
            var piva =parseFloat($('#piva').val());
            var capitali = parseFloat($('#importe').val());

            var cuotas = parseFloat($('#cuotas').val());

            var frecuencia   = parseInt($('#frecuencia').val());
            //Cn= C0*(1+i)^n
            var cuota = tcuota/(1+(piva/100));
            var iva = tcuota-cuota;
            var capitalf=cuotas*cuota;
            var interes=capitalf-capitali;
            //calculamos la tasa
            //r = (( VF / VA )^1/n)− 1
            var tasa = ((capitalf/capitali)**(1/cuotas))-1;
            var tiva= iva*cuotas;
            if(tipo==0){

                var residual=0;
                tasa=tasa*100;
                /*
                if(porcen<tasa){
                    tasa=porcen;
                }*/

                capitalf=capitalf+tiva;

            }else{
                var ffirma = $("#ffirma").val();
                const fecha = ffirma.split('-');
                var fechaFin = new Date(parseFloat(fecha[0]), parseFloat(fecha[1]) , 0);
                var ff = fechaFin.toString();
                var fff =ff.split(' ');
                var dias =parseFloat(fff[2]);

                var residual=(tcuota/dias)*(calculadias());
                //var ivaresidual=residual*((piva/100));
                //residual=residual+ivaresidual;
                if(residual>tcuota)
                    residual=tcuota;
                interes=0;//((cuotas-1)*cuota)+ivaresidual;
                capitalf=capitali+interes+tiva;
                tasa=0;

                oculta();

            }

            $('#importe').val(Math.round10(capitali,-2));
            $('#gastos').val(Math.round10(gastos,-2));
            $('#tcuota').val(Math.round10(tcuota,-0));
            $('#iva').val(Math.round10(iva,-2));
            $('#cuota').val(Math.round10(cuota,-2));
            $('#residual').val(Math.round10(residual,-2));
            if(isNaN(tasa))
                tasa=0;
            $('#porcentaje').val(Math.round10(tasa,-7));
            $('#mostrar_porcentaje').val(Math.round10(tasa,-2));

            //$('#pant1').val(capitalf.toFixed(2));
            $('#pant1').val(Math.round10(capitalf,-2));
            //$('#pant1').val((capitalf));
            //let text=interes.totring('es-ES');
            $('#interes').val(Math.round10(interes,-2));
            if(tipo==0){
                $('#et5').hide();
                presenta_amortizacion();
            }else{
                $('#et5').show();
            }
        //}
    }
    function presenta_amortizacion(){

        $("#amortizacion").show();

    }
    function oculta(){
        var tipo=$('#tipo').val();

        if(tipo==0){
            $('#et1').show();
            $('#et2').show();
            $('#et3').show();
            $('#et4').show();
            $('#et5').hide();
            //$('#et6').show();
        }else{
            //$('#porcentaje').hide();
            $('#et1').hide();
            $('#et2').hide();
            $('#et3').hide();
            $('#et4').hide();
            $('#et5').show();
            //$('#et6').hide();
        }
    }
    $('body').on('change', '#tipo', function () {
        oculta();
        return calc();
    });

    $('body').on('change', '#ffirma', function () {
        //calculadias();
        return calc();
    });

    $('body').on('change', '#importe', function () {
        return calc();
    });
    $('body').on('change', '#frecuencia', function () {
        return calc();
    });

    $('body').on('change', '#cuotas', function () {
        return calc();
    });

    $('body').on('change', '#tcuota', function () {
        return calc();
    });


     $('body').on('change', '#mostrar_porcentaje', function () {
        return calc();
    });


/**
   * Ajuste decimal de un número.
   *
   * @param {String}  tipo  El tipo de ajuste.
   * @param {Number}  valor El numero.
   * @param {Integer} exp   El exponente (el logaritmo 10 del ajuste base).
   * @returns {Number} El valor ajustado.
   */
  function decimalAdjust(type, value, exp) {
    // Si el exp no está definido o es cero...
    if (typeof exp === 'undefined' || +exp === 0) {
      return Math[type](value);
    }
    value = +value;
    exp = +exp;
    // Si el valor no es un número o el exp no es un entero...
    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
      return NaN;
    }
    // Shift
    //value=value.tasa.toLocaleString('es-ES');
    value = value.toString().split('e');
    value = Math[type](+(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp)));
    // Shift back

    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp));
  }

  // Decimal round
  if (!Math.round10) {
    Math.round10 = function(value, exp) {
      return decimalAdjust('round', value, exp);
    };
  }
  // Decimal floor
  if (!Math.floor10) {
    Math.floor10 = function(value, exp) {
      return decimalAdjust('floor', value, exp);
    };
  }
  // Decimal ceil
  if (!Math.ceil10) {
    Math.ceil10 = function(value, exp) {
      return decimalAdjust('ceil', value, exp);
    };
  }

function number_format(amount, decimals) {

amount += ''; // por si pasan un numero en vez de un string
amount = parseFloat(amount.replace(/[^0-9\.]/g, '')); // elimino cualquier cosa que no sea numero o punto

decimals = decimals || 0; // por si la variable no fue fue pasada

// si no es un numero o es igual a cero retorno el mismo cero
if (isNaN(amount) || amount === 0)
    return parseFloat(0).toFixed(decimals);

// si es mayor o menor que cero retorno el valor formateado como numero
amount = '' + amount.toFixed(decimals);

var amount_parts = amount.split('.'),
    regexp = /(\d+)(\d{3})/;

while (regexp.test(amount_parts[0]))
    amount_parts[0] = amount_parts[0].replace(regexp, '$1' + ',' + '$2');

return amount_parts.join('.');
}

var formatNumber = {
 separador: ".", // separador para los miles
 sepDecimal: ',', // separador para los decimales
 formatear:function (num){
 num +='';
 var splitStr = num.split('.');
 var splitLeft = splitStr[0];
 var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : '';
 var regx = /(\d+)(\d{3})/;
 while (regx.test(splitLeft)) {
 splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2');
 }
 return this.simbol + splitLeft +splitRight;
 },
 nueva:function(num, simbol){
 this.simbol = simbol ||'';
 return this.formatear(num);
 }
}
//chequea que se guarde antes de abandonar el módulo

    $(document).ready(function(){
        $("form").submit(function(){
            $('#guardar').prop('disabled',true);
        });
    });

    var modified;
    function salir(){
        window.removeEventListener('beforeunload', chequea);
    }

    window.addEventListener('beforeunload', chequea);
    function chequea(e) {

        if(modified) {
            //return false;
            e.preventDefault();
            e.returnValue = '';
        }
    }


    $(document).ready(function (){
        $("input, select").change(function () {
            modified = true;
        });
        $("input, select").keyup(function () {
            modified = true;
        });

    });
</script>
<script src="{{asset('modal/assets/jquery-1.12.4-jquery.min.js')}}"></script>
<script src="{{asset('modal/assets/jquery.validate.min.js')}}"></script>
<script src="{{asset('modal/assets/ValidarRegistro.js')}}"></script>
<script src="{{asset('modal/dist/js/bootstrap.min.js')}}"></script>
