@extends('layouts.app')

@section('template_title')
    Marca
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('Marcas') }}
                            </span>
                            <a href="{{route('home')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>

                             <div class="float-right">
                                <a href="{{ route('marcas.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nueva Marca') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! $marcas->links() !!}
                        <div class="table-responsive">
                            <form action="{{route('marcas.index')}}" method="GET">
                                <input type="text" name="texto" class="form-label" style="width:200px;">
                                <button type="submit" class="btn btn-primary">Buscar</button>
                            </form>
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>Núm</th>
										<th>Marcas</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($marcas as $marca)
                                        <tr>
                                            <td>{{ ++$i }}</td>

											<td>{{ $marca->nombre }}</td>

                                            <td>
                                                <form action="{{ route('marcas.destroy',$marca->id) }}" method="POST" id='borrar'>
                                                    <a class="btn btn-sm btn-success" href="{{ route('marcas.edit',$marca->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    {{-- <a class="btn btn-sm btn-success" href="{{ route('muestraModelos',$marca->id) }}" style="background-color: silver; color:black;"><i class="fa fa-fw fa-edit" ></i> {{ __('Modelos') }}</a> --}}
                                                    <a onclick="aviso({{$marca->id}});" class="btn btn-danger btn-sm btnn"><i class="fa fa-fw fa-trash">{{ __('Borrar') }}</i> </a>
                                                    @csrf
                                                    @method('DELETE')
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $marcas->links() !!}
            </div>
        </div>
    </div>
@endsection
<script type="text/javascript">
    function aviso(id){

        var resp=confirm("¿Está seguro de eliminar esta Marca?");
        if(!resp){
            event.preventDefault();
            event.returnValue = '';
        }else{
            let url ="<?php echo env('APP_URL'); ?>";

            url=url +'/marcas/'+id;
            document.getElementById("borrar").action = url;
            document.getElementById("borrar").submit();
        }
    }
</script>
