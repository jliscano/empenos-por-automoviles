<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Documento de Entrega</title>
<style type="text/css">
        .pequena{
            font-size: 10px;
        }
        .cabecera{
            width: 100%;
        }
        .cabecera tr{
            width: 100%;
            text-align: center;
        }
        .basico {
            width: 100%;
            border: 2px solid blue;
            padding: 10px;
            border-radius: 25px;
        }
        table.tabla_sin {
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        th.borde-doble{
            text-align: right;
            padding: 0;
            border-bottom:double;
        }
        .amor{
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        td.alinea_derecha {
            text-align: right;
            padding: 0;
        }

        div{
            font-size: 18px;
        }
        div.page_break + div.page_break{
            page-break-before: always;
        }
/** Define the margins of your page **/
@page {
    margin: 160px 110px;
    margin-left: 140px;
    margin-bottom: 80px;
}

header {
position: fixed;
top: -60px;
left: 0px;
right: 0px;

/** Extra personal styles **/

text-align: left;
line-height: 35px;
}

footer {
position: fixed;
bottom: -40px;
left: 0px;
right: 0px;
height: 50px;

/** Extra personal styles **/

line-height: 35px;

}

</style>
</head>
<body>

 <header>
    <img src="{{ asset('images/logo2.png') }}" width="90" height="60">
</header>

<main>
    @foreach ($clientes as $cliente)
        <div align="justify" >
            De una parte, Don. {{($cliente->replegal)}}, mayor de edad, con D.N.I. número {{($cliente->dnireplegal)}}, actúa en representación de la empresa {{($cliente->emp)}}, con domicilio en {{($cliente->domicilio)}},  con C.I.F: nº {{($cliente->cif)}}, en su condición de administrador único y
        </div>
        @php
    if($cliente->sexo=='M')
        $don="Don";
    else
        $don="Doña";
    if($cliente->sexo2=='M')
        $don2="Don";
    else
        $don2="Doña";
        $pnotarial='';
    if($cliente->pnotarial!='')
        $pnotarial=' bajo poder notarial ' .$cliente->pnotarial;

        $nombre2='';$actua='actúa';$hace='hace';$nombre3='';
        $coletilla_empresa=' actúa en su propio nombre y representación.';
            if(!is_null($cliente->proempresa))
            $coletilla_empresa=' quien actúa en nombre y representación de '. $cliente->proempresa.' '.$cliente->dnicif.'.'.
        ' en su calidad de Administrador único '.$pnotarial.'.';

        if($cliente->nombre2!='' and !is_null($cliente->nombre2)){
            $coletilla_empresa=' actúan en su propio nombre y representación.';
            if(!is_null($cliente->proempresa))
            $coletilla_empresa=' quienes actúan en nombre y representación de '. $cliente->proempresa.' '.$cliente->dnicif.'.'.
            ' en su calidad de Administradores '.$pnotarial.'.';
            $nombre2=' y '.$don2.' '.$cliente->nombre2.', mayor de edad, con D.N.I número '. $cliente->dni2;
            $nombre3 =' y '.$don2.' '.$cliente->nombre2;
            $actua='actúan';$hace='hacen';
        }
    @endphp
        <div align="justify" >
            de otra parte, {{$don}} {{(($cliente->nom))}}. mayor de edad, con D.N.I número {{(($cliente->dni))}}{{$nombre2}}, con domicilio en {{($cliente->direccion)}} {{$cliente->loc}} ({{($cliente->prov)}}),
            {{$coletilla_empresa}}
            <br><br>
            En este acto {{$don}} {{(($cliente->nom))}}{{$nombre3}} {{$hace}} entrega del vehículo Marca:{{$cliente->mar}}/{{$cliente->mod}} con Matrícula:{{(($cliente->matricula))}}, a la empresa {{(($cliente->emp))}}.
        </div>

        <br><br><br>


        <table style="width: 100%;">
        <tr>
            <td align="left" valign="bottom">
                <br><br>
                Fdo.:
                @if($cliente->proempresa!='')
                    Por:{{$cliente->proempresa}}<br>
                @endif
                    {{$cliente->nom}}
            </td>
            <td align="right" valign="bottom">
                <img src="{{ asset('images/emp'.$cliente->empresa_id.'.png') }}" width="120" height="80">
                <br>
                Fdo.: {{($cliente->replegal)}}
            </td>
        </tr>
        </table>
        <br>
        <br>
        <br>
        <br>
        <div style="color:blue">
            Teléfono de Contacto: 622 161 442<br>
            www.confiacar.es
            </div>
@endforeach
<footer>
    <script type="text/php">
        if ( isset($pdf) ) {
            // v.0.7.0 and greater
            $pdf->page_script('

            $x = 462;
            $y = 745;

            $text = "Pág :{PAGE_NUM} de {PAGE_COUNT}" ;
            $font = $fontMetrics->get_font("helvetica", "normal");
            $size = 12;
            $color = array(0,0,0);
            $word_space = 0.5;  //  default
            $char_space = 0.3;  //  default
            $angle = 0.0;   //  default
            $pdf->page_text($x, $y, $text, $font, $size, $color, $word_space, $char_space, $angle);
            ');
        }
    </script>

</footer>
</main>
</body>
</html>
