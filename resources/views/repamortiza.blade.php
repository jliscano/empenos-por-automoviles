<<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Amortización</title>
<style type="text/css">
        .totales{
            border-top: 2px solid blue;
            font-weight: bold;
            text-align: right;
        }
        .cabecera{
            width: 100%;
        }
        .cabecera tr{
            width: 100%;
            text-align: center;
        }
        .basico {
            width: 100%;
            border: 2px solid blue;
            padding: 0px;
            border-radius: 12px;
            margin: 5px;
            line-height: 20px;
        }
        
        table.tabla_sin {
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        th.borde-doble{
            text-align: right;
            padding: 0;
            border-bottom:double;
        }
        .amor{
            border-collapse:collapse;
            border: none;
            width: 100%;
        }
        td.alinea_derecha {
            text-align: right;
            padding: 0;
        }
        .rojo{
            background-color: red;
            color: white;
        }

        .page-break {
            page-break-after: always;
        }
/** Define the margins of your page **/
@page {
    margin: 260px 25px;
    margin-left: 50px;
    margin-bottom: 60px;
}

header {
position: fixed;
top: -210px;
left: 0px;
right: 0px;
/*height: 50px;*/

/** Extra personal styles **/

text-align: left;
line-height: 35px;
}

footer {
position: fixed;
bottom: -60px;
left: 0px;
right: 0px;
height: 50px;

/** Extra personal styles **/
/*background-color: #03a9f4;
color: white;
text-align: center;
*/
line-height: 35px;

}

</style>
</head>
<body>
   
    
@foreach ($clientes as $cliente)
@php
$extensiones=array();
$mtotal_extensiones=buscaextensiones($cliente->pre,$extensiones); //monto total en euros de extensiones    
@endphp
    <?php
        $cuotas=$cliente->cuotas;
        $periodo=$cliente->ffirma;
        $capital_pagado=0;
        $capital_resta=$cliente->importe;
    ?>
 <header>
    <img src="{{ asset('images/logo2.png') }}" width="80" height="60">
        <table class="cabecera">
            <tr>
                <th>
                    Tabla de Amortización
                </th>
            </tr>
        </table>
    <table class="basico">
    <tr>
        <td>
            {{'Cliente :'}}
        </td>

        <td>
            {{('DNI :' ) }}
        </td>
        <td>
            {{ ('Tipo de Préstamo :')}} 
        </td>
        <td>
            {{ ('Importe :')}} 
        </td>
        <td>
            {{ ('Extendido a :')}} 
        </td>
    </tr>
    
    <tr>
        <td>
            {{$cliente->nombre }}
        </td>
        <td>
            {{$cliente->dni}}
            
        </td>
        <td>
            {{ ($tipo[$cliente->tipo] )}}
        </td>
        <td>
            {{ number_format( $cliente->importe,2,'.',',')}} 
        </td>
        <td>
            @if ($mtotal_extensiones==0)
                {{'Sin Extensión'}}
            @else
            {{number_format( $mtotal_extensiones,2,'.',',')}}
            @endif
            
            
        </td>
    </tr>
    </table>
    
</header>
    <?php 
    $comp=array(0,0,0);
    echo amortizacion($cliente,$comp);
    ?>
@endforeach
</body>
</html>