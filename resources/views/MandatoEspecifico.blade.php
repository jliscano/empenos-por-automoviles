<<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Mandato Específico</title>
<style type="text/css">
        .pequena{
            font-size: 10px;
        }

        div{
            font-size: 13.4px;
        }
        div.page_break + div.page_break{
            page-break-before: always;
        }

/** Define the margins of your page **/
@page {
    margin: 50px 50px;
    margin-left: 60px;
    margin-bottom: 60px;
}

header {
position: fixed;
top: -60px;
left: 0px;
right: 0px;
/*height: 50px;*/

/** Extra personal styles **/

text-align: left;
line-height: 35px;

}

footer {
position: fixed;
bottom: -40px;
left: 0px;
right: 0px;
height: 50px;

/** Extra personal styles **/
/*background-color: #03a9f4;
color: white;
text-align: center;
*/
line-height: 35px;

}

</style>
</head>
<body>
<header>

</header>
<main>
    <div align="center">
        <img src="{{ asset('images/especifico.png') }}" width="240" height="60">
    </div>
    <br>
    @foreach ($clientes as $cliente)
    <?php
    $dias=date('d',strtotime($cliente->ffirma));
    $mes=date('m',strtotime($cliente->ffirma));
    $nano=date('Y',strtotime($cliente->ffirma));
    $entero=intval($cliente->valoracion);
    $cent = $cliente->valoracion-$entero;
    $nombre2='******';
    $dni2='******';
    if($cliente->nombre2!=''){
        $nombre2=$cliente->nombre2;
        $dni2=$cliente->dni2;
    }

    ?>
    <div align="justify">
    @if($cliente->tipo_tutor=='--')
        @if($cliente->proempresa=='')
        D.<u>{{ $cliente->nom }}</u>
         con DNI <u>{{ $cliente->dni }}</u> y D.<u>{{ $nombre2 }}</u>
         con DNI <u>{{ $dni2 }}</u>, que declara/declaran tener poder
         suficiente para actuar en su propio nombre y/o en representación de
         <u>******</u> con DNI/CIF nº<u>{{ $cliente->dnicif }}</u>
         y domicilio a efectos de notificaciones en <u>{{ $cliente->prov}},
        {{$cliente->mun}}</u>, <u>{{ $cliente->direccion }}</u>, C.P.<u>{{ $cliente->codpostal }}</u>,
         en concepto de MANDANTE, dice y otorga:
        @else
        D.<u>{{ $cliente->nom }}</u>
         con DNI <u>{{ $cliente->dni }}</u> y D.<u>{{ $nombre2 }}</u>
         con DNI <u>{{ $dni2 }}</u>, que declara/declaran tener poder
         suficiente para actuar en su propio nombre y/o en representación de
         <u>{{ $cliente->proempresa }}</u> con DNI/CIF nº<u>{{ $cliente->dnicif }}</u>
         y domicilio a efectos de notificaciones en <u>{{ $cliente->prov}},
        {{$cliente->mun}}</u>, <u>{{ $cliente->direccion }}</u>, C.P.<u>{{ $cliente->codpostal }}</u>
         en concepto de MANDANTE, dice y otorga:
        @endif
    @else
        D.<u>{{ $cliente->nombre_tutor }}</u>
        con DNI <u>{{ $cliente->dni_tutor }}</u> y D.<u>******</u>
        con DNI <u>******</u>, que declara/declaran tener poder
        suficiente para actuar en su propio nombre y/o en representación de
        <u>{{ $cliente->nombre }}</u> con DNI/CIF nº<u>{{ $cliente->dni }}</u>
        y domicilio a efectos de notificaciones en <u>{{ $cliente->prov}},
        {{$cliente->mun}}</u>, <u>{{ $cliente->direccion }}</u>, C.P.<u>{{ $cliente->codpostal }}</u>
        en concepto de MANDANTE, dice y otorga:
    @endif

    </div>
    <br>
    <div align="justify">
        Que por el presente documento confiere, con carácter específico, MANDATO CON REPRESENTACIÓN a favor de D. <u>MIGUEL ÁNGEL GARCÍA LÓPEZ</u>, con DNI<u> 52811400L  </u>, Gestor Administrativo en ejercicio, colegiado número <u>  124  </u>, perteneciente al Colegio Oficial de Gestores Administrativos de <u>   Murcia   </u>, con domicilio en <u>CARAVACA DE LA CRUZ </u>, calle <u>   AVDA DE ALMERÍA  </u> nº <u> 15 </u> C.P. <u>30400</u>, en concepto  de  MANDATARIO, para  su  actuación  ante  todos los órganos y  entidades de la Administración del Estado, Autonómica,  Provincial  y Local que resulten  competentes, y  específicamente  ante la Dirección  General  de Tráfico  del  Ministerio del Interior  del  Gobierno de España, para que promueva, solicite y realice todos los trámites necesarios en relación con el siguiente ASUNTO:
    </div>
    <br>
    <div align="justify">
        . Transferir vehículo {{$cliente->mar}}  {{$cliente->mod}} con Matrícula {{$cliente->matricula}}
    </div>
    <br>
    <div align="justify">
        El presente mandato, que se regirá por los artículos 1709 a 1739 del Código Civil, se confiere al amparo del artículo 5 de la Ley 39/2015, de 1 de octubre, del Procedimiento Administrativo Común de las Administraciones Públicas, y del artículo 1 del Estatuto Orgánico de la Profesión de Gestor Administrativo, aprobado por Decreto 424/1963.
    </div>
    <br>
    <div align="justify">
        El mandante autoriza al mandatario para que nombre sustituto, en caso de necesidad justificada, a favor de un Gestor Administrativo
        colegiado ejerciente. El presente mandato mantendrá su vigencia, como máximo, hasta la finalización del encargo aquí encomendado,
        siempre y cuando no sea expresamente revocado por el mandante con anterioridad, y comunicada fehacientemente su revocación al
        mandatario. En caso de fallecimiento, jubilación, o cese de negocio del mandatario, o cualquier otra causa que impida la terminación del
        mandato, el mandante autoriza de forma expresa que el trámite encomendado sea finalizado por el gestor administrativo que le sustituya oficialmente.
    </div>
    <br>
    <div align="justify">
        El mandante declara bajo su responsabilidad de conformidad con el artículo 69 de la Ley 39/2015, de 1 de octubre, del Procedimiento Administrativo Común de las Administraciones Públicas, que cumple con los requisitos establecidos en la normativa vigente para obtener el reconocimiento de un derecho o facultad o para su ejercicio, que dispone de la documentación que así lo acredita, que es auténtica y su contenido enteramente correcto, y que entrega al gestor Administrativo, el cual se responsabiliza de su custodia, se compromete a ponerla a disposición de la Administración cuando le sea requerida, y a mantener el cumplimiento de las anteriores obligaciones durante el período de tiempo inherente al trámite conferido.
    </div>
    <br>
    <div align="justify">
        El mandante declara, que conoce y consiente que los datos que suministra pueden incorporarse a ficheros automatizados de los que serán responsables el Gestor Administrativo al que se le otorga el mandato, el Colegio Oficial de Gestores Administrativos citado, y el Consejo General de Colegios de Gestores Administrativos de España, con el único objeto y plazo de posibilitar la prestación de los servicios profesionales objeto del presente mandato y el cumplimiento por estos de las obligaciones derivadas del trámite encomendado. No obstante lo anterior, el mandatario se reserva el derecho de custodia y conservación de los datos personales recabados con fines de cumplimiento de obligaciones legales exigidas por la normativa tributaria, laboral, civil o mercantil, así como para la atención o emprendimiento de reclamaciones y/o acciones judiciales. El mandante tendrá derecho a la portabilidad de sus datos, a su acceso, rectificación, supresión, limitación, y oposición, así como a interponer las reclamaciones que estime oportunas ante la Agencia Española de Protección de Datos, o su equivalente en su país de residencia como Autoridad de Control, en los términos previstos en la Ley de Protección de Datos de Carácter personal, y el Reglamento (UE) 2016/679 del Parlamento Europeo y del Consejo de 27 de abril de 2016.
    </div>
    <br>
    <div align="center">
        <b>*</b>
        A <u>{{($dias)}}</u>  de <u>{{($mes)}}</u> de <u>{{($nano)}}</u>
        <br>
        EL MANDANTE
    </div>
    <br>
    <div align="justify">
        El mandatario acepta el mandato conferido y se obliga a cumplirlo de conformidad con las instrucciones del mandante, y declara bajo su responsabilidad que los documentos recibidos del mandante han sido verificados en cuanto a la corrección formal de los datos contenidos en los mismos.
    </div>
    <div align="center">
        <b>*</b>
        A <u>{{($dias)}}</u>  de <u>{{($mes)}}</u> de <u>{{($nano)}}</u>
        <br>
        EL MANDATARIO
    </div>
@endforeach
</main>
</body>
</html>
