@extends('layouts.app')

@section('template_title')
    ITV
@endsection

@section('content')
<style type="text/css">
    .vencido{
        color: white;
        background-color: black;
    }
</style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">

                            <span id="card_title">
                                {{ __('ITV vencidos o por vencer') }}
                            </span>
                            <a href="{{route('itv.index')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>


                             
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <div class="card-body">
                        {!! $clientes->links() !!}
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
                                        
										<th>Nombre</th>
										<th>DNI</th>
										<th>Matrícula</th>
										<th>Marca</th>
										<th>Modelo</th>
										<th>Fecha ITV</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach ($clientes as $cliente)
                                <?php
                                if(
                                    date('Y-m-d',strtotime($cliente->itv))<date('Y-m-d' )
                                )
                                    $clase='class="vencido"';
                                else
                                    $clase=''
                                ?>
                                <tr>
                                    <td>
                                    {{ ++$i }}
                                    </td>

                                    <td>
                                        {{$cliente->nombre}}
                                    </td>
                                    <td>
                                        {{$cliente->dni}}
                                    </td>
                                    <td>
                                        {{$cliente->matricula}}
                                    </td>
                                    <td>
                                        {{$cliente->mar}}
                                    </td>
                                    <td>
                                        {{$cliente->mod}}
                                    </td>
                                    <td >
                                        <div <?php echo $clase;?> align='right'>
                                            @if(is_null($cliente->itv))
                                                --/--/----
                                            @else
                                                {{date('d/m/Y',strtotime($cliente->itv))}}
                                            @endif
                                        </div>
                                    </td>
                                    <td>
                                        <a class="btn btn-sm btn-primary " href="{{ route('muestraCobranza',[$cliente->idcliente,2] ) }}"><i class="fa fa-fw fa-eye"></i> {{ __('Contactar') }}</a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $clientes->links() !!}
            </div>
        </div>
    </div>
@endsection
