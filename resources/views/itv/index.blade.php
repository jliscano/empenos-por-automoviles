@extends('layouts.app')

@section('template_title')
    Busqueda de ITV por vencer
@endsection

<style type="text/css">
    .ui-datepicker-calendar {
        display: none;
    }
</style>

@section('content')
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('datepicker/jquery-ui.css')}}"> 
    <script src="{{asset('datepicker/external/jquery/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('datepicker/jquery-ui.min.js')}}"></script>
</head>
<div class="container-fluid" >
    <div class="row" align="center" style="text-align: center;">
        <form id="myForm" method="POST" action="{{ route('muestraItv') }}"  role="form" enctype="multipart/form-data">
        @csrf

        <div class="col-sm-12">
            @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
            Busqueda de ITV por Vencer/Vencidos
            <div class="form-group">
                <table align="center">
                    <tr>
                        <td>
                            <label for="startDate">Período :</label>
                        <br>
                        <input name="startDate" id="startDate" class="date-picker" style="width:100px"  autocomplete="off"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <br>
                            <a onclick="return ver()">
                            <div class="btn btn-primary">Ver
                            </div>
                        </a>
                        <a href="{{route('home')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                        </td>
                    </tr>
                </table>
                
            </div>
            <br>
        </div>

        
    </form>
    </div>
</div>
<script type="text/javascript">
$(function() {
            $('#startDate').datepicker( {
                monthNames: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            monthNamesShort :[ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'mm/yy',
            closeText: 'Aceptar',
            currentText: 'Hoy',
            viewMode: 'years',
            showOn: 'button',
            onClose: function(dateText, inst) { 
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
            });
        });

function ver(){
    if($('#startDate').val()==''){
        alert("debe elegir un período mes/año");
        return null;
    }else{
        document.getElementById("myForm").submit();
    }
}
</script>
@endsection
