@extends('layouts.app')

@section('template_title')
    Ingreso
@endsection

@section('content')
<head>
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('datepicker/jquery-ui.css')}}"> 
    <script src="{{asset('datepicker/external/jquery/jquery.js')}}"></script>
    <script type="text/javascript" src="{{asset('datepicker/jquery-ui.min.js')}}"></script>
</head>
<style type="text/css">
    .ui-datepicker-calendar {
        display: none;
    }
</style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <div style="display: flex; justify-content: space-between; align-items: center;">
                            <span id="card_title">
                                {{ __('Ingresos') }}
                            </span>
                            <a href="{{route('home')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
                             <div class="float-right">
                                <a href="{{ route('ingresos.create') }}" class="btn btn-primary btn-sm float-right"  data-placement="left">
                                  {{ __('Nuevo Ingreso') }}
                                </a>
                              </div>
                        </div>
                    </div>
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif
                    <div class="card-body">
                        {!! $ingresos->links() !!}
                        <form action="{{route('ingresos.index')}}" method="GET">
                            <table>
                                <tr>
                                    <td>
                                        <label for="startDate">Período :</label><br>
                                        <input name="startDate" id="startDate" style="width: 120px;display: inline;" class="date-picker" autocomplete="off" readonly />
                                    </td>
                                    <td>
                                        <label for="startDate">Clase :</label><br>
                                        {{ Form::select('idclase',$clasificados, '', ['class' => 'form-control date-picker  ' . ($errors->has('sexo') ? ' is-invalid' : ''), 'placeholder' => 'Asigne una clase','id' =>'clase']) }}
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-primary" style="display: block;">Buscar</button>
                                    </td>
                                </tr>
                            </table>
                        </form>
                        <div class="table-responsive">
                            <table class="table table-striped table-hover">
                                <thead class="thead">
                                    <tr>
                                        <th>No</th>
										<th>Monto</th>
										<th>Referencia</th>
										<th>Fecha</th>
                                        <th>Clase</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($ingresos as $ingreso)
                                        <tr>
                                            <td>{{ ++$i }}</td>
											<td>{{ $ingreso->monto }}</td>
											<td>{{ $ingreso->referencia }}</td>
											<td>{{ date('d/m/Y',strtotime($ingreso->fingreso)) }}</td>
                                            <td>{{ $ingreso->clas }}</td>
                                            <td>
                                                <form action="{{ route('ingresos.destroy',$ingreso->id) }}" method="POST">
                                                    <a class="btn btn-sm btn-success" href="{{ route('ingresos.edit',$ingreso->id) }}"><i class="fa fa-fw fa-edit"></i> {{ __('Editar') }}</a>
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-fw fa-trash"></i> {{ __('Borrar') }}</button>
                                                </form>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                {!! $ingresos->links() !!}
            </div>
        </div>
    </div>
<script type="text/javascript">
    $(document).ready(function() {
        $('#startDate').datepicker({
            monthNames: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            monthNamesShort :[ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic" ],
            changeMonth: true,
            changeYear: true,
            showButtonPanel: true,
            dateFormat: 'mm/yy',
            closeText: 'Aceptar',
            currentText: 'Hoy',
            viewMode: 'months',
            showOn: 'button',
            onClose: function(dateText, inst) { 
                $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
            }
        });
    });
</script>
@endsection
