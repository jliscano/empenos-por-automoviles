<style type="text/css">
        .verde {
            border-radius: 25px;
            background-color: green;
            color: white;
            margin: 10px;
            width: 160px;
            height: 80px;
            padding: 15px 15px 15px 15px;
        }

    .grid-container {
            display: grid;
            grid-template-rows: 80px 80px;
            /*grid-template-columns: 1fr 1fr 1fr 1fr;*/
            grid-template-columns: 180px 180px  180px 180px;
            grid-gap: 15px;
            padding: 15px 15px 15px 15px;

    }
    @media only screen and (max-width:800px) {
      /* For tablets: */
     .grid-container {
      display: grid;
      grid-template-columns: 180px 180px;
    }
    }
    @media only screen and (max-width:560px) {
      /* For mobile phones: */
      .grid-container {
      display: grid;
      grid-template-columns: 180px;
      
    }
    }
    </style>
<div class="box box-info padding-1">
    <div class="box-body">
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    @if ($message = Session::get('warmning'))
        <div class="alert alert-warning">
            <p>{{ $message }}</p>
        </div>
    @endif

    <div class="grid-container">
    <div class="grid-item1">
        <div class="form-group">
            {{ Form::label('Empresa :') }}
            {{ Form::select('empresa_id', $empresa ,$ingreso->empresa_id, ['class' => 'form-control' . ($errors->has('empresa_id') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione una Empresa']) }}
            {!! $errors->first('empresa_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item1">
        <div class="form-group">
            {{ Form::label('Clase de Gasto :') }}
            {{ Form::select('clasificado_id', $clasificado,$ingreso->clasificado_id, ['class' => 'form-control' . ($errors->has('clasificado_id') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione una clasificación']) }}
            {!! $errors->first('clasificado_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item2">
        <div class="form-group">
            {{ Form::label('Fecha :') }}
            {{ Form::date('fingreso', $ingreso->fingreso, ['class' => 'form-control' . ($errors->has('fgasto') ? ' is-invalid' : ''), 'placeholder' => 'fingreso']) }}
            {!! $errors->first('fingreso', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item3">
        <div class="form-group">
            {{ Form::label('Monto :') }}
            {{ Form::text('monto', $ingreso->monto, ['class' => 'form-control' . ($errors->has('monto') ? ' is-invalid' : ''), 'placeholder' => 'Monto']) }}
            {!! $errors->first('monto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item4">
        <div class="form-group">
            {{ Form::label('Referencia/Factura :') }}
            {{ Form::text('referencia', $ingreso->referencia, ['class' => 'form-control' . ($errors->has('referencia') ? ' is-invalid' : ''), 'placeholder' => 'Escriba el número de Referencia']) }}
            {!! $errors->first('referencia', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>    
    <div class="grid-item5">
        <div class="form-group">
            {{ Form::label('Pagado :') }}
            {{ Form::select('conciliado', $sino,$ingreso->conciliado, ['class' => 'form-control' . ($errors->has('conciliado') ? ' is-invalid' : ''), 'placeholder' => 'Seleccione un valor']) }}
            {!! $errors->first('conciliado', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="grid-item6">
        <div class="form-group">
            {{ Form::label('Observacion :') }}
            {{ Form::text('observacion', $ingreso->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observacion...','style'=>'width:400px;']) }}
            {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    </div>

    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        <a href="{{route('ingresos.index')}}"  ><div class="grid-item5 btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    </div>
    @if($nuevo==0)
    <!--
    <iframe id="gxv" src="{{route('muestraGastosVehiculos',[$ingreso->id,$ingreso->empresa_id])}}" width="100%" height="600"></iframe>
    -->
    <iframe id="gxv" src="{{route('muestraIngresosVehiculos',[$ingreso->id,$ingreso->empresa_id])}}" width="100%" height="600"></iframe>
    @endif

</div>


<!--
<div class="box box-info padding-1">
    <div class="box-body">
        
        <div class="form-group">
            {{ Form::label('empresa_id') }}
            {{ Form::text('empresa_id', $ingreso->empresa_id, ['class' => 'form-control' . ($errors->has('empresa_id') ? ' is-invalid' : ''), 'placeholder' => 'Empresa Id']) }}
            {!! $errors->first('empresa_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('clasificado_id') }}
            {{ Form::text('clasificado_id', $ingreso->clasificado_id, ['class' => 'form-control' . ($errors->has('clasificado_id') ? ' is-invalid' : ''), 'placeholder' => 'Clasificado Id']) }}
            {!! $errors->first('clasificado_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('monto') }}
            {{ Form::text('monto', $ingreso->monto, ['class' => 'form-control' . ($errors->has('monto') ? ' is-invalid' : ''), 'placeholder' => 'Monto']) }}
            {!! $errors->first('monto', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('referencia') }}
            {{ Form::text('referencia', $ingreso->referencia, ['class' => 'form-control' . ($errors->has('referencia') ? ' is-invalid' : ''), 'placeholder' => 'Referencia']) }}
            {!! $errors->first('referencia', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('fingreso') }}
            {{ Form::text('fingreso', $ingreso->fingreso, ['class' => 'form-control' . ($errors->has('fingreso') ? ' is-invalid' : ''), 'placeholder' => 'Fingreso']) }}
            {!! $errors->first('fingreso', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('observacion') }}
            {{ Form::text('observacion', $ingreso->observacion, ['class' => 'form-control' . ($errors->has('observacion') ? ' is-invalid' : ''), 'placeholder' => 'Observacion']) }}
            {!! $errors->first('observacion', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('conciliado') }}
            {{ Form::text('conciliado', $ingreso->conciliado, ['class' => 'form-control' . ($errors->has('conciliado') ? ' is-invalid' : ''), 'placeholder' => 'Conciliado']) }}
            {!! $errors->first('conciliado', '<div class="invalid-feedback">:message</div>') !!}
        </div>

    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Submit') }}</button>
    </div>
</div>
-->