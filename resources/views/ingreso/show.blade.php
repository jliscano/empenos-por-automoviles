@extends('layouts.app')

@section('template_title')
    {{ $ingreso->name ?? "{{ __('Show') Ingreso" }}
@endsection

@section('content')
    <section class="content container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="float-left">
                            <span class="card-title">{{ __('Show') }} Ingreso</span>
                        </div>
                        <div class="float-right">
                            <a class="btn btn-primary" href="{{ route('ingresos.index') }}"> {{ __('Back') }}</a>
                        </div>
                    </div>

                    <div class="card-body">
                        
                        <div class="form-group">
                            <strong>Empresa Id:</strong>
                            {{ $ingreso->empresa_id }}
                        </div>
                        <div class="form-group">
                            <strong>Clasificado Id:</strong>
                            {{ $ingreso->clasificado_id }}
                        </div>
                        <div class="form-group">
                            <strong>Monto:</strong>
                            {{ $ingreso->monto }}
                        </div>
                        <div class="form-group">
                            <strong>Referencia:</strong>
                            {{ $ingreso->referencia }}
                        </div>
                        <div class="form-group">
                            <strong>Fingreso:</strong>
                            {{ $ingreso->fingreso }}
                        </div>
                        <div class="form-group">
                            <strong>Observacion:</strong>
                            {{ $ingreso->observacion }}
                        </div>
                        <div class="form-group">
                            <strong>Conciliado:</strong>
                            {{ $ingreso->conciliado }}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
