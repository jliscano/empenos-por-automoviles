<div class="box box-info padding-1">
    <div class="box-body">
        <div class="form-group">
            {{ Form::label('Nombre Empresa:') }}
            {{ Form::text('nombre', $empresa->nombre, ['class' => 'form-control' . ($errors->has('nombre') ? ' is-invalid' : ''), 'placeholder' => 'Nombre']) }}
            {!! $errors->first('nombre', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('C.I.F :') }}
            {{ Form::text('cif', $empresa->cif, ['class' => 'form-control' . ($errors->has('cif') ? ' is-invalid' : ''), 'placeholder' => 'C.I.F']) }}
            {!! $errors->first('cif', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Nombre Representante Legal') }}
            {{ Form::text('replegal', $empresa->replegal, ['class' => 'form-control' . ($errors->has('replegal') ? ' is-invalid' : ''), 'placeholder' => 'Nombre']) }}
            {!! $errors->first('replegal', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('DNI Representante Legal') }}
            {{ Form::text('dnireplegal', $empresa->dnireplegal, ['class' => 'form-control' . ($errors->has('dnireplegal') ? ' is-invalid' : ''), 'placeholder' => 'DNI']) }}
            {!! $errors->first('dnireplegal', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Domicilio :') }}
            {{ Form::text('domicilio', $empresa->domicilio, ['class' => 'form-control' . ($errors->has('domicilio') ? ' is-invalid' : ''), 'placeholder' => 'Domicilio']) }}
            {!! $errors->first('domicilio', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Correo Electrónico :') }}
            {{ Form::text('email', $empresa->email, ['class' => 'form-control' . ($errors->has('email') ? ' is-invalid' : ''), 'placeholder' => 'example@example.com']) }}
            {!! $errors->first('email', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Banco :') }}
            {{ Form::select('banco_id', $bancos, $empresa->banco_id, ['class' => 'form-control' . ($errors->has('banco_id') ? ' is-invalid' : ''), 'placeholder' => 'Banco']) }}
            {!! $errors->first('banco_id', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <div class="form-group">
            {{ Form::label('Cuenta Núm. :') }}
            {{ Form::text('cuenta', $empresa->cuenta, ['class' => 'form-control' . ($errors->has('cuenta') ? ' is-invalid' : ''), 'placeholder' => 'Cuenta Nro...']) }}
            {!! $errors->first('cuenta', '<div class="invalid-feedback">:message</div>') !!}
        </div>


    </div>
    <div class="box-footer mt20">
        <button type="submit" class="btn btn-primary">{{ __('Guardar') }}</button>
        <a href="{{route('empresas.index')}}"  ><div class=" btn btn-primary" style="background-color: yellow;color:black;">Volver</div></a>
    </div>
</div>
